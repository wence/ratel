/// @file
/// Implementation of Core Ratel interfaces

#include <ceed.h>
#include <ceed/backend.h>
#include <petsc.h>
#include <petscdmplex.h>
#include <ratel-cl-options.h>
#include <ratel-fem.h>
#include <ratel-impl.h>
#include <ratel-models.h>
#include <ratel-petsc-ops.h>
#include <ratel-utils.h>
#include <ratel.h>
#include <ratel/qfunctions/utils.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief Print Ratel debugging information

  @param ratel   Ratel context
  @param format  Printing format
**/
// LCOV_EXCL_START
void RatelDebugImpl(const Ratel ratel, const char *format, ...) {
  if (!ratel->is_debug) return;
  va_list args;
  va_start(args, format);
  RatelDebugImpl256(ratel, 0, format, args);
  va_end(args);
}
// LCOV_EXCL_STOP

/**
  @brief Print Ratel debugging information in color

  @param ratel   Ratel context
  @param color   Color to print
  @param format  Printing format
**/
// LCOV_EXCL_START
void RatelDebugImpl256(const Ratel ratel, const unsigned char color, const char *format, ...) {
  if (!ratel->is_debug) return;
  va_list args;
  va_start(args, format);
  if (color != RATEL_DEBUG_NO_COLOR) PetscFPrintf(ratel->comm, PETSC_STDOUT, "\033[38;5;%dm", color);
  PetscVFPrintf(PETSC_STDOUT, format, args);
  if (color != RATEL_DEBUG_NO_COLOR) PetscFPrintf(ratel->comm, PETSC_STDOUT, "\033[m");
  PetscPrintf(ratel->comm, "\n");
  fflush(PETSC_STDOUT);
  va_end(args);
}
// LCOV_EXCL_STOP

/// @}

/// @addtogroup RatelCore
/// @{

/**
  @brief Get Ratel library version info

  Ratel version numbers have the form major.minor.patch. Non-release versions may contain unstable interfaces.

  @param[out]  major    Major version of the library
  @param[out]  minor    Minor version of the library
  @param[out]  patch    Patch (subminor) version of the library
  @param[out]  release  True for releases; false for development branches.

  The caller may pass NULL for any arguments that are not needed.

  @sa RATEL_VERSION_GE()

  @return An error code: 0 - success, otherwise - failure
**/
// LCOV_EXCL_START
PetscErrorCode RatelGetVersion(int *major, int *minor, int *patch, PetscBool *release) {
  if (major) *major = RATEL_VERSION_MAJOR;
  if (minor) *minor = RATEL_VERSION_MINOR;
  if (patch) *patch = RATEL_VERSION_PATCH;
  if (release) *release = RATEL_VERSION_RELEASE;
  return 0;
}
// LCOV_EXCL_STOP

/**
  @brief Setup Ratel context object
         Note: This function call initializes the libCEED context.

  @param[in]   comm          MPI communication object
  @param[out]  ratel         Ratel context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelInit(MPI_Comm comm, Ratel *ratel) {
  PetscLogStage       stage_ratel_setup;
  RatelModelFunctions model_functions;
  Ceed                ceed;

  PetscFunctionBeginUser;
  PetscCall(PetscLogStageGetId("Ratel Setup", &stage_ratel_setup));
  if (stage_ratel_setup == -1) PetscCall(PetscLogStageRegister("Ratel Setup", &stage_ratel_setup));
  PetscCall(PetscLogStagePush(stage_ratel_setup));

  PetscCall(PetscCalloc1(1, ratel));
  (*ratel)->comm = comm;

  // Record env variables RATEL_DEBUG or DBG
  {
    PetscMPIInt comm_rank = -1;

    PetscCallMPI(MPI_Comm_rank(comm, &comm_rank));
    (*ratel)->is_debug = comm_rank == 0 && (!!getenv("RATEL_DEBUG") || !!getenv("DEBUG") || !!getenv("DBG"));
    RatelDebug256(*ratel, RATEL_DEBUG_RED, "---------- Ratel debugging mode active ----------");
    RatelDebug256(*ratel, RATEL_DEBUG_RED, "-- Ratel Init");
  }

  // Register models
  PetscCall(PetscCalloc1(1, &model_functions));
  RatelDebug(*ratel, "---- Registering models");
  PetscCall(RatelRegisterModels(*ratel, model_functions));
  (*ratel)->model_functions = model_functions;

  // Read basic command line options
  RatelDebug(*ratel, "---- Processing command line options");
  PetscCall(RatelProcessCommandLineOptions(*ratel));

  // Initalize Ceed
  {
    PetscErrorCode ierr;

    ierr = CeedInit((*ratel)->ceed_resource, &ceed);
    if (ierr) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_LIB, "libCEED resource initialization failed");
    (*ratel)->ceed = ceed;
    ierr           = CeedSetErrorHandler((*ratel)->ceed, CeedErrorStore);
    if (ierr) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_LIB, "Setting libCEED error handler failed");
    // Set default JiT source root
    RatelDebug(*ratel, "---- Adding Ratel JiT root: %s", RatelJitSourceRootDefault);
    RatelCeedCall(*ratel, CeedAddJitSourceRoot((*ratel)->ceed, (char *)RatelJitSourceRootDefault));
    // View ceed
    if ((*ratel)->is_debug) {
      // LCOV_EXCL_START
      const char *ceed_resource;
      RatelCeedCall(*ratel, CeedGetResource((*ratel)->ceed, &ceed_resource));
      RatelDebug(*ratel, "---- libCEED resource initalized: %s", ceed_resource);
      // LCOV_EXCL_STOP
    }
  }

  PetscCall(PetscLogStagePop());
  RatelDebug256(*ratel, RATEL_DEBUG_RED, "-- Ratel Init Success!");
  PetscFunctionReturn(0);
}

/**
  @brief View a Ratel context
         Note: This function can be called with the command line option `-ratel_view`

  @param  ratel  Ratel context to view

  @param  viewer  Optional visualization context
  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelView(Ratel ratel, PetscViewer viewer) {
  PetscInt fine_level = ratel->num_multigrid_levels - 1;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel View");

  if (!viewer) PetscCall(PetscViewerASCIIGetStdout(ratel->comm, &viewer));

  PetscCall(PetscViewerASCIIPrintf(viewer, "Ratel Context:\n"));
  {
    PetscMPIInt comm_size;
    char        hostname[PETSC_MAX_PATH_LEN];

    PetscCall(MPI_Comm_size(ratel->comm, &comm_size));
    PetscCall(PetscGetHostName(hostname, sizeof hostname));
    PetscCall(PetscViewerASCIIPrintf(viewer,
                                     "  MPI:\n"
                                     "    Hostname: %s\n"
                                     "    Total ranks: %d\n",
                                     hostname, comm_size));
  }
  {
    VecType vec_type;
    MatType mat_type;

    PetscCall(DMGetVecType(ratel->dm_hierarchy[fine_level], &vec_type));
    PetscCall(DMGetMatType(ratel->dm_hierarchy[fine_level], &mat_type));
    PetscCall(PetscViewerASCIIPrintf(viewer,
                                     "  PETSc:\n"
                                     "    VecType: %s\n"
                                     "    MatType: %s\n",
                                     vec_type, mat_type));
  }
  {
    const char *ceed_resource_used;
    CeedMemType mem_type_backend;

    RatelCeedCall(ratel, CeedGetResource(ratel->ceed, &ceed_resource_used));
    RatelCeedCall(ratel, CeedGetPreferredMemType(ratel->ceed, &mem_type_backend));

    PetscCall(PetscViewerASCIIPrintf(viewer,
                                     "  libCEED:\n"
                                     "    Backend resource: %s\n"
                                     "    Backend MemType: %s\n",
                                     ceed_resource_used, CeedMemTypes[mem_type_backend]));
  }
  {
    PetscInt num_global_dofs;
    PetscInt num_local_dofs;

    {
      Vec U;
      PetscCall(DMGetGlobalVector(ratel->dm_hierarchy[fine_level], &U));
      PetscCall(VecGetSize(U, &num_global_dofs));
      PetscCall(VecGetLocalSize(U, &num_local_dofs));
      PetscCall(DMRestoreGlobalVector(ratel->dm_hierarchy[fine_level], &U));
    }
    PetscInt64 num_local_dofs_minmax[] = {num_local_dofs, -num_local_dofs};
    PetscCallMPI(MPI_Allreduce(MPI_IN_PLACE, num_local_dofs_minmax, 2, MPI_INT, MPI_MIN, ratel->comm));
    PetscCall(PetscViewerASCIIPrintf(viewer,
                                     "  Mesh:\n"
                                     "    Global DoFs: %" PetscInt_FMT "\n"
                                     "    Local DoFs: [%" PetscInt64_FMT ", %" PetscInt64_FMT "]\n",
                                     num_global_dofs, num_local_dofs_minmax[0], -num_local_dofs_minmax[1]));
  }
  PetscCall(PetscViewerASCIIPrintf(viewer, "  Method: %s\n", RatelMethodTypes[ratel->method]));
  PetscCall(PetscViewerASCIIPrintf(viewer, "  Materials:\n"));
  for (PetscInt material_index = 0; material_index < ratel->num_materials; material_index++) {
    PetscBool has_name = strlen(ratel->material_names[material_index]);
    PetscCall(PetscViewerASCIIPrintf(viewer, "    - %s%s%s\n", has_name ? ratel->material_names[material_index] : "", has_name ? ": " : "",
                                     ratel->material_models[material_index]));
  }
  PetscCall(PetscViewerASCIIPrintf(viewer, "  Forcing: %s\n", RatelForcingTypes[ratel->forcing_choice]));
  if (ratel->bc_clamp_count) {
    PetscCall(PetscViewerASCIIPrintf(viewer, "  Dirichlet (clamp) boundaries:\n"));
    for (PetscInt i = 0; i < ratel->bc_clamp_count; i++) {
      char        option_name[25];
      PetscScalar translation[3] = {0, 0, 0};
      PetscScalar rotation[5]    = {0, 0, 0, 0, 0};
      PetscInt    max_n          = 3;

      PetscOptionsBegin(ratel->comm, NULL, "", NULL);
      PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_clamp_%" PetscInt_FMT "_translate", ratel->bc_clamp_faces[i]));
      PetscCall(PetscOptionsScalarArray(option_name, "Vector to translate clamped end by", NULL, translation, &max_n, NULL));
      max_n = 5;
      PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_clamp_%" PetscInt_FMT "_rotate", ratel->bc_clamp_faces[i]));
      PetscCall(PetscOptionsScalarArray(option_name, "Vector with axis of rotation and rotation, in radians", NULL, rotation, &max_n, NULL));

      // ------ Normalize
      PetscScalar norm_rotation = RatelNorm3((CeedScalar *)rotation);
      if (fabs(norm_rotation) < 1e-16) norm_rotation = 1;
      for (PetscInt j = 0; j < 3; j++) rotation[i] /= norm_rotation;

      PetscOptionsEnd();

      PetscCall(PetscViewerASCIIPrintf(viewer, "    Face %" PetscInt_FMT ":\n", ratel->bc_clamp_faces[i]));
      PetscCall(PetscViewerASCIIPrintf(viewer, "      Translation vector: [%f, %f, %f]\n", translation[0], translation[1], translation[2]));
      PetscCall(PetscViewerASCIIPrintf(viewer, "      Rotation vector: [%f, %f, %f, %f]\n", rotation[0], rotation[1], rotation[2], rotation[3]));
    }
  }
  if (ratel->bc_slip_count) {
    PetscCall(PetscViewerASCIIPrintf(viewer, "  Slip boundaries:\n"));
    for (PetscInt i = 0; i < ratel->bc_slip_count; i++) {
      PetscCall(PetscViewerASCIIPrintf(viewer, "    Face %" PetscInt_FMT ":\n", ratel->bc_slip_faces[i]));

      char    components[8] = "";
      CeedInt head          = 0;
      for (CeedInt j = 0; j < ratel->bc_slip_components_count[i]; j++) {
        components[head] = ratel->bc_slip_components[i][j] == 0 ? 'x' : ratel->bc_slip_components[i][j] == 1 ? 'y' : 'z';
        head++;
        if (j < ratel->bc_slip_components_count[i] - 1) {
          components[head]     = ',';
          components[head + 1] = ' ';
          head += 2;
        }
      }
      PetscViewerASCIIPrintf(viewer, "      Components: %s\n", components);
    }
  }
  if (ratel->bc_traction_count) {
    PetscCall(PetscViewerASCIIPrintf(viewer, "  Neumann (traction) boundaries:\n"));
    for (PetscInt i = 0; i < ratel->bc_traction_count; i++) {
      char        option_name[25];
      PetscScalar traction[3] = {0, 0, 0};
      PetscInt    max_n       = 3;

      PetscOptionsBegin(ratel->comm, NULL, "", NULL);
      PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_traction_%" PetscInt_FMT, ratel->bc_traction_faces[i]));
      PetscCall(PetscOptionsScalarArray(option_name, "Traction vector for constrained face", NULL, traction, &max_n, NULL));
      PetscOptionsEnd();

      PetscCall(PetscViewerASCIIPrintf(viewer, "    Face %" PetscInt_FMT ":\n", ratel->bc_traction_faces[i]));
      PetscCall(PetscViewerASCIIPrintf(viewer, "      Traction vector: [%f, %f, %f]\n", traction[0], traction[1], traction[2]));
    }
  }
  PetscCall(PetscViewerASCIIPrintf(viewer,
                                   "  Multigrid:\n"
                                   "    Type: %s\n"
                                   "    Number of levels: %" PetscInt_FMT "\n",
                                   RatelMutigridTypes[ratel->multigrid_type], ratel->num_multigrid_levels));
  if (ratel->num_multigrid_levels > 1) {
    PetscCall(PetscViewerASCIIPrintf(viewer, "    Level orders: ["));
    for (PetscInt i = ratel->num_multigrid_levels - 1; i > 0; i--) {
      PetscCall(PetscViewerASCIIPrintf(viewer, "%s%" PetscInt_FMT ",", i == fine_level ? "" : " ", ratel->multigrid_level_orders[i]));
    }
    PetscCall(PetscViewerASCIIPrintf(viewer, " %" PetscInt_FMT "]\n", ratel->multigrid_level_orders[0]));
  }
  {
    PetscCall(PetscViewerASCIIPrintf(viewer,
                                     "  Finite elements:\n"
                                     "    Polynomial order: %" PetscInt_FMT "\n"
                                     "    Additional quadrature points: %" PetscInt_FMT "\n",
                                     ratel->multigrid_fine_order, ratel->q_extra));
    if (ratel->dm_hierarchy && ratel->dm_hierarchy[fine_level]) {
      DM             dm = ratel->dm_hierarchy[fine_level];
      DMLabel        depth_label;
      DMPolytopeType cell_type;
      PetscBool      is_simplex = PETSC_FALSE;
      PetscInt       depth = 0, first_point = 0;
      PetscInt       ids[1] = {0};

      PetscCall(DMPlexIsSimplex(dm, &is_simplex));
      PetscCall(PetscViewerASCIIPrintf(viewer, "    Basis application: %s\n", is_simplex ? "simplex" : "tensor product"));

      PetscCall(DMPlexGetDepth(dm, &depth));
      PetscCall(DMPlexGetDepthLabel(dm, &depth_label));
      ids[0] = depth;
      PetscCall(DMGetFirstLabeledPoint(dm, dm, depth_label, 1, ids, 0, &first_point, NULL));
      if (first_point >= 0) {
        PetscCall(DMPlexGetCellType(dm, first_point, &cell_type));
        CeedElemTopology elem_topo = RatelElemTopologyP2C(cell_type);
        PetscCall(PetscViewerASCIIPrintf(viewer, "    Element topology: %s\n", CeedElemTopologies[elem_topo]));
      }
    }
  }
  {
    PetscBool is_diagnostic_geometry_ratel_decide = ratel->diagnostic_geometry_order == RATEL_DECIDE;
    char      diagnostic_geometry_order_string[PETSC_MAX_PATH_LEN];

    if (is_diagnostic_geometry_ratel_decide) {
      PetscCall(PetscSNPrintf(diagnostic_geometry_order_string, sizeof(diagnostic_geometry_order_string), "%s", "use solution mesh geometric order"));
    } else {
      PetscCall(PetscSNPrintf(diagnostic_geometry_order_string, sizeof(diagnostic_geometry_order_string), "%" PetscInt_FMT,
                              ratel->diagnostic_geometry_order));
    }
    PetscCall(PetscViewerASCIIPrintf(viewer, "  Geometry order for diagnostic values mesh: %s\n", diagnostic_geometry_order_string));
  }
  PetscCall(PetscViewerASCIIPrintf(viewer, "  Has manufactured solution: %s\n", ratel->has_mms ? "yes" : "no"));
  if (ratel->expected_strain_energy == 0.0) {
    // LCOV_EXCL_START
    PetscCall(PetscViewerASCIIPrintf(viewer, "  Expected strain energy: not set\n"));
    // LCOV_EXCL_STOP
  } else {
    PetscCall(PetscViewerASCIIPrintf(viewer, "  Expected strain energy: %0.12e\n", ratel->expected_strain_energy));
  }

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel View Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Destroy a Ratel context

  @param  ratel  Ratel context object to destroy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDestroy(Ratel *ratel) {
  PetscFunctionBeginUser;
  RatelDebug256(*ratel, RATEL_DEBUG_RED, "-- Ratel Destroy");

  // View before destroying, if requested
  {
    PetscViewer viewer;
    PetscBool   view_ratel = PETSC_FALSE;

    PetscCall(PetscOptionsGetViewer((*ratel)->comm, NULL, NULL, "-ratel_view", &viewer, NULL, &view_ratel));
    if (view_ratel) PetscCall(RatelView(*ratel, viewer));
  }

  // DMs
  PetscCall(DMDestroy(&(*ratel)->dm_orig));
  PetscCall(DMDestroy(&(*ratel)->dm_energy));
  PetscCall(DMDestroy(&(*ratel)->dm_diagnostic));
  PetscCall(DMDestroy(&(*ratel)->dm_surface_displacement));
  PetscCall(DMDestroy(&(*ratel)->dm_surface_force));
  for (PetscInt i = 0; i < (*ratel)->num_multigrid_levels_setup; i++) {
    PetscCall(DMDestroy(&(*ratel)->dm_hierarchy[i]));
  }
  PetscCall(PetscFree((*ratel)->dm_hierarchy));

  // Vectors
  for (PetscInt i = 0; i < (*ratel)->num_multigrid_levels_setup; i++) {
    PetscCall(VecDestroy(&(*ratel)->X[i]));
    PetscCall(VecDestroy(&(*ratel)->X_loc[i]));
    PetscCall(VecDestroy(&(*ratel)->Y_loc[i]));
  }
  PetscCall(VecDestroy(&(*ratel)->X_loc_residual_u));
  PetscCall(VecDestroy(&(*ratel)->D));
  PetscCall(VecDestroy(&(*ratel)->D_loc));
  PetscCall(VecDestroy(&(*ratel)->E_loc));
  PetscCall(VecDestroy(&(*ratel)->F_loc));
  PetscCall(VecDestroy(&(*ratel)->M_loc));
  PetscCall(PetscFree((*ratel)->X));
  PetscCall(PetscFree((*ratel)->X_loc));
  PetscCall(PetscFree((*ratel)->Y_loc));

  // KSP
  PetscCall(KSPDestroy(&(*ratel)->ksp_diagnostic_projection));

  // Mats
  for (PetscInt i = 0; i < (*ratel)->num_multigrid_levels_setup; i++) {
    PetscCall(MatDestroy(&(*ratel)->mat_jacobian[i]));
    PetscCall(PetscFree((*ratel)->ctx_jacobian[i]));
    if (i > 0) {
      PetscCall(MatDestroy(&(*ratel)->mat_prolong_restrict[i]));
      PetscCall(PetscFree((*ratel)->ctx_prolong_restrict[i]));
    }
  }
  PetscCall(MatDestroy(&(*ratel)->mat_jacobian_coarse));
  PetscCall(PetscFree((*ratel)->mat_jacobian));
  PetscCall(PetscFree((*ratel)->mat_prolong_restrict));
  PetscCall(PetscFree((*ratel)->ctx_residual_u));
  PetscCall(PetscFree((*ratel)->ctx_residual_ut));
  PetscCall(PetscFree((*ratel)->ctx_residual_utt));
  PetscCall(PetscFree((*ratel)->ctx_jacobian));
  PetscCall(PetscFree((*ratel)->ctx_surface_force));
  PetscCall(PetscFree((*ratel)->ctx_energy));
  PetscCall(PetscFree((*ratel)->ctx_diagnostic));
  PetscCall(PetscFree((*ratel)->ctx_diagnostic_projection));
  PetscCall(PetscFree((*ratel)->ctx_error));
  PetscCall(PetscFree((*ratel)->ctx_form_jacobian));
  PetscCall(PetscFree((*ratel)->ctx_prolong_restrict));

  // FLOPs counting
  PetscCall(PetscFree((*ratel)->event_prolong));
  PetscCall(PetscFree((*ratel)->event_restrict));
  PetscCall(PetscFree((*ratel)->flops_jacobian));
  PetscCall(PetscFree((*ratel)->flops_prolong));
  PetscCall(PetscFree((*ratel)->flops_restrict));

  // Materials
  PetscCall(PetscFunctionListDestroy(&(*ratel)->model_functions->setupPhysics));
  PetscCall(PetscFunctionListDestroy(&(*ratel)->model_functions->setupSmootherPhysics));
  PetscCall(PetscFunctionListDestroy(&(*ratel)->model_functions->getModelData));
  for (PetscInt material_index = 0; material_index < ((*ratel)->num_materials); material_index++) {
    PetscCall(PetscFree((*ratel)->material_names[material_index]));
    PetscCall(PetscFree((*ratel)->material_models[material_index]));
    PetscCall(PetscFree((*ratel)->material_label_values[material_index]));
  }
  PetscCall(PetscFree((*ratel)->material_label_name));
  PetscCall(PetscFree((*ratel)->model_functions));
  PetscCall(PetscFree((*ratel)->model_units));

  // Surface Force label names
  for (PetscInt material_index = 0; material_index < ((*ratel)->num_materials); material_index++) {
    for (PetscInt i = 0; i < (*ratel)->surface_force_face_count; i++) {
      PetscCall(PetscFree((*ratel)->surface_force_face_label_names[material_index][i]));
    }
  }

  // Output options
  PetscCall(PetscViewerDestroy(&(*ratel)->monitor_viewer_strain_energy));
  PetscCall(PetscViewerDestroy(&(*ratel)->monitor_viewer_surface_forces));
  PetscCall(PetscViewerDestroy(&(*ratel)->monitor_viewer_diagnostic_quantities));
  PetscCall(PetscFree((*ratel)->monitor_vtk_file_name));
  PetscCall(PetscFree((*ratel)->monitor_vtk_file_extension));

  // Ceed
  PetscCall(PetscFree((*ratel)->ceed_resource));
  RatelCeedCall(*ratel, CeedOperatorDestroy(&(*ratel)->op_dirichlet));
  RatelCeedCall(*ratel, CeedOperatorDestroy(&(*ratel)->op_residual_u));
  RatelCeedCall(*ratel, CeedOperatorDestroy(&(*ratel)->op_residual_ut));
  RatelCeedCall(*ratel, CeedOperatorDestroy(&(*ratel)->op_residual_utt));
  for (PetscInt i = 0; i < (*ratel)->num_multigrid_levels_setup; i++) {
    RatelCeedCall(*ratel, CeedOperatorDestroy(&(*ratel)->op_jacobian[i]));
    if (i > 0) {
      RatelCeedCall(*ratel, CeedOperatorDestroy(&(*ratel)->op_prolong[i]));
      RatelCeedCall(*ratel, CeedOperatorDestroy(&(*ratel)->op_restrict[i]));
    }
  }
  for (PetscInt material_index = 0; material_index < ((*ratel)->num_materials); material_index++) {
    RatelCeedCall(*ratel, CeedQFunctionContextDestroy(&(*ratel)->material_ctx_phys[material_index]));
    RatelCeedCall(*ratel, CeedQFunctionContextDestroy(&(*ratel)->material_ctx_phys_smoother[material_index]));
  }
  PetscCall(PetscFree((*ratel)->op_jacobian));
  PetscCall(PetscFree((*ratel)->op_prolong));
  PetscCall(PetscFree((*ratel)->op_restrict));
  RatelCeedCall(*ratel, CeedOperatorDestroy(&(*ratel)->op_energy));
  RatelCeedCall(*ratel, CeedOperatorDestroy(&(*ratel)->op_mass_diagnostic));
  RatelCeedCall(*ratel, CeedOperatorDestroy(&(*ratel)->op_diagnostic));
  RatelCeedCall(*ratel, CeedOperatorDestroy(&(*ratel)->op_error));
  for (PetscInt i = 0; i < (*ratel)->num_multigrid_levels_setup; i++) {
    RatelCeedCall(*ratel, CeedVectorDestroy(&(*ratel)->x_loc_ceed[i]));
    RatelCeedCall(*ratel, CeedVectorDestroy(&(*ratel)->y_loc_ceed[i]));
  }
  for (PetscInt i = 0; i < (*ratel)->surface_force_face_count; i++) {
    RatelCeedCall(*ratel, CeedOperatorDestroy(&(*ratel)->op_surface_force[i]));
  }
  RatelCeedCall(*ratel, CeedVectorDestroy(&(*ratel)->d_loc_ceed));
  RatelCeedCall(*ratel, CeedVectorDestroy(&(*ratel)->e_loc_ceed));
  RatelCeedCall(*ratel, CeedVectorDestroy(&(*ratel)->f_loc_ceed));
  RatelCeedCall(*ratel, CeedVectorDestroy(&(*ratel)->m_loc_ceed));
  PetscCall(PetscFree((*ratel)->x_loc_ceed));
  PetscCall(PetscFree((*ratel)->y_loc_ceed));
  PetscCall(PetscFree((*ratel)->op_surface_force));
  RatelCeedCall(*ratel, CeedVectorDestroy(&(*ratel)->coo_values_ceed));
  {
    PetscErrorCode ierr;

    ierr = CeedDestroy(&(*ratel)->ceed);
    if (ierr) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_LIB, "Destroying Ceed object failed");
  }

  // Ratel itself
  RatelDebug256(*ratel, RATEL_DEBUG_RED, "-- Ratel Destroy Success!");
  PetscCall(PetscFree(*ratel));

  PetscFunctionReturn(0);
}

/**
  @brief Create DM with SNES or TS hooks from Ratel context

  @param[in]   ratel   Ratel context
  @param[in]   method  Numerical method to use
  @param[out]  dm      DMPlex object with SNES and TS hooks

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMCreate(Ratel ratel, RatelMethodType method, DM *dm) {
  PetscLogStage stage_dm_setup;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel DM Create");
  PetscCall(PetscLogStageGetId("Ratel DM Setup", &stage_dm_setup));
  if (stage_dm_setup == -1) PetscCall(PetscLogStageRegister("Ratel DM Setup", &stage_dm_setup));
  PetscCall(PetscLogStagePush(stage_dm_setup));

  ratel->method = method;
  switch (method) {
    case RATEL_METHOD_STATIC_ELASTICITY:
      RatelDebug(ratel, "---- Method: static elasticity");
      PetscCall(RatelDMCreate_FEM(ratel, dm));
      break;
    case RATEL_METHOD_QUASISTATIC_ELASTICITY:
      RatelDebug(ratel, "---- Method: quasistatic elasticity");
      PetscCall(RatelDMCreate_FEM(ratel, dm));
      break;
    case RATEL_METHOD_DYNAMIC_ELASTICITY:
      RatelDebug(ratel, "---- Method: dynamic elasticity");
      PetscCall(RatelDMCreate_FEM(ratel, dm));
      break;
    // LCOV_EXCL_START
    default:
      SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Method not implemented");
      // LCOV_EXCL_STOP
  }

  PetscCall(PetscLogStagePop());
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel DM Create Success!");
  PetscFunctionReturn(0);
}

/// @}
