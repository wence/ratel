/// @file
/// Provide weak stubs for Ratel models that could be missing

#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-models.h>

#define MACRO(model_cl_argument, model_postfix, physics_postfix)                                                                              \
  RATEL_INTERN PetscErrorCode RatelRegisterModel_##model_postfix(Ratel, const char *, RatelModelFunctions) __attribute__((weak));             \
  PetscErrorCode              RatelRegisterModel_##model_postfix(Ratel ratel, const char *cl_argument, RatelModelFunctions model_functions) { \
                 PetscFunctionBegin;                                                                                                          \
                 RatelDebug256(ratel, RATEL_DEBUG_ORANGE, "---- Weak Registration of model \"%s\"", cl_argument);                             \
                 PetscFunctionReturn(0);                                                                                                      \
  }

#include "ratel-models-list.h"
#undef MACRO
