/// @file
/// Linear elasticity using Neo-Hookean model

#include <ceed.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel-models.h>
#include <ratel-types.h>
#include <ratel.h>
#include <ratel/models/neo-hookean.h>
#include <ratel/qfunctions/linear-manufactured-error.h>
#include <ratel/qfunctions/models/elasticity-linear.h>
#include <ratel/qfunctions/scaled-mass.h>
#include <ratel/qfunctions/surface-force-geometry.h>
#include <ratel/qfunctions/volumetric-geometry.h>

struct RatelModelData_private elasticity_linear_data_private = {
    .name_for_display            = "linear elasticity",
    .setup_geo                   = SetupVolumeGeometry,
    .setup_geo_loc               = SetupVolumeGeometry_loc,
    .setup_surface_force_geo     = SetupSurfaceForceGeometry,
    .setup_surface_force_geo_loc = SetupSurfaceForceGeometry_loc,
    .q_data_size                 = 10,
    .surface_force_q_data_size   = 13,
    .num_comp_u                  = 3,
    .quadrature_mode             = CEED_GAUSS,
    .residual_u                  = ElasticityResidual_Linear,
    .residual_u_loc              = ElasticityResidual_Linear_loc,
    .residual_utt                = ScaledMass,
    .residual_utt_loc            = ScaledMass_loc,
    .number_fields_stored        = 0,
    .jacobian                    = ElasticityJacobian_Linear,
    .jacobian_loc                = ElasticityJacobian_Linear_loc,
    .energy                      = Energy_Linear,
    .energy_loc                  = Energy_Linear_loc,
    .diagnostic                  = Diagnostic_Linear,
    .diagnostic_loc              = Diagnostic_Linear_loc,
    .surface_force               = SurfaceForce_Linear,
    .surface_force_loc           = SurfaceForce_Linear_loc,
    .error                       = MMSError_Linear,
    .error_loc                   = MMSError_Linear_loc,
    .flops_qf_jacobian_u         = 176,
    .flops_qf_jacobian_utt       = 9,
};
RatelModelData elasticity_linear_data = &elasticity_linear_data_private;

/// @addtogroup RatelModels
/// @{

/**
  @brief Model properties for linear elasticity

  @param[in]   ratel       Ratel context
  @param[out]  model_data  Model data object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetModelData_ElasticityLinear(Ratel ratel, RatelModelData *model_data) {
  PetscFunctionBegin;

  PetscCall(RatelModelDataVerifyRelativePath(ratel, elasticity_linear_data));
  *model_data = elasticity_linear_data;

  PetscFunctionReturn(0);
}

/**
  @brief Register linear elastic model

  @param[in]   ratel            Ratel context
  @param[in]   cl_argument      Command line argument to use for model
  @param[out]  model_functions  PETSc function lists for models

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_ElasticityLinear(Ratel ratel, const char *cl_argument, RatelModelFunctions model_functions) {
  PetscFunctionBegin;

  RATEL_MODEL_REGISTER(model_functions, cl_argument, ElasticityLinear, NeoHookean);

  PetscFunctionReturn(0);
}

/// @}
