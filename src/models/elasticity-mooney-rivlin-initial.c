/// @file
/// Hyperelasticity at finite strain using Mooney Rivlin model in initial configuration

#include <ceed.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel-models.h>
#include <ratel-types.h>
#include <ratel.h>
#include <ratel/models/mooney-rivlin.h>
#include <ratel/qfunctions/models/elasticity-mooney-rivlin-initial.h>
#include <ratel/qfunctions/scaled-mass.h>
#include <ratel/qfunctions/surface-force-geometry.h>
#include <ratel/qfunctions/volumetric-geometry.h>

static const char *const field_names[] = {"grad u"};
static CeedInt           field_sizes[] = {9};

struct RatelModelData_private elasticity_Mooney_Rivlin_initial_data_private = {
    .name_for_display            = "hyperelasticity at finite strain, initial configuration Mooney-Rivlin",
    .setup_geo                   = SetupVolumeGeometry,
    .setup_geo_loc               = SetupVolumeGeometry_loc,
    .setup_surface_force_geo     = SetupSurfaceForceGeometry,
    .setup_surface_force_geo_loc = SetupSurfaceForceGeometry_loc,
    .q_data_size                 = 10,
    .surface_force_q_data_size   = 13,
    .num_comp_u                  = 3,
    .quadrature_mode             = CEED_GAUSS,
    .residual_u                  = ElasticityResidual_MooneyRivlinInitial,
    .residual_u_loc              = ElasticityResidual_MooneyRivlinInitial_loc,
    .residual_utt                = ScaledMass,
    .residual_utt_loc            = ScaledMass_loc,
    .number_fields_stored        = sizeof(field_sizes) / sizeof(*field_sizes),
    .field_names                 = field_names,
    .field_sizes                 = field_sizes,
    .jacobian                    = ElasticityJacobian_MooneyRivlinInitial,
    .jacobian_loc                = ElasticityJacobian_MooneyRivlinInitial_loc,
    .energy                      = Energy_MooneyRivlinInitial,
    .energy_loc                  = Energy_MooneyRivlinInitial_loc,
    .diagnostic                  = Diagnostic_MooneyRivlinInitial,
    .diagnostic_loc              = Diagnostic_MooneyRivlinInitial_loc,
    .surface_force               = SurfaceForce_MooneyRivlinInitial,
    .surface_force_loc           = SurfaceForce_MooneyRivlinInitial_loc,
    .flops_qf_jacobian_u         = 767,
    .flops_qf_jacobian_utt       = 9,
};
RatelModelData elasticity_Mooney_Rivlin_initial_data = &elasticity_Mooney_Rivlin_initial_data_private;

/// @addtogroup RatelModels
/// @{

/**
  @brief Model properties for Mooney-Rivlin hyperelasticity at finite strain in initial configuration

  @param[in]   ratel       Ratel context
  @param[out]  model_data  Model data object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetModelData_ElasticityMooneyRivlinInitial(Ratel ratel, RatelModelData *model_data) {
  PetscFunctionBegin;

  PetscCall(RatelModelDataVerifyRelativePath(ratel, elasticity_Mooney_Rivlin_initial_data));
  *model_data = elasticity_Mooney_Rivlin_initial_data;

  PetscFunctionReturn(0);
}

/**
  @brief Register Mooney-Rivlin hyperelasticity at finite strain in initial configuration

  @param[in]   ratel            Ratel context
  @param[in]   cl_argument      Command line argument to use for model
  @param[out]  model_functions  PETSc function lists for models

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_ElasticityMooneyRivlinInitial(Ratel ratel, const char *cl_argument, RatelModelFunctions model_functions) {
  PetscFunctionBegin;

  RATEL_MODEL_REGISTER(model_functions, cl_argument, ElasticityMooneyRivlinInitial, MooneyRivlin);

  PetscFunctionReturn(0);
}

/// @}
