/// @file
/// Hyperelasticity at finite strain using Neo-Hookean model in initial configuration

#include <ceed.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel-models.h>
#include <ratel-types.h>
#include <ratel.h>
#include <ratel/models/neo-hookean.h>
#include <ratel/qfunctions/models/elasticity-neo-hookean-initial.h>
#include <ratel/qfunctions/scaled-mass.h>
#include <ratel/qfunctions/surface-force-geometry.h>
#include <ratel/qfunctions/volumetric-geometry.h>

static const char *const field_names[] = {"grad u"};
static CeedInt           field_sizes[] = {9};

struct RatelModelData_private elasticity_Neo_Hookean_initial_data_private = {
    .name_for_display            = "hyperelasticity at finite strain, initial configuration Neo-Hookean",
    .setup_geo                   = SetupVolumeGeometry,
    .setup_geo_loc               = SetupVolumeGeometry_loc,
    .setup_surface_force_geo     = SetupSurfaceForceGeometry,
    .setup_surface_force_geo_loc = SetupSurfaceForceGeometry_loc,
    .q_data_size                 = 10,
    .surface_force_q_data_size   = 13,
    .num_comp_u                  = 3,
    .quadrature_mode             = CEED_GAUSS,
    .residual_u                  = ElasticityResidual_NeoHookeanInitial,
    .residual_u_loc              = ElasticityResidual_NeoHookeanInitial_loc,
    .residual_utt                = ScaledMass,
    .residual_utt_loc            = ScaledMass_loc,
    .number_fields_stored        = sizeof(field_sizes) / sizeof(*field_sizes),
    .field_names                 = field_names,
    .field_sizes                 = field_sizes,
    .jacobian                    = ElasticityJacobian_NeoHookeanInitial,
    .jacobian_loc                = ElasticityJacobian_NeoHookeanInitial_loc,
    .energy                      = Energy_NeoHookean,
    .energy_loc                  = Energy_NeoHookean_loc,
    .diagnostic                  = Diagnostic_NeoHookean,
    .diagnostic_loc              = Diagnostic_NeoHookean_loc,
    .surface_force               = SurfaceForce_NeoHookean,
    .surface_force_loc           = SurfaceForce_NeoHookean_loc,
    .flops_qf_jacobian_u         = 802,
    .flops_qf_jacobian_utt       = 9,
};
RatelModelData elasticity_Neo_Hookean_initial_data = &elasticity_Neo_Hookean_initial_data_private;

/// @addtogroup RatelModels
/// @{

/**
  @brief Model properties for Neo-Hookean hyperelasticity at finite strain in initial configuration

  @param[in]   ratel       Ratel context
  @param[out]  model_data  Model data object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetModelData_ElasticityNeoHookeanInitial(Ratel ratel, RatelModelData *model_data) {
  PetscFunctionBegin;

  PetscCall(RatelModelDataVerifyRelativePath(ratel, elasticity_Neo_Hookean_initial_data));
  *model_data = elasticity_Neo_Hookean_initial_data;

  PetscFunctionReturn(0);
}

/**
  @brief Register Neo-Hookean hyperelasticity at finite strain in initial configuration

  @param[in]   ratel            Ratel context
  @param[in]   cl_argument      Command line argument to use for model
  @param[out]  model_functions  PETSc function lists for models

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_ElasticityNeoHookeanInitial(Ratel ratel, const char *cl_argument, RatelModelFunctions model_functions) {
  PetscFunctionBegin;

  RATEL_MODEL_REGISTER(model_functions, cl_argument, ElasticityNeoHookeanInitial, NeoHookean);

  PetscFunctionReturn(0);
}

/// @}
