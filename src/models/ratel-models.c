/// @file
/// Setup Ratel models

#include <ceed.h>
#include <petsc.h>
#include <ratel-models.h>
#include <ratel.h>
#include <string.h>

#define MACRO(model_cl_argument, model_postfix, physics_postfix)                          \
  RATEL_INTERN PetscErrorCode RatelGetModelData_##model_postfix(Ratel, RatelModelData *); \
  RATEL_INTERN PetscErrorCode RatelRegisterModel_##model_postfix(Ratel, const char *, RatelModelFunctions);

#include "ratel-models-list.h"
#undef MACRO

/// @addtogroup RatelModels
/// @{

/**
  @brief Register setup functions for models

  @param[in]   ratel            Ratel context
  @param[out]  model_functions  Function lists for material models

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModels(Ratel ratel, RatelModelFunctions model_functions) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Register Models");

#define MACRO(model_cl_argument, model_postfix, physics_postfix) \
  PetscCall(RatelRegisterModel_##model_postfix(ratel, model_cl_argument, model_functions));
#include "ratel-models-list.h"
#undef MACRO

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Register Models Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Verify all QFunction paths are relative to Ratel JiT root directory

  @param[in]      ratel       Ratel context
  @param[in,out]  model_data  Model data to check QFunction paths

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelModelDataVerifyRelativePath(Ratel ratel, RatelModelData model_data) {
  PetscFunctionBeginUser;

  model_data->setup_geo_loc               = RatelQFunctionRelativePath(model_data->setup_geo_loc);
  model_data->setup_surface_force_geo_loc = RatelQFunctionRelativePath(model_data->setup_surface_force_geo_loc);
  model_data->residual_u_loc              = RatelQFunctionRelativePath(model_data->residual_u_loc);
  if (model_data->residual_ut_loc) model_data->residual_ut_loc = RatelQFunctionRelativePath(model_data->residual_ut_loc);
  if (model_data->residual_utt_loc) model_data->residual_utt_loc = RatelQFunctionRelativePath(model_data->residual_utt_loc);
  if (model_data->error_loc) model_data->error_loc = RatelQFunctionRelativePath(model_data->error_loc);
  model_data->jacobian_loc      = RatelQFunctionRelativePath(model_data->jacobian_loc);
  model_data->energy_loc        = RatelQFunctionRelativePath(model_data->energy_loc);
  model_data->diagnostic_loc    = RatelQFunctionRelativePath(model_data->diagnostic_loc);
  model_data->surface_force_loc = RatelQFunctionRelativePath(model_data->surface_force_loc);

  PetscFunctionReturn(0);
}

/**
  @brief Get model data from command line argument string

  @param[in]   ratel        Ratel context
  @param[in]   cl_argument  Command line argument to use for model
  @param[out]  model_data   Model data to check QFunction paths

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetModelData(Ratel ratel, const char *cl_argument, RatelModelData *model_data) {
  PetscFunctionBeginUser;

  PetscErrorCode (*GetModelData)(Ratel, RatelModelData *);
  PetscCall(PetscFunctionListFind(ratel->model_functions->getModelData, cl_argument, &GetModelData));
  if (!GetModelData) SETERRQ(PETSC_COMM_SELF, 1, "Model data for '%s' not found", cl_argument);
  PetscCall((*GetModelData)(ratel, model_data));

  PetscFunctionReturn(0);
}

/**
  @brief Setup physics context from command line argument string

  @param[in]   ratel           Ratel context
  @param[in]   cl_argument     Command line argument to use for model
  @param[in]   material_index  Integer index of the current material in the materials list
  @param[out]  ctx_phys        libCEED QFunctionContext for Neo-Hookean physics

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupModelPhysics(Ratel ratel, const char *cl_argument, PetscInt material_index, CeedQFunctionContext *ctx_phys) {
  PetscFunctionBeginUser;

  PetscErrorCode (*SetupPhysics)(Ratel, PetscInt, CeedQFunctionContext *);
  PetscCall(PetscFunctionListFind(ratel->model_functions->setupPhysics, cl_argument, &SetupPhysics));
  if (!SetupPhysics) SETERRQ(PETSC_COMM_SELF, 1, "Physics setup for '%s' not found", cl_argument);
  PetscCall((*SetupPhysics)(ratel, material_index, ctx_phys));

  PetscFunctionReturn(0);
}

/**
  @brief Setup smoother physics context from command line argument string

  @param[in]   ratel              Ratel context
  @param[in]   cl_argument        Command line argument to use for model
  @param[in]   material_index     Integer index of the current material in the materials list
  @param[in]   ctx_phys           libCEED QFunctionContext for physics
  @param[out]  ctx_phys_smoother  libCEED QFunctionContext for multigrid smoother

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupModelSmootherPhysics(Ratel ratel, const char *cl_argument, PetscInt material_index, CeedQFunctionContext ctx_phys,
                                              CeedQFunctionContext *ctx_phys_smoother) {
  PetscFunctionBeginUser;

  PetscErrorCode (*SetupSmootherPhysics)(Ratel, CeedQFunctionContext, PetscInt, CeedQFunctionContext *);
  PetscCall(PetscFunctionListFind(ratel->model_functions->setupSmootherPhysics, cl_argument, &SetupSmootherPhysics));
  if (!SetupSmootherPhysics) SETERRQ(PETSC_COMM_SELF, 1, "Smoother physics setup for '%s' not found", cl_argument);
  PetscCall((*SetupSmootherPhysics)(ratel, ctx_phys, material_index, ctx_phys_smoother));

  PetscFunctionReturn(0);
}

/**
  @brief Build PetscOptions prefix for material
           Note: Caller is responsible for freeing the allocated prefix string with
                 PetscFree().

  @param[in]   ratel           Ratel context
  @param[in]   material_index  Index of material to build prefix for
  @param[out]  prefix          String holding prefix for material

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetMaterialPrefix(Ratel ratel, PetscInt material_index, char **prefix) {
  size_t    name_len   = strlen(ratel->material_names[material_index]);
  PetscBool has_name   = name_len > 0;
  size_t    prefix_len = (has_name ? 1 : 1) + name_len + 1;

  PetscFunctionBeginUser;

  PetscCall(PetscCalloc1(prefix_len, prefix));
  PetscCall(PetscSNPrintf(*prefix, prefix_len, "%s%s", has_name ? ratel->material_names[material_index] : "", has_name ? "_" : ""));
  RatelDebug(ratel, "---- Material options prefix: %s", *prefix);

  PetscFunctionReturn(0);
}

/**
  @brief Build PetscOptions message for material
           Note: Caller is responsible for freeing the allocated message string with
                 PetscFree().

  @param[in]   ratel           Ratel context
  @param[in]   material_index  Index of material to build prefix for
  @param[out]  message         String holding message for material

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetMaterialMessage(Ratel ratel, PetscInt material_index, char **message) {
  size_t      name_len      = strlen(ratel->material_names[material_index]);
  PetscBool   has_name      = name_len > 0;
  const char *message_start = "Model options for material";
  size_t      message_len   = strlen(message_start) + (has_name ? 1 : 1) + name_len + 1;

  PetscFunctionBeginUser;

  PetscCall(PetscCalloc1(message_len, message));
  PetscCall(
      PetscSNPrintf(*message, message_len, "%s%s%s", message_start, has_name ? " " : "", has_name ? ratel->material_names[material_index] : ""));
  RatelDebug(ratel, "---- Material options message: %s", *message);

  PetscFunctionReturn(0);
}

/**
  @brief Build CeedOperator name for material
           Note: Caller is responsible for freeing the allocated name string with
                 PetscFree().

  @param[in]   ratel           Ratel context
  @param[in]   material_index  Index of material to build prefix for
  @param[in]   base_name       String holding base name for operator
  @param[out]  operator_name   String holding operator name for material

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetMaterialOperatorName(Ratel ratel, PetscInt material_index, const char *base_name, char **operator_name) {
  PetscInt  name_len          = strlen(ratel->material_names[material_index]);
  PetscBool has_name          = name_len > 0;
  size_t    operator_name_len = strlen(base_name) + (has_name ? 5 : 0) + name_len + 1;

  PetscFunctionBeginUser;

  PetscCall(PetscCalloc1(operator_name_len, operator_name));
  PetscCall(PetscSNPrintf(*operator_name, operator_name_len, "%s%s%s", base_name, has_name ? " for " : "",
                          has_name ? ratel->material_names[material_index] : ""));
  RatelDebug(ratel, "---- Material operator name: %s", *operator_name);

  PetscFunctionReturn(0);
}

/**
  @brief Build CeedOperator name for material

  @param[in]      ratel           Ratel context
  @param[in]      material_index  Index of material to build prefix for
  @param[in]      base_name       String holding base name for operator
  @param[in,out]  op              CeedOperator to set the name for

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetMaterialOperatorName(Ratel ratel, PetscInt material_index, const char *base_name, CeedOperator op) {
  char *operator_name;

  PetscFunctionBeginUser;

  PetscCall(RatelGetMaterialOperatorName(ratel, material_index, base_name, &operator_name));
  RatelCeedCall(ratel, CeedOperatorSetName(op, operator_name));
  PetscCall(PetscFree(operator_name));

  PetscFunctionReturn(0);
}

/// @}
