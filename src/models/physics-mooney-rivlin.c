/// @file
/// Setup Mooney-Rivlin physics context objects

#include <ceed.h>
#include <math.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel-models.h>
#include <ratel.h>
#include <ratel/models/mooney-rivlin.h>
#include <stddef.h>

/// @addtogroup RatelModels
/// @{

/**
  @brief Build libCEED QFunctionContext for Mooney-Rivlin physics

  @param[in]   ratel           Ratel context
  @param[in]   material_index  Integer index of the current material in the materials list
  @param[out]  ctx             libCEED QFunctionContext for Mooney-Rivlin physics

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCreatePhysicsContext_MooneyRivlin(Ratel ratel, PetscInt material_index, CeedQFunctionContext *ctx) {
  MooneyRivlinPhysics *phys;

  PetscFunctionBegin;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Setup Physics Context Mooney-Rivlin");

  PetscCall(PetscCalloc1(1, &phys));
  PetscCall(RatelProcessPhysics_MooneyRivlin(ratel, material_index, phys));
  RatelCeedCall(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCeedCall(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*phys), phys));
  // Common parameters
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "rho", offsetof(MooneyRivlinPhysics, common_parameters[RATEL_COMMON_PARAMETER_RHO]),
                                                          1, "density"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(
                           *ctx, "shift v", offsetof(MooneyRivlinPhysics, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]), 1, "shift for U_t"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(
                           *ctx, "shift a", offsetof(MooneyRivlinPhysics, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]), 1, "shift for U_tt"));
  // Mooney Rivlin specific parameters
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "mu 1", offsetof(MooneyRivlinPhysics, mu_1), 1, "first model parameter"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "mu 2", offsetof(MooneyRivlinPhysics, mu_2), 1, "second model parameter"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "lambda", offsetof(MooneyRivlinPhysics, lambda), 1, "first Lamé parameter"));
  if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(*ctx, stdout));
  PetscCall(PetscFree(phys));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Setup Physics Context Mooney-Rivlin Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Build libCEED QFunctionContext for multigrid smoother for
           Mooney-Rivlin physics

  @param[in]   ratel           Ratel context
  @param[in]   ctx             libCEED QFunctionContext for Mooney-Rivlin physics
  @param[in]   material_index  Integer index of the current material in the materials list
  @param[out]  ctx_smoother    libCEED QFunctionContext for multigrid smoother

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCreatePhysicsSmootherContext_MooneyRivlin(Ratel ratel, CeedQFunctionContext ctx, PetscInt material_index,
                                                              CeedQFunctionContext *ctx_smoother) {
  PetscScalar          nu_smoother = 0;
  PetscBool            nu_flag     = PETSC_FALSE;
  MooneyRivlinPhysics *phys, *phys_smoother;

  PetscFunctionBegin;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Setup Physics Smoother Context Mooney-Rivlin");

  char *prefix;

  PetscCall(RatelGetMaterialPrefix(ratel, material_index, &prefix));
  PetscOptionsBegin(ratel->comm, prefix, "Mooney-Rivlin physical parameters for smoother", NULL);

  PetscCall(PetscOptionsScalar("-nu_smoother", "Poisson's ratio for smoother", NULL, nu_smoother, &nu_smoother, &nu_flag));
  if (nu_flag) RatelDebug(ratel, "---- Nu: %f", nu_smoother);
  else RatelDebug(ratel, "---- No Nu for smoother set");
  if (nu_smoother < 0 || nu_smoother >= 0.5) {
    // LCOV_EXCL_START
    SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Mooney-Rivlin model requires Poisson ratio -nu option in [0, .5)");
    // LCOV_EXCL_STOP
  }

  PetscOptionsEnd();  // End of setting Physics
  PetscCall(PetscFree(prefix));

  if (nu_flag) {
    // Copy context
    CeedQFunctionContextGetData(ctx, CEED_MEM_HOST, &phys);
    PetscCall(PetscCalloc1(1, &phys_smoother));
    PetscCall(PetscMemcpy(phys_smoother, phys, sizeof(*phys)));
    RatelCeedCall(ratel, CeedQFunctionContextRestoreData(ctx, &phys));
    // Create smoother context
    CeedQFunctionContextCreate(ratel->ceed, ctx_smoother);
    phys_smoother->lambda = 2 * (phys_smoother->mu_1 + phys_smoother->mu_2) * nu_smoother / (1 - 2 * nu_smoother);
    RatelCeedCall(ratel, CeedQFunctionContextSetData(*ctx_smoother, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*phys_smoother), phys_smoother));
    // Common parameters
    RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(
                             *ctx_smoother, "rho", offsetof(MooneyRivlinPhysics, common_parameters[RATEL_COMMON_PARAMETER_RHO]), 1, "density"));
    RatelCeedCall(
        ratel, CeedQFunctionContextRegisterDouble(
                   *ctx_smoother, "shift v", offsetof(MooneyRivlinPhysics, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]), 1, "shift for U_t"));
    RatelCeedCall(
        ratel, CeedQFunctionContextRegisterDouble(
                   *ctx_smoother, "shift a", offsetof(MooneyRivlinPhysics, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]), 1, "shift for U_tt"));
    // Mooney Rivlin specific parameters
    RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx_smoother, "mu 1", offsetof(MooneyRivlinPhysics, mu_1), 1, "first model parameter"));
    RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx_smoother, "mu 2", offsetof(MooneyRivlinPhysics, mu_2), 1, "second model parameter"));
    RatelCeedCall(ratel,
                  CeedQFunctionContextRegisterDouble(*ctx_smoother, "lambda", offsetof(MooneyRivlinPhysics, lambda), 1, "first Lamé parameter"));
    PetscCall(PetscFree(phys_smoother));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(*ctx_smoother, stdout));
  } else {
    *ctx_smoother = NULL;
  }

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Setup Physics Smoother Context Mooney-Rivlin Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Process command line options for Mooney-Rivlin physics

  @param[in]   ratel           Ratel context
  @param[in]   material_index  Integer index of the current material in the materials list
  @param[out]  phys_arg        Physics context struct

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelProcessPhysics_MooneyRivlin(Ratel ratel, PetscInt material_index, void *phys_arg) {
  MooneyRivlinPhysics *phys                           = (MooneyRivlinPhysics *)phys_arg;
  PetscReal            nu                             = -1.0;
  phys->mu_1                                          = -1.0;
  phys->mu_2                                          = -1.0;
  phys->lambda                                        = -1.0;
  phys->common_parameters[RATEL_COMMON_PARAMETER_RHO] = 1.0;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Process Physics Mooney-Rivlin");

  char *prefix;

  PetscCall(RatelGetMaterialPrefix(ratel, material_index, &prefix));
  PetscOptionsBegin(ratel->comm, prefix, "Mooney-Rivlin physical parameters", NULL);

  PetscCall(PetscOptionsScalar("-mu_1", "Material Property mu_1", NULL, phys->mu_1, &phys->mu_1, NULL));
  RatelDebug(ratel, "---- Mu_1: %f", phys->mu_1);
  if (phys->mu_1 < 0) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Mooney-Rivlin model requires non-negative -mu_1 option (Pa)");

  PetscCall(PetscOptionsScalar("-mu_2", "Material Property mu_2", NULL, phys->mu_2, &phys->mu_2, NULL));
  RatelDebug(ratel, "---- Mu_2: %f", phys->mu_2);
  if (phys->mu_2 < 0) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Mooney-Rivlin model requires non-negative -mu_2 option (Pa)");

  PetscCall(PetscOptionsScalar("-nu", "Poisson ratio", NULL, nu, &nu, NULL));
  RatelDebug(ratel, "---- Nu: %f", nu);
  if (nu < 0 || nu >= 0.5) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Mooney-Rivlin model requires Poisson ratio -nu option in [0, .5)");
  phys->lambda = 2 * (phys->mu_1 + phys->mu_2) * nu / (1 - 2 * nu);

  PetscCall(PetscOptionsScalar("-rho", "Density for scaled mass matrix", NULL, phys->common_parameters[RATEL_COMMON_PARAMETER_RHO],
                               &phys->common_parameters[RATEL_COMMON_PARAMETER_RHO], NULL));
  if (phys->common_parameters[RATEL_COMMON_PARAMETER_RHO] < 0.0) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Density must be a positive value");

  PetscOptionsEnd();  // End of setting Physics
  PetscCall(PetscFree(prefix));

  // Scale model parameters based on units of Pa
  phys->mu_1 *= ratel->model_units->Pascal;
  phys->mu_2 *= ratel->model_units->Pascal;
  phys->lambda *= ratel->model_units->Pascal;
  phys->common_parameters[RATEL_COMMON_PARAMETER_RHO] *= -ratel->model_units->kilogram / pow(ratel->model_units->meter, 3);

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Process Physics Mooney-Rivlin Success!");
  PetscFunctionReturn(0);
}

/// @}
