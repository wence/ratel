/// @file
/// Setup Neo-Hookean physics context objects

#include <ceed.h>
#include <math.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel-models.h>
#include <ratel.h>
#include <ratel/models/neo-hookean.h>
#include <stddef.h>

/// @addtogroup RatelModels
/// @{

/**
  @brief Build libCEED QFunctionContext for Neo-Hookean physics

  @param[in]   ratel           Ratel context
  @param[in]   material_index  Integer index of the current material in the materials list
  @param[out]  ctx             libCEED QFunctionContext for Neo-Hookean physics

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCreatePhysicsContext_NeoHookean(Ratel ratel, PetscInt material_index, CeedQFunctionContext *ctx) {
  NeoHookeanPhysics *phys;

  PetscFunctionBegin;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Setup Physics Context Neo-Hookean");

  PetscCall(PetscCalloc1(1, &phys));
  PetscCall(RatelProcessPhysics_NeoHookean(ratel, material_index, phys));
  RatelCeedCall(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCeedCall(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*phys), phys));
  // Common parameters
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "rho", offsetof(NeoHookeanPhysics, common_parameters[RATEL_COMMON_PARAMETER_RHO]), 1,
                                                          "density"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(
                           *ctx, "shift v", offsetof(NeoHookeanPhysics, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]), 1, "shift for U_t"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(
                           *ctx, "shift a", offsetof(NeoHookeanPhysics, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]), 1, "shift for U_tt"));
  // Neo-Hookean specific parameters
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "nu", offsetof(NeoHookeanPhysics, nu), 1, "Poisson's ratio"));
  RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx, "E", offsetof(NeoHookeanPhysics, E), 1, "Young's Modulus"));
  if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(*ctx, stdout));
  PetscCall(PetscFree(phys));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Setup Physics Context Neo-Hookean Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Build libCEED QFunctionContext for multigrid smoother for
           Neo-Hookean physics

  @param[in]   ratel           Ratel context
  @param[in]   ctx             libCEED QFunctionContext for Neo-Hookean physics
  @param[in]   material_index  Integer index of the current material in the materials list
  @param[out]  ctx_smoother    libCEED QFunctionContext for multigrid smoother

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCreatePhysicsSmootherContext_NeoHookean(Ratel ratel, CeedQFunctionContext ctx, PetscInt material_index,
                                                            CeedQFunctionContext *ctx_smoother) {
  PetscScalar        nu_smoother = 0;
  PetscBool          nu_flag     = PETSC_FALSE;
  NeoHookeanPhysics *phys, *phys_smoother;

  PetscFunctionBegin;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Setup Physics Smoother Context Neo-Hookean");

  char *prefix;
  PetscCall(RatelGetMaterialPrefix(ratel, material_index, &prefix));

  PetscOptionsBegin(ratel->comm, prefix, "Neo-Hookean physical parameters for smoother", NULL);

  PetscCall(PetscOptionsScalar("-nu_smoother", "Poisson's ratio for smoother", NULL, nu_smoother, &nu_smoother, &nu_flag));
  if (nu_flag) RatelDebug(ratel, "---- Nu: %f", nu_smoother);
  else RatelDebug(ratel, "---- No Nu for smoother set");

  PetscOptionsEnd();  // End of setting Physics
  PetscCall(PetscFree(prefix));

  if (nu_flag) {
    // Copy context
    RatelCeedCall(ratel, CeedQFunctionContextGetData(ctx, CEED_MEM_HOST, &phys));
    PetscCall(PetscCalloc1(1, &phys_smoother));
    PetscCall(PetscMemcpy(phys_smoother, phys, sizeof(*phys)));
    RatelCeedCall(ratel, CeedQFunctionContextRestoreData(ctx, &phys));
    // Create smoother context
    RatelCeedCall(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx_smoother));
    phys_smoother->nu = nu_smoother;
    RatelCeedCall(ratel, CeedQFunctionContextSetData(*ctx_smoother, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*phys_smoother), phys_smoother));
    // Common parameters
    RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(
                             *ctx_smoother, "rho", offsetof(NeoHookeanPhysics, common_parameters[RATEL_COMMON_PARAMETER_RHO]), 1, "density"));
    RatelCeedCall(ratel,
                  CeedQFunctionContextRegisterDouble(
                      *ctx_smoother, "shift v", offsetof(NeoHookeanPhysics, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]), 1, "shift for U_t"));
    RatelCeedCall(ratel,
                  CeedQFunctionContextRegisterDouble(
                      *ctx_smoother, "shift a", offsetof(NeoHookeanPhysics, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]), 1, "shift for U_tt"));
    // Neo-Hookean specific parameters
    RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx_smoother, "nu", offsetof(NeoHookeanPhysics, nu), 1, "Poisson's ratio"));
    RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(*ctx_smoother, "E", offsetof(NeoHookeanPhysics, E), 1, "Young's Modulus"));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(*ctx_smoother, stdout));
    PetscCall(PetscFree(phys_smoother));
  } else {
    *ctx_smoother = NULL;
  }

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Setup Physics Smoother Context Neo-Hookean Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Process command line options for Neo-Hookean physics

  @param[in]   ratel           Ratel context
  @param[in]   material_index  Integer index of the current material in the materials list
  @param[out]  phys_arg        Physics context struct

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelProcessPhysics_NeoHookean(Ratel ratel, PetscInt material_index, void *phys_arg) {
  NeoHookeanPhysics *phys                             = (NeoHookeanPhysics *)phys_arg;
  PetscBool          nu_flag                          = PETSC_FALSE;
  PetscBool          Young_flag                       = PETSC_FALSE;
  phys->nu                                            = 0.0;
  phys->E                                             = 0.0;
  phys->common_parameters[RATEL_COMMON_PARAMETER_RHO] = 1.0;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Process Physics Neo-Hookean");

  char *prefix;

  PetscCall(RatelGetMaterialPrefix(ratel, material_index, &prefix));
  PetscOptionsBegin(ratel->comm, prefix, "Neo-Hookean physical parameters", NULL);

  PetscCall(PetscOptionsScalar("-nu", "Poisson's ratio", NULL, phys->nu, &phys->nu, &nu_flag));
  RatelDebug(ratel, "---- Nu: %f", phys->nu);

  PetscCall(PetscOptionsScalar("-E", "Young's Modulus", NULL, phys->E, &phys->E, &Young_flag));
  RatelDebug(ratel, "---- E: %f", phys->E);

  PetscCall(PetscOptionsScalar("-rho", "Density for scaled mass matrix", NULL, phys->common_parameters[RATEL_COMMON_PARAMETER_RHO],
                               &phys->common_parameters[RATEL_COMMON_PARAMETER_RHO], NULL));
  if (phys->common_parameters[RATEL_COMMON_PARAMETER_RHO] < 0.0) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Density must be a positive value");

  PetscOptionsEnd();  // End of setting Physics
  PetscCall(PetscFree(prefix));

  // Check for all required options to be set
  if (!nu_flag) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "-nu option needed");
  if (!Young_flag) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "-E option needed");

  // Scale E to Pa
  phys->E *= ratel->model_units->Pascal;
  phys->common_parameters[RATEL_COMMON_PARAMETER_RHO] *= -ratel->model_units->kilogram / pow(ratel->model_units->meter, 3);

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Process Physics Neo-Hookean Success!");
  PetscFunctionReturn(0);
}

/// @}
