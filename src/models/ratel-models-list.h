// List each model registration function once here. This will be expanded
// inside RatelRegisterModels() to call each registration function in the
// order listed, and also to define weak symbol aliases for models that are
// not configured.

#include <ratel-types.h>
#include <ratel.h>

MACRO("elasticity-linear", ElasticityLinear, NeoHookean)
MACRO("elasticity-neo-hookean-current", ElasticityNeoHookeanCurrent, NeoHookean)
MACRO("elasticity-neo-hookean-initial", ElasticityNeoHookeanInitial, NeoHookean)
MACRO("elasticity-neo-hookean-initial-ad", ElasticityNeoHookeanInitialAD, NeoHookean)
MACRO("elasticity-mooney-rivlin-initial", ElasticityMooneyRivlinInitial, MooneyRivlin)
