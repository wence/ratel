/// @file
/// Implementation of Ratel solver management interfaces

#include <ceed.h>
#include <ceed/backend.h>
#include <petsc.h>
#include <ratel-fem.h>
#include <ratel-impl.h>
#include <ratel-monitor.h>
#include <ratel.h>
#include <string.h>

/// @addtogroup RatelCore
/// @{

/**
  @brief Setup default TS options.
         Note: Sets SNES defaults from `RatelSNESSetDefaults()`.

  @param[in]      ratel  Ratel context
  @param[in,out]  ts     TS object to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSSetDefaults(Ratel ratel, TS ts) {
  SNES snes;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Set Defaults");

  // TS defaults
  PetscCall(TSSetTimeStep(ts, 1.0));
  if (ratel->method == RATEL_METHOD_DYNAMIC_ELASTICITY) {
    PetscCall(TSSetType(ts, TSALPHA2));
  }

  // SNES defaults
  PetscCall(TSGetSNES(ts, &snes));
  PetscCall(RatelSNESSetDefaults(ratel, snes));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Set Defaults Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Set additional Ratel specific TS options.

         `-ts_monitor_strain_energy` sets TSMonitor function for strain energy.
         `-ts_monitor_surface_force viewer:filename.extension` sets TSMonitor function for surface forces
         `-ts_monitor_diagnostic_quantities viewer:filename.extension` sets TSMonitor function for diagnostic quantities

         Note that only one supplemental TSMonitor may be set.

  @param[in]      ratel  Ratel context
  @param[in,out]  ts     TS object to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSSetFromOptions(Ratel ratel, TS ts) {
  PetscBool monitor_strain_energy = PETSC_FALSE, monitor_surface_forces = PETSC_FALSE, monitor_diagnostic_quantities = PETSC_FALSE;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Set From Options");

  PetscOptionsBegin(ratel->comm, NULL, "Ratel TS options", NULL);
  PetscCall(PetscOptionsViewer("-ts_monitor_strain_energy", "Set TSMonitor for strain energy", NULL, &ratel->monitor_viewer_strain_energy, NULL,
                               &monitor_strain_energy));
  PetscCall(PetscOptionsViewer("-ts_monitor_surface_force", "Set TSMonitor for surface forces", NULL, &ratel->monitor_viewer_surface_forces, NULL,
                               &monitor_surface_forces));
  PetscCall(PetscOptionsViewer("-ts_monitor_diagnostic_quantities", "Set TSMonitor for diagnostic quantities", NULL,
                               &ratel->monitor_viewer_diagnostic_quantities, NULL, &monitor_diagnostic_quantities));
  PetscOptionsEnd();

  // Check for supported viewer for strain energy
  if (monitor_strain_energy) {
    PetscViewerType viewer_type;

    PetscCall(PetscViewerGetType(ratel->monitor_viewer_strain_energy, &viewer_type));
    if (strcmp(viewer_type, PETSCVIEWERASCII)) {
      // LCOV_EXCL_START
      SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Only ASCII viewer supported for -ts_monitor_strain_energy");
      // LCOV_EXCL_STOP
    }
  }

  // Check for supported viewer for surface foces
  if (monitor_surface_forces) {
    PetscViewerType viewer_type;
    const char     *file_name;

    PetscCall(PetscViewerGetType(ratel->monitor_viewer_surface_forces, &viewer_type));
    if (strcmp(viewer_type, PETSCVIEWERASCII)) {
      // LCOV_EXCL_START
      SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Only ASCII viewer supported for ts_monitor_surface_force");
      // LCOV_EXCL_STOP
    }

    // Header row
    PetscCall(PetscViewerFileGetName(ratel->monitor_viewer_surface_forces, &file_name));
    if (strcmp(file_name, "stdout")) {
      PetscCall(PetscViewerASCIIPrintf(ratel->monitor_viewer_surface_forces,
                                       "time, face_id, centroid_x, centroid_y, centroid_z, force_x, force_y, force_z\n"));
    }
  }

  // Get filename information if using VTK/VTU viewer
  // TODO: If the PETSc VTK/VTU viewer supports time series in the future, this can be dropped
  if (monitor_diagnostic_quantities) {
    const char *file_name;
    char       *file_extension;

    PetscCall(PetscViewerFileGetName(ratel->monitor_viewer_diagnostic_quantities, &file_name));
    PetscCall(PetscStrrchr(file_name, '.', &file_extension));
    ratel->is_monitor_vtk = !strcmp(file_extension, "vtu") || !strcmp(file_extension, "vtk");
    if (ratel->is_monitor_vtk) {
      size_t file_name_len      = file_extension - file_name;
      size_t file_extension_len = 0;

      PetscCall(PetscStrlen(file_extension, &file_extension_len));
      file_extension_len += 1;
      PetscCall(PetscCalloc1(file_name_len + 1, &ratel->monitor_vtk_file_name));
      PetscCall(PetscSNPrintf(ratel->monitor_vtk_file_name, file_name_len, "%s", file_name));
      PetscCall(PetscCalloc1(file_extension_len + 1, &ratel->monitor_vtk_file_extension));
      PetscCall(PetscSNPrintf(ratel->monitor_vtk_file_extension, file_extension_len, "%s", file_extension));
    }
  }

  // Set monitors
  if (monitor_strain_energy || monitor_surface_forces || monitor_diagnostic_quantities) PetscCall(RatelTSMonitorSet(ratel, ts, RatelTSMonitor));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Set From Options Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup default SNES options.
         Note: Sets default SNES line search to critical point method.

  @param[in]      ratel  Ratel context
  @param[in,out]  snes   SNES object to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESSetDefaults(Ratel ratel, SNES snes) {
  SNESLineSearch line_search;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel SNES Set Defaults");

  // Default linesearch
  PetscCall(SNESGetLineSearch(snes, &line_search));
  PetscCall(SNESLineSearchSetType(line_search, SNESLINESEARCHCP));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel SNES Set Defaults Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup multigrid preconditioner from Ratel context

  @param[in]   ratel           Ratel context
  @param[in]   multigrid_type  Multigrid strategy
  @param[out]  ts              TS object with multigrid preconditioner

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSSetupPCMG(Ratel ratel, RatelMultigridType multigrid_type, TS ts) {
  SNES snes;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Setup PCMG");

  // Setup on SNES
  PetscCall(TSGetSNES(ts, &snes));
  PetscCall(RatelSNESSetupPCMG(ratel, multigrid_type, snes));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Setup PCMG Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup multigrid preconditioner from Ratel context

  @param[in]   ratel           Ratel context
  @param[in]   multigrid_type  Multigrid strategy
  @param[out]  snes            SNES object with multigrid preconditioner

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESSetupPCMG(Ratel ratel, RatelMultigridType multigrid_type, SNES snes) {
  KSP ksp;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel SNES Setup PCMG");

  // Setup KSP and PC
  PetscCall(SNESGetKSP(snes, &ksp));
  PetscCall(RatelKSPSetupPCMG(ratel, multigrid_type, ksp));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel SNES Setup PCMG Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup multigrid preconditioner from Ratel context.
           Note: Sets default KSP to Conjugate Gradient for multigrid with multiple levels
                 or no multigrid and "preconditioner only" with AMG for multigrid with one level.

  @param[in]   ratel           Ratel context
  @param[in]   multigrid_type  Multigrid strategy
  @param[out]  ksp             KSP object with multigrid preconditioner

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelKSPSetupPCMG(Ratel ratel, RatelMultigridType multigrid_type, KSP ksp) {
  PetscLogStage stage_pmg_setup;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel pMG Setup");
  PetscCall(PetscLogStageGetId("Ratel pMG Setup", &stage_pmg_setup));
  if (stage_pmg_setup == -1) PetscCall(PetscLogStageRegister("Ratel pMG Setup", &stage_pmg_setup));
  PetscCall(PetscLogStagePush(stage_pmg_setup));

  // Setup level degrees
  if (ratel->is_cl_multigrid_type) ratel->multigrid_type = ratel->cl_multigrid_type;
  else ratel->multigrid_type = multigrid_type;

  // Determine number of levels
  switch (ratel->multigrid_type) {
    case RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC:
      ratel->num_multigrid_levels = ceil(log(ratel->multigrid_fine_order) / log(2)) - floor(log(ratel->multigrid_coarse_order) / log(2)) + 1;
      break;
    case RATEL_MULTIGRID_P_COARSENING_UNIFORM:
      ratel->num_multigrid_levels = ratel->multigrid_fine_order - ratel->multigrid_coarse_order + 1;
      break;
    case RATEL_MULTIGRID_P_COARSENING_USER:
      // Nothing to do
      break;
    case RATEL_MULTIGRID_AMG_ONLY:
    case RATEL_MULTIGRID_NONE:
      ratel->num_multigrid_levels = 1;
      break;
  }
  CeedInt fine_level = ratel->num_multigrid_levels - 1;

  // Populate array of degrees for each level for multigrid
  switch (ratel->multigrid_type) {
    case RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC:
      ratel->multigrid_level_orders[0] = ratel->multigrid_coarse_order;
      CeedInt offset                   = floor(log(ratel->multigrid_coarse_order) / log(2));
      for (CeedInt i = 1; i < fine_level; i++) {
        ratel->multigrid_level_orders[i] = pow(2, offset + i);
      }
      ratel->multigrid_level_orders[fine_level] = ratel->multigrid_fine_order;
      break;
    case RATEL_MULTIGRID_P_COARSENING_UNIFORM:
      for (CeedInt i = 0; i < fine_level; i++) {
        ratel->multigrid_level_orders[i] = i + ratel->multigrid_coarse_order;
      }
      break;
    case RATEL_MULTIGRID_P_COARSENING_USER:
      // Nothing to do
      break;
    case RATEL_MULTIGRID_AMG_ONLY:
    case RATEL_MULTIGRID_NONE:
      ratel->multigrid_level_orders[0] = ratel->multigrid_fine_order;
      break;
  }

  // Default KSP type
  PetscCall(KSPSetType(ksp, KSPCG));
  PetscCall(KSPSetNormType(ksp, KSP_NORM_NATURAL));

  // Method specific setup
  switch (ratel->method) {
    case RATEL_METHOD_STATIC_ELASTICITY:
    case RATEL_METHOD_QUASISTATIC_ELASTICITY:
    case RATEL_METHOD_DYNAMIC_ELASTICITY:
      PetscCall(RatelKSPSetupPCMG_FEM(ratel, ratel->multigrid_type, ksp));
      break;
    // LCOV_EXCL_START
    default:
      SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Method not implemented");
      // LCOV_EXCL_STOP
  }

  PetscLogStagePop();
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel KSP Setup PCMG Success!");
  PetscFunctionReturn(0);
}

/// @}
