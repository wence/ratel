/// @file
/// Display and command line option strings for Ratel enums

#include <ratel.h>

const char *const RatelMethodTypes[] = {
    [RATEL_METHOD_STATIC_ELASTICITY]      = "static elasticity",
    [RATEL_METHOD_QUASISTATIC_ELASTICITY] = "quasistatic elasticity",
    [RATEL_METHOD_DYNAMIC_ELASTICITY]     = "dynamic elasticity",
};

const char *const RatelForcingTypes[] = {
    [RATEL_FORCING_NONE]     = "none",
    [RATEL_FORCING_CONSTANT] = "constant",
    [RATEL_FORCING_MMS]      = "linear manufactured solution",
};

const char *const RatelForcingTypesCL[] = {
    "none", "constant", "linear-mms", "RatelForcingType", "RATEL_FORCE_", 0,
};

const char *const RatelBoundaryTypes[] = {
    [RATEL_BOUNDARY_CLAMP]    = "Dirichlet clamp",
    [RATEL_BOUNDARY_TRACTION] = "Neumann traction",
};

const char *const RatelMutigridTypes[] = {
    [RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC] = "p-multigrid, logarithmic coarsening",
    [RATEL_MULTIGRID_P_COARSENING_UNIFORM]     = "p-multigrid, uniform coarsening",
    [RATEL_MULTIGRID_P_COARSENING_USER]        = "p-multigrid, user defined coarsening",
    [RATEL_MULTIGRID_AMG_ONLY]                 = "AMG only",
    [RATEL_MULTIGRID_NONE]                     = "no multigrid",
};

const char *const RatelMultigridTypesCL[] = {
    "p_logarithmic", "p_uniform", "p_user", "amg_only", "none", "RatelMultigridTypes", "RATEL_MULTIGRID_", 0,
};
