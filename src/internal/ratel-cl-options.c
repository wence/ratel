/// @file
/// Command line option processing for solid mechanics example using PETSc

#include <math.h>
#include <ratel-cl-options.h>
#include <ratel-impl.h>
#include <ratel-models.h>
#include <ratel-types.h>
#include <ratel.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief Process general command line options

  @param[in,out]  ratel  Ratel context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelProcessCommandLineOptions(Ratel ratel) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Process Command Line Options");

  PetscOptionsBegin(ratel->comm, NULL, "Elasticity / Hyperelasticity in PETSc with libCEED", NULL);

  // libCEED resource
  {
    PetscBool ceed_set    = PETSC_FALSE;
    PetscInt  max_strings = 1;

    PetscCall(PetscOptionsStringArray("-ceed", "CEED resource specifier", NULL, &ratel->ceed_resource, &max_strings, &ceed_set));
    RatelDebug(ratel, "---- Ceed resource requested by user: %s", ceed_set ? ratel->ceed_resource : "none");

    // -- Provide default libCEED resource if not specified
    if (!ceed_set) {
      const char *default_ceed_resource     = "/cpu/self";
      size_t      default_ceed_resource_len = strlen(default_ceed_resource) + 1;

      PetscCall(PetscCalloc1(default_ceed_resource_len, &ratel->ceed_resource));
      PetscCall(PetscStrncpy(ratel->ceed_resource, default_ceed_resource, default_ceed_resource_len));
    }
    RatelDebug(ratel, "---- Ceed resource to initalize: %s", ratel->ceed_resource);
  }

  // Multigrid options
  // -- Type of multigrid and coarsening
  ratel->multigrid_type    = RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC;
  ratel->cl_multigrid_type = RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC;
  PetscCall(PetscOptionsEnum("-multigrid", "Set multigrid type option", NULL, RatelMultigridTypesCL, (PetscEnum)ratel->cl_multigrid_type,
                             (PetscEnum *)&ratel->cl_multigrid_type, &ratel->is_cl_multigrid_type));

  // -- Multigrid level orders
  ratel->multigrid_level_orders[0] = 3;
  ratel->num_multigrid_levels      = RATEL_MAX_MULTIGRID_LEVELS;
  PetscBool found_order_option     = PETSC_FALSE;
  PetscCall(PetscOptionsIntArray("-order", "Polynomial order of basis functions", NULL, ratel->multigrid_level_orders, &ratel->num_multigrid_levels,
                                 &found_order_option));
  if (!found_order_option) ratel->num_multigrid_levels = 1;
  for (PetscInt i = 0; i < ratel->num_multigrid_levels; i++) {
    if (ratel->multigrid_level_orders[i] <= 0) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Polynomial order must be positive integer");
    if (i > 0 && ratel->multigrid_level_orders[i] < ratel->multigrid_level_orders[i - 1]) {
      // LCOV_EXCL_START
      SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Polynomial orders for multigrid levels must be provided in ascending order");
      // LCOV_EXCL_STOP
    }
  }
  ratel->multigrid_fine_order       = ratel->multigrid_level_orders[ratel->num_multigrid_levels - 1];
  ratel->num_multigrid_levels_setup = 0;
  RatelDebug(ratel, "---- Polynomial order: %" PetscInt_FMT, ratel->multigrid_fine_order);

  // -- Coarse grid order
  ratel->multigrid_coarse_order = 1;
  PetscCall(PetscOptionsInt("-coarse_order", "Polynomial order of coarse grid basis functions", NULL, ratel->multigrid_coarse_order,
                            &ratel->multigrid_coarse_order, NULL));
  RatelDebug(ratel, "---- Multigrid coarse grid polynomial order: %" PetscInt_FMT, ratel->multigrid_coarse_order);

  // Additional quadrature points
  ratel->q_extra = 0;
  PetscCall(PetscOptionsInt("-q_extra", "Number of extra quadrature points", NULL, ratel->q_extra, &ratel->q_extra, NULL));
  RatelDebug(ratel, "---- Extra quadrature points: %" PetscInt_FMT, ratel->q_extra);

  // Additional quadrature points
  ratel->q_extra_surface_force = ratel->q_extra > 0 ? ratel->q_extra : 1;
  PetscCall(PetscOptionsInt("-q_extra_surface_force", "Number of extra quadrature points for surface force computation", NULL,
                            ratel->q_extra_surface_force, &ratel->q_extra_surface_force, NULL));
  RatelDebug(ratel, "---- Extra quadrature points for surface force computation: %" PetscInt_FMT, ratel->q_extra_surface_force);

  // Diagnostic quantities order
  ratel->diagnostic_order = ratel->multigrid_fine_order;
  PetscCall(
      PetscOptionsInt("-diagnostic_order", "Polynomial order of diagnostic output", NULL, ratel->diagnostic_order, &ratel->diagnostic_order, NULL));
  RatelDebug(ratel, "---- Diagnostic value order: %" PetscInt_FMT, ratel->diagnostic_order);

  // Geometry order for viewer
  ratel->diagnostic_geometry_order = RATEL_DECIDE;
  PetscCall(PetscOptionsInt("-diagnostic_geometry_order", "Geometry order for diagnostic values mesh", NULL, ratel->diagnostic_geometry_order,
                            &ratel->diagnostic_geometry_order, NULL));
  PetscBool is_diagnostic_geometry_ratel_decide = ratel->diagnostic_geometry_order == RATEL_DECIDE;
  if (!is_diagnostic_geometry_ratel_decide && ratel->diagnostic_geometry_order < 1) {
    // LCOV_EXCL_START
    SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Geometry order must be a positive value: %" PetscInt_FMT, ratel->diagnostic_geometry_order);
    // LCOV_EXCL_STOP
  }
  {
    char diagnostic_geometry_order_string[PETSC_MAX_PATH_LEN];

    if (is_diagnostic_geometry_ratel_decide) {
      PetscCall(PetscSNPrintf(diagnostic_geometry_order_string, sizeof(diagnostic_geometry_order_string), "%s", "use solution mesh geometric order"));
    } else {
      PetscCall(PetscSNPrintf(diagnostic_geometry_order_string, sizeof(diagnostic_geometry_order_string), "%" PetscInt_FMT,
                              ratel->diagnostic_geometry_order));
    }
    RatelDebug(ratel, "---- Geometry order for diagnostic values mesh: %s", diagnostic_geometry_order_string);
  }

  // Material options
  {
    ratel->num_materials            = RATEL_MAX_MATERIALS;
    // -- Get number and names of materials
    PetscBool found_material_option = PETSC_FALSE;
    PetscCall(PetscOptionsStringArray("-material", "List of user-defined material names", __func__, ratel->material_names, &ratel->num_materials,
                                      &found_material_option));

    if (!found_material_option) {
      ratel->num_materials = 1;
      PetscCall(PetscCalloc1(1, &ratel->material_names[0]));
    }

    // -- Set default domain label
    const char *label_name     = "Cell Sets";
    size_t      label_name_len = strlen(label_name) + 1;
    PetscBool   label_name_set = PETSC_FALSE;
    PetscCall(PetscCalloc1(label_name_len, &ratel->material_label_name));
    PetscCall(PetscStrncpy(ratel->material_label_name, label_name, label_name_len));

    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscBool new_label_name_set = PETSC_FALSE;
      char     *prefix, *message;

      PetscCall(RatelGetMaterialPrefix(ratel, i, &prefix));
      PetscCall(RatelGetMaterialMessage(ratel, i, &message));
      PetscOptionsBegin(ratel->comm, prefix, message, NULL);

      // ---- Get material model
      PetscInt max_strings = 1;
      PetscCall(PetscOptionsStringArray("-model", "Material model", NULL, &ratel->material_models[i], &max_strings, NULL));
      {
        RatelModelData model_data;
        PetscCall(RatelGetModelData(ratel, ratel->material_models[i], &model_data));
        RatelDebug(ratel, "---- Material name: %s and model: %s", strlen(ratel->material_names[i]) ? ratel->material_names[i] : "none",
                   model_data->name_for_display);
      }

      // ---- Check label
      char  *previous_label_name;
      size_t previous_label_name_len = strlen(ratel->material_label_name) + 1;
      PetscCall(PetscCalloc1(previous_label_name_len, &previous_label_name));
      PetscCall(PetscStrncpy(previous_label_name, ratel->material_label_name, previous_label_name_len));

      // ---- Get domain label name for material
      PetscCall(PetscOptionsStringArray("-label_name", "Mesh partitioning name for material", NULL, &ratel->material_label_name, &max_strings,
                                        &new_label_name_set));

      // ---- Check for mismatch with previous label, if any
      if (label_name_set && new_label_name_set) {
        PetscBool names_match = PETSC_TRUE;

        PetscCall(PetscStrncmp(previous_label_name, ratel->material_label_name, previous_label_name_len, &names_match));
        if (!names_match) {
          // LCOV_EXCL_START
          SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Label names set for materials %s and %s are different", ratel->material_names[i],
                  ratel->material_names[i - 1]);
          // LCOV_EXCL_STOP
        }
      }
      PetscCall(PetscFree(previous_label_name));
      label_name_set |= new_label_name_set;

      // ---- Domain values
      PetscBool label_values_set = PETSC_FALSE;
      PetscInt  num_label_values = 1000;
      PetscCalloc(num_label_values, &ratel->material_label_values[i]);
      PetscOptionsIntArray("-label_value", "Mesh partitioning values for multi-materials", NULL, ratel->material_label_values[i], &num_label_values,
                           &label_values_set);
      if (!label_values_set) {
        if (ratel->num_materials > 1) {
          // LCOV_EXCL_START
          SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Label values must be set for %s material!", prefix);
          // LCOV_EXCL_STOP
        } else {
          ratel->material_label_values[i][0] = 3;
        }
      }

      PetscOptionsEnd();
      PetscCall(PetscFree(prefix));
      PetscCall(PetscFree(message));
    }
  }

  // Forcing function
  ratel->forcing_choice = RATEL_FORCING_NONE;  // Default - no forcing term
  PetscCall(PetscOptionsEnum("-forcing", "Set forcing function option", NULL, RatelForcingTypesCL, (PetscEnum)ratel->forcing_choice,
                             (PetscEnum *)&ratel->forcing_choice, NULL));
  RatelDebug(ratel, "---- Forcing function: %s", RatelForcingTypes[ratel->forcing_choice]);

  if (ratel->forcing_choice == RATEL_FORCING_CONSTANT) {
    for (CeedInt material_index = 0; material_index < ratel->num_materials; material_index++) {
      if (strcmp(ratel->material_models[material_index], "elasticity-linear")) {
        // LCOV_EXCL_START
        SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP,
                "Cannot use constant forcing and finite strain formulation. "
                "Constant forcing in reference frame currently unavaliable.");
        // LCOV_EXCL_STOP
      }
    }
  }

  // Dirichlet boundary conditions
  ratel->bc_clamp_count = RATEL_MAX_BOUNDARY_FACES;
  PetscCall(
      PetscOptionsIntArray("-bc_clamp", "Face IDs to apply incremental Dirichlet BC", NULL, ratel->bc_clamp_faces, &ratel->bc_clamp_count, NULL));

  // Slip boundary conditions
  ratel->bc_slip_count = RATEL_MAX_BOUNDARY_FACES;
  PetscCall(PetscOptionsIntArray("-bc_slip", "Face IDs to apply slip BC", NULL, ratel->bc_slip_faces, &ratel->bc_slip_count, NULL));
  // Set components for each slip BC
  if (ratel->bc_slip_count) RatelDebug(ratel, "---- Slip Boundaries:");
  for (PetscInt i = 0; i < ratel->bc_slip_count; i++) {
    // Translation vector
    char option_name[25];
    ratel->bc_slip_components_count[i] = sizeof(ratel->bc_slip_components[0]) / sizeof(ratel->bc_slip_components[0][0]);
    for (PetscInt j = 0; j < ratel->bc_slip_components_count[i]; j++) ratel->bc_slip_components[i][j] = 0.;

    PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_slip_%" PetscInt_FMT "_components", ratel->bc_slip_faces[i]));
    PetscCall(PetscOptionsIntArray(option_name, "Components to constrain with slip BC", NULL, ratel->bc_slip_components[i],
                                   &ratel->bc_slip_components_count[i], NULL));

    if (ratel->bc_slip_components_count[i] == 0) {
      // LCOV_EXCL_START
      SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Must set slip components for face %" PetscInt_FMT, ratel->bc_slip_faces[i]);
      // LCOV_EXCL_STOP
    }

    if (ratel->is_debug) {
      // LCOV_EXCL_START
      char    components[8] = "";
      CeedInt head          = 0;
      for (CeedInt j = 0; j < ratel->bc_slip_components_count[i]; j++) {
        components[head] = ratel->bc_slip_components[i][j] == 0 ? 'x' : ratel->bc_slip_components[i][j] == 1 ? 'y' : 'z';
        head++;
        if (j < ratel->bc_slip_components_count[i] - 1) {
          components[head]     = ',';
          components[head + 1] = ' ';
          head += 2;
        }
      }
      RatelDebug(ratel, "----   Slip Boundary %" PetscInt_FMT ": components %s", ratel->bc_slip_faces[i], components);
      // LCOV_EXCL_STOP
    }
  }

  // Neumann boundary conditions
  ratel->bc_traction_count = RATEL_MAX_BOUNDARY_FACES;
  PetscCall(PetscOptionsIntArray("-bc_traction", "Face IDs to apply traction (Neumann) BC", NULL, ratel->bc_traction_faces, &ratel->bc_traction_count,
                                 NULL));

  // Face diagnostics
  ratel->surface_force_face_count = RATEL_MAX_BOUNDARY_FACES;
  PetscCall(PetscOptionsIntArray("-surface_force_faces", "Face IDs to compute surface force", NULL, ratel->surface_force_dm_faces,
                                 &ratel->surface_force_face_count, NULL));
  PetscCall(PetscCalloc1(ratel->surface_force_face_count, &ratel->op_surface_force));

  // Set target strain energy
  {
    PetscBool expected_strain_energy_set = PETSC_FALSE;
    ratel->expected_strain_energy        = 0.;
    PetscCall(PetscOptionsReal("-expected_strain_energy", "Expected final strain energy", NULL, ratel->expected_strain_energy,
                               &ratel->expected_strain_energy, &expected_strain_energy_set));
    if (!expected_strain_energy_set) ratel->expected_strain_energy = 0.0;
  }

  // Physical scaling options
  {
    RatelUnits units;
    PetscCall(PetscMalloc1(1, &units));

    units->meter    = 1.0;
    units->second   = 1.0;
    units->kilogram = 1.0;

    PetscCall(PetscOptionsScalar("-units_meter", "1 meter in scaled length units", NULL, units->meter, &units->meter, NULL));
    units->meter = fabs(units->meter);
    RatelDebug(ratel, "---- units meter: %f", units->meter);

    PetscCall(PetscOptionsScalar("-units_second", "1 second in scaled time units", NULL, units->second, &units->second, NULL));
    units->second = fabs(units->second);
    RatelDebug(ratel, "---- units second: %f", units->second);

    PetscCall(PetscOptionsScalar("-units_kilogram", "1 kilogram in scaled mass units", NULL, units->kilogram, &units->kilogram, NULL));
    units->kilogram = fabs(units->kilogram);
    RatelDebug(ratel, "---- units kilogram: %f", units->kilogram);

    // Define derived units
    units->Pascal = units->kilogram / (units->meter * PetscSqr(units->second));

    ratel->model_units = units;
  }

  PetscOptionsEnd();  // End of setting AppCtx

  // Check for all required values set
  if (!ratel->bc_clamp_count && ratel->forcing_choice != RATEL_FORCING_MMS) {
    // LCOV_EXCL_START
    SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "At least one -bc_clamp condition needed");
    // LCOV_EXCL_STOP
  }

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Process Command Line Options Success!");
  PetscFunctionReturn(0);
}

/// @}
