/// @file
/// Ratel forcing function support

#include <ceed.h>
#include <ceed/backend.h>
#include <ratel-forcing.h>
#include <ratel.h>
#include <ratel/qfunctions/forcing/constant-force.h>
#include <ratel/qfunctions/forcing/linear-manufactured-force.h>

/// @ingroup RatelForcing
const CeedQFunctionUser RatelForcingQFunctionUser[] = {
    [RATEL_FORCING_CONSTANT] = ConstantForce,
    [RATEL_FORCING_MMS]      = MMSForce_ElasticityLinear,
};

/// @ingroup RatelForcing
const char *const RatelForcingSourceFiles[] = {
    [RATEL_FORCING_CONSTANT] = ConstantForce_loc,
    [RATEL_FORCING_MMS]      = MMSForce_ElasticityLinear_loc,
};

/**
  @brief Add forcing term to residual operator

  @param[in,out]  ratel         Ratel context
  @param[in]      x_coord       Coordinate vector for DM
  @param[in]      elem_restr_x  ElemRestriction for coordinates
  @param[in]      basis_x       Basis for coordinates

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelResidualAddForcing(Ratel ratel, CeedVector x_coord, CeedElemRestriction elem_restr_x, CeedBasis basis_x) {
  Ceed          ceed = ratel->ceed;
  CeedOperator *op_residual_u_sub_operators;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Add Forcing Terms");

  RatelCeedCall(ratel, CeedOperatorGetSubList(ratel->op_residual_u, &op_residual_u_sub_operators));

  // Create the QFunction and Operator that computes the forcing term for the residual evaluator
  if (ratel->forcing_choice != RATEL_FORCING_NONE) {
    // *INDENT-OFF*
    RatelDebug(ratel, "---- Setting up forcing term");
    CeedQFunction qf_forcing;
    CeedOperator  op_forcing;

    // Get residual operator data
    CeedInt            num_inputs, num_outputs;
    CeedOperatorField *input_fields, *output_fields;
    RatelCeedCall(ratel, CeedOperatorGetFields(op_residual_u_sub_operators[0], &num_inputs, &input_fields, &num_outputs, &output_fields));

    // Field data
    CeedInt             num_comp_u, num_comp_x, q_data_size;
    CeedVector          q_data;
    CeedBasis           basis_u;
    CeedElemRestriction elem_restr_u, elem_restr_q_data;

    RatelCeedCall(ratel, CeedOperatorFieldGetVector(input_fields[0], &q_data));
    RatelCeedCall(ratel, CeedOperatorFieldGetBasis(input_fields[1], &basis_u));
    RatelCeedCall(ratel, CeedBasisGetNumComponents(basis_u, &num_comp_u));
    RatelCeedCall(ratel, CeedBasisGetDimension(basis_u, &num_comp_x));
    RatelCeedCall(ratel, CeedOperatorFieldGetElemRestriction(input_fields[0], &elem_restr_q_data));
    RatelCeedCall(ratel, CeedOperatorFieldGetElemRestriction(input_fields[1], &elem_restr_u));
    RatelCeedCall(ratel, CeedElemRestrictionGetNumComponents(elem_restr_q_data, &q_data_size));

    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, RatelForcingQFunctionUser[ratel->forcing_choice],
                                                     RatelQFunctionRelativePath(RatelForcingSourceFiles[ratel->forcing_choice]), &qf_forcing));
    // *INDENT-ON*
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_forcing, "qdata", q_data_size, CEED_EVAL_NONE));
    if (ratel->forcing_choice == RATEL_FORCING_MMS) RatelCeedCall(ratel, CeedQFunctionAddInput(qf_forcing, "x", num_comp_x, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_forcing, "force", num_comp_u, CEED_EVAL_INTERP));

    if (ratel->forcing_choice == RATEL_FORCING_MMS) {
      CeedQFunction        qf_residual_u_0;
      CeedQFunctionContext ctx_phys;

      RatelCeedCall(ratel, CeedOperatorGetQFunction(op_residual_u_sub_operators[0], &qf_residual_u_0));
      RatelCeedCall(ratel, CeedQFunctionGetContext(qf_residual_u_0, &ctx_phys));
      RatelCeedCall(ratel, CeedQFunctionSetContext(qf_forcing, ctx_phys));
      RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_forcing, false));
    } else {  // forcing_choice = RATEL_FORCING_CONSTANT
      // set up default forcing vector
      PetscInt             max_n        = 3;
      ForcingVectorContext forcing_data = {.direction[0] = 0, .direction[1] = -1, .direction[2] = 0, .time = 1.0};

      // Read in forcing vector
      PetscOptionsBegin(ratel->comm, NULL, "", NULL);

      PetscCall(PetscOptionsScalarArray("-forcing_vec", "Direction to apply constant force", NULL, forcing_data.direction, &max_n, NULL));
      RatelDebug(ratel, "---- Constant forcing vector: [%.3f, %.3f, %.3f]", forcing_data.direction[0], forcing_data.direction[1],
                 forcing_data.direction[2]);
      PetscOptionsEnd();  // End of reading in forcing vector

      CeedQFunctionContext ctx_forcing;
      RatelCeedCall(ratel, CeedQFunctionContextCreate(ceed, &ctx_forcing));
      RatelCeedCall(ratel, CeedQFunctionContextSetData(ctx_forcing, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(ForcingVectorContext), &forcing_data));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "forcing x", offsetof(ForcingVectorContext, direction[0]), 1,
                                                              "forcing in x direction"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "forcing y", offsetof(ForcingVectorContext, direction[1]), 1,
                                                              "forcing in y direction"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "forcing z", offsetof(ForcingVectorContext, direction[2]), 1,
                                                              "forcing in z direction"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "time", offsetof(ForcingVectorContext, time), 1, "forcing time"));
      RatelCeedCall(ratel, CeedQFunctionSetContext(qf_forcing, ctx_forcing));
      RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_forcing, false));
      if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(ctx_forcing, stdout));
      RatelCeedCall(ratel, CeedQFunctionContextDestroy(&ctx_forcing));
    }
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_forcing, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_forcing));
    RatelCeedCall(ratel, CeedOperatorSetName(op_forcing, "forcing"));
    RatelCeedCall(ratel, CeedOperatorSetField(op_forcing, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, q_data));
    RatelCeedCall(ratel, CeedOperatorSetField(op_forcing, "force", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
    if (ratel->forcing_choice == RATEL_FORCING_MMS) RatelCeedCall(ratel, CeedOperatorSetField(op_forcing, "x", elem_restr_x, basis_x, x_coord));
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ratel->op_residual_u, op_forcing));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_forcing, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_forcing));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_forcing));
  }

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Add Forcing Terms Success!");
  PetscFunctionReturn(0);
}
