/// @file
/// libCEED setup for solid mechanics example using PETSc

#include <ceed.h>
#include <ceed/backend.h>
#include <petsc.h>
#include <petscdm.h>
#include <petscsnes.h>
#include <petscts.h>
#include <ratel-boundary.h>
#include <ratel-dm.h>
#include <ratel-fem.h>
#include <ratel-forcing.h>
#include <ratel-impl.h>
#include <ratel-models.h>
#include <ratel-petsc-ops.h>
#include <ratel-types.h>
#include <ratel-utils.h>
#include <ratel.h>
#include <ratel/models/common-parameters.h>
#include <ratel/models/scaled-mass.h>
#include <ratel/qfunctions/mass.h>

/// @addtogroup RatelMethods
/// @{

/**
  @brief Setup DM with FE space of appropriate degree

  @param[in]   ratel           Ratel context
  @param[in]   setup_boundary  Flag to add Dirichlet boundary
  @param[in]   order           Polynomial order of basis
  @param[in]   coord_order     Polynomial order of coordinate basis, or RATEL_DECIDE for default
  @param[in]   q_extra         Additional quadrature order, or RATEL_DECIDE for default
  @param[in]   num_comp_u      Number of components in solution vector
  @param[out]  dm              DM to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupDMByOrder_FEM(Ratel ratel, PetscBool setup_boundary, PetscInt order, PetscInt q_extra, PetscInt num_comp_u,
                                       PetscInt coord_order, DM dm) {
  PetscInt  dim, q_order = ratel->multigrid_fine_order + (q_extra == RATEL_DECIDE ? ratel->q_extra : q_extra);
  PetscBool is_simplex = PETSC_TRUE;
  PetscFE   fe;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup DMPlex by Degree for Elasticity");

  // Check if simplex or tensor-product mesh
  PetscCall(DMPlexIsSimplex(dm, &is_simplex));

  // Setup DM
  PetscCall(DMGetDimension(dm, &dim));
  PetscCall(PetscFECreateLagrange(ratel->comm, dim, num_comp_u, is_simplex, order, q_order, &fe));
  PetscCall(DMAddField(dm, NULL, (PetscObject)fe));
  PetscCall(DMCreateDS(dm));

  // Project coordinates to enrich quadrature space
  {
    DM             dm_coord;
    PetscDS        ds_coord;
    PetscFE        fe_coord_current, fe_coord_new;
    PetscDualSpace fe_coord_dual_space;
    PetscInt       fe_coord_order, num_comp_coord;

    PetscCall(DMGetCoordinateDM(dm, &dm_coord));
    PetscCall(DMGetCoordinateDim(dm, &num_comp_coord));
    PetscCall(DMGetRegionDS(dm_coord, NULL, NULL, &ds_coord));
    PetscCall(PetscDSGetDiscretization(ds_coord, 0, (PetscObject *)&fe_coord_current));
    PetscCall(PetscFEGetDualSpace(fe_coord_current, &fe_coord_dual_space));
    PetscCall(PetscDualSpaceGetOrder(fe_coord_dual_space, &fe_coord_order));

    // Create FE for coordinates
    if (coord_order != RATEL_DECIDE) fe_coord_order = coord_order;
    PetscCall(PetscFECreateLagrange(ratel->comm, dim, num_comp_coord, is_simplex, fe_coord_order, q_order, &fe_coord_new));
    PetscCall(DMProjectCoordinates(dm, fe_coord_new));
    PetscCall(PetscFEDestroy(&fe_coord_new));
  }
  if (setup_boundary) {
    // Dirichlet boundaries
    RatelDebug(ratel, "---- Dirichlet boundaries");
    PetscCall(RatelDMAddBoundariesDirichlet(ratel, dm));
    // Slip boundaries
    RatelDebug(ratel, "---- Slip boundaries");
    PetscCall(RatelDMAddBoundariesSlip(ratel, dm));
  }
  if (!is_simplex) {
    DM dm_coord;
    PetscCall(DMGetCoordinateDM(dm, &dm_coord));
    PetscCall(DMPlexSetClosurePermutationTensor(dm, PETSC_DETERMINE, NULL));
    PetscCall(DMPlexSetClosurePermutationTensor(dm_coord, PETSC_DETERMINE, NULL));
  }

  // Cleanup
  PetscCall(PetscFEDestroy(&fe));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup DMPlex by Degree for Elasticity Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup context data for residual evaluation, u term

  @param[in]   ratel           Ratel context
  @param[out]  ctx_residual_u  Context data for residual evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupResidualUCtx_FEM(Ratel ratel, RatelOperatorApplyContext ctx_residual_u) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Residual Context for Elasticity u Term");

  // PETSc objects
  ctx_residual_u->dm_x  = ratel->dm_hierarchy[0];
  ctx_residual_u->dm_y  = ratel->dm_hierarchy[0];
  ctx_residual_u->X_loc = ratel->X_loc_residual_u;
  ctx_residual_u->Y_loc = ratel->Y_loc[0];

  // libCEED objects
  ctx_residual_u->ratel      = ratel;
  ctx_residual_u->op         = ratel->op_residual_u;
  ctx_residual_u->x_loc_ceed = ratel->x_loc_ceed[0];
  ctx_residual_u->y_loc_ceed = ratel->y_loc_ceed[0];

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Residual Context for Elasticity u Term Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup context data for residual evaluation, u_t term

  @param[in]   ratel            Ratel context
  @param[out]  ctx_residual_ut  Context data for residual evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupResidualUtCtx_FEM(Ratel ratel, RatelOperatorApplyContext ctx_residual_ut) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Residual Context for Elasticity u_t Term");

  // PETSc objects
  ctx_residual_ut->dm_x  = ratel->dm_hierarchy[0];
  ctx_residual_ut->dm_y  = ratel->dm_hierarchy[0];
  ctx_residual_ut->X_loc = ratel->X_loc[0];
  ctx_residual_ut->Y_loc = ratel->Y_loc[0];

  // libCEED objects
  ctx_residual_ut->ratel      = ratel;
  ctx_residual_ut->op         = ratel->op_residual_ut;
  ctx_residual_ut->x_loc_ceed = ratel->x_loc_ceed[0];
  ctx_residual_ut->y_loc_ceed = ratel->y_loc_ceed[0];

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Residual Context for Elasticity u_t Term Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup context data for residual evaluation, u_tt term

  @param[in]   ratel             Ratel context
  @param[out]  ctx_residual_utt  Context data for residual evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupResidualUttCtx_FEM(Ratel ratel, RatelOperatorApplyContext ctx_residual_utt) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Residual Context for Elasticity u_tt Term");

  // PETSc objects
  ctx_residual_utt->dm_x  = ratel->dm_hierarchy[0];
  ctx_residual_utt->dm_y  = ratel->dm_hierarchy[0];
  ctx_residual_utt->X_loc = ratel->X_loc[0];
  ctx_residual_utt->Y_loc = ratel->Y_loc[0];

  // libCEED objects
  ctx_residual_utt->ratel      = ratel;
  ctx_residual_utt->op         = ratel->op_residual_utt;
  ctx_residual_utt->x_loc_ceed = ratel->x_loc_ceed[0];
  ctx_residual_utt->y_loc_ceed = ratel->y_loc_ceed[0];

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Residual Context for Elasticity u_tt Term Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup context data for Jacobian evaluation

  @param[in]   ratel         Ratel context
  @param[in]   level         Level of Jacobian in DM heirarchy
  @param[out]  ctx_jacobian  Context data for Jacobian evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupJacobianCtx_FEM(Ratel ratel, PetscInt level, RatelOperatorApplyContext ctx_jacobian) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Jacobian Context for Elasticity");

  // PETSc objects
  ctx_jacobian->dm_x  = ratel->dm_hierarchy[level];
  ctx_jacobian->dm_y  = ratel->dm_hierarchy[level];
  ctx_jacobian->X_loc = ratel->X_loc[level];
  ctx_jacobian->Y_loc = ratel->Y_loc[level];

  // FLOPs counting
  ctx_jacobian->flops = ratel->flops_jacobian[level];

  // libCEED objects
  ctx_jacobian->ratel             = ratel;
  ctx_jacobian->op                = ratel->op_jacobian[level];
  ctx_jacobian->x_loc_ceed        = ratel->x_loc_ceed[level];
  ctx_jacobian->y_loc_ceed        = ratel->y_loc_ceed[level];
  ctx_jacobian->ctx_phys          = ratel->material_ctx_phys;
  ctx_jacobian->ctx_phys_smoother = ratel->material_ctx_phys_smoother;

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Jacobian Context for Elasticity Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup context data for Jacobian evaluation

  @param[in]   ratel              Ratel context
  @param[out]  ctx_form_jacobian  Context data for Jacobian evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupFormJacobianCtx_FEM(Ratel ratel, RatelFormJacobianContext ctx_form_jacobian) {
  VecType vec_type;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Form Jacobian Context for Elasticity");

  ctx_form_jacobian->num_levels        = ratel->num_multigrid_levels;
  ctx_form_jacobian->ctx_jacobian_fine = ratel->ctx_jacobian[ratel->num_multigrid_levels - 1];

  // Vec type
  PetscCall(DMGetVecType(ratel->dm_orig, &vec_type));
  ctx_form_jacobian->vec_type = vec_type;

  // PETSc objects
  ctx_form_jacobian->mat_jacobian        = ratel->mat_jacobian;
  ctx_form_jacobian->mat_jacobian_coarse = ratel->mat_jacobian_coarse;

  // libCEED objects
  ctx_form_jacobian->ratel           = ratel;
  ctx_form_jacobian->op_coarse       = ratel->op_jacobian[0];
  ctx_form_jacobian->coo_values_ceed = ratel->coo_values_ceed;

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Form Jacobian Context for Elasticity Success");
  PetscFunctionReturn(0);
}

/**
  @brief Setup context data for multigrid prolongation and restriction operator evaluation

  @param[in]   ratel                 Ratel context
  @param[in]   level                 Level number in multigrid hierarchy
  @param[out]  ctx_prolong_restrict  Context data for prolongation and restriction evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode SetupProlongRestrictCtx_FEM(Ratel ratel, PetscInt level, RatelProlongRestrictContext ctx_prolong_restrict) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Prolong/Restrict Context for Elasticity");

  ctx_prolong_restrict->ratel = ratel;

  // PETSc objects
  ctx_prolong_restrict->dm_c = ratel->dm_hierarchy[level - 1];
  ctx_prolong_restrict->dm_f = ratel->dm_hierarchy[level];

  // Work vectors
  ctx_prolong_restrict->C_X_loc      = ratel->X_loc[level - 1];
  ctx_prolong_restrict->C_Y_loc      = ratel->Y_loc[level - 1];
  ctx_prolong_restrict->F_X_loc      = ratel->X_loc[level];
  ctx_prolong_restrict->F_Y_loc      = ratel->Y_loc[level];
  ctx_prolong_restrict->c_x_loc_ceed = ratel->x_loc_ceed[level - 1];
  ctx_prolong_restrict->c_y_loc_ceed = ratel->y_loc_ceed[level - 1];
  ctx_prolong_restrict->f_x_loc_ceed = ratel->x_loc_ceed[level];
  ctx_prolong_restrict->f_y_loc_ceed = ratel->y_loc_ceed[level];

  // FLOPs counting
  ctx_prolong_restrict->event_prolong  = ratel->event_prolong[level];
  ctx_prolong_restrict->flops_prolong  = ratel->flops_prolong[level];
  ctx_prolong_restrict->event_restrict = ratel->event_restrict[level];
  ctx_prolong_restrict->flops_restrict = ratel->flops_restrict[level];

  // libCEED operators
  ctx_prolong_restrict->op_prolong  = ratel->op_prolong[level];
  ctx_prolong_restrict->op_restrict = ratel->op_restrict[level];

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Prolong/Restrict Context for Elasticity Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup libCEED operators for fine grid non-linear operator and Jacobian

  @param[in,out]  ratel           Ratel context
  @param[in]      model_data      Data and functions for material model
  @param[in]      domain_label    DMLabel for material
  @param[in]      material_index  Index of material to setup operators for

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupResidualJacobian_FEM(Ratel ratel, RatelModelData model_data, DMLabel domain_label, PetscInt material_index) {
  CeedInt             num_nodes, num_qpts, num_comp_x, num_comp_e = 1, num_comp_diagnostic = 15, num_comp_u = model_data->num_comp_u;
  CeedInt             q_data_size = model_data->q_data_size;
  DM                  dm_coord;
  Vec                 coords;
  PetscInt            dim, domain_value = ratel->material_label_values[material_index][0];
  CeedInt             num_elem;
  const PetscScalar  *coord_array;
  Ceed                ceed = ratel->ceed;
  CeedVector          x_coord, q_data, stored_fields[RATEL_MAX_STORED_FIELDS];
  CeedBasis           basis_x, basis_u, basis_diagnostic, basis_energy;
  CeedElemRestriction elem_restr_x, elem_restr_u, elem_restr_energy, elem_restr_diagnostic, elem_restr_q_data,
      elem_restr_stored_fields[RATEL_MAX_STORED_FIELDS];
  CeedQFunctionContext ctx_phys = ratel->material_ctx_phys[material_index];

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Residual and Jacobian Evaluators for Elasticity");

  // libCEED bases
  RatelDebug(ratel, "---- Setting up libCEED bases");
  PetscCall(DMGetDimension(ratel->dm_hierarchy[0], &dim));
  num_comp_x = dim;
  PetscCall(DMGetCoordinateDM(ratel->dm_hierarchy[0], &dm_coord));
  // -- Coordinate basis
  RatelDebug(ratel, "------ Coordinate basis");
  PetscCall(RatelBasisCreateFromPlex(ratel, dm_coord, domain_label, domain_value, 0, 0, &basis_x));
  // -- Solution basis
  RatelDebug(ratel, "------ Solution basis");
  PetscCall(RatelBasisCreateFromPlex(ratel, ratel->dm_hierarchy[0], domain_label, domain_value, 0, 0, &basis_u));
  // -- Energy basis
  RatelDebug(ratel, "------ Energy basis");
  PetscCall(RatelBasisCreateFromPlex(ratel, ratel->dm_energy, domain_label, domain_value, 0, 0, &basis_energy));
  // -- Diagnostic basis
  RatelDebug(ratel, "------ Diagnostics basis");
  PetscCall(RatelBasisCreateFromPlex(ratel, ratel->dm_diagnostic, domain_label, domain_value, 0, 0, &basis_diagnostic));

  // libCEED restrictions
  RatelDebug(ratel, "---- Setting up libCEED restrictions");
  // -- Coordinate restriction
  RatelDebug(ratel, "------ Coordinate restriction");
  PetscCall(RatelRestrictionCreateFromPlex(ratel, dm_coord, domain_label, domain_value, 0, 0, &elem_restr_x));
  // -- Solution restriction
  RatelDebug(ratel, "---- Solution restriction");
  PetscCall(RatelRestrictionCreateFromPlex(ratel, ratel->dm_hierarchy[0], domain_label, domain_value, 0, 0, &elem_restr_u));
  // -- Energy restriction
  RatelDebug(ratel, "------ Energy restriction");
  PetscCall(RatelRestrictionCreateFromPlex(ratel, ratel->dm_energy, domain_label, domain_value, 0, 0, &elem_restr_energy));
  // -- Diagnostic data restriction
  RatelDebug(ratel, "------ Diagnostics restriction");
  PetscCall(RatelRestrictionCreateFromPlex(ratel, ratel->dm_diagnostic, domain_label, domain_value, 0, 0, &elem_restr_diagnostic));
  // -- Stored data at quadrature points
  RatelCeedCall(ratel, CeedElemRestrictionGetNumElements(elem_restr_energy, &num_elem));
  RatelCeedCall(ratel, CeedBasisGetNumNodes(basis_u, &num_nodes));
  RatelCeedCall(ratel, CeedBasisGetNumQuadraturePoints(basis_u, &num_qpts));

  // ---- Geometric data restriction, residual and Jacobian operators
  RatelDebug(ratel,
             "---- Restriction, geometric data: %" CeedInt_FMT " elem with %" CeedInt_FMT " points and %" CeedInt_FMT " fields for %" CeedInt_FMT
             " DoF",
             num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size);
  RatelCeedCall(ratel, CeedElemRestrictionCreateStrided(ceed, num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size,
                                                        CEED_STRIDES_BACKEND, &elem_restr_q_data));
  // ---- Stored field restrictions
  if (model_data->number_fields_stored)
    RatelDebug(ratel, "---- Stored field data restrictions", num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size);
  for (CeedInt i = 0; i < model_data->number_fields_stored; i++) {
    // *INDENT-OFF*
    RatelDebug(ratel, "------ Restriction: %" CeedInt_FMT " elem with %" CeedInt_FMT " points and %" CeedInt_FMT " fields for %" CeedInt_FMT " DoF",
               num_elem, num_qpts, model_data->field_sizes[i], num_elem * num_qpts * model_data->field_sizes[i]);
    RatelCeedCall(ratel, CeedElemRestrictionCreateStrided(ceed, num_elem, num_qpts, model_data->field_sizes[i],
                                                          num_elem * num_qpts * model_data->field_sizes[i], CEED_STRIDES_BACKEND,
                                                          &elem_restr_stored_fields[i]));
    // *INDENT-ON*
  }

  // Element coordinates
  RatelDebug(ratel, "---- Retreiving element coordinates");
  PetscCall(DMGetCoordinatesLocal(ratel->dm_hierarchy[0], &coords));
  PetscCall(VecGetArrayRead(coords, &coord_array));
  RatelCeedCall(ratel, CeedElemRestrictionCreateVector(elem_restr_x, &x_coord, NULL));
  RatelCeedCall(ratel, CeedVectorSetArray(x_coord, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)coord_array));
  PetscCall(VecRestoreArrayRead(coords, &coord_array));

  // libCEED vectors
  RatelDebug(ratel, "---- Setting up libCEED vectors");
  // -- Geometric data vector
  RatelCeedCall(ratel, CeedVectorCreate(ceed, num_elem * num_qpts * q_data_size, &q_data));
  // -- Stored field vectors
  for (CeedInt i = 0; i < model_data->number_fields_stored; i++) {
    // *INDENT-OFF*
    RatelCeedCall(ratel, CeedVectorCreate(ceed, num_elem * num_qpts * model_data->field_sizes[i], &stored_fields[i]));
    // *INDENT-ON*
  }

  // Geometric factor computation:
  //   Create the QFunction and Operator that computes the quadrature data q_data returns dXdx_i,j and w * det
  {
    RatelDebug(ratel, "---- Computing volumetric geometric factors");
    CeedQFunction qf_setup_geo;
    CeedOperator  op_setup_geo;

    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->setup_geo, model_data->setup_geo_loc, &qf_setup_geo));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup_geo, "dx", num_comp_x * dim, CEED_EVAL_GRAD));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup_geo, "weight", 1, CEED_EVAL_WEIGHT));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_setup_geo, "qdata", q_data_size, CEED_EVAL_NONE));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_setup_geo, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_setup_geo));
    PetscCall(RatelSetMaterialOperatorName(ratel, material_index, "volumetric geometric data", op_setup_geo));
    RatelCeedCall(ratel, CeedOperatorSetField(op_setup_geo, "dx", elem_restr_x, basis_x, CEED_VECTOR_ACTIVE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_setup_geo, "weight", CEED_ELEMRESTRICTION_NONE, basis_x, CEED_VECTOR_NONE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_setup_geo, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, CEED_VECTOR_ACTIVE));
    // -- Compute the quadrature data
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_setup_geo, stdout));
    RatelCeedCall(ratel, CeedOperatorApply(op_setup_geo, x_coord, q_data, CEED_REQUEST_IMMEDIATE));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_setup_geo));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_setup_geo));
  }

  // Local residual evaluator:
  //   Create the QFunction and Operator that computes the residual of the non-linear PDE
  {
    RatelDebug(ratel, "---- Setting up local residual evaluator, u term");
    CeedQFunction qf_residual_u;
    CeedOperator  op_residual_u;

    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->residual_u, model_data->residual_u_loc, &qf_residual_u));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_residual_u, "qdata", q_data_size, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_residual_u, "du", num_comp_u * dim, CEED_EVAL_GRAD));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_residual_u, "dv", num_comp_u * dim, CEED_EVAL_GRAD));
    for (CeedInt i = 0; i < model_data->number_fields_stored; i++) {
      // *INDENT-OFF*
      RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_residual_u, model_data->field_names[i], model_data->field_sizes[i], CEED_EVAL_NONE));
      // *INDENT-ON*
    }
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_residual_u, ctx_phys));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_residual_u, false));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_residual_u, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_residual_u));
    PetscCall(RatelSetMaterialOperatorName(ratel, material_index, "volumetric term", op_residual_u));
    RatelCeedCall(ratel, CeedOperatorSetField(op_residual_u, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, q_data));
    RatelCeedCall(ratel, CeedOperatorSetField(op_residual_u, "du", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_residual_u, "dv", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
    for (CeedInt i = 0; i < model_data->number_fields_stored; i++) {
      // *INDENT-OFF*
      RatelCeedCall(ratel, CeedOperatorSetField(op_residual_u, model_data->field_names[i], elem_restr_stored_fields[i], CEED_BASIS_COLLOCATED,
                                                stored_fields[i]));
      // *INDENT-ON*
    }
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ratel->op_residual_u, op_residual_u));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_residual_u, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_residual_u));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_residual_u));
  }

  if (model_data->has_ut_term) {
    RatelDebug(ratel, "---- Setting up local residual evaluator, ut term");
    ratel->has_ut_term = PETSC_TRUE;
    CeedQFunction qf_residual_ut;
    CeedOperator  op_residual_ut;

    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->residual_ut, model_data->residual_ut_loc, &qf_residual_ut));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_residual_ut, "qdata", q_data_size, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_residual_ut, "u_t", num_comp_u * dim, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_residual_ut, "u_t", num_comp_u * dim, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_residual_ut, ctx_phys));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_residual_ut, false));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_residual_ut, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_residual_ut));
    PetscCall(RatelSetMaterialOperatorName(ratel, material_index, "volumetric term", op_residual_ut));
    RatelCeedCall(ratel, CeedOperatorSetField(op_residual_ut, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, q_data));
    RatelCeedCall(ratel, CeedOperatorSetField(op_residual_ut, "u_t", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_residual_ut, "u_t", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ratel->op_residual_ut, op_residual_ut));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_residual_ut, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_residual_ut));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_residual_ut));
  }

  // Scaled mass matrix for local residual evaluator:
  if (ratel->method == RATEL_METHOD_DYNAMIC_ELASTICITY) {
    RatelDebug(ratel, "---- Setting up local residual evaluator, utt term");
    ScaledMassContext   *data_scaled_mass;
    CeedQFunctionContext ctx_scaled_mass;
    CeedQFunction        qf_residual_utt;
    CeedOperator         op_residual_utt;

    // -- QFunctionContext
    PetscCall(PetscCalloc1(1, &data_scaled_mass));
    data_scaled_mass->num_comp = num_comp_u;
    {
      CeedScalar *data_phys;
      RatelCeedCall(ratel, CeedQFunctionContextGetDataRead(ctx_phys, CEED_MEM_HOST, &data_phys));
      data_scaled_mass->rho = data_phys[RATEL_COMMON_PARAMETER_RHO];
      RatelCeedCall(ratel, CeedQFunctionContextRestoreDataRead(ctx_phys, &data_phys));
    }
    RatelCeedCall(ratel, CeedQFunctionContextCreate(ratel->ceed, &ctx_scaled_mass));
    RatelCeedCall(ratel, CeedQFunctionContextSetData(ctx_scaled_mass, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*data_scaled_mass), data_scaled_mass));
    RatelCeedCall(ratel, CeedQFunctionContextRegisterInt32(ctx_scaled_mass, "num comp", offsetof(ScaledMassContext, num_comp), 1,
                                                           "number of components in solution vector"));
    RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_scaled_mass, "rho", offsetof(ScaledMassContext, rho), 1, "material density"));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(ctx_scaled_mass, stdout));
    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->residual_utt, model_data->residual_utt_loc, &qf_residual_utt));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_residual_utt, "qdata", q_data_size, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_residual_utt, "u_tt", num_comp_u, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_residual_utt, "v", num_comp_u, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_residual_utt, ctx_scaled_mass));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_residual_utt, false));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_residual_utt, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_residual_utt));
    PetscCall(RatelSetMaterialOperatorName(ratel, material_index, "volumetric term", op_residual_utt));
    RatelCeedCall(ratel, CeedOperatorSetField(op_residual_utt, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, q_data));
    RatelCeedCall(ratel, CeedOperatorSetField(op_residual_utt, "u_tt", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_residual_utt, "v", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ratel->op_residual_utt, op_residual_utt));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_residual_utt, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionContextDestroy(&ctx_scaled_mass));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_residual_utt));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_residual_utt));
  }

  // Forcing term:
  PetscCall(RatelResidualAddForcing(ratel, x_coord, elem_restr_x, basis_x));

  // Jacobian evaluator:
  //   Create the QFunction and Operator that computes the action of the Jacobian for each linear solve
  {
    RatelDebug(ratel, "---- Setting up Jacobian evaluator");
    CeedQFunction qf_jacobian;
    CeedOperator  op_jacobian;

    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->jacobian, model_data->jacobian_loc, &qf_jacobian));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_jacobian, "qdata", q_data_size, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_jacobian, "delta du", num_comp_u * dim, CEED_EVAL_GRAD));
    if (ratel->method == RATEL_METHOD_DYNAMIC_ELASTICITY) {
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_jacobian, "delta u", num_comp_u, CEED_EVAL_INTERP));
    }
    for (CeedInt i = 0; i < model_data->number_fields_stored; i++) {
      // *INDENT-OFF*
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_jacobian, model_data->field_names[i], model_data->field_sizes[i], CEED_EVAL_NONE));
      // *INDENT-ON*
    }
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_jacobian, "delta dv", num_comp_u * dim, CEED_EVAL_GRAD));
    if (ratel->method == RATEL_METHOD_DYNAMIC_ELASTICITY) {
      RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_jacobian, "delta v", num_comp_u, CEED_EVAL_INTERP));
    }
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_jacobian, ctx_phys));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_jacobian, false));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_jacobian, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_jacobian));
    PetscCall(RatelSetMaterialOperatorName(ratel, material_index, "volumetric term", op_jacobian));
    RatelCeedCall(ratel, CeedOperatorSetField(op_jacobian, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, q_data));
    RatelCeedCall(ratel, CeedOperatorSetField(op_jacobian, "delta du", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_jacobian, "delta dv", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
    for (CeedInt i = 0; i < model_data->number_fields_stored; i++) {
      // *INDENT-OFF*
      RatelCeedCall(
          ratel, CeedOperatorSetField(op_jacobian, model_data->field_names[i], elem_restr_stored_fields[i], CEED_BASIS_COLLOCATED, stored_fields[i]));
      // *INDENT-ON*
    }
    if (ratel->method == RATEL_METHOD_DYNAMIC_ELASTICITY) {
      RatelCeedCall(ratel, CeedOperatorSetField(op_jacobian, "delta u", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_jacobian, "delta v", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
    }
    // -- FLOPs counting
    CeedSize flops = model_data->flops_qf_jacobian_u + model_data->flops_qf_jacobian_ut;
    if (ratel->method == RATEL_METHOD_DYNAMIC_ELASTICITY) flops += model_data->flops_qf_jacobian_utt;
    RatelCeedCall(ratel, CeedQFunctionSetUserFlopsEstimate(qf_jacobian, flops));
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ratel->op_jacobian[0], op_jacobian));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_jacobian, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_jacobian));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_jacobian));
  }

  // Local energy computation:
  //   Create the QFunction and Operator that computes the strain energy
  {
    RatelDebug(ratel, "---- Setting up local energy computation");
    CeedQFunction qf_energy;
    CeedOperator  op_energy;

    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->energy, model_data->energy_loc, &qf_energy));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_energy, "qdata", q_data_size, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_energy, "du", num_comp_u * dim, CEED_EVAL_GRAD));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_energy, "energy", num_comp_e, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_energy, ctx_phys));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_energy, false));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_energy, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_energy));
    PetscCall(RatelSetMaterialOperatorName(ratel, material_index, "volumetric term", op_energy));
    RatelCeedCall(ratel, CeedOperatorSetField(op_energy, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, q_data));
    RatelCeedCall(ratel, CeedOperatorSetField(op_energy, "du", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_energy, "energy", elem_restr_energy, basis_energy, CEED_VECTOR_ACTIVE));
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ratel->op_energy, op_energy));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_energy, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_energy));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_energy));
  }

  // Diagnostic value computation:
  //   Create the QFunction and Operator that computes nodal diagnostic quantities
  //    as well as QFunction and Operator for mass matrix to complete l2 projection
  {
    RatelDebug(ratel, "---- Setting up diagnostic value computation");
    CeedQFunctionContext ctx_mass;
    CeedQFunction        qf_mass, qf_diagnostic;
    CeedOperator         op_mass, op_diagnostic;

    // Mass matrix
    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, Mass, RatelQFunctionRelativePath(Mass_loc), &qf_mass));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_mass, "qdata", q_data_size, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_mass, "u", num_comp_diagnostic, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_mass, "v", num_comp_diagnostic, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionContextCreate(ceed, &ctx_mass));
    RatelCeedCall(ratel, CeedQFunctionContextSetData(ctx_mass, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(num_comp_diagnostic), &num_comp_diagnostic));
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_mass, ctx_mass));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_mass, false));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_mass, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_mass));
    PetscCall(RatelSetMaterialOperatorName(ratel, material_index, "volumetric term", op_mass));
    RatelCeedCall(ratel, CeedOperatorSetField(op_mass, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, q_data));
    RatelCeedCall(ratel, CeedOperatorSetField(op_mass, "u", elem_restr_diagnostic, basis_diagnostic, CEED_VECTOR_ACTIVE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_mass, "v", elem_restr_diagnostic, basis_diagnostic, CEED_VECTOR_ACTIVE));
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ratel->op_mass_diagnostic, op_mass));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_mass, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionContextDestroy(&ctx_mass));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_mass));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_mass));

    // Diagnostic quantities
    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->diagnostic, model_data->diagnostic_loc, &qf_diagnostic));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_diagnostic, "qdata", q_data_size, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_diagnostic, "u", num_comp_u, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_diagnostic, "du", num_comp_u * dim, CEED_EVAL_GRAD));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_diagnostic, "diagnostic", num_comp_diagnostic, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_diagnostic, ctx_phys));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_diagnostic, false));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_diagnostic, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_diagnostic));
    PetscCall(RatelSetMaterialOperatorName(ratel, material_index, "volumetric term", op_diagnostic));
    RatelCeedCall(ratel, CeedOperatorSetField(op_diagnostic, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, q_data));
    RatelCeedCall(ratel, CeedOperatorSetField(op_diagnostic, "u", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_diagnostic, "du", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_diagnostic, "diagnostic", elem_restr_diagnostic, basis_diagnostic, CEED_VECTOR_ACTIVE));
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ratel->op_diagnostic, op_diagnostic));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_diagnostic, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_diagnostic));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_diagnostic));
  }

  // Surface force computation:
  //   Create the QFunction and Operator that computes surface forces
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    RatelDebug(ratel, "---- Setting up surface %" PetscInt_FMT " force computation", ratel->surface_force_dm_faces[i]);
    CeedInt             num_elem, num_qpts, q_data_size = model_data->surface_force_q_data_size, num_comp_force = num_comp_u + dim;
    CeedVector          q_data;
    CeedBasis           basis_x_face, basis_x_cell_to_face, basis_u_face, basis_u_cell_to_face, basis_surface_force;
    CeedElemRestriction elem_restr_x_face, elem_restr_x_cell_to_face, elem_restr_u_face, elem_restr_u_cell_to_face, elem_restr_surface_force,
        elem_restr_q_data;
    CeedQFunction qf_setup_surface_force, qf_surface_force;
    CeedOperator  op_setup_surface_force, op_surface_force;

    // Coordinates
    DM dm_coord;
    PetscCall(DMGetCoordinateDM(ratel->dm_surface_displacement, &dm_coord));

    // Domain label for Face Sets
    if (!ratel->surface_force_face_label_names[material_index][i]) continue;
    DMLabel face_domain_label = NULL;
    PetscCall(DMGetLabel(ratel->dm_surface_displacement, ratel->surface_force_face_label_names[material_index][i], &face_domain_label));
    if (!face_domain_label) continue;

    // Loop over all domain values
    IS              is_face_domain_values;
    PetscInt        num_face_domain_values;
    const PetscInt *face_domain_values;

    PetscCall(DMLabelGetNonEmptyStratumValuesIS(face_domain_label, &is_face_domain_values));
    PetscCall(ISGetSize(is_face_domain_values, &num_face_domain_values));
    PetscCall(ISGetIndices(is_face_domain_values, &face_domain_values));
    for (PetscInt face_domain_index = 0; face_domain_index < num_face_domain_values; face_domain_index++) {
      PetscInt face_domain_value = face_domain_values[face_domain_index];
      RatelDebug(ratel, "------ Face orientation %" PetscInt_FMT, face_domain_value);

      // Check for face on process
      PetscInt height_face = 1, height_cell = 0;
      {
        PetscInt first_point;
        PetscInt ids[1] = {face_domain_value};

        PetscCall(DMGetFirstLabeledPoint(dm_coord, dm_coord, face_domain_label, 1, ids, height_cell, &first_point, NULL));
        if (first_point == -1) continue;
      }

      // Geometric factors setup
      RatelDebug(ratel, "-------- Setting up geometric factors");
      // -- Bases
      PetscCall(RatelBasisCreateFromPlex(ratel, dm_coord, face_domain_label, face_domain_value, height_face, 0, &basis_x_face));
      PetscCall(RatelOrientedBasisCreateCellToFaceFromPlex(ratel, dm_coord, face_domain_label, face_domain_value, face_domain_value, height_cell,
                                                           &basis_x_cell_to_face));
      RatelCeedCall(ratel, CeedBasisGetNumQuadraturePoints(basis_x_face, &num_qpts));
      // -- ElemRestrictions
      PetscCall(RatelRestrictionCreateFromPlex(ratel, dm_coord, face_domain_label, face_domain_value, height_face, 0, &elem_restr_x_face));
      PetscCall(RatelRestrictionCreateFromPlex(ratel, dm_coord, face_domain_label, face_domain_value, height_cell, 0, &elem_restr_x_cell_to_face));
      RatelCeedCall(ratel, CeedElemRestrictionGetNumElements(elem_restr_x_cell_to_face, &num_elem));
      RatelDebug(ratel,
                 "---------- Restriction, geometric data: %" CeedInt_FMT " elem with %" CeedInt_FMT " points and %" CeedInt_FMT
                 " fields for %" CeedInt_FMT " DoF",
                 num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size);
      RatelCeedCall(ratel, CeedElemRestrictionCreateStrided(ceed, num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size,
                                                            CEED_STRIDES_BACKEND, &elem_restr_q_data));
      // -- Vectors
      RatelCeedCall(ratel, CeedElemRestrictionCreateVector(elem_restr_q_data, &q_data, NULL));
      // -- QFunction
      RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->setup_surface_force_geo, model_data->setup_surface_force_geo_loc,
                                                       &qf_setup_surface_force));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup_surface_force, "dx cell", num_comp_u * (dim - height_cell), CEED_EVAL_GRAD));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup_surface_force, "dx face", num_comp_u * (dim - height_face), CEED_EVAL_GRAD));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup_surface_force, "weight", 1, CEED_EVAL_WEIGHT));
      RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_setup_surface_force, "qdata", q_data_size, CEED_EVAL_NONE));
      // -- Operator
      RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_setup_surface_force, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_setup_surface_force));
      PetscCall(RatelSetMaterialOperatorName(ratel, material_index, "surface force geometric data", op_setup_surface_force));
      RatelCeedCall(ratel,
                    CeedOperatorSetField(op_setup_surface_force, "dx cell", elem_restr_x_cell_to_face, basis_x_cell_to_face, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup_surface_force, "dx face", elem_restr_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup_surface_force, "weight", CEED_ELEMRESTRICTION_NONE, basis_x_face, CEED_VECTOR_NONE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup_surface_force, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, CEED_VECTOR_ACTIVE));
      // -- Compute values
      RatelCeedCall(ratel, CeedOperatorApply(op_setup_surface_force, x_coord, q_data, CEED_REQUEST_IMMEDIATE));

      // Diagnostic quantities
      RatelDebug(ratel, "-------- Setting up surface force operator");
      // -- Bases
      PetscCall(RatelBasisCreateFromPlex(ratel, ratel->dm_surface_displacement, face_domain_label, face_domain_value, height_face, 0, &basis_u_face));
      PetscCall(RatelOrientedBasisCreateCellToFaceFromPlex(ratel, ratel->dm_surface_displacement, face_domain_label, face_domain_value,
                                                           face_domain_value, height_cell, &basis_u_cell_to_face));
      PetscCall(RatelBasisCreateFromPlex(ratel, ratel->dm_surface_force, face_domain_label, face_domain_value, height_face, 0, &basis_surface_force));
      // -- ElemRestriction
      PetscCall(RatelRestrictionCreateFromPlex(ratel, ratel->dm_surface_displacement, face_domain_label, face_domain_value, height_face, 0,
                                               &elem_restr_u_face));
      PetscCall(RatelRestrictionCreateFromPlex(ratel, ratel->dm_surface_displacement, face_domain_label, face_domain_value, height_cell, 0,
                                               &elem_restr_u_cell_to_face));
      PetscCall(RatelRestrictionCreateFromPlex(ratel, ratel->dm_surface_force, face_domain_label, face_domain_value, height_face, 0,
                                               &elem_restr_surface_force));
      // -- QFunction
      RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->surface_force, model_data->surface_force_loc, &qf_surface_force));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_surface_force, "qdata", q_data_size, CEED_EVAL_NONE));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_surface_force, "u", num_comp_u, CEED_EVAL_INTERP));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_surface_force, "du", num_comp_u * (dim - height_cell), CEED_EVAL_GRAD));
      RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_surface_force, "diagnostic", num_comp_force, CEED_EVAL_INTERP));
      RatelCeedCall(ratel, CeedQFunctionSetContext(qf_surface_force, ctx_phys));
      RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_surface_force, false));
      // -- Operator
      RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_surface_force, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_surface_force));
      {
        char operator_name[30];

        PetscCall(
            PetscSNPrintf(operator_name, sizeof operator_name, "surface %" PetscInt_FMT " force computation", ratel->surface_force_dm_faces[i]));
        PetscCall(RatelSetMaterialOperatorName(ratel, material_index, operator_name, op_surface_force));
      }
      RatelCeedCall(ratel, CeedOperatorSetField(op_surface_force, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, q_data));
      RatelCeedCall(ratel, CeedOperatorSetField(op_surface_force, "u", elem_restr_u_face, basis_u_face, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_surface_force, "du", elem_restr_u_cell_to_face, basis_u_cell_to_face, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_surface_force, "diagnostic", elem_restr_surface_force, basis_surface_force, CEED_VECTOR_ACTIVE));
      // -- Add to composite operator
      RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ratel->op_surface_force[i], op_surface_force));
      if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_surface_force, stdout));

      // -- Cleanup
      RatelCeedCall(ratel, CeedVectorDestroy(&q_data));
      RatelCeedCall(ratel, CeedBasisDestroy(&basis_x_face));
      RatelCeedCall(ratel, CeedBasisDestroy(&basis_x_cell_to_face));
      RatelCeedCall(ratel, CeedBasisDestroy(&basis_u_face));
      RatelCeedCall(ratel, CeedBasisDestroy(&basis_u_cell_to_face));
      RatelCeedCall(ratel, CeedBasisDestroy(&basis_surface_force));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_x_face));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_x_cell_to_face));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_u_face));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_u_cell_to_face));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_surface_force));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_q_data));
      RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_setup_surface_force));
      RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_surface_force));
      RatelCeedCall(ratel, CeedOperatorDestroy(&op_setup_surface_force));
      RatelCeedCall(ratel, CeedOperatorDestroy(&op_surface_force));
    }
    PetscCall(ISRestoreIndices(is_face_domain_values, &face_domain_values));
    PetscCall(ISDestroy(&is_face_domain_values));
  }

  // Error calculation, for MMS:
  //   Create the QFunction and Operator that computes the l2 error from the true solution for validation
  if (model_data->error) {
    // *INDENT-OFF*
    RatelDebug(ratel, "---- Setting up error computation for MMS");
    CeedQFunction qf_error;
    CeedOperator  op_error;

    // *INDENT-ON*
    // -- QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->error, model_data->error_loc, &qf_error));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_error, "qdata", q_data_size, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_error, "x", num_comp_x, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_error, "u", num_comp_u, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_error, "l2 error", num_comp_u, CEED_EVAL_INTERP));
    // -- Operator
    RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_error, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_error));
    PetscCall(RatelSetMaterialOperatorName(ratel, material_index, "volumetric term", op_error));
    RatelCeedCall(ratel, CeedOperatorSetField(op_error, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, q_data));
    RatelCeedCall(ratel, CeedOperatorSetField(op_error, "x", elem_restr_x, basis_x, x_coord));
    RatelCeedCall(ratel, CeedOperatorSetField(op_error, "u", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
    RatelCeedCall(ratel, CeedOperatorSetField(op_error, "l2 error", elem_restr_u, basis_u, CEED_VECTOR_ACTIVE));
    // -- Add to composite operator
    RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ratel->op_error, op_error));
    if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_error, stdout));
    // -- Cleanup
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_error));
    RatelCeedCall(ratel, CeedOperatorDestroy(&op_error));
    ratel->has_mms = PETSC_TRUE;
  }

  // Cleanup
  RatelDebug(ratel, "---- Cleaning up libCEED objects");
  RatelCeedCall(ratel, CeedVectorDestroy(&x_coord));
  RatelCeedCall(ratel, CeedVectorDestroy(&q_data));
  for (PetscInt i = 0; i < model_data->number_fields_stored; i++) {
    // *INDENT-OFF*
    RatelCeedCall(ratel, CeedVectorDestroy(&stored_fields[i]));
    // *INDENT-ON*
  }
  RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_x));
  RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_u));
  RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_energy));
  RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_diagnostic));
  RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_q_data));
  for (PetscInt i = 0; i < model_data->number_fields_stored; i++) {
    // *INDENT-OFF*
    RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_stored_fields[i]));
    // *INDENT-ON*
  }
  RatelCeedCall(ratel, CeedBasisDestroy(&basis_x));
  RatelCeedCall(ratel, CeedBasisDestroy(&basis_u));
  RatelCeedCall(ratel, CeedBasisDestroy(&basis_energy));
  RatelCeedCall(ratel, CeedBasisDestroy(&basis_diagnostic));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Residual and Jacobian Evaluators for Elasticity Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup DM for fine grid elasticity operator

  @param[in]   ratel  Ratel context
  @param[out]  dm     Address of DM to create and setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMCreate_FEM(Ratel ratel, DM *dm) {
  CeedMemType mem_type_backend = CEED_MEM_HOST;
  VecType     vec_type         = VECSTANDARD;
  PetscInt    num_comp_u = 3, num_comp_e = 1, num_comp_diagnostic = 15, num_comp_force = 6;
  PetscInt    X_loc_size;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Create DMPlex for Elasticity");

  // Get VecType
  RatelCeedCall(ratel, CeedGetPreferredMemType(ratel->ceed, &mem_type_backend));
  RatelDebug(ratel, "---- Memory type: %s", CeedMemTypes[mem_type_backend]);
  switch (mem_type_backend) {
    case CEED_MEM_HOST:
      vec_type = VECSTANDARD;
      break;
    case CEED_MEM_DEVICE: {
      const char *resolved;
      RatelCeedCall(ratel, CeedGetResource(ratel->ceed, &resolved));
      if (strstr(resolved, "/gpu/cuda")) vec_type = VECCUDA;
      else if (strstr(resolved, "/gpu/hip")) vec_type = VECKOKKOS;
      else vec_type = VECSTANDARD;
    }
  }
  ratel->is_ceed_backend_gpu = mem_type_backend == CEED_MEM_DEVICE;

  // Determine num_comp_u (TODO: separate num_comp_u for separate materials)
  for (PetscInt material_index = 0; material_index < ratel->num_materials; material_index++) {
    RatelModelData model_data;

    PetscCall(RatelGetModelData(ratel, ratel->material_models[material_index], &model_data));
    num_comp_u = model_data->num_comp_u;
  }

  // Setup DM
  // -- Parallel distributed DM
  PetscCall(RatelDMCreateFromOptions(ratel, vec_type, &ratel->dm_orig));
  {
    PetscInt cStart, cEnd, count;
    PetscCall(DMPlexGetHeightStratum(ratel->dm_orig, 0, &cStart, &cEnd));
    count = cEnd - cStart;
    PetscCall(MPI_Allreduce(MPI_IN_PLACE, &count, 1, MPIU_INT, MPI_SUM, ratel->comm));
    PetscCheck(count >= 1, ratel->comm, PETSC_ERR_USER, "Cannot use DM with %" PetscInt_FMT " cells; check -dm_plex_filename or -dm_plex_box_faces",
               count);
  }
  PetscCall(DMGetVecType(ratel->dm_orig, &vec_type));
  // -- Fine grid residual and Jacobian DM
  RatelDebug(ratel, "---- Residual and Jacobian DM");
  PetscCall(PetscCalloc1(1, &ratel->dm_hierarchy));
  PetscCall(DMClone(ratel->dm_orig, &ratel->dm_hierarchy[0]));
  PetscCall(RatelSetupDMByOrder_FEM(ratel, true, ratel->multigrid_fine_order, RATEL_DECIDE, num_comp_u, RATEL_DECIDE, ratel->dm_hierarchy[0]));
  PetscCall(DMSetVecType(ratel->dm_hierarchy[0], vec_type));
  {
    MatType   mat_type = MATSHELL;
    char      mat_type_cl[25];
    PetscBool is_mat_type_cl = PETSC_FALSE;

    if (ratel->multigrid_fine_order == ratel->multigrid_coarse_order) {
      if (strstr(vec_type, VECCUDA)) mat_type = MATAIJCUSPARSE;
      else if (strstr(vec_type, VECKOKKOS)) mat_type = MATAIJKOKKOS;
      else mat_type = MATAIJ;
    }

    PetscOptionsBegin(ratel->comm, NULL, "", NULL);
    PetscCall(PetscOptionsString("-fine_dm_mat_type", "fine grid DM MatType", NULL, mat_type_cl, mat_type_cl, sizeof(mat_type_cl), &is_mat_type_cl));
    PetscOptionsEnd();

    PetscCall(DMSetMatType(ratel->dm_hierarchy[0], is_mat_type_cl ? mat_type_cl : mat_type));
    PetscCall(DMSetMatrixPreallocateSkip(ratel->dm_hierarchy[0], PETSC_TRUE));
    PetscCall(DMSetOptionsPrefix(ratel->dm_hierarchy[0], "fine_"));

    PetscCall(DMGetMatType(ratel->dm_hierarchy[0], &mat_type));
    RatelDebug(ratel, "------ Fine DM MatType: %s", mat_type);
  }

  {
    // -- Label field components for viewing
    //      Empty name for conserved field (because there is only one field)
    PetscSection section;
    PetscCall(DMGetLocalSection(ratel->dm_hierarchy[0], &section));
    PetscCall(PetscSectionSetFieldName(section, 0, ""));
    PetscCall(PetscSectionSetComponentName(section, 0, 0, "displacement_x"));
    PetscCall(PetscSectionSetComponentName(section, 0, 1, "displacement_y"));
    PetscCall(PetscSectionSetComponentName(section, 0, 2, "displacement_z"));
  }

  // -- Add labels on DM faces for FE face numbers
  for (PetscInt material_index = 0; material_index < ratel->num_materials; material_index++) {
    for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
      PetscCall(RatelDMCreateFaceLabel(ratel, ratel->dm_hierarchy[0], material_index, ratel->surface_force_dm_faces[i],
                                       &ratel->surface_force_face_label_names[material_index][i]));
    }
  }
  {
    // -- Copy label to coordinate DM
    DM dm_coord;

    PetscCall(DMGetCoordinateDM(ratel->dm_hierarchy[0], &dm_coord));
    PetscCall(DMCopyLabels(ratel->dm_hierarchy[0], dm_coord, PETSC_OWN_POINTER, PETSC_FALSE, DM_COPY_LABELS_KEEP));
  }

  // -- Set Ratel as DM application context
  PetscCall(PetscObjectReference((PetscObject)ratel->dm_hierarchy[0]));
  PetscCall(DMSetApplicationContext(ratel->dm_hierarchy[0], ratel));
  *dm = ratel->dm_hierarchy[0];
  PetscCall(DMSetOptionsPrefix(*dm, "final_"));
  PetscCall(DMViewFromOptions(*dm, NULL, "-dm_view"));

  // -- Energy and diagnostic information DMs
  RatelDebug(ratel, "---- Energy DM");
  PetscCall(DMClone(ratel->dm_orig, &ratel->dm_energy));
  PetscCall(RatelSetupDMByOrder_FEM(ratel, false, ratel->multigrid_fine_order, RATEL_DECIDE, num_comp_e, RATEL_DECIDE, ratel->dm_energy));
  PetscCall(DMSetVecType(ratel->dm_energy, vec_type));
  RatelDebug(ratel, "---- Diagnostic information DM");
  PetscCall(DMClone(ratel->dm_orig, &ratel->dm_diagnostic));
  PetscCall(RatelSetupDMByOrder_FEM(ratel, false, ratel->diagnostic_order, RATEL_DECIDE, num_comp_diagnostic, ratel->diagnostic_geometry_order,
                                    ratel->dm_diagnostic));
  PetscCall(DMSetVecType(ratel->dm_diagnostic, vec_type));
  {
    // -- Label field components for viewing
    //      Empty name for conserved field (because there is only one field)
    PetscSection section;
    PetscCall(DMGetLocalSection(ratel->dm_diagnostic, &section));
    PetscCall(PetscSectionSetFieldName(section, 0, ""));
    PetscCall(PetscSectionSetComponentName(section, 0, 0, "displacement_x"));
    PetscCall(PetscSectionSetComponentName(section, 0, 1, "displacement_y"));
    PetscCall(PetscSectionSetComponentName(section, 0, 2, "displacement_z"));
    PetscCall(PetscSectionSetComponentName(section, 0, 3, "Cauchy_stress_xx"));
    PetscCall(PetscSectionSetComponentName(section, 0, 4, "Cauchy_stress_xy"));
    PetscCall(PetscSectionSetComponentName(section, 0, 5, "Cauchy_stress_xz"));
    PetscCall(PetscSectionSetComponentName(section, 0, 6, "Cauchy_stress_yy"));
    PetscCall(PetscSectionSetComponentName(section, 0, 7, "Cauchy_stress_yz"));
    PetscCall(PetscSectionSetComponentName(section, 0, 8, "Cauchy_stress_zz"));
    PetscCall(PetscSectionSetComponentName(section, 0, 9, "pressure"));
    PetscCall(PetscSectionSetComponentName(section, 0, 10, "volumetric_strain"));
    PetscCall(PetscSectionSetComponentName(section, 0, 11, "trace_E2"));
    PetscCall(PetscSectionSetComponentName(section, 0, 12, "det_J"));
    PetscCall(PetscSectionSetComponentName(section, 0, 13, "strain_energy_density"));
    PetscCall(PetscSectionSetComponentName(section, 0, 14, "von_Mises_stress"));
  }
  RatelDebug(ratel, "---- Surface force DM");
  PetscCall(DMClone(ratel->dm_orig, &ratel->dm_surface_force));
  PetscCall(RatelSetupDMByOrder_FEM(ratel, false, ratel->multigrid_fine_order, ratel->q_extra_surface_force, num_comp_force, RATEL_DECIDE,
                                    ratel->dm_surface_force));
  PetscCall(DMSetVecType(ratel->dm_surface_force, vec_type));
  RatelDebug(ratel, "---- Surface displacement DM");
  PetscCall(DMClone(ratel->dm_orig, &ratel->dm_surface_displacement));
  PetscCall(RatelSetupDMByOrder_FEM(ratel, false, ratel->multigrid_fine_order, ratel->q_extra_surface_force, num_comp_u, RATEL_DECIDE,
                                    ratel->dm_surface_displacement));
  PetscCall(DMSetVecType(ratel->dm_surface_displacement, vec_type));
  {
    // -- Copy labels to surface force DMs
    DM dm_coord;

    PetscCall(DMGetCoordinateDM(ratel->dm_surface_displacement, &dm_coord));
    PetscCall(DMCopyLabels(ratel->dm_hierarchy[0], ratel->dm_surface_displacement, PETSC_OWN_POINTER, PETSC_FALSE, DM_COPY_LABELS_KEEP));
    PetscCall(DMCopyLabels(ratel->dm_hierarchy[0], dm_coord, PETSC_OWN_POINTER, PETSC_FALSE, DM_COPY_LABELS_KEEP));
  }

  // -- Work vectors
  RatelDebug(ratel, "---- Setting up work vectors");
  ratel->num_multigrid_levels_setup = 1;
  PetscCall(PetscCalloc1(1, &ratel->X));
  PetscCall(DMCreateGlobalVector(ratel->dm_hierarchy[0], &ratel->X[0]));
  PetscCall(PetscCalloc1(1, &ratel->X_loc));
  PetscCall(DMCreateLocalVector(ratel->dm_hierarchy[0], &ratel->X_loc[0]));
  PetscCall(VecDuplicate(ratel->X_loc[0], &ratel->X_loc_residual_u));
  PetscCall(PetscCalloc1(1, &ratel->Y_loc));
  PetscCall(VecDuplicate(ratel->X_loc[0], &ratel->Y_loc[0]));
  PetscCall(PetscCalloc1(1, &ratel->x_loc_ceed));
  PetscCall(VecGetSize(ratel->X_loc[0], &X_loc_size));
  RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, X_loc_size, &ratel->x_loc_ceed[0]));
  PetscCall(PetscCalloc1(1, &ratel->y_loc_ceed));
  RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, X_loc_size, &ratel->y_loc_ceed[0]));

  // FLOPs counting
  PetscCall(PetscCalloc1(1, &ratel->flops_jacobian));

  // Get material model data, ctx objects
  // -- Physics contexts
  //      Setup physics context and wrap in libCEED object
  for (PetscInt material_index = 0; material_index < ratel->num_materials; material_index++) {
    const char *cl_argument = ratel->material_models[material_index];
    RatelDebug(ratel, "---- Setup material model: %s", cl_argument);
    PetscCall(RatelSetupModelPhysics(ratel, cl_argument, material_index, &ratel->material_ctx_phys[material_index]));
    PetscCall(RatelSetupModelSmootherPhysics(ratel, cl_argument, material_index, ratel->material_ctx_phys[material_index],
                                             &ratel->material_ctx_phys_smoother[material_index]));
  }

  // Jacobian event
  PetscCall(PetscClassIdRegister("libCEED", &ratel->libceed_class_id));
  PetscCall(PetscLogEventRegister("RatelJacobianApp", ratel->libceed_class_id, &ratel->event_jacobian));

  // libCEED operators
  RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ratel->op_dirichlet));
  RatelCeedCall(ratel, CeedOperatorSetName(ratel->op_dirichlet, "Dirichlet boundary conditions"));
  RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ratel->op_residual_u));
  RatelCeedCall(ratel, CeedOperatorSetName(ratel->op_residual_u, "Residual evaluator, u term"));
  RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ratel->op_residual_ut));
  RatelCeedCall(ratel, CeedOperatorSetName(ratel->op_residual_ut, "Residual evaluator, ut term"));
  RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ratel->op_residual_utt));
  RatelCeedCall(ratel, CeedOperatorSetName(ratel->op_residual_utt, "Residual evaluator, utt term"));
  PetscCall(PetscCalloc1(1, &ratel->op_jacobian));
  RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ratel->op_jacobian[0]));
  RatelCeedCall(ratel, CeedOperatorSetName(ratel->op_jacobian[0], "Jacobian"));
  RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ratel->op_energy));
  RatelCeedCall(ratel, CeedOperatorSetName(ratel->op_energy, "energy"));
  RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ratel->op_diagnostic));
  RatelCeedCall(ratel, CeedOperatorSetName(ratel->op_diagnostic, "diagnostic values"));
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    char operator_name[30];

    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ratel->op_surface_force[i]));
    PetscCall(PetscSNPrintf(operator_name, sizeof operator_name, "surface force on face %" PetscInt_FMT, ratel->surface_force_dm_faces[i]));
    RatelCeedCall(ratel, CeedOperatorSetName(ratel->op_surface_force[i], operator_name));
  }
  RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ratel->op_mass_diagnostic));
  RatelCeedCall(ratel, CeedOperatorSetName(ratel->op_mass_diagnostic, "mass operator for diagnostic value projection"));
  RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ratel->op_error));
  RatelCeedCall(ratel, CeedOperatorSetName(ratel->op_error, "error"));

  // --- libCEED sub-operators for volumetric terms
  PetscInt sub_operator_index = 0;
  for (PetscInt material_index = 0; material_index < ratel->num_materials; material_index++) {
    DMLabel domain_label;
    // check if label exists if not
    PetscBool has_label = PETSC_FALSE;
    PetscCall(DMHasLabel(ratel->dm_orig, ratel->material_label_name, &has_label));
    if (!has_label && ratel->num_materials == 1) {
      // num_materials == 1 create label containing all elements
      PetscCall(DMCreateLabel(ratel->dm_orig, ratel->material_label_name));
      PetscInt c_start, c_end;
      PetscCall(DMPlexGetHeightStratum(ratel->dm_hierarchy[0], 0, &c_start, &c_end));
      for (PetscInt c = c_start; c < c_end; c++) {
        PetscCall(DMSetLabelValue(ratel->dm_orig, ratel->material_label_name, c, ratel->material_label_values[material_index][0]));
      }
    } else if (!has_label && ratel->num_materials > 1) {
      // num_materials > 1 throw and error
      // LCOV_EXCL_START
      SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Domain label doesn't exist for multiple materials!");
      // LCOV_EXCL_STOP
    }

    // ---- Get material data
    RatelModelData model_data;
    PetscCall(RatelGetModelData(ratel, ratel->material_models[material_index], &model_data));

    // ---- Build libCEED operators
    {
      PetscInt height = 0, first_point;
      PetscInt ids[1] = {ratel->material_label_values[material_index][0]};

      PetscCall(DMGetLabel(ratel->dm_orig, ratel->material_label_name, &domain_label));
      PetscCall(DMGetFirstLabeledPoint(ratel->dm_hierarchy[0], ratel->dm_hierarchy[0], domain_label, 1, ids, height, &first_point, NULL));
      // Add suboperators if material is present on this process
      if (first_point == -1) {
        ratel->material_sub_operator_index[material_index] = -1;
      } else {
        ratel->material_sub_operator_index[material_index] = sub_operator_index++;
        PetscCall(RatelSetupResidualJacobian_FEM(ratel, model_data, domain_label, material_index));
      }
    }
  }
  RatelCeedCall(ratel, CeedOperatorSetQFunctionAssemblyReuse(ratel->op_jacobian[0], true));

  // ---- Neumann boundaries
  RatelDebug(ratel, "---- Neumann boundaries");
  PetscCall(RatelCeedAddBoundariesNeumann(ratel, ratel->dm_hierarchy[0]));

  // ---- Dirichlet boundaries
  RatelDebug(ratel, "---- Dirichlet boundaries");
  {
    Vec boundary_mask;

    PetscCall(DMGetNamedLocalVector(ratel->dm_hierarchy[0], "boundary mask", &boundary_mask));
    PetscCall(VecZeroEntries(boundary_mask));
    PetscCall(VecSet(ratel->X[0], 1.0));
    PetscCall(DMGlobalToLocal(ratel->dm_hierarchy[0], ratel->X[0], INSERT_VALUES, boundary_mask));
    PetscCall(DMRestoreNamedLocalVector(ratel->dm_hierarchy[0], "boundary mask", &boundary_mask));
  }
  PetscCall(RatelCeedAddBoundariesDirichletClamp(ratel, ratel->dm_hierarchy[0]));

  // ---- Surface force face centroids
  RatelDebug(ratel, "---- Face centroids");
  PetscCall(RatelSetupSurfaceForceCentroids(ratel, ratel->dm_hierarchy[0]));

  // Debugging information
  if (ratel->is_debug) {
    // LCOV_EXCL_START
    RatelDebug(ratel, "---- Final residual operator");
    RatelCeedCall(ratel, CeedOperatorView(ratel->op_residual_u, stdout));
    if (ratel->method == RATEL_METHOD_DYNAMIC_ELASTICITY) {
      RatelDebug(ratel, "---- Final scaled mass operator for residual");
      RatelCeedCall(ratel, CeedOperatorView(ratel->op_residual_utt, stdout));
    }
    RatelDebug(ratel, "---- Final Jacobian operator");
    RatelCeedCall(ratel, CeedOperatorView(ratel->op_jacobian[0], stdout));
    // LCOV_EXCL_STOP
  }

  // FLOPs counting
  {
    CeedSize ceed_flops_estimate = 0;

    RatelCeedCall(ratel, CeedOperatorGetFlopsEstimate(ratel->op_jacobian[0], &ceed_flops_estimate));
    ratel->flops_jacobian[0] = ceed_flops_estimate;
  }

  // Get context labels
  {
    RatelCeedCall(ratel, CeedOperatorContextGetFieldLabel(ratel->op_dirichlet, "time", &ratel->op_dirichlet_time_label));
    RatelCeedCall(ratel, CeedOperatorContextGetFieldLabel(ratel->op_residual_u, "time", &ratel->op_residual_u_time_label));
    RatelCeedCall(ratel, CeedOperatorContextGetFieldLabel(ratel->op_jacobian[0], "time", &ratel->op_jacobian_time_label));
    RatelCeedCall(ratel, CeedOperatorContextGetFieldLabel(ratel->op_jacobian[0], "shift v", &ratel->op_jacobian_shift_v_label));
    RatelCeedCall(ratel, CeedOperatorContextGetFieldLabel(ratel->op_jacobian[0], "shift a", &ratel->op_jacobian_shift_a_label));
  }

  // Setup residual function, Jacobian
  RatelDebug(ratel, "---- Creating contexts");
  // -- IResidual
  PetscCall(PetscCalloc1(1, &ratel->ctx_residual_u));
  PetscCall(RatelSetupResidualUCtx_FEM(ratel, ratel->ctx_residual_u));
  PetscCall(PetscCalloc1(1, &ratel->ctx_residual_ut));
  PetscCall(RatelSetupResidualUtCtx_FEM(ratel, ratel->ctx_residual_ut));
  PetscCall(PetscCalloc1(1, &ratel->ctx_residual_utt));
  PetscCall(RatelSetupResidualUttCtx_FEM(ratel, ratel->ctx_residual_utt));
  // -- Jacobian
  PetscCall(PetscCalloc1(1, &ratel->ctx_jacobian));
  PetscCall(PetscCalloc1(1, &ratel->ctx_jacobian[0]));
  PetscCall(PetscCalloc1(1, &ratel->ctx_form_jacobian));
  PetscCall(RatelSetupJacobianCtx_FEM(ratel, 0, ratel->ctx_jacobian[0]));
  PetscCall(RatelSetupFormJacobianCtx_FEM(ratel, ratel->ctx_form_jacobian));
  PetscCall(PetscCalloc1(1, &ratel->mat_jacobian));
  {
    PetscInt X_l_size, X_g_size;
    PetscCall(VecGetSize(ratel->X[0], &X_g_size));
    PetscCall(VecGetLocalSize(ratel->X[0], &X_l_size));
    PetscCall(MatCreateShell(ratel->comm, X_l_size, X_l_size, X_g_size, X_g_size, ratel->ctx_jacobian[0], &ratel->mat_jacobian[0]));
    PetscCall(MatShellSetOperation(ratel->mat_jacobian[0], MATOP_MULT, (void (*)(void))RatelApplyJacobian));
    PetscCall(MatShellSetOperation(ratel->mat_jacobian[0], MATOP_GET_DIAGONAL, (void (*)(void))RatelGetDiagonal));
    PetscCall(MatShellSetVecType(ratel->mat_jacobian[0], vec_type));
  }

  // Initial boundary values for residual evaluation
  PetscCall(RatelUpdateBoundaryValues(ratel, 1.0));

  switch (ratel->method) {
    case RATEL_METHOD_STATIC_ELASTICITY:
      RatelDebug(ratel, "---- Setting SNES contexts");
      // -- SNES Function
      PetscCall(DMSNESSetFunction(ratel->dm_hierarchy[0], RatelSNESFormResidual, ratel->ctx_residual_u));
      // -- SNES Jacobian
      PetscCall(DMSNESSetJacobian(ratel->dm_hierarchy[0], RatelSNESFormJacobian, ratel->ctx_form_jacobian));
      break;
    case RATEL_METHOD_QUASISTATIC_ELASTICITY:
      RatelDebug(ratel, "---- Setting TS contexts");
      // -- TS IFunction
      PetscCall(DMTSSetIFunction(ratel->dm_hierarchy[0], RatelTSFormIResidual, ratel->ctx_residual_ut));
      // -- TS IJacobian
      PetscCall(DMTSSetIJacobian(ratel->dm_hierarchy[0], RatelTSFormIJacobian, ratel->ctx_form_jacobian));
      break;
    case RATEL_METHOD_DYNAMIC_ELASTICITY:
      // -- TS I2Function
      PetscCall(DMTSSetI2Function(ratel->dm_hierarchy[0], RatelTSFormI2Residual, ratel->ctx_residual_utt));
      // -- TS IJacobian
      PetscCall(DMTSSetI2Jacobian(ratel->dm_hierarchy[0], RatelTSFormI2Jacobian, ratel->ctx_form_jacobian));
      break;
  }

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Create DMPlex for Elasticity Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup libCEED operators for multigrid prolongation, restriction, and coarse
           grid Jacobian evaluation

  @param[in,out]  ratel           Ratel context
  @param[in]      model_data      Data and functions for material model
  @param[in]      level           Index of level in multigrid hierarchy
  @param[in]      material_index  Index of material for sub-operator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupMultigridLevel_FEM(Ratel ratel, RatelModelData model_data, PetscInt level, PetscInt material_index) {
  PetscInt            dim, X_loc_size, sub_operator_index = ratel->material_sub_operator_index[material_index];
  CeedElemRestriction elem_restr_u;
  CeedBasis           basis_u;
  CeedOperator       *sub_operators, op_jacobian, op_prolong, op_restrict;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Multigrid Level for Elasticity");
  RatelDebug(ratel, "---- Level: %" PetscInt_FMT, level);

  PetscCall(DMGetDimension(ratel->dm_hierarchy[level], &dim));

  DMLabel domain_label;
  PetscCall(DMGetLabel(ratel->dm_orig, ratel->material_label_name, &domain_label));
  PetscInt domain_value = ratel->material_label_values[material_index][0];  // TODO: fix this to handle multiple values

  // Vector size
  PetscCall(VecGetSize(ratel->X_loc[level], &X_loc_size));

  // libCEED restrictions
  // -- Solution restriction
  RatelDebug(ratel, "---- Setting up libCEED restriction for level");
  PetscCall(RatelRestrictionCreateFromPlex(ratel, ratel->dm_hierarchy[level], domain_label, domain_value, 0, 0, &elem_restr_u));

  // libCEED bases
  // -- Solution basis
  RatelDebug(ratel, "---- Setting up libCEED basis for level");
  PetscCall(RatelBasisCreateFromPlex(ratel, ratel->dm_hierarchy[level], domain_label, domain_value, 0, 0, &basis_u));

  // libCEED vectors
  RatelDebug(ratel, "---- Setting up libCEED vectors for level");
  if (!ratel->x_loc_ceed[level]) {
    RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, X_loc_size, &ratel->x_loc_ceed[level]));
    RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, X_loc_size, &ratel->y_loc_ceed[level]));
  }

  // Coarse Grid, Prolongation, and Restriction Operators:
  //   Create the Operators that compute the prolongation and restriction between the p-multigrid levels and the coarse grid eval
  // -- Create coarse grid, prolongation, and restriction operators
  RatelDebug(ratel, "---- Setting up level Jacobian, prolongation, and restriction operators");
  // ---- Get multiplicity
  const PetscScalar *m;
  PetscMemType       m_mem_type;
  PetscCall(VecGetArrayReadAndMemType(ratel->X_loc[level + 1], &m, &m_mem_type));
  RatelCeedCall(ratel, CeedVectorSetArray(ratel->x_loc_ceed[level + 1], RatelMemTypeP2C(m_mem_type), CEED_USE_POINTER, (CeedScalar *)m));
  // ---- Create multigrid operators
  RatelCeedCall(ratel, CeedOperatorGetSubList(ratel->op_jacobian[level + 1], &sub_operators));
  RatelCeedCall(ratel, CeedOperatorMultigridLevelCreate(sub_operators[sub_operator_index], ratel->x_loc_ceed[level + 1], elem_restr_u, basis_u,
                                                        &op_jacobian, &op_prolong, &op_restrict));
  // ---- Restore PETSc vector
  RatelCeedCall(ratel, CeedVectorTakeArray(ratel->x_loc_ceed[level + 1], RatelMemTypeP2C(m_mem_type), (CeedScalar **)&m));
  PetscCall(VecRestoreArrayReadAndMemType(ratel->X_loc[level + 1], &m));

  // -- Add to composite operators
  RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ratel->op_jacobian[level], op_jacobian));
  RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ratel->op_prolong[level + 1], op_prolong));
  RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ratel->op_restrict[level + 1], op_restrict));

  // Cleanup
  if (ratel->is_debug) {
    // LCOV_EXCL_START
    RatelCeedCall(ratel, CeedOperatorView(op_jacobian, stdout));
    RatelCeedCall(ratel, CeedOperatorView(op_prolong, stdout));
    RatelCeedCall(ratel, CeedOperatorView(op_restrict, stdout));
    // LCOV_EXCL_STOP
  }
  RatelCeedCall(ratel, CeedBasisDestroy(&basis_u));
  RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_u));
  RatelCeedCall(ratel, CeedOperatorDestroy(&op_jacobian));
  RatelCeedCall(ratel, CeedOperatorDestroy(&op_prolong));
  RatelCeedCall(ratel, CeedOperatorDestroy(&op_restrict));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Multigrid Level for Elasticity Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup multigrid preconditioner from Ratel context for Elasticity problem

  @param[in]   ratel           Ratel context
  @param[in]   multigrid_type  Multigrid strategy
  @param[out]  ksp             KSP object with multigrid preconditioner

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelKSPSetupPCMG_FEM(Ratel ratel, RatelMultigridType multigrid_type, KSP ksp) {
  VecType  vec_type;
  PetscInt num_comp_u = 3, fine_level = ratel->num_multigrid_levels - 1;
  ;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel KSP Setup PCMG for Elasticity");

  // Determine num_comp_u (TODO: separate num_comp_u for separate materials)
  {
    // ---- Get material data
    CeedInt        material_index = 0;
    RatelModelData model_data;
    PetscCall(RatelGetModelData(ratel, ratel->material_models[material_index], &model_data));
    num_comp_u = model_data->num_comp_u;
  }

  // DMs and work vectors for each level
  // -- Realloc arrays
  RatelDebug(ratel, "---- Reallocating arrays");
  {
    // ---- DM
    DM dm = ratel->dm_hierarchy[0];
    PetscCall(PetscFree(ratel->dm_hierarchy));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->dm_hierarchy));
    ratel->dm_hierarchy[fine_level] = dm;
    // ---- Vectors
    Vec X                           = ratel->X[0];
    PetscCall(PetscFree(ratel->X));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->X));
    ratel->X[fine_level] = X;
    Vec X_loc            = ratel->X_loc[0];
    PetscCall(PetscFree(ratel->X_loc));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->X_loc));
    ratel->X_loc[fine_level] = X_loc;
    Vec Y_loc                = ratel->Y_loc[0];
    PetscCall(PetscFree(ratel->Y_loc));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->Y_loc));
    ratel->Y_loc[fine_level] = Y_loc;
    // ---- libCEED vectors
    CeedVector x_loc_ceed    = ratel->x_loc_ceed[0];
    PetscCall(PetscFree(ratel->x_loc_ceed));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->x_loc_ceed));
    ratel->x_loc_ceed[fine_level] = x_loc_ceed;
    CeedVector y_loc_ceed         = ratel->y_loc_ceed[0];
    PetscCall(PetscFree(ratel->y_loc_ceed));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->y_loc_ceed));
    ratel->y_loc_ceed[fine_level] = y_loc_ceed;
    // ---- FLOPs counting
    PetscLogDouble flops_jacobian = ratel->flops_jacobian[0];
    PetscCall(PetscFree(ratel->flops_jacobian));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->flops_jacobian));
    ratel->flops_jacobian[fine_level] = flops_jacobian;
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->event_prolong));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->flops_prolong));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->event_restrict));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->flops_restrict));
    // ---- libCEED operators
    CeedOperator op_jacobian = ratel->op_jacobian[0];
    PetscCall(PetscFree(ratel->op_jacobian));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->op_jacobian));
    ratel->op_jacobian[fine_level]         = op_jacobian;
    // ---- Operator contexts
    RatelOperatorApplyContext ctx_jacobian = ratel->ctx_jacobian[0];
    PetscCall(PetscFree(ratel->ctx_jacobian));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->ctx_jacobian));
    ratel->ctx_jacobian[fine_level] = ctx_jacobian;
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->ctx_prolong_restrict));
    // ---- MatShells
    Mat mat_jacobian = ratel->mat_jacobian[0];
    PetscCall(PetscFree(ratel->mat_jacobian));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->mat_jacobian));
    ratel->mat_jacobian[fine_level] = mat_jacobian;
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->mat_prolong_restrict));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->op_prolong));
    PetscCall(PetscCalloc1(ratel->num_multigrid_levels, &ratel->op_restrict));
  }

  // -- Setup DMs and work vectors
  RatelDebug(ratel, "---- Setup DMs and work vectors");
  PetscCall(DMGetVecType(ratel->dm_orig, &vec_type));
  for (CeedInt i = 0; i < ratel->num_multigrid_levels - 1; i++) {
    RatelDebug(ratel, "------ Multigrid level: %" CeedInt_FMT, i);
    // ---- DM
    PetscCall(DMClone(ratel->dm_orig, &ratel->dm_hierarchy[i]));
    PetscCall(RatelSetupDMByOrder_FEM(ratel, true, ratel->multigrid_level_orders[i], RATEL_DECIDE, num_comp_u, RATEL_DECIDE, ratel->dm_hierarchy[i]));
    PetscCall(DMSetVecType(ratel->dm_hierarchy[i], vec_type));
    // ---- Label field components for viewing
    //        Empty name for conserved field (because there is only one field)
    PetscSection section;
    PetscCall(DMGetLocalSection(ratel->dm_hierarchy[i], &section));
    PetscCall(PetscSectionSetFieldName(section, 0, "Displacement"));
    PetscCall(PetscSectionSetComponentName(section, 0, 0, "Displacement_X"));
    PetscCall(PetscSectionSetComponentName(section, 0, 1, "Displacement_Y"));
    PetscCall(PetscSectionSetComponentName(section, 0, 2, "Displacement_Z"));
    // ---- Work vectors
    PetscCall(DMCreateGlobalVector(ratel->dm_hierarchy[i], &ratel->X[i]));
    PetscCall(DMCreateLocalVector(ratel->dm_hierarchy[i], &ratel->X_loc[i]));
    PetscCall(VecDuplicate(ratel->X_loc[i], &ratel->Y_loc[i]));
  }

  // libCEED operators for each level
  RatelDebug(ratel, "---- Setup libCEED operators");
  for (CeedInt level = ratel->num_multigrid_levels - 2; level >= 0; level--) {
    RatelDebug(ratel, "------ Multigrid level: %" CeedInt_FMT, level);
    // -- Composite operators
    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ratel->op_jacobian[level]));
    RatelCeedCall(ratel, CeedOperatorSetName(ratel->op_jacobian[level], "Jacobian"));
    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ratel->op_prolong[level + 1]));
    RatelCeedCall(ratel, CeedOperatorSetName(ratel->op_prolong[level + 1], "prolongation"));
    RatelCeedCall(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ratel->op_restrict[level + 1]));
    RatelCeedCall(ratel, CeedOperatorSetName(ratel->op_restrict[level + 1], "restriction"));
    // -- Compute Multiplicity
    {
      RatelDebug(ratel, "-------- Compute multiplicity");
      PetscCall(VecZeroEntries(ratel->X[level + 1]));
      PetscCall(VecSet(ratel->X_loc[level + 1], 1.0));
      PetscCall(DMLocalToGlobal(ratel->dm_hierarchy[level + 1], ratel->X_loc[level + 1], ADD_VALUES, ratel->X[level + 1]));
      PetscCall(DMGlobalToLocal(ratel->dm_hierarchy[level + 1], ratel->X[level + 1], INSERT_VALUES, ratel->X_loc[level + 1]));
      PetscCall(VecZeroEntries(ratel->X[level + 1]));
    }
    // -- Sub-operators for volumetric terms
    for (PetscInt material_index = 0; material_index < ratel->num_materials; material_index++) {
      // ---- Get material model data
      RatelModelData model_data;
      PetscCall(RatelGetModelData(ratel, ratel->material_models[material_index], &model_data));

      // ---- Build libCEED operators
      if (ratel->material_sub_operator_index[material_index] != -1) PetscCall(RatelSetupMultigridLevel_FEM(ratel, model_data, level, material_index));
    }
    // -- Clean up multiplicity values in local vectors
    PetscCall(VecZeroEntries(ratel->X_loc[level + 1]));
    // -- FLOPs counting
    {
      CeedSize ceed_flops_estimate = 0;

      RatelCeedCall(ratel, CeedOperatorGetFlopsEstimate(ratel->op_jacobian[level], &ceed_flops_estimate));
      ratel->flops_jacobian[level] = ceed_flops_estimate;
      RatelCeedCall(ratel, CeedOperatorGetFlopsEstimate(ratel->op_prolong[level + 1], &ceed_flops_estimate));
      ratel->flops_prolong[level + 1] = ceed_flops_estimate;
      RatelCeedCall(ratel, CeedOperatorGetFlopsEstimate(ratel->op_restrict[level + 1], &ceed_flops_estimate));
      ratel->flops_restrict[level + 1] = ceed_flops_estimate;

      char event_name_prolong[25];
      PetscCall(PetscSNPrintf(event_name_prolong, sizeof(event_name_prolong), "RatelProlong %" CeedInt_FMT, level + 1));
      PetscCall(PetscLogEventRegister(event_name_prolong, ratel->libceed_class_id, &ratel->event_prolong[level + 1]));

      char event_name_restrict[26];
      PetscCall(PetscSNPrintf(event_name_restrict, sizeof(event_name_restrict), "RatelRestrict %" CeedInt_FMT, level + 1));
      PetscCall(PetscLogEventRegister(event_name_restrict, ratel->libceed_class_id, &ratel->event_restrict[level + 1]));
    }
  }

  // MatShells
  RatelDebug(ratel, "---- Setup MatShells");
  for (CeedInt i = 0; i < ratel->num_multigrid_levels; i++) {
    RatelDebug(ratel, "------ Multigrid level: %" CeedInt_FMT, i);
    PetscInt X_l_size, X_g_size;
    PetscCall(VecGetSize(ratel->X[i], &X_g_size));
    PetscCall(VecGetLocalSize(ratel->X[i], &X_l_size));

    // -- Jacobian
    if (i < ratel->num_multigrid_levels - 1) {
      RatelDebug(ratel, "------ Jacobian MatShell");
      PetscCall(PetscCalloc1(1, &ratel->ctx_jacobian[i]));
      PetscCall(RatelSetupJacobianCtx_FEM(ratel, i, ratel->ctx_jacobian[i]));
      PetscCall(MatCreateShell(ratel->comm, X_l_size, X_l_size, X_g_size, X_g_size, ratel->ctx_jacobian[i], &ratel->mat_jacobian[i]));
      PetscCall(MatSetOption(ratel->mat_jacobian[i], MAT_SPD, PETSC_TRUE));
      PetscCall(MatShellSetOperation(ratel->mat_jacobian[i], MATOP_MULT, (void (*)(void))RatelApplyJacobian));
      PetscCall(MatShellSetOperation(ratel->mat_jacobian[i], MATOP_GET_DIAGONAL, (void (*)(void))RatelGetDiagonal));
    }

    // -- Prolongation/Restriction
    if (i > 0) {
      RatelDebug(ratel, "------ Prolongation/Restriction MatShell");
      PetscInt X_l_size_coarse, X_g_size_coarse;
      PetscCall(VecGetSize(ratel->X[i - 1], &X_g_size_coarse));
      PetscCall(VecGetLocalSize(ratel->X[i - 1], &X_l_size_coarse));
      PetscCall(PetscCalloc1(1, &ratel->ctx_prolong_restrict[i]));
      PetscCall(SetupProlongRestrictCtx_FEM(ratel, i, ratel->ctx_prolong_restrict[i]));
      PetscCall(MatCreateShell(ratel->comm, X_l_size, X_l_size_coarse, X_g_size, X_g_size_coarse, ratel->ctx_prolong_restrict[i],
                               &ratel->mat_prolong_restrict[i]));
      PetscCall(MatShellSetOperation(ratel->mat_prolong_restrict[i], MATOP_MULT, (void (*)(void))RatelProlong));
      PetscCall(MatShellSetOperation(ratel->mat_prolong_restrict[i], MATOP_MULT_TRANSPOSE, (void (*)(void))RatelRestrict));
    }
  }

  // Flag number of multigrid levels setup
  ratel->num_multigrid_levels_setup = ratel->num_multigrid_levels;

  // Coarse Jacobian
  if (ratel->multigrid_type != RATEL_MULTIGRID_NONE) {
    // -- Coarse Mat
    {
      MatType   mat_type = MATAIJ;
      char      mat_type_cl[25];
      PetscBool is_mat_type_cl = PETSC_FALSE;

      if (strstr(vec_type, VECCUDA)) mat_type = MATAIJCUSPARSE;
      else if (strstr(vec_type, VECKOKKOS)) mat_type = MATAIJKOKKOS;
      else mat_type = MATAIJ;

      PetscOptionsBegin(ratel->comm, NULL, "", NULL);
      PetscCall(
          PetscOptionsString("-coarse_dm_mat_type", "coarse grid DM MatType", NULL, mat_type_cl, mat_type_cl, sizeof(mat_type_cl), &is_mat_type_cl));
      PetscOptionsEnd();

      PetscCall(DMSetMatType(ratel->dm_hierarchy[0], is_mat_type_cl ? mat_type_cl : mat_type));
      PetscCall(DMSetMatrixPreallocateSkip(ratel->dm_hierarchy[0], PETSC_TRUE));
      PetscCall(DMSetOptionsPrefix(ratel->dm_hierarchy[0], "coarse_"));

      PetscCall(DMGetMatType(ratel->dm_hierarchy[0], &mat_type));
      RatelDebug(ratel, "------ Coarse DM MatType: %s", mat_type);
      PetscCall(DMViewFromOptions(ratel->dm_hierarchy[0], NULL, "-dm_view"));
    }

    // -- Assemble sparsity pattern
    if (ratel->num_multigrid_levels > 1) {
      MatNullSpace near_null;
      PetscCall(DMCreateMatrix(ratel->dm_hierarchy[0], &ratel->mat_jacobian_coarse));
      PetscCall(MatSetOption(ratel->mat_jacobian_coarse, MAT_SPD, PETSC_TRUE));
      PetscCall(DMPlexCreateRigidBody(ratel->dm_hierarchy[0], 0, &near_null));
      PetscCall(MatSetNearNullSpace(ratel->mat_jacobian_coarse, near_null));
      PetscCall(MatNullSpaceDestroy(&near_null));
      PetscCall(RatelMatSetPreallocationCOO(ratel, ratel->op_jacobian[0], &ratel->coo_values_ceed, ratel->mat_jacobian_coarse));
    }

    // -- Update Jacobian context
    PetscCall(RatelSetupFormJacobianCtx_FEM(ratel, ratel->ctx_form_jacobian));
  }

  // PC setup
  RatelDebug(ratel, "---- Setup PCMG");
  PC pc;
  PetscCall(KSPSetType(ksp, KSPCG));
  PetscCall(KSPGetPC(ksp, &pc));
  if (multigrid_type == RATEL_MULTIGRID_NONE) {
    // ---- No multigrid
    RatelDebug(ratel, "------ No multigrid");
    PetscCall(PCJacobiSetType(pc, PC_JACOBI_DIAGONAL));
  } else if (multigrid_type == RATEL_MULTIGRID_AMG_ONLY) {
    // ---- AMG only
    RatelDebug(ratel, "------ AMG only");
    PetscCall(PCSetType(pc, PCGAMG));
  } else {
    // ---- P-multigrid with PCMG
    RatelDebug(ratel, "------ Full p-multigrid");
    PetscCall(PCSetType(pc, PCMG));
    PetscCall(PCMGSetLevels(pc, ratel->num_multigrid_levels, NULL));
    // ------ Loop over levels
    for (PetscInt i = 0; i < ratel->num_multigrid_levels; i++) {
      RatelDebug(ratel, "-------- Setup PCMG level: %" PetscInt_FMT, i);
      // -------- Smoother
      KSP ksp_smoother;
      PC  pc_smoother;
      // ---------- Smoother KSP
      PetscCall(PCMGGetSmoother(pc, i, &ksp_smoother));
      PetscCall(KSPSetDM(ksp_smoother, ratel->dm_hierarchy[i]));
      PetscCall(KSPSetDMActive(ksp_smoother, PETSC_FALSE));
      // ---------- Chebyshev options
      PetscCall(KSPSetType(ksp_smoother, KSPCHEBYSHEV));
      PetscCall(KSPChebyshevEstEigSet(ksp_smoother, 0, 0.1, 0, 1.1));
      PetscCall(KSPChebyshevEstEigSetUseNoisy(ksp_smoother, PETSC_TRUE));
      PetscCall(KSPSetOperators(ksp_smoother, ratel->mat_jacobian[i], ratel->mat_jacobian[i]));
      // ---------- Smoother preconditioner
      PetscCall(KSPGetPC(ksp_smoother, &pc_smoother));
      PetscCall(PCSetType(pc_smoother, PCJACOBI));
      PetscCall(PCJacobiSetType(pc_smoother, PC_JACOBI_DIAGONAL));
      // -------- Work vector
      if (i != fine_level) PetscCall(PCMGSetX(pc, i, ratel->X[i]));
      // -------- Level prolongation/restriction operator
      if (i > 0) {
        PetscCall(PCMGSetInterpolation(pc, i, ratel->mat_prolong_restrict[i]));
        PetscCall(PCMGSetRestriction(pc, i, ratel->mat_prolong_restrict[i]));
      }
    }
    // ------ PCMG coarse solve
    RatelDebug(ratel, "-------- Setup PCMG coarse solve");
    KSP ksp_coarse;
    PC  pc_coarse;
    // -------- Coarse KSP
    PetscCall(PCMGGetCoarseSolve(pc, &ksp_coarse));
    PetscCall(KSPSetType(ksp_coarse, KSPPREONLY));
    PetscCall(KSPSetOperators(ksp_coarse, ratel->mat_jacobian_coarse, ratel->mat_jacobian_coarse));
    // -------- Coarse preconditioner
    PetscCall(KSPGetPC(ksp_coarse, &pc_coarse));
    PetscCall(PCSetType(pc_coarse, PCGAMG));
    // ------ PCMG options
    PetscCall(PCMGSetType(pc, PC_MG_MULTIPLICATIVE));
    PetscCall(PCMGSetNumberSmooth(pc, 3));
    PetscCall(PCMGSetCycleType(pc, PC_MG_CYCLE_V));
  }

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel KSP Setup PCMG for Elasticity Success!");
  PetscFunctionReturn(0);
}

/// @}
