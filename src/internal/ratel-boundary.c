/// @file
/// Boundary condition functions for Ratel

#include <ceed.h>
#include <math.h>
#include <petsc.h>
#include <petscdmplex.h>
#include <ratel-boundary.h>
#include <ratel-dm.h>
#include <ratel-impl.h>
#include <ratel-petsc-ops.h>
#include <ratel.h>
#include <ratel/qfunctions/boundaries/clamp-boundary.h>
#include <ratel/qfunctions/boundaries/traction-boundary.h>
#include <ratel/qfunctions/mass.h>
#include <ratel/qfunctions/surface-geometry.h>
#include <ratel/qfunctions/utils.h>
#include <stdio.h>
#include <string.h>

/// @addtogroup RatelBoundary
/// @{

// -----------------------------------------------------------------------------
// Setup DM
// -----------------------------------------------------------------------------

/**
  @brief Create boundary label

  @param[in,out]  dm    DM to add boundary label
  @param[in]      name  Name for new boundary label

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCreateBCLabel(DM dm, const char name[]) {
  DMLabel label;

  PetscFunctionBeginUser;

  PetscCall(DMCreateLabel(dm, name));
  PetscCall(DMGetLabel(dm, name, &label));
  PetscCall(DMPlexMarkBoundaryFaces(dm, PETSC_DETERMINE, label));
  PetscCall(DMPlexLabelComplete(dm, label));

  PetscFunctionReturn(0);
};

/**
  @brief Add Dirichlet boundaries to DM

  @param[in]   ratel  Ratel context
  @param[out]  dm     DM to update with Dirichlet boundaries

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMAddBoundariesDirichlet(Ratel ratel, DM dm) {
  const char *name = "Face Sets";

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel DMPlex Add Boundaries Dirichlet");

  if (ratel->forcing_choice == RATEL_FORCING_MMS) {
    // BCs given by manufactured solution
    RatelDebug(ratel, "---- MMS boundaries for mesh");
    PetscBool   has_label;
    const char *name        = "MMS Face Sets";
    PetscInt    face_ids[1] = {1};
    PetscCall(DMHasLabel(dm, name, &has_label));
    if (!has_label) PetscCall(RatelCreateBCLabel(dm, name));
    DMLabel label;
    PetscCall(DMGetLabel(dm, name, &label));
    PetscCall(DMAddBoundary(dm, DM_BC_ESSENTIAL, "mms", label, 1, face_ids, 0, 0, NULL, (void (*)(void))RatelBoundaryMMS, NULL, NULL, NULL));
  } else {
    // User specified BCs
    RatelDebug(ratel, "---- Dirichlet boundaries for mesh");
    PetscBool has_label;
    PetscCall(DMHasLabel(dm, name, &has_label));
    if (!has_label) PetscCall(RatelCreateBCLabel(dm, name));
    DMLabel label;
    PetscCall(DMGetLabel(dm, name, &label));
    for (PetscInt i = 0; i < ratel->bc_clamp_count; i++) {
      char bcName[25];

      PetscCall(PetscSNPrintf(bcName, sizeof bcName, "clamp_%" PetscInt_FMT, ratel->bc_clamp_faces[i]));
      PetscCall(DMAddBoundary(dm, DM_BC_ESSENTIAL, bcName, label, 1, &ratel->bc_clamp_faces[i], 0, 0, NULL, NULL, NULL, NULL, NULL));
    }
  }

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel DMPlex Add Boundaries Dirichlet Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Add Slip boundaries to DM

  @param[in]   ratel  Ratel context
  @param[out]  dm     DM to update with Dirichlet boundaries

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMAddBoundariesSlip(Ratel ratel, DM dm) {
  DMLabel     label;
  PetscBool   has_label;
  const char *name = "Face Sets";

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel DMPlex Add Boundaries Slip");

  // Set slip BCs in each direction
  PetscCall(DMHasLabel(dm, name, &has_label));
  if (!has_label) PetscCall(RatelCreateBCLabel(dm, name));
  PetscCall(DMGetLabel(dm, name, &label));
  for (PetscInt i = 0; i < ratel->bc_slip_count; i++) {
    char bcName[25];

    PetscCall(PetscSNPrintf(bcName, sizeof bcName, "slip_%" PetscInt_FMT, ratel->bc_slip_faces[i]));
    PetscCall(DMAddBoundary(dm, DM_BC_ESSENTIAL, bcName, label, 1, &ratel->bc_slip_faces[i], 0, ratel->bc_slip_components_count[i],
                            ratel->bc_slip_components[i], (void (*)(void))NULL, NULL, NULL, NULL));
  }

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel DMPlex Add Boundaries Slip Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Add Dirichlet clamp boundaries to Dirichlet operator

  @param[in]  ratel  Ratel context
  @param[in]  dm     DM to use for Dirichlet boundaries

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCeedAddBoundariesDirichletClamp(Ratel ratel, DM dm) {
  Ceed                 ceed = ratel->ceed;
  CeedInt              Q    = ratel->multigrid_fine_order + 1 + ratel->q_extra;
  PetscInt             dim;
  CeedInt              num_comp_x, num_comp_u = 3, num_elem, elem_size;
  Vec                  coords;
  const PetscScalar   *coord_array;
  CeedVector           x_coord = NULL, multiplicity, x_stored, scale_stored;
  CeedBasis            basis_x_face, basis_u_face, basis_x_to_u_face;
  CeedElemRestriction  elem_restr_x_face, elem_restr_u_face, elem_restr_x_stored, elem_restr_scale;
  CeedQFunctionContext ctx_clamp;
  CeedQFunction        qf_setup, qf_clamp;
  CeedOperator         op_setup, op_clamp;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Ceed Add Boundaries Dirichlet Clamp");

  PetscCall(DMGetDimension(dm, &dim));
  num_comp_x = dim;

  if (ratel->bc_clamp_count > 0) {
    DMLabel domain_label;
    PetscCall(DMGetLabel(dm, "Face Sets", &domain_label));
    PetscCall(DMGetDimension(dm, &dim));

    // Basis
    RatelDebug(ratel, "---- Setting up libCEED bases");
    CeedInt height = 1;
    DM      dm_coord;
    PetscCall(DMGetCoordinateDM(dm, &dm_coord));
    PetscCall(RatelBasisCreateFromPlex(ratel, dm_coord, 0, 0, height, 0, &basis_x_face));
    PetscCall(RatelBasisCreateFromPlex(ratel, dm, 0, 0, height, 0, &basis_u_face));
    PetscCall(CeedBasisCreateProjection(basis_x_face, basis_u_face, &basis_x_to_u_face));

    // Setup QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, SetupClampBCs, RatelQFunctionRelativePath(SetupClampBCs_loc), &qf_setup));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup, "x", num_comp_x, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup, "multiplicity", num_comp_u, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_setup, "x stored", num_comp_x, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_setup, "scale", 1, CEED_EVAL_NONE));

    // Compute contribution on each boundary face
    for (CeedInt i = 0; i < ratel->bc_clamp_count; i++) {
      RatelDebug(ratel, "---- Setting up clamp boundary: %" CeedInt_FMT, i);

      // -- Set up context data
      BCClampData clamp_data = {
          .translation[0]         = 0.0,
          .translation[1]         = 0.0,
          .translation[2]         = 0.0,
          .rotation_axis[0]       = 0.0,
          .rotation_axis[1]       = 0.0,
          .rotation_axis[2]       = 0.0,
          .rotation_polynomial[0] = 0.0,
          .rotation_polynomial[1] = 0.0,
          .time                   = 1.0,
          .final_time             = 1.0,
      };

      // ---- Read CL options for clamp conditions
      PetscOptionsBegin(ratel->comm, NULL, "", NULL);

      // ------ Translation vector
      char option_name[25];

      PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_clamp_%" PetscInt_FMT "_translate", ratel->bc_clamp_faces[i]));
      PetscInt max_n = 3;
      PetscCall(PetscOptionsScalarArray(option_name, "Vector to translate clamped end by", NULL, clamp_data.translation, &max_n, NULL));

      // ------ Rotation vector
      max_n                   = 5;
      PetscScalar rotation[5] = {0, 0, 0, 0, 0};
      PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_clamp_%" PetscInt_FMT "_rotate", ratel->bc_clamp_faces[i]));
      PetscCall(PetscOptionsScalarArray(option_name, "Vector with axis of rotation and rotation, in radians", NULL, rotation, &max_n, NULL));

      // ------ Normalize
      CeedScalar norm_rotation = RatelNorm3((CeedScalar *)rotation);
      if (fabs(norm_rotation) < CEED_EPSILON) norm_rotation = 1;
      for (PetscInt j = 0; j < 3; j++) clamp_data.rotation_axis[j] = rotation[j] / norm_rotation;
      clamp_data.rotation_polynomial[0] = rotation[3];
      clamp_data.rotation_polynomial[1] = rotation[4];
      RatelDebug(ratel, "----   Clamp Boundary %" PetscInt_FMT ": [tx: %.3f, ty: %.3f, tz: %.3f, rx: %.3f, ry: %.3f, rz: %.3f, rt: %.3f]",
                 ratel->bc_clamp_faces[i], clamp_data.translation[0], clamp_data.translation[1], clamp_data.translation[2],
                 clamp_data.rotation_axis[0], clamp_data.rotation_axis[1], clamp_data.rotation_axis[2], clamp_data.rotation_polynomial[0]);

      clamp_data.final_time = 1.0;
      PetscCall(PetscOptionsScalar("-ts_max_time", "final time", NULL, clamp_data.final_time, &clamp_data.final_time, NULL));

      PetscOptionsEnd();  // End of setting clamp conditions

      // -- Skip if zero BC
      CeedScalar norm_translation = RatelNorm3(clamp_data.translation);
      CeedScalar norm_rotation    = RatelNorm3(clamp_data.rotation_axis);
      if (fabs(norm_translation) < CEED_EPSILON && fabs(norm_rotation) < CEED_EPSILON) {
        RatelDebug(ratel, "---- Zero Dirichlet clamp boundary, no operator needed");
        continue;
      }
      ratel->bc_clamp_nonzero_count++;

      // -- Restrictions
      RatelDebug(ratel, "---- Setting up libCEED restrictions");
      PetscCall(RatelGetRestrictionForDomain(ratel, dm, domain_label, ratel->bc_clamp_faces[i], height, 0, Q, 0, &elem_restr_u_face,
                                             &elem_restr_x_face, NULL));
      RatelCeedCall(ratel, CeedElemRestrictionCreateVector(elem_restr_u_face, &multiplicity, NULL));
      RatelCeedCall(ratel, CeedElemRestrictionGetMultiplicity(elem_restr_u_face, multiplicity));
      RatelCeedCall(ratel, CeedElemRestrictionGetNumElements(elem_restr_u_face, &num_elem));
      RatelCeedCall(ratel, CeedElemRestrictionGetElementSize(elem_restr_u_face, &elem_size));
      RatelDebug(ratel,
                 "---- Restriction, stored coordinates: %" CeedInt_FMT " elem with %" CeedInt_FMT " points and %" CeedInt_FMT
                 " fields for %" CeedInt_FMT " DoF",
                 num_elem, elem_size, num_comp_x, num_elem * elem_size * num_comp_x);
      RatelCeedCall(ratel, CeedElemRestrictionCreateStrided(ceed, num_elem, elem_size, num_comp_x, num_elem * elem_size * num_comp_x,
                                                            CEED_STRIDES_BACKEND, &elem_restr_x_stored));
      RatelCeedCall(ratel, CeedElemRestrictionCreateVector(elem_restr_x_stored, &x_stored, NULL));
      RatelCeedCall(ratel,
                    CeedElemRestrictionCreateStrided(ceed, num_elem, elem_size, 1, num_elem * elem_size, CEED_STRIDES_BACKEND, &elem_restr_scale));
      RatelCeedCall(ratel, CeedElemRestrictionCreateVector(elem_restr_scale, &scale_stored, NULL));

      // Coordinates vector
      if (!x_coord) {
        RatelDebug(ratel, "---- Retrieving element coordinates");
        PetscCall(DMGetCoordinatesLocal(dm, &coords));
        PetscCall(VecGetArrayRead(coords, &coord_array));
        RatelCeedCall(ratel, CeedElemRestrictionCreateVector(elem_restr_x_face, &x_coord, NULL));
        RatelCeedCall(ratel, CeedVectorSetArray(x_coord, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)coord_array));
        PetscCall(VecRestoreArrayRead(coords, &coord_array));
      }

      // -- Setup Operator
      RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_setup, NULL, NULL, &op_setup));
      RatelCeedCall(ratel, CeedOperatorSetName(op_setup, "surface geometric data"));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "x", elem_restr_x_face, basis_x_to_u_face, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "multiplicity", elem_restr_u_face, CEED_BASIS_COLLOCATED, multiplicity));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "x stored", elem_restr_x_stored, CEED_BASIS_COLLOCATED, x_stored));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "scale", elem_restr_scale, CEED_BASIS_COLLOCATED, scale_stored));

      // -- Compute geometric factors
      RatelDebug(ratel, "---- Projecting coordinates and computing multiplicity scaling factor");
      if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_setup, stdout));
      RatelCeedCall(ratel, CeedOperatorApply(op_setup, x_coord, CEED_VECTOR_NONE, CEED_REQUEST_IMMEDIATE));

      // -- Apply QFunction
      RatelDebug(ratel, "---- Setting up Dirichlet clamp operator");
      RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, ApplyClampBCs, RatelQFunctionRelativePath(ApplyClampBCs_loc), &qf_clamp));

      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_clamp, "x", num_comp_x, CEED_EVAL_NONE));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_clamp, "scale", 1, CEED_EVAL_NONE));
      RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_clamp, "u", num_comp_u, CEED_EVAL_NONE));
      RatelCeedCall(ratel, CeedQFunctionContextCreate(ceed, &ctx_clamp));
      RatelCeedCall(ratel, CeedQFunctionContextSetData(ctx_clamp, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(BCClampData), &clamp_data));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "translation x", offsetof(BCClampData, translation[0]), 1,
                                                              "translation in x direction"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "translation y", offsetof(BCClampData, translation[1]), 1,
                                                              "translation in y direction"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "translation z", offsetof(BCClampData, translation[2]), 1,
                                                              "translation in z direction"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "rotation x", offsetof(BCClampData, rotation_axis[0]), 1,
                                                              "x component for axis of rotation"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "rotation y", offsetof(BCClampData, rotation_axis[1]), 1,
                                                              "y component for axis of rotation"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "rotation z", offsetof(BCClampData, rotation_axis[2]), 1,
                                                              "z component for axis of rotation"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "rotation c_0", offsetof(BCClampData, rotation_polynomial[0]), 1,
                                                              "coefficient 0 for rotation polynomial"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "rotation c_1", offsetof(BCClampData, rotation_polynomial[1]), 1,
                                                              "coefficient 1 for rotation polynomial"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "time", offsetof(BCClampData, time), 1, "current solver time"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "final time", offsetof(BCClampData, time), 1, "final solver time"));
      RatelCeedCall(ratel, CeedQFunctionSetContext(qf_clamp, ctx_clamp));
      RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_clamp, false));
      if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(ctx_clamp, stdout));

      // -- Apply operator
      RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_clamp, NULL, NULL, &op_clamp));
      {
        char operator_name[25];

        PetscCall(PetscSNPrintf(operator_name, sizeof operator_name, "clamp - face %" PetscInt_FMT, ratel->bc_clamp_faces[i]));
        RatelCeedCall(ratel, CeedOperatorSetName(op_clamp, operator_name));
      }
      RatelCeedCall(ratel, CeedOperatorSetField(op_clamp, "x", elem_restr_x_stored, CEED_BASIS_COLLOCATED, x_stored));
      RatelCeedCall(ratel, CeedOperatorSetField(op_clamp, "scale", elem_restr_scale, CEED_BASIS_COLLOCATED, scale_stored));
      RatelCeedCall(ratel, CeedOperatorSetField(op_clamp, "u", elem_restr_u_face, CEED_BASIS_COLLOCATED, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetNumQuadraturePoints(op_clamp, elem_size));
      // -- Add to composite operator
      RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ratel->op_dirichlet, op_clamp));
      if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_clamp, stdout));

      // -- Cleanup
      RatelCeedCall(ratel, CeedVectorDestroy(&x_stored));
      RatelCeedCall(ratel, CeedVectorDestroy(&scale_stored));
      RatelCeedCall(ratel, CeedVectorDestroy(&multiplicity));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_x_face));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_u_face));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_x_stored));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_scale));
      RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_clamp));
      RatelCeedCall(ratel, CeedQFunctionContextDestroy(&ctx_clamp));
      RatelCeedCall(ratel, CeedOperatorDestroy(&op_setup));
      RatelCeedCall(ratel, CeedOperatorDestroy(&op_clamp));
    }
    // Cleanup
    RatelCeedCall(ratel, CeedVectorDestroy(&x_coord));
    RatelCeedCall(ratel, CeedBasisDestroy(&basis_x_face));
    RatelCeedCall(ratel, CeedBasisDestroy(&basis_u_face));
    RatelCeedCall(ratel, CeedBasisDestroy(&basis_x_to_u_face));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_setup));

    // Override DMPlexInsertBoundaryValues
    RatelDebug(ratel, "---- Overriding DMPlexInsertBoundaryValues_C");
    PetscCall(PetscObjectComposeFunction((PetscObject)dm, "DMPlexInsertBoundaryValues_C", RatelDMPlexInsertBoundaryValues));
  }

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Ceed Add Boundaries Dirichlet Clamp Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Add Neumann boundaries to residual operator

  @param[in]  ratel  Ratel context
  @param[in]  dm     DM to use for Neumann boundaries

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCeedAddBoundariesNeumann(Ratel ratel, DM dm) {
  Ceed                 ceed = ratel->ceed;
  CeedInt              Q    = ratel->multigrid_fine_order + 1 + ratel->q_extra;
  PetscInt             dim;
  CeedInt              num_comp_x, num_comp_u = 3, q_data_size = 4, num_elem, num_qpts;
  Vec                  coords;
  const PetscScalar   *coord_array;
  CeedVector           x_coord = NULL, q_data;
  CeedBasis            basis_x_face, basis_u_face;
  CeedElemRestriction  elem_restr_x_face, elem_restr_u_face, elem_restr_q_data;
  CeedQFunctionContext ctx_traction;
  CeedQFunction        qf_setup, qf_traction;
  CeedOperator         op_setup, op_traction;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Ceed Add Boundaries Neumann");

  PetscCall(DMGetDimension(dm, &dim));
  num_comp_x = dim;

  if (ratel->bc_traction_count > 0) {
    DMLabel domain_label;
    PetscCall(DMGetLabel(dm, "Face Sets", &domain_label));
    PetscCall(DMGetDimension(dm, &dim));

    // Basis
    RatelDebug(ratel, "---- Setting up libCEED bases");
    CeedInt height = 1;
    DM      dm_coord;
    PetscCall(DMGetCoordinateDM(dm, &dm_coord));
    PetscCall(RatelBasisCreateFromPlex(ratel, dm_coord, 0, 0, height, 0, &basis_x_face));
    PetscCall(RatelBasisCreateFromPlex(ratel, dm, 0, 0, height, 0, &basis_u_face));

    // Setup QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, SetupSurfaceGeometry, RatelQFunctionRelativePath(SetupSurfaceGeometry_loc), &qf_setup));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup, "dx", num_comp_x * (num_comp_x - height), CEED_EVAL_GRAD));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup, "weight", 1, CEED_EVAL_WEIGHT));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_setup, "qdata", q_data_size, CEED_EVAL_NONE));

    // Compute contribution on each boundary face
    for (CeedInt i = 0; i < ratel->bc_traction_count; i++) {
      RatelDebug(ratel, "---- Setting up traction boundary: %" CeedInt_FMT, i);

      // -- Set up context data
      BCTractionData traction_data = {.direction[0] = 0, .direction[1] = 0, .direction[2] = 0, .time = 1.0, .final_time = 1.0};

      // ---- Read CL options for traction conditions
      PetscOptionsBegin(ratel->comm, NULL, "", NULL);
      // ------ Translation vector
      char option_name[25];

      PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_traction_%" PetscInt_FMT, ratel->bc_traction_faces[i]));
      PetscInt  max_n = 3;
      PetscBool set   = false;
      PetscCall(PetscOptionsScalarArray(option_name, "Traction vector for constrained face", NULL, traction_data.direction, &max_n, &set));

      if (!set) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Traction vector must be set for all traction boundary conditions.");
      RatelDebug(ratel, "----   Traction Boundary %" PetscInt_FMT ": [tx: %.3f, ty: %.3f, tz: %.3f]", ratel->bc_traction_faces[i],
                 traction_data.direction[0], traction_data.direction[1], traction_data.direction[2]);

      PetscCall(PetscOptionsScalar("-ts_max_time", "final time", NULL, traction_data.final_time, &traction_data.final_time, NULL));

      PetscOptionsEnd();  // End of setting traction conditions

      // -- Restrictions
      RatelDebug(ratel, "---- Setting up libCEED restrictions");
      PetscCall(RatelGetRestrictionForDomain(ratel, dm, domain_label, ratel->bc_traction_faces[i], height, 0, Q, 0, &elem_restr_u_face,
                                             &elem_restr_x_face, NULL));
      RatelCeedCall(ratel, CeedBasisGetNumQuadraturePoints(basis_u_face, &num_qpts));
      RatelCeedCall(ratel, CeedElemRestrictionGetNumElements(elem_restr_x_face, &num_elem));
      RatelDebug(ratel,
                 "---- Restriction, geometric data: %" CeedInt_FMT " elem with %" CeedInt_FMT " points and %" CeedInt_FMT " fields for %" CeedInt_FMT
                 " DoF",
                 num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size);
      RatelCeedCall(ratel, CeedElemRestrictionCreateStrided(ceed, num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size,
                                                            CEED_STRIDES_BACKEND, &elem_restr_q_data));
      RatelCeedCall(ratel, CeedElemRestrictionCreateVector(elem_restr_q_data, &q_data, NULL));

      // Coordinates vector
      if (!x_coord) {
        RatelDebug(ratel, "---- Retrieving element coordinates");
        PetscCall(DMGetCoordinatesLocal(dm, &coords));
        PetscCall(VecGetArrayRead(coords, &coord_array));
        RatelCeedCall(ratel, CeedElemRestrictionCreateVector(elem_restr_x_face, &x_coord, NULL));
        RatelCeedCall(ratel, CeedVectorSetArray(x_coord, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)coord_array));
        PetscCall(VecRestoreArrayRead(coords, &coord_array));
      }

      // -- Setup Operator
      RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_setup, NULL, NULL, &op_setup));
      RatelCeedCall(ratel, CeedOperatorSetName(op_setup, "surface geometric data"));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "dx", elem_restr_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "weight", CEED_ELEMRESTRICTION_NONE, basis_x_face, CEED_VECTOR_NONE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, CEED_VECTOR_ACTIVE));

      // -- Compute geometric factors
      RatelDebug(ratel, "---- Computing geometric factors");
      if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_setup, stdout));
      RatelCeedCall(ratel, CeedOperatorApply(op_setup, x_coord, q_data, CEED_REQUEST_IMMEDIATE));

      // -- Apply QFunction
      RatelDebug(ratel, "---- Setting up Neumann traction operator");
      RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, ApplyTractionBCs, RatelQFunctionRelativePath(ApplyTractionBCs_loc), &qf_traction));
      RatelCeedCall(ratel, CeedQFunctionAddInput(qf_traction, "qdata", q_data_size, CEED_EVAL_NONE));
      RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_traction, "v", num_comp_u, CEED_EVAL_INTERP));
      RatelCeedCall(ratel, CeedQFunctionContextCreate(ceed, &ctx_traction));
      RatelCeedCall(ratel, CeedQFunctionContextSetData(ctx_traction, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(BCTractionData), &traction_data));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_traction, "traction x", offsetof(BCTractionData, direction[0]), 1,
                                                              "traction in x direction"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_traction, "traction y", offsetof(BCTractionData, direction[1]), 1,
                                                              "traction in y direction"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_traction, "traction z", offsetof(BCTractionData, direction[2]), 1,
                                                              "traction in z direction"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_traction, "time", offsetof(BCTractionData, time), 1, "current solver time"));
      RatelCeedCall(ratel, CeedQFunctionContextRegisterDouble(ctx_traction, "final time", offsetof(BCTractionData, time), 1, "final solver time"));
      RatelCeedCall(ratel, CeedQFunctionSetContext(qf_traction, ctx_traction));
      RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_traction, false));
      if (ratel->is_debug) RatelCeedCall(ratel, CeedQFunctionContextView(ctx_traction, stdout));

      // -- Apply operator
      RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_traction, NULL, NULL, &op_traction));
      {
        char operator_name[25];

        PetscCall(PetscSNPrintf(operator_name, sizeof operator_name, "traction - face %" PetscInt_FMT, ratel->bc_traction_faces[i]));
        RatelCeedCall(ratel, CeedOperatorSetName(op_traction, operator_name));
      }
      RatelCeedCall(ratel, CeedOperatorSetField(op_traction, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, q_data));
      RatelCeedCall(ratel, CeedOperatorSetField(op_traction, "v", elem_restr_u_face, basis_u_face, CEED_VECTOR_ACTIVE));
      // -- Add to composite operator
      RatelCeedCall(ratel, CeedCompositeOperatorAddSub(ratel->op_residual_u, op_traction));
      if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_traction, stdout));

      // -- Cleanup
      RatelCeedCall(ratel, CeedVectorDestroy(&q_data));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_x_face));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_u_face));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_q_data));
      RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_traction));
      RatelCeedCall(ratel, CeedQFunctionContextDestroy(&ctx_traction));
      RatelCeedCall(ratel, CeedOperatorDestroy(&op_setup));
      RatelCeedCall(ratel, CeedOperatorDestroy(&op_traction));
    }
    // Cleanup
    RatelCeedCall(ratel, CeedVectorDestroy(&x_coord));
    RatelCeedCall(ratel, CeedBasisDestroy(&basis_x_face));
    RatelCeedCall(ratel, CeedBasisDestroy(&basis_u_face));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_setup));
  }

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Ceed Add Boundaries Neumann Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Setup surface force face centroid computation

  @param[in]  ratel  Ratel context
  @param[in]  dm     DM with surface force faces

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupSurfaceForceCentroids(Ratel ratel, DM dm) {
  Ceed                 ceed = ratel->ceed;
  CeedInt              Q    = ratel->multigrid_fine_order + 1 + ratel->q_extra;
  PetscInt             dim;
  CeedInt              num_comp_x, q_data_size = 4, num_elem, num_qpts;
  Vec                  coords;
  const PetscScalar   *coord_array;
  PetscScalar          centroid_local[3], surface_area_local;
  CeedVector           x_coord = NULL, q_data, centroid, one;
  CeedBasis            basis_x_face;
  CeedElemRestriction  elem_restr_x_face, elem_restr_q_data;
  CeedQFunctionContext ctx_mass;
  CeedQFunction        qf_setup, qf_mass, qf_centroid;
  CeedOperator         op_setup, op_mass, op_centroid;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Surface Force Centroids");

  PetscCall(DMGetDimension(dm, &dim));
  num_comp_x = dim;

  if (ratel->surface_force_face_count > 0) {
    DMLabel domain_label;
    PetscCall(DMGetLabel(dm, "Face Sets", &domain_label));
    PetscCall(DMGetDimension(dm, &dim));

    // Basis
    RatelDebug(ratel, "---- Setting up libCEED bases");
    CeedInt height = 1;
    DM      dm_coord;
    PetscCall(DMGetCoordinateDM(dm, &dm_coord));
    PetscCall(RatelBasisCreateFromPlex(ratel, dm_coord, 0, 0, height, 0, &basis_x_face));

    // Setup QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, SetupSurfaceGeometry, RatelQFunctionRelativePath(SetupSurfaceGeometry_loc), &qf_setup));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup, "dx", num_comp_x * (num_comp_x - height), CEED_EVAL_GRAD));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_setup, "weight", 1, CEED_EVAL_WEIGHT));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_setup, "qdata", q_data_size, CEED_EVAL_NONE));

    // Surface area QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, Mass, RatelQFunctionRelativePath(Mass_loc), &qf_mass));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_mass, "u", num_comp_x, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_mass, "qdata", q_data_size, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_mass, "v", num_comp_x, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionContextCreate(ceed, &ctx_mass));
    RatelCeedCall(ratel, CeedQFunctionContextSetData(ctx_mass, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(CeedScalar), &dim));
    RatelCeedCall(ratel, CeedQFunctionSetContext(qf_mass, ctx_mass));
    RatelCeedCall(ratel, CeedQFunctionSetContextWritable(qf_mass, false));

    // Centroid QFunction
    RatelCeedCall(ratel, CeedQFunctionCreateInterior(ceed, 1, ComputeCentroid, RatelQFunctionRelativePath(ComputeCentroid_loc), &qf_centroid));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_centroid, "x", num_comp_x, CEED_EVAL_INTERP));
    RatelCeedCall(ratel, CeedQFunctionAddInput(qf_centroid, "qdata", q_data_size, CEED_EVAL_NONE));
    RatelCeedCall(ratel, CeedQFunctionAddOutput(qf_centroid, "centroid", num_comp_x, CEED_EVAL_INTERP));

    // Compute contribution on each boundary face
    for (CeedInt i = 0; i < ratel->surface_force_face_count; i++) {
      RatelDebug(ratel, "---- Setting up centroid on surface face %" CeedInt_FMT, i);

      // -- Restrictions
      RatelDebug(ratel, "---- Setting up libCEED restrictions");
      PetscCall(
          RatelGetRestrictionForDomain(ratel, dm, domain_label, ratel->surface_force_dm_faces[i], height, 0, Q, 0, NULL, &elem_restr_x_face, NULL));
      RatelCeedCall(ratel, CeedBasisGetNumQuadraturePoints(basis_x_face, &num_qpts));
      RatelCeedCall(ratel, CeedElemRestrictionGetNumElements(elem_restr_x_face, &num_elem));
      RatelDebug(ratel,
                 "---- Restriction, geometric data: %" CeedInt_FMT " elem with %" CeedInt_FMT " points and %" CeedInt_FMT " fields for %" CeedInt_FMT
                 " DoF",
                 num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size);
      RatelCeedCall(ratel, CeedElemRestrictionCreateStrided(ceed, num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size,
                                                            CEED_STRIDES_BACKEND, &elem_restr_q_data));
      RatelCeedCall(ratel, CeedElemRestrictionCreateVector(elem_restr_q_data, &q_data, NULL));

      // Coordinates vector
      if (!x_coord) {
        RatelDebug(ratel, "---- Retrieving element coordinates");
        PetscCall(DMGetCoordinatesLocal(dm, &coords));
        PetscCall(VecGetArrayRead(coords, &coord_array));
        RatelCeedCall(ratel, CeedElemRestrictionCreateVector(elem_restr_x_face, &x_coord, NULL));
        RatelCeedCall(ratel, CeedElemRestrictionCreateVector(elem_restr_x_face, &centroid, NULL));
        RatelCeedCall(ratel, CeedElemRestrictionCreateVector(elem_restr_x_face, &one, NULL));
        RatelCeedCall(ratel, CeedVectorSetValue(one, 1.0));
        RatelCeedCall(ratel, CeedVectorSetArray(x_coord, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)coord_array));
        PetscCall(VecRestoreArrayRead(coords, &coord_array));
      }

      // -- Setup Operator
      RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_setup, NULL, NULL, &op_setup));
      RatelCeedCall(ratel, CeedOperatorSetName(op_setup, "surface geometric data"));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "dx", elem_restr_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "weight", CEED_ELEMRESTRICTION_NONE, basis_x_face, CEED_VECTOR_NONE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_setup, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, CEED_VECTOR_ACTIVE));

      // -- Compute geometric factors
      RatelDebug(ratel, "---- Computing geometric factors");
      if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_setup, stdout));
      RatelCeedCall(ratel, CeedOperatorApply(op_setup, x_coord, q_data, CEED_REQUEST_IMMEDIATE));

      // -- Surface area operator
      RatelDebug(ratel, "---- Setting surface area operator");
      RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_mass, NULL, NULL, &op_mass));
      {
        char operator_name[25];

        PetscCall(PetscSNPrintf(operator_name, sizeof operator_name, "surfae area - face %" PetscInt_FMT, ratel->bc_traction_faces[i]));
        RatelCeedCall(ratel, CeedOperatorSetName(op_mass, operator_name));
      }
      RatelCeedCall(ratel, CeedOperatorSetField(op_mass, "u", elem_restr_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_mass, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, q_data));
      RatelCeedCall(ratel, CeedOperatorSetField(op_mass, "v", elem_restr_x_face, basis_x_face, CEED_VECTOR_ACTIVE));

      // -- Compute surface area
      RatelDebug(ratel, "---- Computing surface area");
      if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_mass, stdout));
      RatelCeedCall(ratel, CeedOperatorApply(op_mass, one, centroid, CEED_REQUEST_IMMEDIATE));
      {
        const CeedScalar *array;
        CeedSize          length;

        RatelCeedCall(ratel, CeedVectorGetLength(centroid, &length));
        RatelCeedCall(ratel, CeedVectorGetArrayRead(centroid, CEED_MEM_HOST, &array));
        surface_area_local = 0.0;
        for (CeedInt j = 0; j < length / dim; j++) {
          for (CeedInt d = 0; d < dim; d++) surface_area_local += array[j * dim + d];
        }
        RatelCeedCall(ratel, CeedVectorRestoreArrayRead(centroid, &array));
      }
      PetscCall(MPIU_Allreduce(&surface_area_local, &ratel->surface_force_face_area[i], 1, MPIU_REAL, MPI_SUM, ratel->comm));
      RatelDebug(ratel, "------   Surface area: %0.10e", ratel->surface_force_face_area[i]);

      // -- Centroid QFunction
      RatelDebug(ratel, "---- Setting up centroid operator");
      RatelCeedCall(ratel, CeedOperatorCreate(ceed, qf_centroid, NULL, NULL, &op_centroid));
      {
        char operator_name[25];

        PetscCall(PetscSNPrintf(operator_name, sizeof operator_name, "centroid - face %" PetscInt_FMT, ratel->bc_traction_faces[i]));
        RatelCeedCall(ratel, CeedOperatorSetName(op_centroid, operator_name));
      }
      RatelCeedCall(ratel, CeedOperatorSetField(op_centroid, "x", elem_restr_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
      RatelCeedCall(ratel, CeedOperatorSetField(op_centroid, "qdata", elem_restr_q_data, CEED_BASIS_COLLOCATED, q_data));
      RatelCeedCall(ratel, CeedOperatorSetField(op_centroid, "centroid", elem_restr_x_face, basis_x_face, CEED_VECTOR_ACTIVE));

      // -- Compute initial centroid
      RatelDebug(ratel, "---- Computing initial centroid");
      if (ratel->is_debug) RatelCeedCall(ratel, CeedOperatorView(op_centroid, stdout));
      RatelCeedCall(ratel, CeedOperatorApply(op_centroid, x_coord, centroid, CEED_REQUEST_IMMEDIATE));
      {
        const CeedScalar *array;
        CeedSize          length;

        RatelCeedCall(ratel, CeedVectorGetLength(centroid, &length));
        RatelCeedCall(ratel, CeedVectorGetArrayRead(centroid, CEED_MEM_HOST, &array));
        for (CeedInt d = 0; d < dim; d++) centroid_local[d] = 0.0;
        for (CeedInt j = 0; j < length / dim; j++) {
          for (CeedInt d = 0; d < dim; d++) centroid_local[d] += array[j * dim + d];
        }
        RatelCeedCall(ratel, CeedVectorRestoreArrayRead(centroid, &array));
      }
      PetscCall(MPIU_Allreduce(centroid_local, &ratel->surface_force_face_centroid[i], dim, MPIU_REAL, MPI_SUM, ratel->comm));
      for (CeedInt d = 0; d < dim; d++) ratel->surface_force_face_centroid[i][d] /= ratel->surface_force_face_area[i];
      RatelDebug(ratel, "------   Centroid: [%0.10e, %0.10e, %0.10e]", ratel->surface_force_face_centroid[i][0],
                 ratel->surface_force_face_centroid[i][1], ratel->surface_force_face_centroid[i][2]);

      // -- Cleanup
      RatelCeedCall(ratel, CeedVectorDestroy(&q_data));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_x_face));
      RatelCeedCall(ratel, CeedElemRestrictionDestroy(&elem_restr_q_data));
      RatelCeedCall(ratel, CeedOperatorDestroy(&op_setup));
      RatelCeedCall(ratel, CeedOperatorDestroy(&op_centroid));
      RatelCeedCall(ratel, CeedOperatorDestroy(&op_mass));
    }
    // Cleanup
    RatelCeedCall(ratel, CeedVectorDestroy(&x_coord));
    RatelCeedCall(ratel, CeedVectorDestroy(&centroid));
    RatelCeedCall(ratel, CeedVectorDestroy(&one));
    RatelCeedCall(ratel, CeedBasisDestroy(&basis_x_face));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_setup));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_mass));
    RatelCeedCall(ratel, CeedQFunctionDestroy(&qf_centroid));
    RatelCeedCall(ratel, CeedQFunctionContextDestroy(&ctx_mass));
  }

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Setup Surface Force Centroid Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Boundary function for linear elasticity manufactured solution

  @param[in]   dim         Dimension of the mesh
  @param[in]   t           Current time
  @param[in]   coords      Coordinates of point
  @param[in]   num_comp_u  Number of components in solution vector
  @param[out]  u           Solution vector at point
  @param[in]   ctx         Context data

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelBoundaryMMS(PetscInt dim, PetscReal t, const PetscReal coords[], PetscInt num_comp_u, PetscScalar *u, void *ctx) {
  const PetscScalar x = coords[0];
  const PetscScalar y = coords[1];
  const PetscScalar z = coords[2];

  PetscFunctionBeginUser;

  u[0] = exp(2 * x) * sin(3 * y) * cos(4 * z) / 1e8 * t;
  u[1] = exp(3 * y) * sin(4 * z) * cos(2 * x) / 1e8 * t;
  u[2] = exp(4 * z) * sin(2 * x) * cos(3 * y) / 1e8 * t;

  PetscFunctionReturn(0);
}

/// @}
