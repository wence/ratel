#include <ratel-impl.h>

// This file and definition is used for in-source builds.
// The definition for installs is in ratel-jit-source-root-install.c.
const char *RatelJitSourceRootDefault = RATEL_JIT_SOUCE_ROOT_DEFAULT;
