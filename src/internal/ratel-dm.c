/// @file
/// Ratel DM setup

#include <ceed.h>
#include <petsc.h>
#include <petscdmplex.h>
#include <ratel-boundary.h>
#include <ratel-dm.h>
#include <ratel-impl.h>
#include <ratel-utils.h>
#include <ratel.h>
#include <stdbool.h>
#include <stddef.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief Read mesh and set from options

  @param[in]   ratel     Ratel context
  @param[in]   vec_type  Default vector type (can be changed at run-time using -dm_vec_type)
  @param[out]  dm        New distributed DM

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMCreateFromOptions(Ratel ratel, VecType vec_type, DM *dm) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel DM Create From Options");

  // Distribute DM in parallel
  PetscCall(DMCreate(ratel->comm, dm));
  PetscCall(DMSetType(*dm, DMPLEX));
  PetscCall(DMSetVecType(*dm, vec_type));
  PetscCall(DMSetFromOptions(*dm));
  PetscCall(DMViewFromOptions(*dm, NULL, "-dm_view"));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel DM Create From Options Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Create a label with separate values for the FE orientations for all cells with this material for the DMPlex face

  @param[in,out]  ratel            Ratel context
  @param[in,out]  dm               DM holding mesh
  @param[in]      material_index   Index of material to setup operators for
  @param[in]      dm_face          Face number on PETSc DMPlex
  @param[out]     face_label_name  Label name for newly created label, or NULL if no elements match the `material_index` and `dm_face`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMCreateFaceLabel(Ratel ratel, DM dm, PetscInt material_index, PetscInt dm_face, char **face_label_name) {
  PetscInt first_face_point, last_face_point, face_height = 1;
  DMLabel  face_label            = NULL;
  PetscInt material_domain_value = ratel->material_label_values[material_index][0];

  PetscFunctionBeginUser;

  // Find FE face number for all points in "Face Sets"
  PetscCall(DMPlexGetHeightStratum(dm, face_height, &first_face_point, &last_face_point));

  for (PetscInt face_point = first_face_point; face_point < last_face_point; face_point++) {
    const PetscInt *face_support, *cell_cone;
    PetscInt        face_support_size = 0, cell_cone_size = 0, fe_face_on_cell = -1, value = -1;

    // Check if current point is on target DMPlex face
    PetscCall(DMGetLabelValue(dm, "Face Sets", face_point, &value));
    if (value != dm_face) continue;

    // Get cell supporting face
    PetscCall(DMPlexGetSupport(dm, face_point, &face_support));
    PetscCall(DMPlexGetSupportSize(dm, face_point, &face_support_size));
    if (face_support_size != 1) {
      // LCOV_EXCL_START
      SETERRQ(ratel->comm, PETSC_ERR_SUP, "Expected one cell in support of exterior face, but got %" PetscInt_FMT " cells", face_support_size);
      // LCOV_EXCL_STOP
    }
    PetscInt cell_point = face_support[0];

    // Check if current point is on material domain label
    PetscBool has_material_label;
    PetscCall(DMHasLabel(dm, ratel->material_label_name, &has_material_label));
    if (has_material_label) PetscCall(DMGetLabelValue(dm, ratel->material_label_name, cell_point, &value));
    if (has_material_label && value != material_domain_value) continue;

    // Find face number
    PetscCall(DMPlexGetCone(dm, cell_point, &cell_cone));
    PetscCall(DMPlexGetConeSize(dm, cell_point, &cell_cone_size));
    for (PetscInt i = 0; i < cell_cone_size; i++) {
      if (cell_cone[i] == face_point) fe_face_on_cell = i;
    }
    if (fe_face_on_cell == -1) {
      // LCOV_EXCL_START
      SETERRQ(ratel->comm, PETSC_ERR_SUP, "Could not find face %" PetscInt_FMT " in cone of its support", face_point);
      // LCOV_EXCL_STOP
    }

    // Add to labels
    if (!face_label) {
      const char *material_name  = ratel->material_names[material_index];
      PetscBool   has_name       = strlen(material_name);
      size_t      label_name_len = strlen(material_name) + (has_name ? 1 : 0) + 26;
      PetscInt    dm_face_index  = -1;

      for (PetscInt i = 0; i < RATEL_MAX_BOUNDARY_FACES; i++) {
        if (ratel->surface_force_dm_faces[i] == dm_face) dm_face_index = i;
      }
      if (dm_face_index == -1) {
        // LCOV_EXCL_START
        SETERRQ(ratel->comm, PETSC_ERR_SUP, "Could not find face %" PetscInt_FMT " in surface force dm faces list", dm_face);
        // LCOV_EXCL_STOP
      }

      // Face label
      PetscCall(PetscCalloc1(label_name_len, face_label_name));
      PetscCall(
          PetscSNPrintf(*face_label_name, label_name_len, "%s%sface label for DM face %" PetscInt_FMT, material_name, has_name ? " " : "", dm_face));
      PetscCall(DMCreateLabel(dm, *face_label_name));
      PetscCall(DMGetLabel(dm, *face_label_name, &face_label));
    }
    PetscCall(DMLabelSetValue(face_label, face_point, fe_face_on_cell));
  }
  if (face_label) PetscCall(DMPlexLabelAddCells(dm, face_label));

  PetscFunctionReturn(0);
}

/**
  @brief Convert DM field to DS field

  @param[in]   ratel         Ratel context
  @param[in]   dm            DM holding mesh
  @param[in]   domain_label  Label for DMPlex domain
  @param[in]   dm_field      Index of DMPlex field
  @param[out]  ds_field      Index of DS field

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelDMFieldToDSField(Ratel ratel, DM dm, DMLabel domain_label, PetscInt dm_field, PetscInt *ds_field) {
  PetscDS         ds;
  IS              field_is;
  const PetscInt *fields;
  PetscInt        num_fields;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel DM Field to DS Field");

  // Translate dm_field to ds_field
  PetscCall(DMGetRegionDS(dm, domain_label, &field_is, &ds));
  PetscCall(ISGetIndices(field_is, &fields));
  PetscCall(ISGetSize(field_is, &num_fields));
  for (PetscInt i = 0; i < num_fields; i++) {
    if (dm_field == fields[i]) {
      *ds_field = i;
      break;
    }
  }
  PetscCall(ISRestoreIndices(field_is, &fields));

  if (*ds_field == -1) SETERRQ(PetscObjectComm((PetscObject)dm), PETSC_ERR_SUP, "Could not find dm_field %" PetscInt_FMT " in DS", dm_field);

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel DM Field to DS Field Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Create libCEED Basis from PetscTabulation

  @param[in]   ratel             Ratel context
  @param[in]   dm                DM holding mesh
  @param[in]   domain_label      Label for DMPlex domain
  @param[in]   label_value       Stratum value
  @param[in]   height            Height of DMPlex topology
  @param[in]   face              Index of basis face, or 0
  @param[in]   fe                PETSc basis FE object
  @param[in]   basis_tabulation  PETSc basis tabulation
  @param[in]   quadrature        PETSc basis quadrature
  @param[out]  basis             libCEED basis

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelBasisCreateFromTabulation(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height, PetscInt face,
                                                     PetscFE fe, PetscTabulation basis_tabulation, PetscQuadrature quadrature, CeedBasis *basis) {
  PetscInt           first_point;
  PetscInt           ids[1] = {label_value};
  DMLabel            depth_label;
  DMPolytopeType     cell_type;
  CeedElemTopology   elem_topo;
  PetscScalar       *q_points, *interp, *grad;
  const PetscScalar *q_weights;
  PetscDualSpace     dual_space;
  PetscInt           num_dual_basis_vectors;
  PetscInt           dim, num_comp, P, Q;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Create Basis From Tabulation");

  // General basis information
  PetscCall(PetscFEGetSpatialDimension(fe, &dim));
  PetscCall(PetscFEGetNumComponents(fe, &num_comp));
  PetscCall(PetscFEGetDualSpace(fe, &dual_space));
  PetscCall(PetscDualSpaceGetDimension(dual_space, &num_dual_basis_vectors));
  P = num_dual_basis_vectors / num_comp;

  // Use depth label if no domain label present
  if (!domain_label) {
    PetscInt depth;

    PetscCall(DMPlexGetDepth(dm, &depth));
    PetscCall(DMPlexGetDepthLabel(dm, &depth_label));
    ids[0] = depth - height;
  }

  // Get cell interp, grad, and quadrature data
  PetscCall(DMGetFirstLabeledPoint(dm, dm, domain_label ? domain_label : depth_label, 1, ids, height, &first_point, NULL));
  PetscCall(DMPlexGetCellType(dm, first_point, &cell_type));
  elem_topo = RatelElemTopologyP2C(cell_type);
  if (!elem_topo) SETERRQ(PetscObjectComm((PetscObject)dm), PETSC_ERR_SUP, "DMPlex topology not supported");
  {
    size_t             q_points_size;
    const PetscScalar *q_points_petsc;
    PetscInt           q_dim;

    PetscCall(PetscQuadratureGetData(quadrature, &q_dim, NULL, &Q, &q_points_petsc, &q_weights));
    q_points_size = Q * dim * sizeof(CeedScalar);
    PetscCall(PetscCalloc(q_points_size, &q_points));
    for (PetscInt q = 0; q < Q; q++) {
      for (PetscInt d = 0; d < q_dim; d++) q_points[q * dim + d] = q_points_petsc[q * q_dim + d];
    }
  }

  // Convert to libCEED orientation
  {
    PetscBool       is_simplex  = PETSC_FALSE;
    IS              permutation = NULL;
    const PetscInt *permutation_indices;

    PetscCall(DMPlexIsSimplex(dm, &is_simplex));
    if (!is_simplex) {
      PetscSection section;

      // -- Get permutation
      PetscCall(DMGetLocalSection(dm, &section));
      PetscCall(PetscSectionGetClosurePermutation(section, (PetscObject)dm, dim, num_comp * P, &permutation));
      PetscCall(ISGetIndices(permutation, &permutation_indices));
    }

    // -- Copy interp, grad matrices
    PetscCall(PetscCalloc(P * Q * sizeof(CeedScalar), &interp));
    PetscCall(PetscCalloc(P * Q * dim * sizeof(CeedScalar), &grad));
    const CeedInt c = 0;
    for (CeedInt q = 0; q < Q; q++) {
      for (CeedInt p_ceed = 0; p_ceed < P; p_ceed++) {
        CeedInt p_petsc = is_simplex ? (p_ceed * num_comp) : permutation_indices[p_ceed * num_comp];

        interp[q * P + p_ceed] = basis_tabulation->T[0][((face * Q + q) * P * num_comp + p_petsc) * num_comp + c];
        for (CeedInt d = 0; d < dim; d++) {
          grad[(d * Q + q) * P + p_ceed] = basis_tabulation->T[1][(((face * Q + q) * P * num_comp + p_petsc) * num_comp + c) * dim + d];
        }
      }
    }

    // -- Cleanup
    if (permutation) PetscCall(ISRestoreIndices(permutation, &permutation_indices));
    PetscCall(ISDestroy(&permutation));
  }

  // Finally, create libCEED basis
  RatelCeedCall(ratel, CeedBasisCreateH1(ratel->ceed, elem_topo, num_comp, P, Q, interp, grad, q_points, q_weights, basis));
  PetscCall(PetscFree(q_points));
  PetscCall(PetscFree(interp));
  PetscCall(PetscFree(grad));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Create Basis From Tabulation Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Create libCEED Basis from DMPlex

  @param[in]   ratel         Ratel context
  @param[in]   dm            DM holding mesh
  @param[in]   domain_label  Label for DMPlex domain
  @param[in]   label_value   Stratum value
  @param[in]   height        Height of DMPlex topology
  @param[in]   dm_field      Index of DMPlex field
  @param[out]  basis         libCEED basis

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelBasisCreateFromPlex(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height, PetscInt dm_field,
                                        CeedBasis *basis) {
  PetscDS         ds;
  PetscFE         fe;
  PetscQuadrature quadrature;
  PetscBool       is_simplex = PETSC_TRUE;
  PetscInt        ds_field   = -1;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Create Basis From DMPlex");

  // Get element information
  PetscCall(DMGetRegionDS(dm, domain_label, NULL, &ds));
  PetscCall(RatelDMFieldToDSField(ratel, dm, domain_label, dm_field, &ds_field));
  PetscCall(PetscDSGetDiscretization(ds, ds_field, (PetscObject *)&fe));
  PetscCall(PetscFEGetHeightSubspace(fe, height, &fe));
  PetscCall(PetscFEGetQuadrature(fe, &quadrature));

  // Check if simplex or tensor-product mesh
  PetscCall(DMPlexIsSimplex(dm, &is_simplex));

  // Build libCEED basis
  if (is_simplex) {
    PetscTabulation basis_tabulation;
    PetscInt        num_derivatives = 1, face = 0;

    PetscCall(PetscFEGetCellTabulation(fe, num_derivatives, &basis_tabulation));
    PetscCall(RatelBasisCreateFromTabulation(ratel, dm, domain_label, label_value, height, face, fe, basis_tabulation, quadrature, basis));
  } else {
    PetscDualSpace dual_space;
    PetscInt       num_dual_basis_vectors;
    PetscInt       dim, num_comp, P, Q;

    PetscCall(PetscFEGetSpatialDimension(fe, &dim));
    PetscCall(PetscFEGetNumComponents(fe, &num_comp));
    PetscCall(PetscFEGetDualSpace(fe, &dual_space));
    PetscCall(PetscDualSpaceGetDimension(dual_space, &num_dual_basis_vectors));
    P = num_dual_basis_vectors / num_comp;
    PetscCall(PetscQuadratureGetData(quadrature, NULL, NULL, &Q, NULL, NULL));

    CeedInt P_1d = (CeedInt)round(pow(P, 1.0 / dim));
    CeedInt Q_1d = (CeedInt)round(pow(Q, 1.0 / dim));

    RatelCeedCall(ratel, CeedBasisCreateTensorH1Lagrange(ratel->ceed, dim, num_comp, P_1d, Q_1d, CEED_GAUSS, basis));
  }

  if (ratel->is_debug) RatelCeedCall(ratel, CeedBasisView(*basis, stdout));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Create Basis From DMPlex Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Create libCEED Basis for cell to face from DMPlex

  @param[in]   ratel         Ratel context
  @param[in]   dm            DM holding mesh
  @param[in]   domain_label  Label for DMPlex domain
  @param[in]   label_value   Stratum value
  @param[in]   face          Index of face
  @param[in]   dm_field      Index of DMPlex field
  @param[out]  basis         libCEED basis

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelOrientedBasisCreateCellToFaceFromPlex(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt face,
                                                          PetscInt dm_field, CeedBasis *basis) {
  PetscDS         ds;
  PetscFE         fe;
  PetscQuadrature face_quadrature;
  PetscInt        ds_field = -1, height = 0;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Create Basis for Cell to Face From DMPlex");

  // Get element information
  PetscCall(DMGetRegionDS(dm, domain_label, NULL, &ds));
  PetscCall(RatelDMFieldToDSField(ratel, dm, domain_label, dm_field, &ds_field));
  PetscCall(PetscDSGetDiscretization(ds, ds_field, (PetscObject *)&fe));
  PetscCall(PetscFEGetFaceQuadrature(fe, &face_quadrature));

  // Build libCEED basis
  {
    PetscTabulation basis_tabulation;
    PetscInt        num_derivatives = 1;

    PetscCall(PetscFEGetFaceTabulation(fe, num_derivatives, &basis_tabulation));
    PetscCall(RatelBasisCreateFromTabulation(ratel, dm, domain_label, label_value, height, face, fe, basis_tabulation, face_quadrature, basis));
  }

  if (ratel->is_debug) RatelCeedCall(ratel, CeedBasisView(*basis, stdout));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Create Basis for Cell to Face From DMPlex Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Create local libCEED ElemRestriction from DMPlex

  @param[in]   ratel         Ratel context
  @param[in]   dm            DM holding mesh
  @param[in]   domain_label  Label for DMPlex domain
  @param[in]   label_value   Stratum value
  @param[in]   height        Height of DMPlex topology
  @param[in]   dm_field      Index of DMPlex field
  @param[out]  elem_restr    libCEED element restriction

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRestrictionCreateFromPlex(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height, PetscInt dm_field,
                                              CeedElemRestriction *elem_restr) {
  PetscInt num_elem, elem_size, num_dof, num_comp, *elem_restr_offsets_petsc;
  CeedInt *elem_restr_offsets_ceed = NULL;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Create Restriction From DMPlex");

  PetscCall(
      DMPlexGetLocalOffsets(dm, domain_label, label_value, height, dm_field, &num_elem, &elem_size, &num_comp, &num_dof, &elem_restr_offsets_petsc));
  PetscCall(RatelIntArrayP2C(num_elem * elem_size, &elem_restr_offsets_petsc, &elem_restr_offsets_ceed));
  RatelCeedCall(ratel, CeedElemRestrictionCreate(ratel->ceed, num_elem, elem_size, num_comp, 1, num_dof, CEED_MEM_HOST, CEED_COPY_VALUES,
                                                 elem_restr_offsets_ceed, elem_restr));
  PetscCall(PetscFree(elem_restr_offsets_ceed));

  if (ratel->is_debug) RatelCeedCall(ratel, CeedElemRestrictionView(*elem_restr, stdout));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Create Restriction From DMPlex Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Create local libCEED restriction from DMPlex domain

  @param[in]   ratel            Ratel context
  @param[in]   dm               DM holding mesh
  @param[in]   domain_label     Label for DMPlex domain
  @param[in]   label_value      Stratum value
  @param[in]   height           Height of DMPlex topology
  @param[in]   dm_field         Index of DMPlex field
  @param[in]   Q                Number of quadrature points
  @param[in]   q_data_size      Size of stored quadrature point data
  @param[out]  elem_restr_q     libCEED element restriction for solution
  @param[out]  elem_restr_x     libCEED element restriction for mesh
  @param[out]  elem_restr_qd_i  libCEED element restriction for quadrature point data

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetRestrictionForDomain(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height, PetscInt dm_field,
                                            CeedInt Q, CeedInt q_data_size, CeedElemRestriction *elem_restr_q, CeedElemRestriction *elem_restr_x,
                                            CeedElemRestriction *elem_restr_qd_i) {
  Ceed     ceed = ratel->ceed;
  DM       dm_coord;
  PetscInt dim;
  CeedInt  Q_dim, num_local_elem;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Get Restriction for Domain");

  PetscCall(DMGetDimension(dm, &dim));
  dim -= height;
  Q_dim = CeedIntPow(Q, dim);
  PetscCall(DMGetCoordinateDM(dm, &dm_coord));
  if (elem_restr_q) PetscCall(RatelRestrictionCreateFromPlex(ratel, dm, domain_label, label_value, height, dm_field, elem_restr_q));
  if (elem_restr_x) PetscCall(RatelRestrictionCreateFromPlex(ratel, dm_coord, domain_label, label_value, height, dm_field, elem_restr_x));
  if (elem_restr_qd_i) {
    if (!elem_restr_q) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Cannot provide qdata restriction without field restriction");
    RatelCeedCall(ratel, CeedElemRestrictionGetNumElements(*elem_restr_q, &num_local_elem));
    RatelDebug(ratel,
               "---- Strided Restriction: %" CeedInt_FMT " elem with %" CeedInt_FMT " points and %" CeedInt_FMT " fields for %" CeedInt_FMT " DoF",
               num_local_elem, Q_dim, q_data_size, q_data_size * num_local_elem * Q_dim);
    RatelCeedCall(ratel, CeedElemRestrictionCreateStrided(ceed, num_local_elem, Q_dim, q_data_size, q_data_size * num_local_elem * Q_dim,
                                                          CEED_STRIDES_BACKEND, elem_restr_qd_i));
  }

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Get Restriction for Domain Success!");
  PetscFunctionReturn(0);
}

/// @}
