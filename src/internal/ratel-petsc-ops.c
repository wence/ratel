/// @file
/// Ratel matrix shell and SNES/TS operators

#include <ceed.h>
#include <ceed/backend.h>
#include <petsc.h>
#include <petscsnes.h>
#include <ratel-impl.h>
#include <ratel-petsc-ops.h>
#include <ratel-utils.h>
#include <ratel.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief Compute the diagonal of an operator via libCEED

  @param[in]   A  Operator MatShell
  @param[out]  D  Vector holding operator diagonal

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetDiagonal(Mat A, Vec D) {
  Ratel                     ratel;
  RatelOperatorApplyContext ctx;
  PetscScalar              *y;
  PetscMemType              y_mem_type;
  CeedOperator             *sub_operators;
  PetscBool                 has_smoother_parameter = PETSC_FALSE, is_smoother_parameter_changed = PETSC_FALSE;

  PetscFunctionBeginUser;

  PetscCall(MatShellGetContext(A, &ctx));
  ratel = ctx->ratel;

  // Get sub-operators
  for (PetscInt i = 0; i < ratel->num_multigrid_levels_setup; i++) has_smoother_parameter |= ctx->op == ratel->op_jacobian[i];
  if (has_smoother_parameter) {
    RatelCeedCall(ratel, CeedOperatorGetSubList(ctx->op, &sub_operators));

    // Set smoother physics context
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscInt sub_operator_index = ratel->material_sub_operator_index[i];

      if (ctx->ctx_phys_smoother[i] && sub_operator_index != -1) {
        CeedQFunction qf;

        RatelCeedCall(ratel, CeedOperatorGetQFunction(sub_operators[sub_operator_index], &qf));
        RatelCeedCall(ratel, CeedQFunctionSetContext(qf, ctx->ctx_phys_smoother[i]));
        is_smoother_parameter_changed = PETSC_TRUE;
      }
    }
    if (is_smoother_parameter_changed) {
      if (ratel->has_ut_term) {
        RatelCeedCall(ratel, CeedOperatorContextSetDouble(ratel->op_jacobian[0], ratel->op_jacobian_shift_v_label, &ratel->shift_v));
      }
      if (ratel->shift_a)
        RatelCeedCall(ratel, CeedOperatorContextSetDouble(ratel->op_jacobian[0], ratel->op_jacobian_shift_a_label, &ratel->shift_a));
      RatelCeedCall(ratel, CeedOperatorSetQFunctionAssemblyDataUpdateNeeded(ctx->op, true));
    }
  }

  // Place PETSc vector in libCEED vector
  PetscCall(VecGetArrayAndMemType(ctx->Y_loc, &y, &y_mem_type));
  RatelCeedCall(ratel, CeedVectorSetArray(ctx->y_loc_ceed, RatelMemTypeP2C(y_mem_type), CEED_USE_POINTER, y));

  // Compute Diagonal
  RatelCeedCall(ratel, CeedOperatorLinearAssembleDiagonal(ctx->op, ctx->y_loc_ceed, CEED_REQUEST_IMMEDIATE));

  // Reset physics context
  if (is_smoother_parameter_changed) {
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscInt sub_operator_index = ratel->material_sub_operator_index[i];

      if (ctx->ctx_phys_smoother[i] && sub_operator_index != -1) {
        CeedQFunction qf;

        RatelCeedCall(ratel, CeedOperatorGetQFunction(sub_operators[sub_operator_index], &qf));
        RatelCeedCall(ratel, CeedQFunctionSetContext(qf, ctx->ctx_phys[i]));
      }
    }
    RatelCeedCall(ratel, CeedOperatorSetQFunctionAssemblyDataUpdateNeeded(ctx->op, true));
  }

  // Restore PETSc vector
  RatelCeedCall(ratel, CeedVectorTakeArray(ctx->y_loc_ceed, RatelMemTypeP2C(y_mem_type), NULL));
  PetscCall(VecRestoreArrayAndMemType(ctx->Y_loc, &y));

  // Local-to-Global
  PetscCall(VecZeroEntries(D));
  PetscCall(DMLocalToGlobal(ctx->dm_y, ctx->Y_loc, ADD_VALUES, D));

  PetscFunctionReturn(0);
}

/**
  @brief Set COO preallocation for Mat associated with a CeedOperator

  @param[in]      ratel            Ratel context
  @param[in]      op_ceed          CeedOperator to assemble sparsity pattern
  @param[out]     coo_values_ceed  CeedVector to hold COO values
  @param[in,out]  A                Sparse matrix to hold assembled CeedOperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMatSetPreallocationCOO(Ratel ratel, CeedOperator op_ceed, CeedVector *coo_values_ceed, Mat A) {
  PetscLogStage stage_amg_setup;
  PetscCount    num_entries;
  CeedInt      *rows_ceed, *cols_ceed;
  PetscInt     *rows_petsc = NULL, *cols_petsc = NULL;

  PetscFunctionBeginUser;

  // -- Assemble sparsity pattern
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel AMG Setup");
  PetscCall(PetscLogStageGetId("Ratel AMG Setup", &stage_amg_setup));
  if (stage_amg_setup == -1) {
    PetscCall(PetscLogStageRegister("Ratel AMG Setup", &stage_amg_setup));
  }
  PetscCall(PetscLogStagePush(stage_amg_setup));
  RatelCeedCall(ratel, CeedOperatorLinearAssembleSymbolic(op_ceed, &num_entries, &rows_ceed, &cols_ceed));
  PetscCall(RatelIntArrayC2P(num_entries, &rows_ceed, &rows_petsc));
  PetscCall(RatelIntArrayC2P(num_entries, &cols_ceed, &cols_petsc));
  PetscCall(MatSetPreallocationCOOLocal(A, num_entries, rows_petsc, cols_petsc));
  free(rows_petsc);
  free(cols_petsc);
  RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, num_entries, coo_values_ceed));

  PetscCall(PetscLogStagePop());
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel AMG Setup Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Set COO values for Mat associated with a CeedOperator

  @param[in]      ratel            Ratel context
  @param[in]      op_ceed          CeedOperator to assemble
  @param[in]      coo_values_ceed  CeedVector to hold COO values
  @param[in]      mem_type         MemType to pass COO values
  @param[in,out]  A                Sparse matrix to hold assembled CeedOperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMatSetValuesCOO(Ratel ratel, CeedOperator op_ceed, CeedVector coo_values_ceed, CeedMemType mem_type, Mat A) {
  const CeedScalar *values;

  PetscFunctionBeginUser;

  // -- Assemble matrix values
  RatelCeedCall(ratel, CeedOperatorLinearAssemble(op_ceed, coo_values_ceed));
  RatelCeedCall(ratel, CeedVectorGetArrayRead(coo_values_ceed, mem_type, &values));
  PetscCall(MatSetValuesCOO(A, values, INSERT_VALUES));
  PetscCall(MatSetOption(A, MAT_SPD, PETSC_TRUE));
  RatelCeedCall(ratel, CeedVectorRestoreArrayRead(coo_values_ceed, &values));

  PetscFunctionReturn(0);
}

// -----------------------------------------------------------------------------
// Action of libCEED operators for PETSc objects
// -----------------------------------------------------------------------------
/**
  @brief Apply the local action of a libCEED operator and store result in PETSc vector
         i.e. compute A X = Y

  @param[in]   X    Input PETSc vector
  @param[out]  Y    Output PETSc vector, or NULL to only write into local vector
  @param[in]   ctx  User context struct containing libCEED operator data

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelApplyLocalCeedOp(Vec X, Vec Y, RatelOperatorApplyContext ctx) {
  PetscFunctionBeginUser;

  // Zero target vector
  if (Y) PetscCall(VecZeroEntries(Y));

  // Sum into target vector
  PetscCall(RatelApplyAddLocalCeedOp(X, Y, ctx));

  PetscFunctionReturn(0);
}

// -----------------------------------------------------------------------------
// Action of libCEED operators for PETSc objects
// -----------------------------------------------------------------------------
/**
  @brief Apply the local action of a libCEED operator and store result in PETSc vector
         i.e. compute A X = Y

  @param[in]   X    Input PETSc vector
  @param[out]  Y    Output PETSc vector, or NULL to only write into local vector
  @param[in]   ctx  User context struct containing libCEED operator data

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelApplyAddLocalCeedOp(Vec X, Vec Y, RatelOperatorApplyContext ctx) {
  Ratel        ratel = ctx->ratel;
  PetscScalar *x, *y;
  PetscMemType x_mem_type, y_mem_type;

  PetscFunctionBeginUser;

  // Global-to-local
  PetscCall(DMGlobalToLocal(ctx->dm_x, X, INSERT_VALUES, ctx->X_loc));

  // Setup libCEED vectors
  PetscCall(VecGetArrayReadAndMemType(ctx->X_loc, (const PetscScalar **)&x, &x_mem_type));
  PetscCall(VecGetArrayAndMemType(ctx->Y_loc, &y, &y_mem_type));
  RatelCeedCall(ratel, CeedVectorSetArray(ctx->x_loc_ceed, RatelMemTypeP2C(x_mem_type), CEED_USE_POINTER, x));
  RatelCeedCall(ratel, CeedVectorSetArray(ctx->y_loc_ceed, RatelMemTypeP2C(y_mem_type), CEED_USE_POINTER, y));

  // Apply libCEED operator
  RatelCeedCall(ratel, CeedOperatorApply(ctx->op, ctx->x_loc_ceed, ctx->y_loc_ceed, CEED_REQUEST_IMMEDIATE));

  // Restore PETSc vectors
  RatelCeedCall(ratel, CeedVectorTakeArray(ctx->x_loc_ceed, RatelMemTypeP2C(x_mem_type), NULL));
  RatelCeedCall(ratel, CeedVectorTakeArray(ctx->y_loc_ceed, RatelMemTypeP2C(y_mem_type), NULL));
  PetscCall(VecRestoreArrayReadAndMemType(ctx->X_loc, (const PetscScalar **)&x));
  PetscCall(VecRestoreArrayAndMemType(ctx->Y_loc, &y));

  // Local-to-global
  if (Y) PetscCall(DMLocalToGlobal(ctx->dm_y, ctx->Y_loc, ADD_VALUES, Y));

  PetscFunctionReturn(0);
}

/**
  @brief Compute the Dirichlet boundary values via libCEED

  @param[in,out]  ratel  Ratel context to update boundary conditons
  @param[in]      time   Current time

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelUpdateTimeContexts(Ratel ratel, PetscReal time) {
  PetscFunctionBeginUser;

  // Update contexts as needed
  if (ratel->time != time) {
    if (ratel->op_dirichlet_time_label) {
      RatelCeedCall(ratel, CeedOperatorContextSetDouble(ratel->op_dirichlet, ratel->op_dirichlet_time_label, &time));
    }
    if (ratel->op_residual_u_time_label) {
      RatelCeedCall(ratel, CeedOperatorContextSetDouble(ratel->op_residual_u, ratel->op_residual_u_time_label, &time));
    }
    if (ratel->op_jacobian_time_label) {
      RatelCeedCall(ratel, CeedOperatorContextSetDouble(ratel->op_jacobian[0], ratel->op_jacobian_time_label, &time));
    }
    ratel->time = time;
  }

  PetscFunctionReturn(0);
}

/**
  @brief Compute the Dirichlet boundary values via libCEED

  @param[in,out]  ratel  Ratel context to update boundary conditons
  @param[in]      time   Current time

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelUpdateBoundaryValues(Ratel ratel, PetscReal time) {
  PetscFunctionBeginUser;

  // Used cached values if time unchanged
  if (ratel->time != time) {
    PetscCall(DMPlexInsertBoundaryValues(ratel->ctx_residual_u->dm_x, PETSC_TRUE, ratel->ctx_residual_u->X_loc, time, NULL, NULL, NULL));
  }

  PetscFunctionReturn(0);
}

/**
  @brief Compute the Dirichlet boundary values via libCEED

  @param[in]   dm                 DM to insert boundary values on
  @param[in]   insert_essential   Boolean flag for Dirichlet or Neumann boundary conditions
  @param[out]  U_loc              Local vector to insert the boundary values into
  @param[in]   time               Current time
  @param[in]   face_geometry_FVM  Face geometry data for finite volume discretizations
  @param[in]   cell_geometry_FVM  Cell geometry data for finite volume discretizations
  @param[in]   grad_FVM           Gradient reconstruction data for finite volume discretizations

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMPlexInsertBoundaryValues(DM dm, PetscBool insert_essential, Vec U_loc, PetscReal time, Vec face_geometry_FVM,
                                               Vec cell_geometry_FVM, Vec grad_FVM) {
  Ratel        ratel;
  PetscScalar *x;
  PetscMemType x_mem_type;
  Vec          boundary_mask;

  PetscFunctionBeginUser;

  PetscCall(DMGetApplicationContext(dm, &ratel));
  PetscInt  fine_level       = ratel->num_multigrid_levels_setup - 1;
  PetscBool have_nonzero_bcs = ratel->bc_clamp_nonzero_count > 0;

  // Update time context information
  PetscCall(RatelUpdateTimeContexts(ratel, time));

  // Update boundary values
  Vec X_loc = U_loc;
  while (X_loc) {
    // -- Mask Dirichlet entries
    PetscCall(DMGetNamedLocalVector(dm, "boundary mask", &boundary_mask));
    PetscCall(VecPointwiseMult(X_loc, X_loc, boundary_mask));
    PetscCall(DMRestoreNamedLocalVector(dm, "boundary mask", &boundary_mask));

    // -- Compute boundary values
    if (have_nonzero_bcs) {
      // ---- Setup libCEED vector
      PetscCall(VecGetArrayAndMemType(X_loc, &x, &x_mem_type));
      RatelCeedCall(ratel, CeedVectorSetArray(ratel->x_loc_ceed[fine_level], RatelMemTypeP2C(x_mem_type), CEED_USE_POINTER, x));

      // ---- Apply libCEED operator
      RatelCeedCall(ratel, CeedOperatorApplyAdd(ratel->op_dirichlet, CEED_VECTOR_NONE, ratel->x_loc_ceed[fine_level], CEED_REQUEST_IMMEDIATE));

      // ---- Restore PETSc vectors
      RatelCeedCall(ratel, CeedVectorTakeArray(ratel->x_loc_ceed[fine_level], RatelMemTypeP2C(x_mem_type), NULL));
      PetscCall(VecRestoreArrayAndMemType(X_loc, &x));
    }

    // -- Update cached boundary values, if needed
    Vec X_loc_residual = ratel->ctx_residual_u->X_loc;
    if (X_loc != X_loc_residual) X_loc = X_loc_residual;
    else X_loc = NULL;
  }

  PetscFunctionReturn(0);
}

/**
  @brief Compute the action of the matrix-free operator libCEED

  @param[in]   A  Operator MatShell
  @param[in]   X  Input PETSc vector
  @param[out]  Y  Output PETSc vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelApplyOperator(Mat A, Vec X, Vec Y) {
  RatelOperatorApplyContext ctx;

  PetscFunctionBeginUser;

  PetscCall(MatShellGetContext(A, &ctx));
  PetscCall(RatelApplyLocalCeedOp(X, Y, ctx));

  PetscFunctionReturn(0);
}

/**
  @brief Compute the action of the Jacobian for a SNES operator via libCEED

  @param[in]   A  Jacobian operator MatShell
  @param[in]   X  Input PETSc vector
  @param[out]  Y  Output PETSc vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelApplyJacobian(Mat A, Vec X, Vec Y) {
  Ratel                     ratel;
  RatelOperatorApplyContext ctx;

  PetscFunctionBeginUser;

  PetscCall(MatShellGetContext(A, &ctx));
  ratel = ctx->ratel;

  // libCEED for local action of Jacobian
  // Note: X_loc should be zeroed already at this point
  PetscCall(PetscLogEventBegin(ratel->event_jacobian, A, X, Y, 0));

  PetscCall(RatelApplyLocalCeedOp(X, Y, ctx));
  if (ratel->is_ceed_backend_gpu) PetscCall(PetscLogGpuFlops(ctx->flops));
  else PetscCall(PetscLogFlops(ctx->flops));

  PetscCall(PetscLogEventEnd(ratel->event_jacobian, A, X, Y, 0));

  PetscFunctionReturn(0);
}

/**
  @brief Compute action of a p-multigrid prolongation operator via libCEED

  @param[in]   A  Prolongation operator MatShell
  @param[in]   C  Input PETSc vector on coarse grid
  @param[out]  F  Output PETSc vector on fine grid

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelProlong(Mat A, Vec C, Vec F) {
  Ratel                       ratel;
  RatelProlongRestrictContext ctx;
  PetscScalar                *c, *f;
  PetscMemType                c_mem_type, f_mem_type;

  PetscFunctionBeginUser;

  PetscCall(MatShellGetContext(A, &ctx));
  ratel = ctx->ratel;

  // FLOPs counting
  PetscCall(PetscLogEventBegin(ctx->event_prolong, A, C, F, 0));
  if (ratel->is_ceed_backend_gpu) PetscCall(PetscLogGpuFlops(ctx->flops_prolong));
  else PetscCall(PetscLogFlops(ctx->flops_prolong));

  // Global-to-local
  // Note: C_X_loc should be zeroed already at this point
  PetscCall(DMGlobalToLocal(ctx->dm_c, C, INSERT_VALUES, ctx->C_X_loc));

  // Setup libCEED vectors
  PetscCall(VecGetArrayReadAndMemType(ctx->C_X_loc, (const PetscScalar **)&c, &c_mem_type));
  PetscCall(VecGetArrayAndMemType(ctx->F_Y_loc, &f, &f_mem_type));
  RatelCeedCall(ratel, CeedVectorSetArray(ctx->c_x_loc_ceed, RatelMemTypeP2C(c_mem_type), CEED_USE_POINTER, c));
  RatelCeedCall(ratel, CeedVectorSetArray(ctx->f_y_loc_ceed, RatelMemTypeP2C(f_mem_type), CEED_USE_POINTER, f));

  // Apply libCEED operator
  RatelCeedCall(ratel, CeedOperatorApply(ctx->op_prolong, ctx->c_x_loc_ceed, ctx->f_y_loc_ceed, CEED_REQUEST_IMMEDIATE));

  // Restore PETSc vectors
  RatelCeedCall(ratel, CeedVectorTakeArray(ctx->c_x_loc_ceed, RatelMemTypeP2C(c_mem_type), NULL));
  RatelCeedCall(ratel, CeedVectorTakeArray(ctx->f_y_loc_ceed, RatelMemTypeP2C(f_mem_type), NULL));
  PetscCall(VecRestoreArrayReadAndMemType(ctx->C_X_loc, (const PetscScalar **)&c));
  PetscCall(VecRestoreArrayAndMemType(ctx->F_Y_loc, &f));

  // Local-to-global
  PetscCall(VecZeroEntries(F));
  PetscCall(DMLocalToGlobal(ctx->dm_f, ctx->F_Y_loc, ADD_VALUES, F));

  PetscCall(PetscLogEventEnd(ctx->event_prolong, A, C, F, 0));
  PetscFunctionReturn(0);
}

/**
  @brief Compute action of a p-multigrid restriction operator via libCEED

  @param[in]   A  Restriction operator MatShell
  @param[in]   F  Input PETSc vector on fine grid
  @param[out]  C  Output PETSc vector on coarse grid

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRestrict(Mat A, Vec F, Vec C) {
  Ratel                       ratel;
  RatelProlongRestrictContext ctx;
  PetscScalar                *c, *f;
  PetscMemType                c_mem_type, f_mem_type;

  PetscFunctionBeginUser;

  PetscCall(MatShellGetContext(A, &ctx));
  ratel = ctx->ratel;

  // FLOPs counting
  PetscCall(PetscLogEventBegin(ctx->event_restrict, A, F, C, 0));
  if (ratel->is_ceed_backend_gpu) PetscCall(PetscLogGpuFlops(ctx->flops_restrict));
  else PetscCall(PetscLogFlops(ctx->flops_restrict));

  // Global-to-local
  // Note: F_X_loc should be zeroed already at this point
  PetscCall(DMGlobalToLocal(ctx->dm_f, F, INSERT_VALUES, ctx->F_X_loc));

  // Setup libCEED vectors
  PetscCall(VecGetArrayReadAndMemType(ctx->F_X_loc, (const PetscScalar **)&f, &f_mem_type));
  PetscCall(VecGetArrayAndMemType(ctx->C_Y_loc, &c, &c_mem_type));
  RatelCeedCall(ratel, CeedVectorSetArray(ctx->f_x_loc_ceed, RatelMemTypeP2C(f_mem_type), CEED_USE_POINTER, f));
  RatelCeedCall(ratel, CeedVectorSetArray(ctx->c_y_loc_ceed, RatelMemTypeP2C(c_mem_type), CEED_USE_POINTER, c));

  // Apply libCEED operator
  RatelCeedCall(ratel, CeedOperatorApply(ctx->op_restrict, ctx->f_x_loc_ceed, ctx->c_y_loc_ceed, CEED_REQUEST_IMMEDIATE));

  // Restore PETSc vectors
  RatelCeedCall(ratel, CeedVectorTakeArray(ctx->f_x_loc_ceed, RatelMemTypeP2C(f_mem_type), NULL));
  RatelCeedCall(ratel, CeedVectorTakeArray(ctx->c_y_loc_ceed, RatelMemTypeP2C(c_mem_type), NULL));
  PetscCall(VecRestoreArrayReadAndMemType(ctx->F_X_loc, (const PetscScalar **)&f));
  PetscCall(VecRestoreArrayAndMemType(ctx->C_Y_loc, &c));

  // Local-to-global
  PetscCall(VecZeroEntries(C));
  PetscCall(DMLocalToGlobal(ctx->dm_c, ctx->C_Y_loc, ADD_VALUES, C));

  PetscCall(PetscLogEventEnd(ctx->event_restrict, A, F, C, 0));
  PetscFunctionReturn(0);
}

/**
  @brief Compute the non-linear residual for a SNES operator via libCEED

  @param[in]   snes            Non-linear solver
  @param[in]   X               Current state vector
  @param[out]  Y               Residual vector
  @param[in]   ctx_residual_u  User context struct containing libCEED operator data

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESFormResidual(SNES snes, Vec X, Vec Y, void *ctx_residual_u) {
  RatelOperatorApplyContext ctx   = (RatelOperatorApplyContext)ctx_residual_u;
  Ratel                     ratel = ctx->ratel;

  PetscFunctionBeginUser;

  // Apply Dirichlet BCs
  if (ratel->time != 1.0) {
    PetscCall(DMPlexInsertBoundaryValues(ctx->dm_x, PETSC_TRUE, ctx->X_loc, 1.0, NULL, NULL, NULL));
    ratel->time = 1.0;
  }

  // libCEED for local action of residual evaluator
  PetscCall(RatelApplyLocalCeedOp(X, Y, ctx));

  // Mark QFunction as requiring re-assembly
  RatelCeedCall(ratel, CeedOperatorSetQFunctionAssemblyDataUpdateNeeded(ctx->ratel->op_jacobian[0], true));

  PetscFunctionReturn(0);
}

/**
  @brief Assemble SNES Jacobian for each level of multigrid hierarchy

  @param[in]      snes               Non-linear solver
  @param[in]      X                  Current non-linear residual
  @param[out]     J                  Fine grid Jacobian operator
  @param[out]     J_pre              Fine grid Jacobian operator for preconditioning
  @param[in,out]  ctx_form_jacobian  Jacobian context, holding Jacobian operators for each level of multigrid hierarchy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESFormJacobian(SNES snes, Vec X, Mat J, Mat J_pre, void *ctx_form_jacobian) {
  RatelFormJacobianContext ctx                 = (RatelFormJacobianContext)ctx_form_jacobian;
  Ratel                    ratel               = ctx->ratel;
  PetscInt                 num_levels          = ctx->num_levels;
  Mat                     *mat_jacobian        = ctx->mat_jacobian;
  Mat                      mat_jacobian_coarse = num_levels > 1 ? ctx->mat_jacobian_coarse : NULL;
  MatType                  mat_type;

  PetscFunctionBeginUser;

  // Set context for fine level
  PetscCall(MatGetType(J, &mat_type));
  if (!strcmp(mat_type, MATSHELL)) {
    PetscCall(MatShellSetContext(J, ctx->ctx_jacobian_fine));
    PetscCall(MatShellSetOperation(J, MATOP_MULT, (void (*)(void))RatelApplyJacobian));
    PetscCall(MatShellSetOperation(J, MATOP_GET_DIAGONAL, (void (*)(void))RatelGetDiagonal));
    PetscCall(MatShellSetVecType(J, ctx->vec_type));
    PetscCall(MatSetUp(J));
  } else {
    mat_jacobian_coarse = J;
  }

  // Update Jacobian on each level
  for (PetscInt level = 1; level < num_levels; level++) {
    PetscCall(MatAssemblyBegin(mat_jacobian[level], MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(mat_jacobian[level], MAT_FINAL_ASSEMBLY));
    PetscCall(MatSetOption(mat_jacobian[level], MAT_SPD, PETSC_TRUE));
  }

  // Form coarse assembled matrix
  if (mat_jacobian_coarse) {
    const PetscScalar *x;
    PetscMemType       mem_type;

    if (!ctx->coo_values_ceed) {
      PetscCall(RatelMatSetPreallocationCOO(ratel, ctx->op_coarse, &ratel->coo_values_ceed, mat_jacobian_coarse));
      ctx->coo_values_ceed = ratel->coo_values_ceed;
    }
    PetscCall(VecGetArrayReadAndMemType(X, &x, &mem_type));
    PetscCall(VecRestoreArrayReadAndMemType(X, &x));
    PetscCall(RatelMatSetValuesCOO(ratel, ctx->op_coarse, ctx->coo_values_ceed, RatelMemTypeP2C(mem_type), mat_jacobian_coarse));
  }

  // J_pre might be AIJ (e.g., when using coloring), so we need to assemble it
  PetscCall(MatAssemblyBegin(J, MAT_FINAL_ASSEMBLY));
  PetscCall(MatAssemblyEnd(J, MAT_FINAL_ASSEMBLY));
  PetscCall(MatSetOption(J, MAT_SPD, PETSC_TRUE));
  if (J != J_pre) {
    PetscCall(MatAssemblyBegin(J_pre, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(J_pre, MAT_FINAL_ASSEMBLY));
    PetscCall(MatSetOption(J_pre, MAT_SPD, PETSC_TRUE));
  }

  PetscFunctionReturn(0);
}

/**
  @brief Compute the non-linear residual for a TS operator via libCEED

  @param[in]      ts               Time-stepper
  @param[in]      time             Current time
  @param[in]      X                Current state vector
  @param[in]      X_t              Time derivative of current state vector
  @param[out]     Y                Function vector
  @param[in,out]  ctx_residual_ut  User context struct containing libCEED operator data

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSFormIResidual(TS ts, PetscReal time, Vec X, Vec X_t, Vec Y, void *ctx_residual_ut) {
  RatelOperatorApplyContext ctx   = (RatelOperatorApplyContext)ctx_residual_ut;
  Ratel                     ratel = ctx->ratel;

  PetscFunctionBeginUser;

  // Timestep specific updates
  PetscCall(RatelUpdateBoundaryValues(ratel, time));

  // libCEED for local action of residual evaluator
  PetscCall(RatelApplyLocalCeedOp(X, Y, ratel->ctx_residual_u));

  // Add contribution of X_t term, if required
  if (ratel->has_ut_term) {
    PetscCall(RatelApplyAddLocalCeedOp(X_t, Y, ctx));
  }

  // Mark QFunction as requiring re-assembly
  RatelCeedCall(ratel, CeedOperatorSetQFunctionAssemblyDataUpdateNeeded(ratel->op_jacobian[0], true));

  PetscFunctionReturn(0);
}

/**
  @brief Assemble TS Jacobian for each level of multigrid hierarchy

  @param[in]      ts                 Time-stepper
  @param[in]      time               Current time
  @param[in]      X                  Current non-linear residual
  @param[in]      X_t                Time derivative of current non-linear residual
  @param[in]      v                  Shift
  @param[out]     J                  Fine grid Jacobian operator
  @param[out]     J_pre              Fine grid Jacobian operator for preconditioning
  @param[in,out]  ctx_form_jacobian  Jacobian context, holding Jacobian operators for each level of multigrid hierarchy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSFormIJacobian(TS ts, PetscReal time, Vec X, Vec X_t, PetscReal v, Mat J, Mat J_pre, void *ctx_form_jacobian) {
  RatelFormJacobianContext ctx   = (RatelFormJacobianContext)ctx_form_jacobian;
  Ratel                    ratel = ctx->ratel;

  PetscFunctionBeginUser;

  // Update time context data
  PetscCall(RatelUpdateTimeContexts(ratel, time));

  if (ratel->has_ut_term) {
    RatelCeedCall(ratel, CeedOperatorContextSetDouble(ratel->op_jacobian[0], ratel->op_jacobian_shift_v_label, &v));
    ratel->shift_v = v;
  }
  PetscCall(RatelSNESFormJacobian(NULL, X, J, J_pre, ctx_form_jacobian));

  PetscFunctionReturn(0);
}

/**
  @brief Compute the non-linear residual for a TS operator via libCEED

  @param[in]      ts                Time-stepper
  @param[in]      time              Current time
  @param[in]      X                 Current state vector
  @param[in]      X_t               Time derivative of current state vector
  @param[in]      X_tt              Second time derivative of current state vector
  @param[out]     Y                 Function vector
  @param[in,out]  ctx_residual_utt  User context struct containing libCEED operator data

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSFormI2Residual(TS ts, PetscReal time, Vec X, Vec X_t, Vec X_tt, Vec Y, void *ctx_residual_utt) {
  RatelOperatorApplyContext ctx   = (RatelOperatorApplyContext)ctx_residual_utt;
  Ratel                     ratel = ctx->ratel;

  PetscFunctionBeginUser;

  // Apply the portion that only depends upon X, X_t
  PetscCall(RatelTSFormIResidual(ts, time, X, X_t, Y, ratel->ctx_residual_ut));

  // libCEED for local action of scaled mass matrix
  PetscCall(RatelApplyAddLocalCeedOp(X_tt, Y, ctx));

  PetscFunctionReturn(0);
}

/**
  @brief Assemble TS Jacobian for each level of multigrid hierarchy

  @param[in]      ts                 Time-stepper
  @param[in]      time               Current time
  @param[in]      X                  Current non-linear residual
  @param[in]      X_t                Time derivative of current non-linear residual
  @param[in]      X_tt               Second time derivative of current non-linear residual
  @param[in]      v                  Shift for X_t
  @param[in]      a                  Shift for X_tt
  @param[out]     J                  Fine grid Jacobian operator
  @param[out]     J_pre              Fine grid Jacobian operator for preconditioning
  @param[in,out]  ctx_form_jacobian  Jacobian context, holding Jacobian operators for each level of multigrid hierarchy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSFormI2Jacobian(TS ts, PetscReal time, Vec X, Vec X_t, Vec X_tt, PetscReal v, PetscReal a, Mat J, Mat J_pre,
                                     void *ctx_form_jacobian) {
  RatelFormJacobianContext ctx   = (RatelFormJacobianContext)ctx_form_jacobian;
  Ratel                    ratel = ctx->ratel;

  PetscFunctionBeginUser;

  RatelCeedCall(ratel, CeedOperatorContextSetDouble(ratel->op_jacobian[0], ratel->op_jacobian_shift_a_label, &a));
  ratel->shift_a = a;
  PetscCall(RatelTSFormIJacobian(ts, time, X, X_t, v, J, J_pre, ctx_form_jacobian));

  PetscFunctionReturn(0);
}

/// @}
