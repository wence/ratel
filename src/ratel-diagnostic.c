/// @file
/// Implementation of Ratel diagnostic data interfaces

#include <ceed.h>
#include <ceed/backend.h>
#include <petsc.h>
#include <petscdmplex.h>
#include <ratel-diagnostic.h>
#include <ratel-impl.h>
#include <ratel-petsc-ops.h>
#include <ratel.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief Internal code for computing diagnostic quantities

  @param[in]   ratel  Ratel context
  @param[in]   U      Computed solution vector
  @param[in]   time   Final time value, or 1.0 for SNES solution
  @param[out]  D      Computed diagonstic quantities vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeDiagnosticQuantities_Internal(Ratel ratel, Vec U, PetscScalar time, Vec D) {
  PetscInt fine_level = ratel->num_multigrid_levels - 1;

  PetscFunctionBeginUser;

  // Work vectors
  if (!ratel->ctx_diagnostic) {
    RatelDebug(ratel, "---- Creating operator context data");
    PetscInt                  loc_size;
    RatelOperatorApplyContext ctx_diagnostic, ctx_diagnostic_projection;

    PetscCall(PetscCalloc1(1, &ctx_diagnostic));
    PetscCall(PetscCalloc1(1, &ctx_diagnostic_projection));

    // -- Create vectors
    PetscCall(DMCreateLocalVector(ratel->dm_diagnostic, &ratel->D_loc));
    PetscCall(VecGetSize(ratel->D_loc, &loc_size));
    RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, loc_size, &ratel->d_loc_ceed));
    PetscCall(VecDuplicate(ratel->D_loc, &ratel->M_loc));
    RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, loc_size, &ratel->m_loc_ceed));

    // -- Setting up dignostic quantities context
    RatelDebug(ratel, "---- Setting up diagnostic quantities context");
    ctx_diagnostic->ratel      = ratel;
    ctx_diagnostic->op         = ratel->op_diagnostic;
    ctx_diagnostic->dm_x       = ratel->dm_hierarchy[fine_level];
    ctx_diagnostic->dm_y       = ratel->dm_diagnostic;
    ctx_diagnostic->X_loc      = ratel->X_loc_residual_u;
    ctx_diagnostic->Y_loc      = ratel->D_loc;
    ctx_diagnostic->x_loc_ceed = ratel->x_loc_ceed[fine_level];
    ctx_diagnostic->y_loc_ceed = ratel->d_loc_ceed;
    ratel->ctx_diagnostic      = ctx_diagnostic;

    // -- Setting up dignostic projection context
    RatelDebug(ratel, "---- Setting up diagnostic projection context");
    ctx_diagnostic_projection->ratel      = ratel;
    ctx_diagnostic_projection->op         = ratel->op_mass_diagnostic;
    ctx_diagnostic_projection->dm_x       = ratel->dm_diagnostic;
    ctx_diagnostic_projection->dm_y       = ratel->dm_diagnostic;
    ctx_diagnostic_projection->X_loc      = ratel->D_loc;
    ctx_diagnostic_projection->Y_loc      = ratel->M_loc;
    ctx_diagnostic_projection->x_loc_ceed = ratel->d_loc_ceed;
    ctx_diagnostic_projection->y_loc_ceed = ratel->m_loc_ceed;
    ratel->ctx_diagnostic_projection      = ctx_diagnostic_projection;

    // -- Setting up diagnostic projection KSP
    RatelDebug(ratel, "---- Setting up diagnostic projection KSP");
    {
      KSP ksp;
      PC  pc;
      Mat mat;

      // ---- Projection operator
      PetscInt l_size, g_size;
      PetscCall(VecGetSize(D, &g_size));
      PetscCall(VecGetLocalSize(D, &l_size));
      PetscCall(MatCreateShell(ratel->comm, l_size, l_size, g_size, g_size, ratel->ctx_diagnostic_projection, &mat));
      PetscCall(MatShellSetOperation(mat, MATOP_MULT, (void (*)(void))RatelApplyOperator));
      PetscCall(MatShellSetOperation(mat, MATOP_GET_DIAGONAL, (void (*)(void))RatelGetDiagonal));
      {
        VecType vec_type;

        PetscCall(VecGetType(D, &vec_type));
        PetscCall(MatShellSetVecType(mat, vec_type));
      }

      // ---- Projection KSP
      PetscCall(KSPCreate(ratel->comm, &ksp));
      PetscCall(KSPSetOptionsPrefix(ksp, "projection_"));
      PetscCall(KSPSetType(ksp, KSPCG));
      PetscCall(KSPSetOperators(ksp, mat, mat));
      PetscCall(KSPSetTolerances(ksp, 1e-04, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT));
      PetscCall(KSPGetPC(ksp, &pc));
      PetscCall(PCSetType(pc, PCJACOBI));
      PetscCall(KSPSetFromOptions(ksp));

      // ---- Cleanup
      ratel->ksp_diagnostic_projection = ksp;
      PetscCall(MatDestroy(&mat));
    }
  }

  // Compute quantities
  RatelDebug(ratel, "---- Computing diagnostic quantities");
  PetscCall(RatelUpdateBoundaryValues(ratel, time));
  PetscCall(RatelApplyLocalCeedOp(U, D, ratel->ctx_diagnostic));

  // Project quantities
  PetscCall(KSPSolve(ratel->ksp_diagnostic_projection, D, D));

  PetscFunctionReturn(0);
}

/**
  @brief Internal routine for computing diagnostic quantities on mesh faces

  @param[in]   ratel           Ratel context
  @param[in]   U               Computed solution vector
  @param[in]   time            Current time value, or 1.0 for SNES solution
  @param[out]  surface_forces  Computed face forces

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeSurfaceForces_Internal(Ratel ratel, Vec U, PetscScalar time, PetscScalar *surface_forces) {
  // TODO: Update for num_comp_u != 3
  PetscInt fine_level = ratel->num_multigrid_levels - 1, dim = 3, num_comp_force = 6;

  PetscFunctionBeginUser;

  if (ratel->surface_force_face_count == 0) PetscFunctionReturn(0);

  // Work vectors
  if (!ratel->ctx_surface_force) {
    RatelDebug(ratel, "---- Creating operator context data");
    PetscInt                  loc_size;
    RatelOperatorApplyContext ctx_surface_force;

    // -- Create vectors
    PetscCall(DMCreateLocalVector(ratel->dm_surface_force, &ratel->F_loc));
    PetscCall(VecGetSize(ratel->F_loc, &loc_size));
    RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, loc_size, &ratel->f_loc_ceed));

    PetscCall(PetscCalloc1(1, &ctx_surface_force));

    // -- Populate context data
    ctx_surface_force->ratel      = ratel;
    ctx_surface_force->op         = ratel->op_surface_force[0];
    ctx_surface_force->dm_x       = ratel->dm_hierarchy[fine_level];
    ctx_surface_force->dm_y       = ratel->dm_surface_force;
    ctx_surface_force->X_loc      = ratel->X_loc_residual_u;
    ctx_surface_force->Y_loc      = ratel->F_loc;
    ctx_surface_force->x_loc_ceed = ratel->x_loc_ceed[fine_level];
    ctx_surface_force->y_loc_ceed = ratel->f_loc_ceed;
    ratel->ctx_surface_force      = ctx_surface_force;
  }

  // Compute quantities
  RatelDebug(ratel, "---- Computing surface forces");
  PetscCall(RatelUpdateBoundaryValues(ratel, time));
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    RatelDebug(ratel, "------ Face %" PetscInt_FMT, ratel->surface_force_dm_faces[i]);
    PetscScalar local_buffer[num_comp_force];

    ratel->ctx_surface_force->op = ratel->op_surface_force[i];
    PetscCall(RatelApplyLocalCeedOp(U, NULL, ratel->ctx_surface_force));
    PetscCall(VecStrideSumAll(ratel->ctx_surface_force->Y_loc, local_buffer));
    for (PetscInt j = 0; j < dim; j++) local_buffer[j] /= ratel->surface_force_face_area[i];
    PetscCall(MPIU_Allreduce(local_buffer, &surface_forces[num_comp_force * i], num_comp_force, MPIU_REAL, MPI_SUM, ratel->comm));
    for (PetscInt j = 0; j < dim; j++) surface_forces[num_comp_force * i + j] += ratel->surface_force_face_centroid[i][j];
    RatelDebug(ratel, "------   Centroid: [%f, %f, %f]", surface_forces[num_comp_force + 0], surface_forces[num_comp_force + 1],
               surface_forces[num_comp_force + 2]);
    RatelDebug(ratel, "------   Forces:   [%f, %f, %f]", surface_forces[num_comp_force + dim + 0], surface_forces[num_comp_force + dim + 1],
               surface_forces[num_comp_force + dim + 2]);
  }

  PetscFunctionReturn(0);
}

/// @}

/// @addtogroup RatelCore
/// @{

/**
  @brief Determine if Ratel context has MMS solution

  @param[in]   ratel    Ratel context
  @param[out]  has_mms  Boolan flag if Ratel context has MMS solution

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelHasMMS(Ratel ratel, PetscBool *has_mms) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Checking MMS Status");

  *has_mms = ratel->has_mms;
  RatelDebug(ratel, "---- MMS status: %s", *has_mms ? "has mms" : "no mms");

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Checking MMS status success!");
  PetscFunctionReturn(0);
}

/**
  @brief Compute the L2 error from the manufactured solution

  @param[in]   ratel     Ratel context
  @param[in]   U         Computed solution vector
  @param[out]  l2_error  Computed L2 error for solution compared to MMS

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeMMSL2Error(Ratel ratel, Vec U, PetscScalar *l2_error) {
  CeedScalar l2_norm_U  = 1.;
  PetscInt   fine_level = ratel->num_multigrid_levels - 1;
  Vec        E;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Compute MMS L2 Error");

  if (!ratel->has_mms) SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "No manufactured solution avaliable");

  // Work vectors
  RatelDebug(ratel, "---- Creating work vectors");
  PetscCall(VecDuplicate(U, &E));

  // Work vectors
  if (!ratel->ctx_error) {
    RatelDebug(ratel, "---- Creating operator context data");
    RatelOperatorApplyContext ctx_error;

    PetscCall(PetscCalloc1(1, &ctx_error));

    // -- Setup context for energy computation
    ctx_error->ratel      = ratel;
    ctx_error->op         = ratel->op_error;
    ctx_error->dm_x       = ratel->dm_hierarchy[fine_level];
    ctx_error->dm_y       = ratel->dm_hierarchy[fine_level];
    ctx_error->X_loc      = ratel->X_loc_residual_u;
    ctx_error->Y_loc      = ratel->Y_loc[fine_level];
    ctx_error->x_loc_ceed = ratel->x_loc_ceed[fine_level];
    ctx_error->y_loc_ceed = ratel->y_loc_ceed[fine_level];
    ratel->ctx_error      = ctx_error;
  }

  // Compute error
  RatelDebug(ratel, "---- Computing MMS error");
  PetscCall(RatelApplyLocalCeedOp(U, E, ratel->ctx_error));

  // Compute L2 error
  *l2_error = 1.0;
  PetscCall(VecNorm(E, NORM_1, l2_error));
  RatelDebug(ratel, "---- L2 Error: %f", *l2_error);
  PetscCall(VecNorm(U, NORM_2, &l2_norm_U));
  RatelDebug(ratel, "---- L2 U norm: %f", l2_norm_U);
  *l2_error /= l2_norm_U;
  RatelDebug(ratel, "---- L2 Error: %f", *l2_error);

  // Cleanup
  RatelDebug(ratel, "---- Cleaning up");
  PetscCall(VecDestroy(&E));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Compute MMS L2 Error Success!");

  PetscFunctionReturn(0);
}

/**
  @brief Retrieve expected strain energy, or `NULL` if no strain energy set

  @param[in]   ratel                   Ratel context
  @param[out]  expected_strain_energy  Expected strain energy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetExpectedStrainEnergy(Ratel ratel, PetscScalar *expected_strain_energy) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Expected Strain Energy");

  *expected_strain_energy = ratel->expected_strain_energy;
  RatelDebug(ratel, "---- expected strain energy: %f", *expected_strain_energy);

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Expected Strain Energy Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Compute the final strain energy in the computed solution

  @param[in]   ratel          Ratel context
  @param[in]   U              Computed solution vector
  @param[in]   time           Final time value, or 1.0 for SNES solution
  @param[out]  strain_energy  Computed strain energy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeStrainEnergy(Ratel ratel, Vec U, PetscScalar time, PetscScalar *strain_energy) {
  PetscScalar local_strain_energy = 0;
  PetscInt    fine_level          = ratel->num_multigrid_levels - 1;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Compute Strain Energy");

  // Work vectors
  if (!ratel->ctx_energy) {
    RatelDebug(ratel, "---- Creating operator context data");
    PetscInt                  loc_size;
    RatelOperatorApplyContext ctx_energy;

    PetscCall(PetscCalloc1(1, &ctx_energy));

    // -- Create vectors
    PetscCall(DMCreateLocalVector(ratel->dm_energy, &ratel->E_loc));
    PetscCall(VecGetSize(ratel->E_loc, &loc_size));
    RatelCeedCall(ratel, CeedVectorCreate(ratel->ceed, loc_size, &ratel->e_loc_ceed));

    // -- Setup context for energy computation
    ctx_energy->ratel      = ratel;
    ctx_energy->op         = ratel->op_energy;
    ctx_energy->dm_x       = ratel->dm_hierarchy[fine_level];
    ctx_energy->dm_y       = ratel->dm_energy;
    ctx_energy->X_loc      = ratel->X_loc_residual_u;
    ctx_energy->Y_loc      = ratel->E_loc;
    ctx_energy->x_loc_ceed = ratel->x_loc_ceed[fine_level];
    ctx_energy->y_loc_ceed = ratel->e_loc_ceed;
    ratel->ctx_energy      = ctx_energy;
  }

  // Compute strain energy
  RatelDebug(ratel, "---- Computing Strain Energy");
  PetscCall(RatelUpdateBoundaryValues(ratel, time));
  PetscCall(RatelApplyLocalCeedOp(U, NULL, ratel->ctx_energy));

  // Reduce max error
  PetscCall(VecSum(ratel->ctx_energy->Y_loc, &local_strain_energy));
  PetscCall(MPIU_Allreduce(&local_strain_energy, strain_energy, 1, MPIU_REAL, MPI_SUM, ratel->comm));
  RatelDebug(ratel, "---- strain energy: %f", *strain_energy);

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Compute Strain Energy Success!");
  PetscFunctionReturn(0);
}

/**
  @brief List mesh face numbers for diagnostic force computation

  @param[in]   ratel         Ratel context
  @param[out]  num_faces     Number of faces where forces are computed
  @param[out]  face_numbers  DMPlex mesh face numbers

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetSurfaceForceFaces(Ratel ratel, PetscInt *num_faces, const PetscInt **face_numbers) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Get Surface Force Faces");

  *num_faces    = ratel->surface_force_face_count;
  *face_numbers = (const PetscInt *)ratel->surface_force_dm_faces;

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Get Surface Force Faces Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Compute diagnostic quantities on mesh faces

  @param[in]   ratel           Ratel context
  @param[in]   U               Computed solution vector
  @param[in]   time            Final time value, or 1.0 for SNES solution
  @param[out]  surface_forces  Computed face forces - caller is responsible for freeing

  Note: The component `i` of the centroid for face `f` is at index `surface_forces[(2 * num_comp) * f + i]`.
        The component `i` of the force for face `f` is at index `surface_forces[(2 * num_comp + 1) * f + i]`.

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeSurfaceForces(Ratel ratel, Vec U, PetscScalar time, PetscScalar **surface_forces) {
  // TODO: Update for num_comp_u != 3
  PetscInt num_comp_u = 3;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Compute Surface Forces");

  // Work array
  RatelDebug(ratel, "---- Creating surface force array");
  PetscCall(PetscCalloc1(ratel->surface_force_face_count * 2 * num_comp_u, surface_forces));

  // Internal routine
  PetscCall(RatelComputeSurfaceForces_Internal(ratel, U, time, *surface_forces));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Compute Surface Forces Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Compute diagnostic quantities

  @param[in]   ratel  Ratel context
  @param[in]   U      Computed solution vector
  @param[in]   time   Final time value, or 1.0 for SNES solution
  @param[out]  D      Computed diagonstic quantities vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeDiagnosticQuantities(Ratel ratel, Vec U, PetscScalar time, Vec *D) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Compute Diagnostic Quantities");

  // Work vectors
  RatelDebug(ratel, "---- Creating diagnostic quantity vector");
  PetscCall(DMCreateGlobalVector(ratel->dm_diagnostic, D));
  PetscCall(PetscObjectSetName((PetscObject)*D, "diagnostic_quantities"));

  // Internal routine
  PetscCall(RatelComputeDiagnosticQuantities_Internal(ratel, U, time, *D));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel Compute Diagnostic Quantities Success!");
  PetscFunctionReturn(0);
}

/// @}
