/// @file
/// Implementation of Ratel solver monitor interfaces

#include <ceed.h>
#include <ceed/backend.h>
#include <petsc.h>
#include <petscdmplex.h>
#include <ratel-diagnostic.h>
#include <ratel-impl.h>
#include <ratel-monitor.h>
#include <ratel.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief RatelTSMonitor to allow multiple monitor outputs

  @param[in]  ts     TS object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding Ratel context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitor(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  Ratel ratel = (Ratel)ctx;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Monitor");

  // Selectively call RatelTSMonitor functions
  //  based upon if monitor viewers exist
  if (ratel->monitor_viewer_strain_energy) PetscCall(RatelTSMonitorStrainEnergy(ts, steps, time, U, ctx));
  if (ratel->monitor_viewer_surface_forces) PetscCall(RatelTSMonitorSurfaceForces(ts, steps, time, U, ctx));
  if (ratel->monitor_viewer_diagnostic_quantities) PetscCall(RatelTSMonitorDiagnosticQuantities(ts, steps, time, U, ctx));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Monitor Success!");
  PetscFunctionReturn(0);
}

/// @}

/// @addtogroup RatelCore
/// @{

/**
  @brief RatelTSMonitor function for strain energy

  @param[in]  ts     TS object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding Ratel context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorStrainEnergy(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  Ratel       ratel         = (Ratel)ctx;
  PetscScalar strain_energy = 0;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Monitor Strain Energy");

  PetscCall(RatelComputeStrainEnergy(ratel, U, time, &strain_energy));
  PetscCall(
      PetscViewerASCIIPrintf(ratel->monitor_viewer_strain_energy, "%" PetscInt_FMT " TS time %g strain energy %0.12e\n", steps, time, strain_energy));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Monitor Strain Energy Success!");
  PetscFunctionReturn(0);
}

/**
  @brief RatelTSMonitor function for surface forces

  @param[in]  ts     TS object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding Ratel context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorSurfaceForces(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  Ratel ratel = (Ratel)ctx;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Monitor Surface Forces");

  // Compute surface forces
  PetscScalar *surface_forces;
  PetscCall(RatelComputeSurfaceForces(ratel, U, time, &surface_forces));

  // View
  // TODO: Update for num_comp_u != 3
  PetscInt num_comp_u = 3;
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    PetscCall(PetscViewerASCIIPrintf(ratel->monitor_viewer_surface_forces,
                                     "%0.12e, %" PetscInt_FMT ", %0.12e, %0.12e, %0.12e, %0.12e, %0.12e, %0.12e\n", time,
                                     ratel->surface_force_dm_faces[i], surface_forces[2 * i * num_comp_u + 0], surface_forces[2 * i * num_comp_u + 1],
                                     surface_forces[2 * i * num_comp_u + 2], surface_forces[(2 * i + 1) * num_comp_u + 0],
                                     surface_forces[(2 * i + 1) * num_comp_u + 1], surface_forces[(2 * i + 1) * num_comp_u + 2]));
  }

  // Cleanup
  PetscCall(PetscFree(surface_forces));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Monitor Surface Forces Success!");
  PetscFunctionReturn(0);
}

/**
  @brief RatelTSMonitor function for diagnostic quantities

  @param[in]  ts     TS object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding Ratel context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorDiagnosticQuantities(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  Ratel ratel = (Ratel)ctx;

  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Monitor Diagnostic Quantities");

  // Cached diagnostics vector
  if (!ratel->D) {
    RatelDebug(ratel, "-- Creating diagnostic quantities vector");
    PetscCall(DMCreateGlobalVector(ratel->dm_diagnostic, &ratel->D));
    PetscCall(PetscObjectSetName((PetscObject)ratel->D, "diagnostic_quantities"));
  }

  // Compute and store diagnostic quantities
  PetscCall(RatelComputeDiagnosticQuantities_Internal(ratel, U, time, ratel->D));
  PetscCall(DMSetOutputSequenceNumber(ratel->dm_diagnostic, steps, time));

  // Update VTK/VTU output filename, if needed
  if (ratel->is_monitor_vtk) {
    char output_filename[PETSC_MAX_PATH_LEN];

    PetscCall(PetscSNPrintf(output_filename, sizeof output_filename, "%s_%" PetscInt_FMT ".%s", ratel->monitor_vtk_file_name, steps,
                            ratel->monitor_vtk_file_extension));
    PetscCall(PetscViewerFileSetName(ratel->monitor_viewer_diagnostic_quantities, output_filename));
  }

  // View
  PetscCall(VecView(ratel->D, ratel->monitor_viewer_diagnostic_quantities));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Monitor Diagnostic Quantities Success!");
  PetscFunctionReturn(0);
}

/**
  @brief Set Ratel TSMonitor function

  @param[in]  ratel    Ratel context
  @param[in]  ts       TS object to monitor
  @param[in]  monitor  Monitor function to set

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorSet(Ratel ratel, TS ts, RatelTSMonitorFunction monitor) {
  PetscFunctionBeginUser;
  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Monitor Set");

  // Set default ASCII monitor, if needed
  if (monitor != RatelTSMonitor) {
    PetscViewer *monitor_viewer = NULL;

    if (monitor == RatelTSMonitorStrainEnergy) monitor_viewer = &ratel->monitor_viewer_strain_energy;
    else if (monitor == RatelTSMonitorSurfaceForces) monitor_viewer = &ratel->monitor_viewer_surface_forces;
    else monitor_viewer = &ratel->monitor_viewer_diagnostic_quantities;

    if (!(*monitor_viewer)) {
      PetscCall(PetscViewerCreate(ratel->comm, monitor_viewer));
      PetscCall(PetscViewerSetType(*monitor_viewer, PETSCVIEWERASCII));
    }
  }

  // Set Ratel monitor
  PetscCall(TSMonitorSet(ts, RatelTSMonitor, ratel, NULL));

  RatelDebug256(ratel, RATEL_DEBUG_RED, "-- Ratel TS Monitor Set Success!");
  PetscFunctionReturn(0);
}

/// @}
