(constitutive-modeling)=

# Constitutive Modeling

In their most general form, constitutive models define stress, $\bm{S}$ ($\bm{\tau}$ in Eulerian coordinate), in terms of state variables.
For the materials used in Ratel, the state variables are constituted by the vector displacement field $\bm{u}$, and its gradient $\bm{H} = \nabla_X \bm{u}$.
We consider constitutive models for stress in terms of strain due to deformation, given by $\bm{S} \left( \bm{E} \right)$ ($\bm{\tau} \left( \bm{e} \right) $ in Eulerian coordinate), which is a tensor-valued function of a tensor-valued input.

:::{note}
The general Seth-Hill formula of strain measures in the Lagrangian and Eulerian coordinates are defined as

$$
  \begin{aligned}
    &\frac{1}{n} \left( \textbf{U}^n - \bm{I}_3 \right), \quad  &\frac{1}{n} \left( \textbf{v}^n - \bm{I}_3 \right), \quad \text{if} \quad n \neq 0,\\
    &\log \textbf{U}, \quad &\log \textbf{v}, \quad \text{if} \quad n = 0,
  \end{aligned}
$$

where $n$ is a real number and $\textbf{U}, \textbf{v}$ are unique SPD right (or material) and left (or spatial) stretch tensors defined by the *unique polar decomposition* of the deformation gradient {eq}`deformation-gradient` $\bm{F} = \textbf{R} \textbf{U} = \textbf{v} \textbf{R}$ with $\textbf{R}^T \textbf{R} = \bm{I}_3$ as the rotation tensor.

For the special case $n=2$, we define right and left Cauchy-Green tensors 

$$
  \bm{C} = \bm{F}^T \bm{F} = \textbf{U}^2, \quad \bm{b} = \bm{F} \bm{F}^T = \textbf{v}^2
$$

and Green-Lagrange and Green-Euler strains

$$
  \bm{E} = \frac{1}{2} \left( \bm{C} - \bm{I}_3 \right) = \frac{1}{2} \left( \bm{H} + \bm{H}^T + \bm{H}^T \bm{H} \right).
$$ (eq-green-lagrange-strain)

$$
  \bm{e} = \frac{1}{2} \left( \bm{b} - \bm{I}_3 \right) = \frac{1}{2} \left( \bm{H} + \bm{H}^T + \bm{H} \bm{H}^T \right).
$$ (eq-green-euler-strain)

:::


A physical model must not depend on the coordinate system chosen to express it; an arbitrary choice of function will generally not be invariant under orthogonal transformations and thus will not admissible.
Given an orthogonal transformation $Q$, we desire

$$
  Q \bm{S} \left( \bm{E} \right) Q^T = \bm{S} \left( Q \bm{E} Q^T \right),
$$ (elastic-invariance)

which means that we can change our reference frame before or after computing $\bm{S}$ and get the same result.
Constitutive relations in which $\bm{S}$ is uniquely determined by $\bm{E}$ while satisfying the invariance property {eq}`elastic-invariance` are known as Cauchy elastic materials.

For our constitutive relationships, we define a strain energy density functional $\psi \left( \bm{E} \right) \in \mathbb R$ ($\psi \left( \bm{e} \right) \in \mathbb R $) and obtain the strain energy from its gradient,

$$
  \bm{S} \left( \bm{E} \right) = \frac{\partial \psi}{\partial \bm{E}}, \quad \bm{\tau} \left( \bm{e} \right) = \frac{\partial \psi}{\partial \bm{e}} \bm{b}
$$ (strain-energy-grad)

:::{note}
The strain energy density functional cannot be an arbitrary function $\psi \left( \bm{E} \right)$.
It can only depend on *invariants*, scalar-valued functions $\gamma$ satisfying

$$
  \gamma \left( \bm{E} \right) = \gamma \left( Q \bm{E} Q^T \right)
$$

for all orthogonal matrices $Q$.
:::

```{toctree}
:caption: Material Models
:maxdepth: 4

Neo-Hookean Materials <neo-hookean>
Mooney-Rivlin Materials <mooney-rivlin>
```
