# Mooney-Rivlin Materials

While the Neo-Hookean model depends on just two scalar invariants, $\mathbb{I}_1 = \trace \bm{C} = 3 + 2 \trace \bm{E}$ and $J$, Mooney-Rivlin models depend on the additional invariant, $\mathbb{I}_2 = \frac 1 2 \left( \mathbb{I}_1^2 - \bm{C} \tcolon \bm{C} \right)$.
A coupled Mooney-Rivlin strain energy density (cf. Neo-Hookean {eq}`neo-hookean-energy`) is {cite}`holzapfel2000nonlinear`

$$
  \psi \left( \mathbb{I_1}, \mathbb{I_2}, J \right) = \frac{\lambda}{2} \left( \log J \right)^2 - \left( \mu_1 + 2\mu_2 \right) \log J + \frac{\mu_1}{2} \left( \mathbb{I_1} - 3 \right) + \frac{\mu_2}{2} \left( \mathbb{I_2} - 3 \right).
$$ (mooney-rivlin-energy_coupled)

We differentiate $\psi$ as in the Neo-Hookean case {eq}`neo-hookean-stress` to yield the second Piola-Kirchoff tensor,

$$
  \begin{aligned}
    \bm{S} &=  \lambda \log J \bm{C}^{-1} - \left( \mu_1 + 2\mu_2 \right) \bm{C}^{-1} + \mu_1\bm{I}_3 + \mu_2 \left( \mathbb{I_1} \bm{I}_3 - \bm{C} \right) \\
           &= \lambda \log J \bm{C}^{-1} + \mu_1 \left( \bm{I}_3 - \bm{C}^{-1} \right) + \mu_2 \left( \mathbb{I_1} \bm{I}_3 - 2 \bm{C}^{-1} - \bm{C} \right),
  \end{aligned}
$$ (mooney-rivlin-stress_coupled)

where we have used

$$
  \begin{aligned}
    \frac{\partial \mathbb{I_1}}{\partial \bm{E}} &= 2 \bm{I}_3, & \frac{\partial \mathbb{I_2}}{\partial \bm{E}} &= 2 \mathbb{I}_1 \bm{I}_3 - 2 \bm{C}, & \frac{\partial \log J}{\partial \bm{E}} &= \bm{C}^{-1}.
  \end{aligned}
$$ (None)

This is a common model for vulcanized rubber, with a shear modulus (defined for the small-strain limit) of $\mu_1 + \mu_2$ that should be significantly smaller than the first Lamé parameter $\lambda$.

:::{tip}
Similar to the Neo-Hookean materials, the stable form for the Mooney-Rivlin model in initial configuration {eq}`mooney-rivlin-stress_coupled` can be written as

$$
\bm{S} = \lambda \mathtt{log1p} \left(J-1\right) \bm{C}^{-1} + 2 \left( \mu_1 + 2\mu_2 \right)\bm{C}^{-1} \bm{E} + 2\mu_2 \left(\trace \left(\bm{E} \right) \bm{I}_3 - \bm{E} \right).
$$ (mooney-rivlin-stress-stable)
:::

The Kirchhoff stress tensor $\bm{\tau}$ for Mooney-Rivilin model is given by

$$
    \bm{\tau} = \bm{F}\bm{S}\bm{F}^T = \lambda \log J \bm{I}_{3} + \mu_1 \left( \bm{b} - \bm{I}_3 \right) + \mu_2 \left( \mathbb{I_1} \bm{b} - 2 \bm{I}_{3} - \bm{b}^2 \right).
$$ (mooney-rivlin-tau)

Note that $\mathbb{I_i}\left(\bm{b}\right) = \mathbb{I_i} \left(\bm{C} \right),$ for $i=1,2,3$.

:::{tip}
The stable Kirchhoff stress tensor version of {eq}`mooney-rivlin-tau` is given by

$$
\bm{\tau} = \lambda \mathtt{log1p} \left(J-1\right) \bm{I}_{3} + 2 \left( \mu_1 + 2\mu_2 \right)\bm{e} + 2\mu_2 \left(\trace \left(\bm{e}\right) \bm{I}_3 - \bm{e} \right) \bm{b}.
$$ (mooney-rivlin-tau-stable)
:::
