# Neo-Hookean Materials

In the *total Lagrangian* approach for the Neo-Hookean constitutive model, the model is formulated with respect to the initial configuration.
Our notation for the Neo-Hookean constitutive model is inspired by {cite}`holzapfel2000nonlinear` to distinguish between the current and initial configurations.
As explained in the {ref}`continuum-mechanics` section, we denote coordinates in the reference frame by capital letters and the current frame by lowercase letters.

For the constitutive modeling of Neo-Hookean hyperelasticity at finite strain, we will assume without loss of generality that $\bm{E}$ is diagonal and take its set of eigenvalues as the invariants.
It is clear that there can be only three invariants, and there are many alternate choices, such as $\operatorname{trace} \left( \bm{E} \right), \operatorname{trace}\left( \bm{E}^2 \right), \lvert \bm{E} \rvert$, and combinations thereof.
It is common in the literature for invariants to be taken from $\bm{C} = \bm{I}_3 + 2 \bm{E}$ instead of $\bm{E}$.

For example, if we take the compressible Neo-Hookean model,

$$
  \begin{aligned}
    \psi \left(\bm{E} \right) &= \frac{\lambda}{2} \left( \log J \right)^2 - \mu \log J + \frac \mu 2 \left( \operatorname{trace} \bm{C} - 3 \right) \\
                             &= \frac{\lambda}{2} \left( \log J \right)^2 - \mu \log J + \mu \operatorname{trace} \bm{E},
  \end{aligned}
$$ (neo-hookean-energy)

where $J = \lvert \bm{F} \rvert = \sqrt{\lvert \bm{C} \rvert}$ is the determinant of deformation (i.e., volume change {eq}`volume-ratio`) and $\lambda$ and $\mu$ are the Lamé parameters in the infinitesimal strain limit.

To evaluate the gradient of the strain energy density functional {eq}`strain-energy-grad`, we make use of

$$
  \frac{\partial J}{\partial \bm{E}} = \frac{\partial \sqrt{\lvert \bm{C} \rvert}}{\partial \bm{E}} = \lvert \bm{C} \rvert^{-1/2} \lvert \bm{C} \rvert \bm{C}^{-1} = J \bm{C}^{-1},
$$

where the factor of $\frac{1}{2}$ has been absorbed due to $\bm{C} = \bm{I}_3 + 2 \bm{E}.$
Carrying through the differentiation {eq}`strain-energy-grad` for the model {eq}`neo-hookean-energy`, we arrive at

$$
  \bm{S} = \lambda \log J \bm{C}^{-1} + \mu \left( \bm{I}_3 - \bm{C}^{-1} \right),
$$ (neo-hookean-stress)

which is the second Piola-Kirchoff tensor for Neo-Hookean materials.

:::{note}
One can linearize {eq}`neo-hookean-stress` around $\bm{E} = 0$, for which $\bm{C} = \bm{I}_3 + 2 \bm{E} \to \bm{I}_3$ and $J \to 1 + \operatorname{trace} \bm{E}$, therefore {eq}`neo-hookean-stress` reduces to

$$
\bm{S} = \lambda \left( \trace \bm{E} \right) \bm{I}_3 + 2 \mu \bm{E},
$$ (eq-st-venant-kirchoff)

which is the St. Venant-Kirchoff model (constitutive linearization without geometric linearization).
This model can be used for geometrically nonlinear mechanics (e.g., snap-through of thin structures), but is inappropriate for large strain.
:::

:::{tip}
In most of the constitutive models we have $\log J$ function with the condition number

$$
\kappa_f(J) = \frac{1}{\lvert \log J \rvert}
$$ (condition-number-logJ)

where in the case of nearly incompressible materials, {eq}`condition-number-logJ` becomes very large and thus *ill-conditioned*. 
In our simulation we use `log1p`, which is more numerically stable in case of $J\approx 1$.
To sketch this idea, suppose we have the $2\times 2$ non-symmetric matrix $\bm{F} = \left( \begin{smallmatrix} 1 + u_{0,0} & u_{0,1} \\ u_{1,0} & 1 + u_{1,1} \end{smallmatrix} \right)$.
Then we compute

$$
\log J = \mathtt{log1p} \left( u_{0,0} + u_{1,1} + u_{0,0} u_{1,1} - u_{0,1} u_{1,0} \right),
$$ (log1p)

which gives accurate results even in the limit when the entries $u_{i,j}$ are very small.

Another source of error in constitutive models is *loss of significance* which occurs in $\bm{I}_3 - \bm{C}^{-1}$ term. We use an equivalent form of {eq}`neo-hookean-stress` for Neo-Hookean materials as

$$
\bm{S} = \lambda \mathtt{log1p} \left(J-1\right) \bm{C}^{-1} + 2 \mu \bm{C}^{-1} \bm{E},
$$ (neo-hookean-stress-stable)

which is more numerically stable for small $\bm{E}$, and thus preferred for computation.
Note that the product $\bm{C}^{-1} \bm{E}$ is also symmetric, and that $\bm{E}$ should be computed using {eq}`eq-green-lagrange-strain`.
For example, if $u_{i,j} \sim 10^{-8}$, then naive computation of $\bm{I}_3 - \bm{C}^{-1}$ and $\log J$ will have a relative accuracy of order $10^{-8}$ in double precision and no correct digits in single precision.
When using the stable choices above, these quantities retain full $\varepsilon_{\text{machine}}$ relative accuracy.
:::

The Neo-Hookean stress relation can be represented in current configuration by pushing forward {eq}`neo-hookean-stress` using {eq}`S-to-tau`

$$
  \bm{\tau} = \bm{F}\bm{S}\bm{F}^T = \lambda \log J \bm{I}_{3} + \mu \left( \bm{b} - \bm{I}_{3} \right),
$$ (neo-hookean-tau)

One can arrive at the same relation {eq}`neo-hookean-tau` by expressing {eq}`neo-hookean-energy` in terms of current configuration invariant $\operatorname{trace} \bm{e}$ and use the second equation of {eq}`strain-energy-grad`.

:::{tip}
In our simulation we use the stable version of Kirchhoff stress tensor {eq}`neo-hookean-tau` as

$$
\bm{\tau} = \lambda \mathtt{log1p} \left(J-1\right) \bm{I}_{3} + 2 \mu \bm{e}.
$$ (neo-hookean-tau-stable)
:::
