# Numerical Formulations

Ratel implements three formulations for simulating solid mechanics materials.
The static hyperelastic formulation solves the steady-state static momentum balance equations using unstructured high-order finite/spectral element spatial discretizations. 
The quasistatic hyperelastic formulation solves the same set of steady-state equations but applies a constant to boundary conditions. 
The elastodynamics formulation applies time varying boundary conditions to the hyperelastic formulation.
The material point method projects the Lagrangian materials onto a background Eulerian finite element mesh to compute the material derivatives.

```{toctree}
:caption: Contents
:maxdepth: 4

Hyperelasticity <hyperelasticity>
Static and Quasistatic <static-and-quasistatic>
Elastodynamics <elastodynamics>
Material Point Method <material-point-method>
```
