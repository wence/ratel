# Hyperelasticity

Ratel solves the momentum balance equations using unstructured high-order finite/spectral element spatial discretizations.

:::{note}
Linear elasticity and small-strain hyperelasticity can both by obtained from the finite-strain hyperelastic formulation by linearization of geometric and constitutive nonlinearities.
The effect of these linearizations is sketched in the diagram below, where $\bm{\sigma}$ and $\bm{\epsilon}$ are stress and strain, respectively, in the small strain regime, while $\bm{S}$ and $\bm{E}$ are their finite-strain generalizations (second Piola-Kirchoff tensor and Green-Lagrange strain tensor, respectively) defined in the initial configuration, and $\mathsf{C}$ is a linearized constitutive model.

$$
  \begin{CD}
    {\overbrace{\bm{S} \left( \bm{E} \right)}^{\text{Finite Strain Hyperelastic}}}
    @>{\text{constitutive}}>{\text{linearization}}>
    {\overbrace{\bm{S} = \mathsf{C} \bm{E}}^{\text{St. Venant-Kirchoff}}} \\
    @V{\text{geometric}}V{\begin{smallmatrix}\bm{E} \to \bm{\epsilon} \\ \bm{S} \to \bm{\sigma} \end{smallmatrix}} V
    @V{\begin{smallmatrix}\bm{E} \to \bm{\epsilon} \\ \bm{S} \to \bm{\sigma} \end{smallmatrix}}V{\text{geometric}} V \\
    {\underbrace{\bm{\sigma} \left( \bm{\epsilon} \right)}_\text{Small Strain Hyperelastic}}
    @>{\text{constitutive}}>\text{linearization}>
    {\underbrace{\bm{\sigma} = \mathsf{C} \bm{\epsilon}}_\text{Linear Elastic}}
  \end{CD}
$$ (hyperelastic-cd)
:::


## Initial configuration

In the *total Lagrangian* approach for the Neo-Hookean hyperelasticity problem, the discrete equations are formulated with respect to the initial configuration.
In this formulation, we solve for displacement $\bm{u} \left( \bm{X} \right)$ in the reference frame $\bm{X}$.
The notation for elasticity at finite strain is inspired by {cite}`holzapfel2000nonlinear` to distinguish between the current and initial configurations.
As explained in the {ref}`continuum-mechanics` section, we denote by capital letters the reference frame and by small letters the current one.

### Weak form

We multiply balance of linear momentum explained in {ref}`continuum-mechanics` for the static case, by a test function $\bm{v}$ and integrate by parts to obtain the weak form for finite-strain hyperelasticity:
find $\bm{u} \in \mathcal{V} \subset H^1 \left( \Omega_0 \right)$ such that

$$
  \int_{\Omega_0}{\nabla_X \bm{v} \tcolon \bm{P}} \, dV
   - \int_{\Omega_0}{\bm{v} \cdot \rho_0 \bm{g}} \, dV
   - \int_{\partial \Omega_0}{\bm{v} \cdot \left( \bm{P} \cdot \hat{\bm{N}} \right)} \, dS
   = 0, \quad \forall \bm{v} \in \mathcal{V},
$$ (hyperelastic-weak-form-initial)

where $\bm{P} \cdot \hat{\bm{N}}|_{\partial\Omega}$ is replaced by any prescribed force/traction boundary condition written in terms of the initial configuration.
This equation contains material/constitutive nonlinearities in defining $\bm{S}(\bm{E})$, as well as geometric nonlinearities through $\bm{P} = \bm{F}\, \bm{S}$, $\bm{E}(\bm{F})$, and the body force $\bm{g}$, which must be pulled back from the current configuration to the initial configuration.
Discretization of {eq}`hyperelastic-weak-form-initial` produces a finite-dimensional system of nonlinear algebraic equations, which we solve using Newton-Raphson methods.
One attractive feature of Galerkin discretization is that we can arrive at the same linear system by discretizing the Newton linearization of the continuous form; that is, discretization and differentiation (Newton linearization) commute.

### Newton linearization

To derive a Newton linearization, we need the Jacobian form of the first term in {eq}`hyperelastic-weak-form-initial`:
find $\diff \bm{u} \in \mathcal{V}$ such that

$$
\int_{\Omega_0} \nabla_X \bm{v} \tcolon \diff \bm{P} dV = \text{rhs}, \quad \forall \bm{v} \in \mathcal{V},
$$

where

$$
  \diff \bm{P} = \frac{\partial \bm{P}}{\partial \bm{F}} \tcolon \diff \bm{F} = \diff \bm{F}\, \bm{S} + \bm{F} \underbrace{\frac{\partial \bm{S}}{\partial \bm{E}} \tcolon \diff \bm{E}}_{\diff \bm{S}}
$$ (eq-diff-P)

with $\diff \bm{F} = \nabla_X\diff \bm{u}$ and

$$
  \diff \bm{E} = \frac{\partial \bm{E}}{\partial \bm{F}} \tcolon \diff \bm{F} = \frac{1}{2} \left(\diff \bm{F}^T \bm{F} + \bm{F}^T \diff \bm{F} \right).
$$

The linearization of the second Piola-Kirchhoff stress tensor, $\diff \bm{S}$, depends upon the material model.

:::{dropdown} Deriving $\diff\bm{S}$ for Neo-Hookean material

For the Neo-Hookean model {eq}`neo-hookean-stress`,

$$
\diff \bm{S} = \frac{\partial \bm{S}}{\partial \bm{E}} \tcolon \diff \bm{E}
= \lambda \left( \bm{C}^{-1} \tcolon \diff \bm{E} \right) \bm{C}^{-1} + 2 \left( \mu - \lambda \log J \right) \bm{C}^{-1} \diff \bm{E} \, \bm{C}^{-1},
$$ (eq-neo-hookean-incremental-stress)

where we have used

$$
\diff \bm{C}^{-1} = \frac{\partial \bm{C}^{-1}}{\partial \bm{E}} \tcolon \diff \bm{E} = -2 \bm{C}^{-1} \diff \bm{E} \, \bm{C}^{-1} .
$$

The quantity ${\partial \bm{S}} / {\partial \bm{E}}$ is known as the incremental elasticity tensor, and is analogous to the linear elasticity tensor $\mathsf C$.
:::

:::{dropdown} $\diff \bm{S}$ in index notation

It is sometimes useful to express {eq}`eq-neo-hookean-incremental-stress` in index notation,

$$
\begin{aligned}
\diff \bm{S}_{IJ} &= \frac{\partial \bm{S}_{IJ}}{\partial \bm{E}_{KL}} \diff \bm{E}_{KL} \\
&= \lambda \left( \bm{C}^{-1}_{KL} \diff \bm{E}_{KL} \right) \bm{C}^{-1}_{IJ} + 2 \left( \mu - \lambda \log J \right) \bm{C}^{-1}_{IK} \diff \bm{E}_{KL} \bm{C}^{-1}_{LJ} \\
&= \underbrace{\left( \lambda \bm{C}^{-1}_{IJ} \bm{C}^{-1}_{KL} + 2 \left( \mu - \lambda \log J \right) \bm{C}^{-1}_{IK} \bm{C}^{-1}_{JL} \right)}_{\mathsf C_{IJKL}} \diff \bm{E}_{KL} ,
\end{aligned}
$$ (eq-neo-hookean-incremental-stress-index)

where we have identified the effective elasticity tensor $\mathsf C = \mathsf C_{IJKL}$.
It is generally not desirable to store $\mathsf C$, but rather to use the earlier expressions so that only $3 \times 3$ tensors (most of which are symmetric) must be manipulated. That is, given the linearization point $\bm F$ and solution increment $\diff \bm F = \nabla_X (\diff \bm u)$ (which we are solving for in the Newton step), we compute $\diff \bm P$ via

1. recover $\bm C^{-1}$ and $\log J$ (either stored at quadrature points or recomputed),
2. proceed with $3\times 3$ matrix products as in {eq}`eq-neo-hookean-incremental-stress` or the second line of {eq}`eq-neo-hookean-incremental-stress-index` to compute $\diff \bm S$ while avoiding computation or storage of higher order tensors, and
3. conclude by {eq}`eq-diff-P`, where $\bm S$ is either stored or recomputed from its definition exactly as in the nonlinear residual evaluation.
:::

:::{dropdown} Deriving $\diff\bm{S}$ for Mooney-Rivlin material

Similar to the linearization of the constitutive relationship for Neo-Hookean materials {eq}`eq-neo-hookean-incremental-stress`, we differentiate {eq}`mooney-rivlin-stress_coupled` using variational notation,

$$
\begin{aligned}
\diff \bm{S} &= \lambda \left( \bm{C}^{-1} \tcolon \diff \bm{E} \right) \bm{C}^{-1} \\
&\quad + 2 \left( \mu_1 + 2\mu_2 - \lambda \log J \right) \bm{C}^{-1} \diff \bm{E} \bm{C}^{-1} \\
&\quad + 2 \mu_2 \left( \trace \left( \diff \bm{E} \right) \bm{I}_3 - \diff \bm{E} \right) .
\end{aligned}
$$ (mooney-rivlin-dS-coupled)

Note that this agrees with the linearization of the Neo-Hookean constitutive relationship {eq}eq-neo-hookean-incremental-stress if $\mu_1 = \mu, \mu_2 = 0$.
Moving from Neo-Hookean to Mooney-Rivlin modifies the second term and adds the third.
:::

:::{dropdown} Cancellation vs symmetry

Some cancellation is possible (at the expense of symmetry) if we substitute {eq}`eq-neo-hookean-incremental-stress` into {eq}`eq-diff-P`,

$$
\begin{aligned}
\diff \bm P &= \diff \bm F\, \bm S
  + \lambda (\bm C^{-1} : \diff \bm E) \bm F^{-T} + 2(\mu - \lambda \log J) \bm F^{-T} \diff\bm E \, \bm C^{-1} \\
&= \diff \bm F\, \bm S
  + \lambda (\bm F^{-T} : \diff \bm F) \bm F^{-T} + (\mu - \lambda \log J) \bm F^{-T} (\bm F^T \diff \bm F + \diff \bm F^T \bm F) \bm C^{-1} \\
&= \diff \bm F\, \bm S
  + \lambda (\bm F^{-T} : \diff \bm F) \bm F^{-T} + (\mu - \lambda \log J) \Big( \diff \bm F\, \bm C^{-1} + \bm F^{-T} \diff \bm F^T \bm F^{-T} \Big),
\end{aligned}
$$ (eq-diff-P-dF)

where we have exploited $\bm F \bm C^{-1} = \bm F^{-T}$ and

$$
\begin{aligned} \bm C^{-1} \tcolon \diff \bm E = \bm C_{IJ}^{-1} \diff \bm E_{IJ} &= \frac 1 2 \bm F_{Ik}^{-1} \bm F_{Jk}^{-1} (\bm F_{\ell I} \diff \bm F_{\ell J} + \diff \bm F_{\ell I} \bm F_{\ell J}) \\ &= \frac 1 2 \Big( \delta_{\ell k} \bm F_{Jk}^{-1} \diff \bm F_{\ell J} + \delta_{\ell k} \bm F_{Ik}^{-1} \diff \bm F_{\ell I} \Big) \\ &= \bm F_{Ik}^{-1} \diff \bm F_{kI} = \bm F^{-T} \tcolon \diff \bm F. \end{aligned}
$$

We prefer to compute with {eq}`eq-neo-hookean-incremental-stress` because {eq}`eq-diff-P-dF` is more expensive, requiring access to (non-symmetric) $\bm F^{-1}$ in addition to (symmetric) $\bm C^{-1} = \bm F^{-1} \bm F^{-T}$, having fewer symmetries to exploit in contractions, and being less numerically stable.
:::

## Current configuration

In the preceding discussion, all equations have been formulated in the initial configuration.
This may feel convenient in that the computational domain is clearly independent of the solution, but there are some advantages to defining the equations in the current configuration.

1. Body forces (such as gravity), traction, and contact are more easily defined in the current configuration.
2. Mesh quality in the initial configuration can be very bad for large deformation.
3. The required storage and numerical representation can be smaller in the current configuration.

Most of the benefit in case 3 can be attained solely by moving the Jacobian representation to the current configuration {cite}`davydov2020matrix`, though residual evaluation may also be slightly faster in current configuration.
There are multiple commuting paths from the nonlinear weak form in initial configuration {eq}`hyperelastic-weak-form-initial` to the Jacobian weak form in current configuration {eq}`jacobian-weak-form-current`.
One may push forward to the current configuration and then linearize or linearize in initial configuration and then push forward, as summarized below.

$$
  \begin{CD}
    {\overbrace{\nabla_X \bm{v} \tcolon \bm{FS}}^{\text{Initial Residual}}}
    @>{\text{push forward}}>{}>
    {\overbrace{\nabla_x \bm{v} \tcolon \bm{\tau}}^{\text{Current Residual}}} \\
    @V{\text{linearize}}V{\begin{smallmatrix} \diff \bm{F} = \nabla_X\diff \bm{u} \\ \diff \bm{S} \left( \diff \bm{E} \right) \end{smallmatrix}}V
    @V{\begin{smallmatrix} \diff\nabla_x\bm{v} = -\nabla_x\bm{v} \nabla_x \diff \bm{u} \\ \diff \bm{\tau} \left( \diff \bm{\epsilon} \right) \end{smallmatrix}}V{\text{linearize}}V \\
    {\underbrace{\nabla_X\bm{v}\tcolon \left( \diff \bm{F}\bm{S} + \bm{F}\diff \bm{S} \right)}_\text{Initial Jacobian}}
    @>{\text{push forward}}>{}>
    {\underbrace{\nabla_x\bm{v}\tcolon \left( \diff \bm{\tau} -\bm{\tau} \left( \nabla_x \diff \bm{u} \right)^T \right)}_\text{Current Jacobian}}
\end{CD}
$$ (initial-current-linearize)

We will follow both paths for consistency and because both intermediate representations may be useful for implementation.

### Push forward, then linearize

The first term of {eq}`hyperelastic-weak-form-initial` can be rewritten in terms of the symmetric Kirchhoff stress tensor
$\bm{\tau} = J \bm{\sigma} = \bm{P} \bm{F}^T = \bm{F} \bm{S} \bm{F}^T$ as

$$
  \nabla_X \bm{v} \tcolon \bm{P} = \nabla_X \bm{v} \tcolon \bm{\tau} \bm{F}^{-T} = \nabla_X \bm{v} \bm{F}^{-1} \tcolon \bm{\tau} = \nabla_x \bm{v} \tcolon \bm{\tau}
$$

therefore, the weak form in terms of $\bm{\tau}$ and $\nabla_x$ with integral over $\Omega_0$ is

$$
  \int_{\Omega_0}{\nabla_x \bm{v} \tcolon \bm{\tau}} \, dV
   - \int_{\Omega_0}{\bm{v} \cdot \rho_0 \bm{g}} \, dV
   - \int_{\partial \Omega_0}{\bm{v} \cdot \left( \bm{P}\cdot\hat{\bm{N}} \right)} \, dS
   = 0, \quad \forall \bm{v} \in \mathcal{V}.
$$ (hyperelastic-weak-form-current)

#### Linearize in current configuration

To derive a Newton linearization of {eq}`hyperelastic-weak-form-current`, first we define

$$
  \nabla_x \diff \bm{u} = \nabla_X \diff \bm{u} \  \bm{F}^{-1} = \diff \bm{F} \bm{F}^{-1}
$$ (nabla_xdu)

Then by expanding the directional derivative of $\nabla_x \bm{v} \tcolon \bm{\tau}$, we arrive at

$$
\diff \ \left(\nabla_x \bm{v} \tcolon \bm{\tau} \right) = \diff \ \left( \nabla_x \bm{v} \right) \tcolon \bm{\tau} + \nabla_x \bm{v} \tcolon \diff \bm{\tau} .
$$ (hyperelastic-linearization-current1)

The first term of {eq}`hyperelastic-linearization-current1` can be written as

$$
  \begin{aligned}
    \diff \ (\nabla_x \bm{v})\tcolon \bm{\tau} &= \diff \ \left( \nabla_X \bm{v} \bm{F}^{-1} \right) \tcolon \bm{\tau} = \left( \underbrace{\nabla_X \left(\diff \bm{v}\right)}_{0}\bm{F}^{-1} +  \nabla_X \bm{v}\diff \bm{F}^{-1} \right) \tcolon \bm{\tau}\\
                                               &= \left( -\nabla_X \bm{v} \bm{F}^{-1}\diff \bm{F}\bm{F}^{-1} \right) \tcolon \bm{\tau} = \left( -\nabla_x \bm{v} \diff \bm{F}\bm{F}^{-1} \right) \tcolon \bm{\tau}\\
                                               &= \left( -\nabla_x \bm{v} \nabla_x \diff \bm{u} \right) \tcolon \bm{\tau} = -\nabla_x \bm{v}\tcolon\bm{\tau} \left( \nabla_x \diff \bm{u} \right)^T \,,
  \end{aligned}
$$

where we have used $\diff \bm{F}^{-1}=-\bm{F}^{-1} \diff \bm{F} \bm{F}^{-1}$ and {eq}`nabla_xdu`.
Using this and {eq}`hyperelastic-linearization-current1` in {eq}`hyperelastic-weak-form-current` yields the Jacobian form in the current configuration: find $\diff \bm{u} \in \mathcal{V}$ such that

$$
  \int_{\Omega_0} \nabla_x \bm{v} \tcolon \left( \diff \bm{\tau} - \bm{\tau} \left( \nabla_x \diff \bm{u} \right)^T \right) = \text{rhs}.
$$ (jacobian-weak-form-current)

:::{dropdown} Deriving $\diff\bm\tau$ for Neo-Hookean material

To derive a useful expression of $\diff\bm\tau$ for Neo-Hookean materials, we will use the representations

$$
\begin{aligned}
\diff \bm{b} &= \diff \bm{F} \bm{F}^T + \bm{F} \diff \bm{F}^T \\
&= \nabla_x \diff \bm{u} \ \bm{b} + \bm{b} \ (\nabla_x \diff \bm{u})^T \\
&= (\nabla_x \diff\bm u)(\bm b - \bm I_3) + (\bm b - \bm I_3) (\nabla_x \diff\bm u)^T + 2 \diff\bm\epsilon
\end{aligned}
$$

and

$$
\begin{aligned} \diff\ (\log J) &= \frac{\partial \log J}{\partial \bm{b}}\tcolon \diff \bm{b} = \frac{\partial J}{J\partial \bm{b}}\tcolon \diff \bm{b}=\frac{1}{2}\bm{b}^{-1}\tcolon \diff \bm{b} \\ &= \frac 1 2 \bm b^{-1} \tcolon \Big(\nabla_x \diff\bm u \ \bm b + \bm b (\nabla_x \diff\bm u)^T \Big) \\ &= \trace (\nabla_x \diff\bm u) \\ &= \trace \diff\bm\epsilon . \end{aligned}
$$

Substituting into {eq}`neo-hookean-tau` gives

$$
\begin{aligned}
\diff \bm{\tau} &= \mu \diff \bm{b} + \lambda \trace (\diff\bm\epsilon) \bm I_3 \\
&= \underbrace{2 \mu \diff\bm\epsilon + \lambda \trace (\diff\bm\epsilon) \bm I_3 - 2\lambda \log J \diff\bm\epsilon}_{\bm F \diff\bm S \bm F^T} \\
&\quad + (\nabla_x \diff\bm u)\underbrace{\Big( \mu (\bm b - \bm I_3) + \lambda \log J \bm I_3 \Big)}_{\bm\tau} \\
&\quad + \underbrace{\Big( \mu (\bm b - \bm I_3) + \lambda \log J \bm I_3 \Big)}_{\bm\tau}  (\nabla_x \diff\bm u)^T ,
\end{aligned}
$$ (dtau-neo-hookean)

where the final expression has been identified according to

$$
\diff\bm\tau = \diff\ (\bm F \bm S \bm F^T) = (\nabla_x \diff\bm u) \bm\tau + \bm F \diff\bm S \bm F^T + \bm\tau(\nabla_x \diff\bm u)^T.
$$

Collecting terms, we may thus opt to use either of the two forms

$$
\begin{aligned}
\diff \bm{\tau} -\bm{\tau}(\nabla_x \diff\bm{u})^T &= (\nabla_x \diff\bm u)\bm\tau + \bm F \diff\bm S \bm F^T \\
&= (\nabla_x \diff\bm u)\bm\tau + \lambda \trace(\diff\bm\epsilon) \bm I_3 + 2(\mu - \lambda \log J) \diff\bm\epsilon,
\end{aligned}
$$ (cur_simp_Jac)

with the last line showing the especially compact representation available for Neo-Hookean materials.
:::

:::{dropdown} Deriving $\diff\bm\tau$ for Mooney-Rivlin material

To derive a useful expression of $\diff\bm\tau$ for Mooney-Rivlin materials, we will use the representations of $\diff \bm{b}, \diff\ (\log J)$ as derived in previous section and

$$
\begin{aligned}
\diff \mathbb{I_1} &= \frac{\partial \mathbb{I_1}}{\partial \bm{b}}\tcolon \diff \bm{b} = \bm{I}_3 \tcolon \diff \bm{b} =  \operatorname{trace} (\diff \bm{b})\\
\end{aligned}
$$

Substituting into {eq}`mooney-rivlin-tau` gives

$$
\begin{aligned}
\diff \bm{\tau} &= \mu_1 \diff \bm{b} + \lambda \trace (\diff\bm\epsilon) \bm I_3 + \mu_2 \left(\operatorname{trace} (\diff \bm{b}) \bm{b} +  \mathbb{I_1}\diff \bm{b} - \bm{b}\diff \bm{b} - \diff \bm{b} \bm{b}\right)\\
&=2 \mu_1 \diff\bm\epsilon + \lambda \trace (\diff\bm\epsilon) \bm I_3 - 2\lambda \log J \diff\bm\epsilon + \left(\nabla_x \diff\bm u \right) \Big( \mu_1 (\bm b - \bm I_3) + \lambda \log J \bm I_3 \Big)\\
&\quad + \Big( \mu_1 (\bm b - \bm I_3) + \lambda \log J \bm I_3 \Big) (\nabla_x \diff\bm u)^T  + \mu_2 \operatorname{trace} (\diff \bm{b}) \bm{b} \\
&\quad + \mu_2\mathbb{I_1} \left(\nabla_x \diff\bm u \right) \bm{b}  +  \mu_2\mathbb{I_1} \bm{b} \left(\nabla_x \diff\bm u \right)^T - \mu_2 \left(\nabla_x \diff\bm u \right) \bm{b}^2 - \mu_2 \bm{b} \left(\nabla_x \diff\bm u \right)^T \bm{b} \\
&\quad - \mu_2 \bm{b} \left(\nabla_x \diff\bm u \right) \bm{b} - \mu_2 \bm{b}^2 \left(\nabla_x \diff\bm u \right)^T + 4\mu_2 \diff\bm\epsilon - 4\mu_2 \diff\bm\epsilon \\
&= \underbrace{\lambda \operatorname{trace}(\diff\bm \epsilon) \bm I_3 + 2 \left( \mu_1 + 2\mu_2 - \lambda \log J \right) \diff \bm{\epsilon} + \mu_2 \trace \left( \diff \bm{b} \right) \bm{b} - 2\mu_2 \bm{b} \diff \bm{\epsilon} \bm{b}}_{\bm F \diff\bm S \bm F^T} \\
&\quad + (\nabla_x \diff\bm u)\underbrace{\Big( \lambda \log J \bm{I}_{3} + \mu_1 \left( \bm{b} - \bm{I}_3 \right) + \mu_2 \left( \mathbb{I_1} \bm{b} - 2 \bm{I}_{3} - \bm{b}^2 \right) \Big)}_{\bm\tau} \\
&\quad + \underbrace{\Big( \lambda \log J \bm{I}_{3} + \mu_1 \left( \bm{b} - \bm{I}_3 \right) + \mu_2 \left( \mathbb{I_1} \bm{b} - 2 \bm{I}_{3} - \bm{b}^2 \right) \Big)}_{\bm\tau}  (\nabla_x \diff\bm u)^T ,
\end{aligned}
$$ (dtau-mooney-rivlin)

where the final expression has been identified according to

$$
\diff\bm\tau = \diff\ (\bm F \bm S \bm F^T) = (\nabla_x \diff\bm u) \bm\tau + \bm F \diff\bm S \bm F^T + \bm\tau(\nabla_x \diff\bm u)^T.
$$

Collecting terms, we may thus opt to use either of the two forms

$$
\begin{aligned}
\diff \bm{\tau} -\bm{\tau}(\nabla_x \diff\bm{u})^T &= (\nabla_x \diff\bm u)\bm\tau + \bm F \diff\bm S \bm F^T \\
&= (\nabla_x \diff\bm u)\bm\tau + \lambda \operatorname{trace}(\diff\bm \epsilon) \bm I_3 + 2 \left( \mu_1 + 2\mu_2 - \lambda \log J \right) \diff \bm{\epsilon} \\
&\quad + \mu_2 \trace \left( \diff \bm{b} \right) \bm{b} - 2\mu_2 \bm{b} \diff \bm{\epsilon} \bm{b}.\\
\end{aligned}
$$ (cur_simp_Jac_mooney-rivlin)

with the last line showing the especially compact representation available for Mooney-Rivlin materials.

:::

### Linearize, then push forward

We can move the derivatives to the current configuration via

$$
  \nabla_X \bm{v} \tcolon \diff \bm{P} = \left( \nabla_X \bm{v} \right) \bm{F}^{-1} \tcolon \diff \bm{P} \bm{F}^T = \nabla_x \bm{v} \tcolon \diff \bm{P} \bm{F}^T
$$

and expand

$$
  \begin{aligned}
    \diff \bm{P} \bm{F}^T &= \diff \bm{F} \bm{S} \bm{F}^T + \bm{F} \diff \bm{S} \bm{F}^T \\
                          &= \underbrace{\diff \bm{F} \bm{F}^{-1}}_{\nabla_x \diff \bm{u}} \underbrace{\bm{F} \bm{S} \bm{F}^T}_{\bm{\tau}} + \bm{F} \diff \bm{S} \bm{F}^T .
  \end{aligned}
$$

:::{dropdown} Representation of $\bm F \diff\bm S \bm F^T$ for Neo-Hookean materials

Now we push {eq}`eq-neo-hookean-incremental-stress` forward via

$$
\begin{aligned}
\bm F \diff\bm S \bm F^T &= \lambda (\bm C^{-1} \tcolon \diff\bm E) \bm F \bm C^{-1} \bm F^T
  + 2 (\mu - \lambda \log J) \bm F \bm C^{-1} \diff\bm E \, \bm C^{-1} \bm F^T \\
    &= \lambda (\bm C^{-1} \tcolon \diff\bm E) \bm I_3 + 2 (\mu - \lambda \log J) \bm F^{-T} \diff\bm E \, \bm F^{-1} \\
    &= \lambda \operatorname{trace}(\nabla_x \diff\bm u) \bm I_3 + 2 (\mu - \lambda \log J) \diff\bm \epsilon
\end{aligned}
$$

where we have used

$$
\begin{aligned}
\bm C^{-1} \tcolon \diff\bm E &= \bm F^{-1} \bm F^{-T} \tcolon \bm F^T \diff\bm F \\
&= \operatorname{trace}(\bm F^{-1} \bm F^{-T} \bm F^T \diff \bm F) \\
&= \operatorname{trace}(\bm F^{-1} \diff\bm F) \\
&= \operatorname{trace}(\diff \bm F \bm F^{-1}) \\
&= \operatorname{trace}(\nabla_x \diff\bm u)
\end{aligned}
$$

and

$$
\begin{aligned}
\bm F^{-T} \diff\bm E \, \bm F^{-1} &= \frac 1 2 \bm F^{-T} \left(\bm F^T \diff\bm F + \diff\bm F^T \bm F\right) \bm F^{-1} \\
&= \frac 1 2 \left(\diff \bm F \bm F^{-1} + \bm F^{-T} \diff\bm F^T\right) \\
&= \frac 1 2 \Big(\nabla_x \diff\bm u + (\nabla_x\diff\bm u)^T \Big) \equiv \diff\bm\epsilon.
\end{aligned}
$$

Collecting terms, the weak form of the Newton linearization for Neo-Hookean materials in the current configuration is

$$
\int_{\Omega_0} \nabla_x \bm v \tcolon \Big( (\nabla_x \diff\bm u) \bm\tau + \lambda \operatorname{trace}(\diff\bm\epsilon)\bm I_3 + 2(\mu - \lambda\log J)\diff \bm\epsilon \Big) dV = \text{rhs},
$$ (jacobian-weak-form-current2)

which equivalent to Algorithm 2 of {cite}`davydov2020matrix` and requires only derivatives with respect to the current configuration. Note that {eq}`cur_simp_Jac` and {eq}`jacobian-weak-form-current2` have recovered the same representation
using different algebraic manipulations.
:::

:::{dropdown} Representation of $\bm F \diff\bm S \bm F^T$ for Mooney-Rivlin materials
We push forward $\diff\bm S$ {eq}`mooney-rivlin-dS-coupled` which yields to 

$$
\begin{aligned}
\bm F \diff\bm S \bm F^T &= \lambda (\bm C^{-1} \tcolon \diff\bm E) \bm F \bm C^{-1} \bm F^T
  + 2 \left( \mu_1 + 2\mu_2 - \lambda \log J \right) \bm F \bm C^{-1} \diff\bm E \, \bm C^{-1} \bm F^T \\
    &\quad + 2 \mu_2 \left( \trace \left( \diff \bm{E} \right) \bm{b} - \bm{F} \diff \bm{E} \bm{F}^T \right) \\
    &= \lambda \operatorname{trace}(\diff\bm \epsilon) \bm I_3 + 2 \left( \mu_1 + 2\mu_2 - \lambda \log J \right) \diff \bm{\epsilon} \\
    &\quad + 2 \mu_2 \left( \frac{1}{2} \trace \left( \diff \bm{b} \right) \bm{I}_3 - \bm{b} \diff \bm{\epsilon} \right)\bm{b}
\end{aligned}
$$

where we have used

$$
\begin{aligned}
\operatorname{trace}\left(\diff\bm E \right)&= \operatorname{trace}\left(\bm{F}^T \diff\bm{F} \right) = \operatorname{trace}\left(\bm{F} \diff\bm{F}^T \right) \\
&= \operatorname{trace}\left(\diff\bm F \bm{F}^T \right) = \frac{1}{2}\operatorname{trace}\left(\diff \bm b \right) \\
\end{aligned}
$$

and

$$
\begin{aligned}
\bm{F} \diff\bm{E} \, \bm F^{T} &= \frac{1}{2} \bm{F} \left(\bm{F}^T \diff\bm{F} + \diff\bm{F}^T \bm{F} \right) \bm{F}^{T} \\
&= \frac{1}{2} \left(\bm{F}\bm{F}^T \diff\bm{F}\bm{F}^{-1}\bm{F}\bm{F}^{T} + \bm{F}\bm{F}^{T}\bm{F}^{-T}\diff\bm{F}^T \bm{F}\bm{F}^T \right) \\
&= \frac 1 2 \bm{b}\Big(\nabla_x \diff\bm u + (\nabla_x\diff\bm u)^T \Big)\bm{b} =\bm{b} \, \diff\bm\epsilon \, \bm{b}.
\end{aligned}
$$

Collecting terms, we arrive at

$$
  \begin{aligned}
    \diff \bm{P} \bm{F}^T &= \left(\nabla_x \diff \bm{u} \right) \bm{\tau} + \bm{F} \diff \bm{S} \bm{F}^T \\
    &= \left( \nabla_x \diff \bm{u} \right) \bm{\tau} + \lambda \operatorname{trace}(\diff\bm \epsilon) \bm I_3 + 2 \left( \mu_1 + 2\mu_2 - \lambda \log J \right) \diff \bm{\epsilon} \\
    &\quad + \mu_2 \trace \left( \diff \bm{b} \right) \bm{b} - 2 \mu_2 \bm{b} \diff \bm{\epsilon} \bm{b}
  \end{aligned}
$$ (dPFT_mooney-rivlin)

Note that {eq}`cur_simp_Jac_mooney-rivlin` and {eq}`dPFT_mooney-rivlin` have recovered the same representation
using different algebraic manipulations.
:::
