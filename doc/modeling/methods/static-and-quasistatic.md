# Quasistatic formulation 

This formulation solves the steady-state equations as described in [hyperelasticity](hyperelasticity.md) but with updates to the time parameter in the boundary conditions. 
Ratel also offers a static solve option via a PETSc SNES solve; this is covered in the [static elasticity example](example-static).
The [quasistatic example](example-quasistatic) uses the PETSc Timestepper (TS) object to manage pseudo-timestepping. 

Large deformation solid mechanics exhibits both geometric and material nonlinearities, leading to path dependence by which there are be multiple static solutions for a specified set of boundary conditions.
To disambiguate the multiple solutions, we solve the hyperelastic problem as a non-autonomous differential algebraic equation of index 1, with boundary conditions/loading a function of time $t \in [0, 1]$.
The current quasistatic example uses applied load (rather than displacement) and use backward Euler from PETSc's TS with extrapolation-based hot starts disabled for simplicity.

Each pseudo time step requires a nonlinear solve, which is implemented using PETSc's Scalable Nonlinear Equations Solver (SNES).
By default, Ratel uses Newton-CG in which a multigrid V-cycle is used as a preconditioner for conjugate gradients.
Additionally, we use a ``critical point'' line search, which supposes that the residual is the functional gradient of a latent objective function, $\bm F(\bm u) = \nabla_{\bm u}\Psi(\bm u)$ and uses one step of a secant method to find $\alpha$ for which $F(\bm u + \alpha \delta \bm u)^{T} \delta \bm u = 0$ where $\delta \bm u$ is the search direction found by Newton.
This line search is inspired by the strong Wolfe conditions in optimization {cite}`NW99`, but without explicit evaluation of the objective $\Psi$, which may not be available or may not exist (e.g., for non-conservative models).

More information on the PETSc TS and SNES objects can be found in the [PETSc documentation](https://petsc.org/release).
