# Elastodynamics

This formulation solves the momentum balance equations as described in [hyperelasticity](hyperelasticity.md).
The [dynamic example](example-dynamic) uses the PETSc Timestepper (TS) object to manage timestepping. 

Specifically, Ratel uses the Generalized-Alpha method for second order systems (`TSALPHA2`) to solve the second order system of equations given by the momentum balance.
More information on the PETSc `TSALPHA2` time stepper can be found in the [PETSc documentation](https://petsc.org/release/docs/manualpages/TS/index.html).
