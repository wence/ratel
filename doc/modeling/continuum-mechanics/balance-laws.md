# Balance laws

Mathematical models for continuum mechanics rely upon governing laws that ensure the balance of mass and momentum in the system being modeled.
Below we provide the equations for balance of mass and linear momentum used in the material methods section.

## Lagrangian view

| Law                         | Formulation                                                                           |
|-----------------------------|---------------------------------------------------------------------------------------|
| Balance of Mass             | $\rho J - \rho_0 = 0$                                                                 |
| Balance of Linear Momentum  | $\rho_0 \frac{\partial}{\partial t} \bm{V} - \nabla_X \cdot \bm{P} - \rho_0 \bm{g} = 0$ |

$\bm{P}$ is the *first Piola-Kirchhoff stress tensor*, $\bm{V}$ is the Lagrangian velocity defined in {eq}`vel-acc-lagrangian`, and $\bm{g}$ is the gravitational acceleration.
$\rho$ and $\rho_0$ denote the mass density and initial mass density, respectively.

The *first Piola-Kirchhoff stress tensor* can be decomposed as

$$
  \bm{P} = \bm{F} \bm{S},
$$ (first-piola-kirchhoff)

where $\bm{S}$ is the *second Piola-Kirchhoff stress tensor*, given by the constitutive model for the material,

$$
  \bm{F} = \frac{\partial \bm x}{\partial \bm X} = \bm{I}_3 + \bm{H},
$$ (deformation-gradient)

is the *deformation gradient* with $\bm{H} = \nabla_X \bm{u}$, displacement $\bm{u} = \bm x - \bm X$ from the initial configuration, and

$$
  J = |\bm{F}|
$$ (volume-ratio)

is the infinitesimal volume ratio between current and initial configuration.

## Eulerian view

| Law                         | Formulation                                                                  |
|-----------------------------|------------------------------------------------------------------------------|
| Balance of Mass             | $\frac{\partial}{\partial t} \rho + \nabla \cdot (\rho \bm{v}) = 0$                      |
| Balance of Linear Momentum  | $\rho \frac{D}{D t} \bm{v} - \nabla \cdot \bm{\sigma} - \rho \bm{g} = 0$ |

where $\bm{v}$ is the Eulerian velocity {eq}`vel-eulerian` and the *Cauchy stress tensor*, given by

$$
  \bm{\sigma} = \frac{1}{J} \bm{P} \bm{F}^T = \frac 1 J \underbrace{\bm F \bm S \bm F^T}_{\bm\tau}.
$$ (S-to-tau)

where the *symmetric Kirchhoff stress tensor* $\bm\tau$ is the push-forward of the second Piola-Kirchoff tensor $\bm S$.
Note that $\bm S$, $\bm\sigma$, and $\bm\tau$ are all symmetric.

:::{note}
The *balance of linear momentum* can be written in the equivalent form 

$$
  \frac{\partial}{\partial t} \left(\rho \bm{v} \right) + \nabla \cdot \left( \rho \bm{v} \otimes \bm{v} \right) - \nabla \cdot \bm{\sigma} - \rho \bm{g} = 0.
$$ (equivalent-balance-linear-momentum)

by expanding $\rho \frac{D}{D t} \bm{v}$ using {eq}`acc-eulerian` as

$$
  \begin{aligned}
    \rho \frac{D}{D t} \bm{v} &= \rho \frac{\partial \bm{v}}{\partial t} + \rho \left( \nabla \bm{v} \right) \bm{v} = \frac{\partial \left( \rho \bm{v} \right)}{\partial t} - \frac{\partial \rho}{\partial t} \bm{v} + \left( \nabla \bm{v} \right) \left(\rho \bm{v} \right) \\
                              &= \frac{\partial \left( \rho \bm{v} \right)}{\partial t} + \bm{v} \nabla \cdot \left( \rho \bm{v} \right) + \left( \nabla \bm{v} \right) \left(\rho \bm{v} \right) \\
                              &= \frac{\partial \left( \rho \bm{v} \right)}{\partial t} + \nabla \cdot \left( \rho \bm{v} \otimes \bm{v} \right)
  \end{aligned}
$$ 

where we have used the identity $\nabla \cdot \left( \bm{u} \otimes \bm{v} \right) = \bm{u} \nabla \cdot \bm{v} + \left( \nabla \bm{u} \right) \bm{v}$.

:::