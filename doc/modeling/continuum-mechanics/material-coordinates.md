# Material coordinates

Consider a continuum body $\Omega$ which is embedded in the 3D Euclidean space at a given time $t$. As the continuum body $\Omega$ moves in space from one instant of time to another, it occupies a sequence of geometrical regions denoted by $\Omega_0, \dotsc, \Omega$. We can measure the position vector of point $P \in \Omega_0$ at time $t=0$ with coordinate $\bm{X}$, which is referred to initial (or undeformed) configuration. On the other hand, when $\Omega_0$ moves to $\Omega$ at time $t$, we measure the position vector of point $p \in \Omega$ by $\bm{x}$ which is known as current (or deformed) configuration. The Lagrangian view of the material is based on the initial configuration while the Eulerian view of the material is based in the current configuration.

## Lagrangian view

Consider $\bm{\chi}$ as a vector field motion that moves body $\Omega_0$ to $\Omega$. In the Lagrangian view, velocity and acceleration functions are given by

$$
  \begin{aligned}
    \bm{V} \left( \bm{X}, t \right) &= \frac{\partial \bm{\chi}}{\partial t} \left( \bm{X}, t \right), \\
    \bm{A} \left( \bm{X}, t \right) &= \frac{\partial^2 \bm{\chi}}{\partial t^2} \left( \bm{X}, t \right).
  \end{aligned}
$$ (vel-acc-lagrangian)

## Eulerian view

It is natural to assume that the mapping from the initial configuration to the current configuration is smooth, as we assume in continuum mechanics that no two particles occupy the same space at the same time.
Therefore, can 'push forward' these velocity and acceleration functions to the current configuration by using the inverse mapping

$$
  \bm{X} = \bm{\chi}^{-1} \left( \bm{x}, t \right).
$$

Velocity is relatively straightforward to compute

$$
  \bm{v} \left( \bm{x}, t \right) = \bm{V} \left( \bm{\chi}^{-1} \left( \bm{x}, t \right), t \right) = \frac{\partial \bm{\chi}}{\partial t} \left( \bm{\chi}^{-1} \left( \bm{x}, t \right), t \right),
$$ (vel-eulerian)

while acceleration is a more involved computation

$$
  \begin{aligned}
    \bm{a} \left( \bm{x}, t \right) &= \bm{A} \left( \bm{\chi}^{-1} \left( \bm{x}, t \right), t \right) \\
                          &= \frac{\partial \bm{v}}{\partial t} \left( \bm{x}, t \right) + \frac{\partial \bm{v}}{\partial \bm{x}} \left( \bm{x}, t \right) \bm{v} \left( \bm{x}, t \right).
  \end{aligned}
$$

Note that this leads to the somewhat surprising result in the Eulearian view that

$$
  \bm{a} \left( \bm{x}, t \right) \neq \frac{\partial \bm{v}}{\partial t} \left( \bm{x}, t \right).
$$

Also note that we can 'pull back' to the Lagrangian view via $\bm{x} = \bm{\chi} \left(\bm{X}, t  \right)$ as

$$
  \begin{aligned}
    \bm{V} \left( \bm{X}, t \right) &= \bm{v} \left( \bm{\chi} \left( \bm{X}, t \right), t \right), \\
    \bm{A} \left( \bm{X}, t \right) &= \bm{a} \left( \bm{\chi} \left( \bm{X}, t \right), t \right).
  \end{aligned}
$$

## Material derivative

The relationship between $\bm{v} \left( \bm{x}, t \right)$ and $\bm{a} \left( \bm{x}, t \right)$ is common in our derivations and is known as the material derivative.

$$
  \bm{a} \left( \bm{x}, t \right) = \frac{D}{D t} \bm{v} \left( \bm{x}, t \right) =  \frac{\partial \bm{v}}{\partial t} \left( \bm{x}, t \right) + \frac{\partial \bm{v}}{\partial \bm{x}} \left( \bm{x}, t \right) \bm{v} \left( \bm{x}, t \right)
$$ (acc-eulerian)

For an Eulerian function $\bm{f} \left( \bm{x}, t \right)$ with a corresponding Lagrangian function $\bm{F} \left( \bm{X}, t \right)$, the material derivative $\frac{D}{D t} \bm{f}$ arises from pushing forward the derivative $\frac{\partial}{\partial t} \bm{F}$.
