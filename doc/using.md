(using)=
# Using Ratel

The Ratel library includes support for processing inputs and handling command-line options.
Most examples and applications using Ratel will inherit these options. 

## Command Line Options

Ratel is controlled via command-line options.
These command line options may be stored in a YML file specified by the runtime option `-options_file`.
The following command line options are mandatory:

:::{list-table} Mandatory Runtime Options
:header-rows: 1
:widths: 3 7

* - Option
  - Description
* - `-dm_plex_filename [filename]`
  - Path to mesh file in any format supported by PETSc. Alternatively, a built-in mesh, such as `-dm_plex_box` may be specified.
* - `-bc_clamp [int list]`
  - List of face sets on which to displace by `-bc_clamp_[facenumber]_translate [x,y,z]` and/or `bc_clamp_[facenumber]_rotate [rx,ry,rz,c_0,c_1]`.
    Note: The default for a clamped face is zero displacement. All displacement is with respect to the initial configuration. At least one clamped boundary is required.
:::

:::{note}
This solver can use any mesh format that PETSc's `DMPlex` can read (Exodus, Gmsh, Med, etc.).
Our tests have primarily been using Exodus meshes created using [CUBIT]; sample meshes used for the example runs suggested here can be found in [this repository].
Note that many mesh formats require PETSc to be configured appropriately; e.g., `--download-exodusii` for Exodus support.
:::

Consider the specific example of the mesh seen below:

```{image} https://github.com/jeremylt/ceedSampleMeshes/raw/master/cylinderDiagram.png
```

With the sidesets defined in the figure, we provide here an example of a minimal set of command line options:

```console
$ ./bin/ratel-quasistatic -dm_plex_filename [.exo file] -order 4 -E 1e6 -nu 0.3 -bc_clamp 998,999 -bc_clamp_998_translate 0,-0.5,1
```

In this example, we set the left boundary, face set $999$, to zero displacement and the right boundary, face set $998$, to displace $0$ in the $x$ direction, $-0.5$ in the $y$, and $1$ in the $z$.

As an alternative to specifying a mesh with {code}`-dm_plex_filename`, the user may use a DMPlex box mesh by specifying {code}`-dm_plex_box_faces [int list]`, {code}`-dm_plex_box_upper [real list]`, and {code}`-dm_plex_box_lower [real list]`.

As an alternative example exploiting {code}`-dm_plex_box_faces`, we consider a {code}`4 x 4 x 4` mesh where essential (Drichlet) boundary condition is placed on the top and bottom.
Side 1 is held in place while side 2 is rotated around $x$-axis:

```console
$ ./bin/ratel-quasistatic -model elasticity-neo-hookean-initial -E 1 -nu 0.3 -dm_plex_dim 3 -dm_plex_simplex 0 -dm_plex_box_faces 4,4,4 -bc_clamp 1,2 -bc_clamp_2_rotate 0,0,1,0,.05
```

:::{note}
If the coordinates for a particular side of a mesh are zero along the axis of rotation, it may appear that particular side is clamped zero.
:::

On each boundary node, the rotation magnitude is computed: {code}`theta = (c_0 + c_1 * cx) * loadIncrement` where {code}`cx = kx * x + ky * y + kz * z`, with {code}`kx`, {code}`ky`, {code}`kz` are normalized values.

The command line options just shown are the minimum requirements to run the application, but additional options may also be set as follows

:::{list-table} Additional Runtime Options
:header-rows: 1

* - Option
  - Description
  - Default value

* - `-ceed [string]`
  - CEED resource specifier
  - `/cpu/self`

* - `-options_file [filename]`
  - Filepath to yml file with runtime options
  - `/path/to/my.yml`

* - `-order [int list]`
  - Polynomial order of solution basis functions, or an array of orders for each multigrid level
  - `3`

* - `-coarse_order [int]`
  - Polynomial order of coarse grid basis functions
  - `1`

* - `-q_extra [int]`
  - Increased quadrature space order; final order given by `order[0] + q_extra`
  - `0`

* - `-q_extra_surface_force [int]`
  - Increased quadrature space order for surface force computation; final order given by `order[0] + q_extra_surface_force`
  - `1`

* - `surface_force_faces [int list]`
  - List of face sets on which to compute surface force
  - 

* - `-diagnostic_order [int]`
  - Order for diagnostic values mesh
  - Same value as multigrid fine level order specified via `-order`

* - `-diagnostic_geometry_order [int]`
  - Geometry order for diagnostic values mesh
  -

* - `-bc_clamp [int list]`
  - List of face sets on which to displace by `-bc_clamp_[facenumber]_translate [x,y,z]` and/or `bc_clamp_[facenumber]_rotate [rx,ry,rz,c_0,c_1]`.
    Note: The default for a clamped face is zero displacement.
  -

* - `-bc_traction [int list]`
  - List of face sets on which to set traction boundary conditions with the traction vector `-bc_traction_[facenumber] [tx,ty,tz]`, with components given with respect to the global coordinate system
  -

* - `-bc_slip [int list]`
  - List of face sets on which to set slip boundary conditions for the components `-bc_slip_[facenumber]_components [int list]`
  -

* - `-model`
  - Material model to use (`elasticity-linear`, `elasticity-neo-hookean-current`, `elasticity-mooney-rivlin-initial`, etc.)
  - `elasticity-linear`

* - `-forcing`
  -  Forcing term option (`none`, `constant`, or `linear-mms`)
  - `none`

* - `-forcing_vec [real list]`
  -  Forcing vector
  - `0,-1,0`

* - `-multigrid`
  - Multigrid coarsening to use (`p_logarithmic`, `p_uniform`, `p_user`, `amg_only`, or `none`)
  - `logarithmic`

* - `expected_strain_energy [real]`
  - Expected strain energy, for testing
  -

* - `-view_diagnostic_quantities viewer:filename.extension`
  - Output final solution for viewing, ASCII format to STDOUT is used if no viewer is passed
  -

* - `-ts_monitor_strain_energy`
  - Output computed strain energy on each time step
  -

* - `-ts_monitor_diagnostic_quantities viewer:filename.extension`
  - Output final solution for viewing, ASCII format to STDOUT is used if no viewer is passed
  -

* - `-ts_monitor_surface_force`
  - Output computed surface forces for faces given by `-surface_force_faces` on each time step
  -

* - `-ts_view`
  - View PETSc `TS` time steper configuration
  -

* - `-snes_view`
  - View PETSc `SNES` nonlinear solver configuration
  -

* - `-log_view`
  - View PETSc performance log
  -

* - `-help`
  - View comprehensive information about run-time options
  -
:::

To verify the convergence of the linear elasticity formulation on a given mesh with the method of manufactured solutions, run:

```console
$ ./bin/ratel-quasistatic -dm_plex_filename [mesh] -order [order] -model elasticity-linear -nu [nu] -E [E] -forcing linear-mms
```

This option attempts to recover a known solution from an analytically computed forcing term.

## Material Properties

Each material model has properties that need to be specified.
All properties are mandatory.

:::{list-table} Material Property Options
:header-rows: 1

* - Option
  - Description
  - Model

* - `-E [real]`
  - [Young's modulus](https://en.wikipedia.org/wiki/Young%27s_modulus), $E > 0$
  - Neo-Hookean

* - `-nu [real]`
  - [Poisson's ratio](https://en.wikipedia.org/wiki/Poisson%27s_ratio), $\nu < 0.5$
  - Neo-Hookean or Mooney-Rivlin

* - `-nu_smoother [real]`
  - Poisson's ratio for multigrid smoothers, $\nu < 0.5$
  - Neo-Hookean or Mooney-Rivlin

* - `-mu_1 [real]`
  - [Mooney-Rivlin material constant](https://en.wikipedia.org/wiki/Mooney–Rivlin_solid), $\mu_1 > 0$,
  - Mooney-Rivlin

* - `-mu_2 [real]`
  - [Mooney-Rivlin material constant](https://en.wikipedia.org/wiki/Mooney–Rivlin_solid), $\mu_2 > 0$
  - Mooney-Rivlin
:::

## Multiple Materials

Ratel supports the use of solving with different material models defined for different segments of the mesh. 
This feature requires additional runtime flags as well as some modifications to existing flags. 
Different materials should be specified over labeled volumes of the mesh; an example of the header of a Gmsh mesh (provided in {code}`examples/meshes/materials_2.msh`) with two materials ("rod" and "binder") is shown below:
```
$ head examples/meshes/materials_2.msh 
$MeshFormat
4.1 0 8
$EndMeshFormat
$PhysicalNames
4
2 1 "start"
2 2 "end"
3 3 "rod"
3 4 "binder"
$EndPhysicalNames
```
In this example, the ID value of the "rod" and "binder" volumes are 3 and 4, respectively. 
In order to tell Ratel to treat these volumes as different materials, we use {code}`-material rod,binder` to provide label names for our specified materials (Note: these names do not have to match the names in the Gmsh mesh).
These label names will be used as prefixes (as {code}`-{material name}_`) to specify other aspects for each material at runtime.
We also specify, for each material, which domain label values to use with {code}`-rod_label_value 3 -binder_label_value 4`. 
To define material parameters such as $E$ and $\nu$, we now use {code}`-binder_E 2.0 -binder_nu 0.4`. 

An example set of command line options for the setting rods and binder materials is given below:

```console
$ ./bin/ratel-quasistatic -material rod,binder -rod_model elasticity-mooney-rivlin-initial -rod_mu_1 0.5 -rod_mu_2 0.5 -rod_nu 0.4 -binder_label_value 3 -binder_model elasticity-neo-hookean-initial -binder_E 2.0 -binder_nu 0.4 -binder_label_value 4
```

A complete list of command line options for specifying multiple materials is given below in the next table:

:::{list-table} Multiple Material Runtime Options
:header-rows: 1

* - Option
  - Description
  - Default value

* - `-material [string list]`
  - List of names to use as labels for each material.
  - 

* - `-{material name}_model`
  - Material to use (`elasticity-linear`, `elasticity-neo-hookean-current`, `elasticity-mooney-rivlin-initial`, etc.) for a given material. 
  - `elasticity-linear`

* - `-{material name}_label_name`
  - Domain label specfying the type of volume to use for specifying materials. Optional.
  - `"Cell Sets"`

* - `-{material name}_label_value [int]`
  - Domain value specifying the volume to use for a given material.
  - 

* - `-{material name}_E [real]`
  - [Young's modulus](https://en.wikipedia.org/wiki/Young%27s_modulus), $E > 0$
  - 

* - `-{material name}_nu [real]`
  - [Poisson's ratio](https://en.wikipedia.org/wiki/Poisson%27s_ratio), $\nu < 0.5$
  - 

* - `-{material name}_nu_smoother [real]`
  - Poisson's ratio for multigrid smoothers, $\nu < 0.5$
  -
:::

An example of specifying a two material quasistatic solve with YAML is provided in {code}`examples/ex02-quasistatic-elasticity-multi-material.yml`. 

## Algebraic Solvers

The examples are configured to use the following Newton-Krylov-Multigrid method by default.

- Newton-type methods for the nonlinear solve, with the hyperelasticity models globalized using load increments.
- Preconditioned conjugate gradients to solve the symmetric positive definite linear systems arising at each Newton step.
- Preconditioning via $p$-version multigrid coarsening to linear elements, with algebraic multigrid (PETSc's `GAMG`) for the coarse solve.
  The default smoother uses degree 3 Chebyshev with Jacobi preconditioning.
  (Lower degree is often faster, albeit less robust; try {code}`-mg_levels_ksp_max_it 2`, for example.)
  Application of the linear operators for all levels with order $p > 1$ is performed matrix-free using analytic Newton linearization, while the lowest order $p = 1$ operators are assembled explicitly (using coloring at present).

Many related solvers can be implemented by composing PETSc command-line options.
For example, to use Hypre's BoomerAMG for the coarse solve (using the assembled linear elements), one would use `-mg_coarse_pc_type hypre`.
Run with `-help` to see (many!) available command-line options related to algebraic solvers.

## Nondimensionalization

Quantities such as the Young's modulus vary over many orders of magnitude, and thus can lead to poorly scaled equations.
One can nondimensionalize the model by choosing an alternate system of units, such that displacements and residuals are of reasonable scales.

:::{list-table} (Non)dimensionalization options
:header-rows: 1

* - Option
  - Description
  - Default value

* - `-units_meter`
  - 1 meter in scaled length units
  - `1`

* - `-units_second`
  - 1 second in scaled time units
  - `1`

* - `-units_kilogram`
  - 1 kilogram in scaled mass units
  - `1`
:::

For example, consider a problem involving metals subject to gravity.

:::{list-table} Characteristic units for metals
:header-rows: 1

* - Quantity
  - Typical value in SI units

* - Displacement, $\bm u$
  - $1 \,\mathrm{cm} = 10^{-2} \,\mathrm m$

* - Young's modulus, $E$
  - $10^{11} \,\mathrm{Pa} = 10^{11} \,\mathrm{kg}\, \mathrm{m}^{-1}\, \mathrm s^{-2}$

* - Body force (gravity) on volume, $\int \rho \bm g$
  - $5 \cdot 10^4 \,\mathrm{kg}\, \mathrm m^{-2} \, \mathrm s^{-2} \cdot (\text{volume} \, \mathrm m^3)$
:::

One can choose units of displacement independently (e.g., {code}`-units_meter 100` to measure displacement in centimeters), but $E$ and $\int \rho \bm g$ have the same dependence on mass and time, so cannot both be made of order 1.
This reflects the fact that both quantities are not equally significant for a given displacement size; the relative significance of gravity increases as the domain size grows.

## Diagnostic Quantities

Diagnostic quantities for viewing are provided when the command line option for visualization output, {code}`-view_diagnostic_quantities viewer:filename.extension` is used.
The diagnostic quantities include displacement in the $x$ direction, displacement in the $y$ direction, displacement in the $z$ direction, pressure, $\operatorname{trace} \bm{E}$, $\operatorname{trace} \bm{E}^2$, $\lvert J \rvert$, and strain energy density $\psi$.
The table below summarizes the formulations of each of these quantities for each material type.

:::{list-table} Diagnostic quantities
   :header-rows: 1

   * - Quantity
     - Linear elasticity
     - Neo-Hookean hyperelasticity

   * - Cauchy stress tensor
     - $\bm{\sigma}$
     - $\bm{\sigma} = \bm{F} \bm{S} \bm{F}^T / \lvert J \rvert$

   * - Pressure
     - $\lambda \operatorname{trace} \bm{\epsilon}$
     - $\lambda \log J$

   * - Volumetric strain
     - $\operatorname{trace} \bm{\epsilon}$
     - $\operatorname{trace} \bm{E}$

   * - $\operatorname{trace} \bm{E}^2$
     - $\operatorname{trace} \bm{\epsilon}^2$
     - $\operatorname{trace} \bm{E}^2$

   * - $\lvert J \rvert$
     - $1 + \operatorname{trace} \bm{\epsilon}$
     - $\lvert J \rvert$

   * - Strain energy density
     - $\frac{\lambda}{2} (\operatorname{trace} \bm{\epsilon})^2 + \mu \bm{\epsilon} : \bm{\epsilon}$
     - $\frac{\lambda}{2}(\log J)^2 + \mu \operatorname{trace} \bm{E} - \mu \log J$

   * - von Mises stress
     - $\sqrt{\frac 3 2 \bm{\hat \sigma} \tcolon \bm {\hat \sigma}}$
     - $\sqrt{\frac 3 2 \bm{\hat \sigma} \tcolon \bm {\hat \sigma}}$
:::

The von Mises stress uses the deviatoric part $\bm{\hat\sigma} = \bm{\sigma} - \frac 1 3 \trace \bm{\sigma}$ of the Cauchy stress $\bm{\sigma}$.
The integrated strain energy $\Psi = \int_{\Omega_0} \psi$ is also computed and printed upon completion of a solve.

[cubit]: https://cubit.sandia.gov/
[this repository]: https://github.com/jeremylt/ceedSampleMeshes
