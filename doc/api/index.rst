**************************************
API Documentation
**************************************

Ratel Public API
======================================

Core Ratel functionality for users.

.. toctree::

   core

Ratel Internal API
======================================

Numerical methods
--------------------------------------

These functions setup the various numerical methods.

.. toctree::

   methods

Models
--------------------------------------

These functions setup the various material models.

.. toctree::

   models


Boundary conditions
--------------------------------------

These functions apply boundary conditions.

.. toctree::

   boundary


Internal functions
--------------------------------------

These functions are internal setup and application functions.

.. toctree::

   internal
