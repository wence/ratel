# Developer Notes

## Style Guide

Please check your code for style issues by running

`make style`

In addition to those automatically enforced style rules, Ratel tends to follow the following code style conventions:

- Variable names: `snake_case`
- Strut members: `snake_case`
- Function names: `PascalCase`
- Type names: `PascalCase`
- Constant names: `CAPS_SNAKE_CASE`

Also, documentation files should have one sentence per line to help make git diffs clearer and less disruptive.

## Clang-tidy

Please check your code for common issues by running

`make tidy`

which uses the `clang-tidy` utility included in recent releases of Clang.
This tool is much slower than actual compilation (`make -j` parallelism helps).
All issues reported by `make tidy` should be fixed.

## Header Files

Header inclusion for source files should follow the principal of 'include what you use' rather than relying upon transitive `#include` to define all symbols.

Every symbol that is used in the source file `foo.c` should be defined in `foo.c`, `foo.h`, or in a header file ``` #include``d in one of these two locations.
Please check your code by running the tool ``include-what-you-use ``` to see recommendations for changes to your source.
Most issues reported by `include-what-you-use` should be fixed; however this rule is flexible to account for differences in header file organization in external libraries.

Header files should be listed in alphabetical order, with installed headers preceding local headers.

```c
#include <ceed.h>
#include <petsc.h>
#include <stdbool.h>
#include <string.h>
#include "include/ratel.h"
```

## `restrict` Semantics

QFunction arguments can be assumed to have `restrict` semantics. That is, each input and output array must reside in distinct memory without overlap.
