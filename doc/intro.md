# Getting Started

```{include} ../README.md
:start-after: "<!-- getting-started -->"
```

## Build Containers Locally

As an alterative to using the published Ratel Docker image or building Ratel locally, users can also build Docker images locally.

### Building Docker Containers

```{include} ../containers/docker/README.md
```

### Building Singularity Containers

```{include} ../containers/singularity/README.md
```
