#ifndef ratel_models_h
#define ratel_models_h

#include <ceed.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel.h>
#include <ratel/models/mooney-rivlin.h>
#include <ratel/models/neo-hookean.h>

// Physics options
#define RATEL_MODEL_REGISTER(function_list, model_cl_argument, model_postfix, physics_postfix)                                   \
  RatelDebug(ratel, "---- Registration of model \"%s\"", model_cl_argument);                                                     \
  RatelDebug(ratel, "------ Model data: %s", #model_postfix);                                                                    \
  PetscCall(PetscFunctionListAdd(&function_list->getModelData, model_cl_argument, RatelGetModelData_##model_postfix));           \
  RatelDebug(ratel, "------ Physics: %s", #physics_postfix);                                                                     \
  PetscCall(PetscFunctionListAdd(&function_list->setupPhysics, model_cl_argument, RatelCreatePhysicsContext_##physics_postfix)); \
  PetscCall(PetscFunctionListAdd(&function_list->setupSmootherPhysics, model_cl_argument, RatelCreatePhysicsSmootherContext_##physics_postfix));

RATEL_INTERN PetscErrorCode RatelRegisterModels(Ratel ratel, RatelModelFunctions model_functions);
RATEL_INTERN PetscErrorCode RatelModelDataVerifyRelativePath(Ratel ratel, RatelModelData model_data);
RATEL_INTERN PetscErrorCode RatelGetModelData(Ratel ratel, const char *cl_argument, RatelModelData *model_data);
RATEL_INTERN PetscErrorCode RatelSetupModelPhysics(Ratel ratel, const char *cl_argument, PetscInt material_index, CeedQFunctionContext *ctx_phys);
RATEL_INTERN PetscErrorCode RatelSetupModelSmootherPhysics(Ratel ratel, const char *cl_argument, PetscInt material_index,
                                                           CeedQFunctionContext ctx_phys, CeedQFunctionContext *ctx_phys_smoother);

RATEL_INTERN PetscErrorCode RatelGetMaterialPrefix(Ratel ratel, PetscInt material_index, char **prefix);
RATEL_INTERN PetscErrorCode RatelGetMaterialMessage(Ratel ratel, PetscInt material_index, char **message);
RATEL_INTERN PetscErrorCode RatelGetMaterialOperatorName(Ratel ratel, PetscInt material_index, const char *base_name, char **operator_name);
RATEL_INTERN PetscErrorCode RatelSetMaterialOperatorName(Ratel ratel, PetscInt material_index, const char *base_name, CeedOperator op);

#define RATEL_PHYSICS(physics_postfix)                                                                                                            \
  RATEL_INTERN PetscErrorCode RatelCreatePhysicsContext_##physics_postfix(Ratel, PetscInt, CeedQFunctionContext *);                               \
  RATEL_INTERN PetscErrorCode RatelCreatePhysicsSmootherContext_##physics_postfix(Ratel, CeedQFunctionContext, PetscInt, CeedQFunctionContext *); \
  RATEL_INTERN PetscErrorCode RatelProcessPhysics_##physics_postfix(Ratel, PetscInt, void *);

RATEL_PHYSICS(NeoHookean);
RATEL_PHYSICS(MooneyRivlin);

#endif  // ratel_models_h
