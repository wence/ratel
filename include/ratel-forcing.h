#ifndef ratel_forcing_h
#define ratel_forcing_h

#include <ratel-impl.h>
#include <ratel.h>

RATEL_INTERN PetscErrorCode RatelResidualAddForcing(Ratel ratel, CeedVector x_coord, CeedElemRestriction elem_restr_x, CeedBasis basis_x);

#endif

// ratel_forcing_h
