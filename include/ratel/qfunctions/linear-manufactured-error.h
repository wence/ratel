/// @file
/// Ratel error computation linear elasticity manufactured solution QFunction source

#ifndef ratel_linear_manufactured_error_qf_h
#define ratel_linear_manufactured_error_qf_h

#include <math.h>

// -----------------------------------------------------------------------------
// True solution for linear elasticity manufactured solution
// -----------------------------------------------------------------------------
CEED_QFUNCTION(MMSError_Linear)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*qdata)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*coords)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*error)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  const CeedScalar scale[3][3] = {
      {2, 3, 4},
      {3, 4, 2},
      {4, 2, 3}
  };

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    CeedScalar x = coords[0][i], y = coords[1][i], z = coords[2][i];

    // Error
    for (CeedInt j = 0; j < 3; j++) {
      const CeedScalar true_u = exp(scale[j][0] * x) * sin(scale[j][1] * y) * cos(scale[j][2] * z) / 1e8;
      error[j][i]             = (true_u - u[j][i]) * (true_u - u[j][i]) * qdata[0][i];
    }
  }  // End of Quadrature Point Loop

  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_linear_manufactured_error_qf_h
