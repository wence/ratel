/// @file
/// Ratel constant volumetric body force QFunction source

#ifndef ratel_constant_qf_h
#define ratel_constant_qf_h

#include <math.h>

// forcing context
#ifndef forcing_vector_h
#define forcing_vector_h
typedef struct {
  CeedScalar direction[3];
  CeedScalar time;
} ForcingVectorContext;
#endif

// -----------------------------------------------------------------------------
// Constant forcing term along specified vector
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ConstantForce)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];

  // Outputs
  CeedScalar(*force)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const ForcingVectorContext *context = (ForcingVectorContext *)ctx;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    CeedScalar wdetJ = q_data[0][i];

    // Forcing function
    for (CeedInt j = 0; j < 3; j++) {
      force[j][i] = -context->time * context->direction[j] * wdetJ;
    }

  }  // End of Quadrature Point Loop

  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_constant_qf_h
