/// @file
/// Ratel surface geometric factors computation QFunction source

#ifndef ratel_surface_geometry_qf_h
#define ratel_surface_geometry_qf_h

#include "utils.h"

// -----------------------------------------------------------------------------
// This QFunction sets up the geometric factors required for integration and
//   coordinate transformations
//
// Reference (parent) 2D coordinates: X
// Physical (current) 3D coordinates: x
// Change of coordinate matrix:
//   dxdX_{i,j} = dx_i/dX_j (indicial notation) [3 * 2]
//
// (N_1, N_2, N_3) is given by the cross product of the columns of dxdX_{i,j}
//
// detNb is the magnitude of (N_1, N_2, N_3)
//
// Stored:
//   (N_1, N_2, N_3)
//   w detNb
//
// -----------------------------------------------------------------------------
CEED_QFUNCTION(SetupSurfaceGeometry)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*J)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[0];
  const CeedScalar(*w)                = in[1];

  // Outputs
  CeedScalar(*q_data)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  const CeedInt dim = 3;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    // N_1, N_2, and N_3 are given by the cross product of the columns of dxdX
    CeedScalar normal[3];
    for (CeedInt j = 0; j < dim; j++) {
      // Equivalent code with no mod operations:
      // normal[j] = J[0][j+1]*J[1][j+2] - J[0][j+2]*J[1][j+1]
      normal[j] = J[0][(j + 1) % dim][i] * J[1][(j + 2) % dim][i] - J[0][(j + 2) % dim][i] * J[1][(j + 1) % dim][i];
    }
    const CeedScalar detJb = RatelNorm3(normal);

    // Qdata
    // -- Interp-to-Interp q_data
    q_data[0][i] = w[i] * detJb;
    // -- Normal vector
    for (CeedInt j = 0; j < dim; j++) q_data[j + 1][i] = normal[j] / detJb;

  }  // End of Quadrature Point Loop

  // Return
  return 0;
}

// -----------------------------------------------------------------------------
// This QFunction computes the centroid of a face
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ComputeCentroid)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*x)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*centroid)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  const CeedInt dim = 3;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Centroid
    for (CeedInt j = 0; j < dim; j++) centroid[j][i] = x[j][i] * q_data[0][i];
  }  // End of Quadrature Point Loop

  // Return
  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_surface_geometry_qf_h
