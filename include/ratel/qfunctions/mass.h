/// @file
/// Ratel mass operator QFunction source

#ifndef ratel_mass_qf_h
#define ratel_mass_qf_h

#include <math.h>

#include "utils.h"

// -----------------------------------------------------------------------------
// Mass operator with full volumetric qdata
// -----------------------------------------------------------------------------
CEED_QFUNCTION(Mass)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  const CeedInt num_comp = *(CeedInt *)ctx;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Mass operator
    ScaledMassApplyAtQuadraturePoint(Q, i, num_comp, q_data[0][i], (const CeedScalar *)u, (CeedScalar *)v);
  }  // End of Quadrature Point Loop

  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_mass_qf_h
