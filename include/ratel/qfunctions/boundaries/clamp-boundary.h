/// @file
/// Ratel clamp boundary condition QFunction source

#ifndef ratel_clamp_boundary_qf_h
#define ratel_clamp_boundary_qf_h

#include <math.h>

#include "../utils.h"

// clamp boundary context
#ifndef bc_clamp
#define bc_clamp
typedef struct {
  CeedScalar translation[3];
  CeedScalar rotation_axis[3];
  CeedScalar rotation_polynomial[2];
  CeedScalar time;
  CeedScalar final_time;
} BCClampData;
#endif

// -----------------------------------------------------------------------------
// This QFunction sets up coodinate and scaling data for the Dirichlet clamp boundary
// -----------------------------------------------------------------------------
CEED_QFUNCTION(SetupClampBCs)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*coords)[CEED_Q_VLA]       = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*multiplicity)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*coords_stored)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];
  CeedScalar(*scale_stored)              = (CeedScalar(*))out[1];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Stored values
    for (CeedInt j = 0; j < 3; j++) coords_stored[j][i] = coords[j][i];
    scale_stored[i] = 1.0 / multiplicity[0][i];
  }  // End of Quadrature Point Loop

  // Return
  return 0;
}

// -----------------------------------------------------------------------------
// This QFunction computes the Dirichlet clamp boundary values
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ApplyClampBCs)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*coords)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*scale)              = (const CeedScalar(*))in[1];

  // Outputs
  CeedScalar(*u)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // User context
  const BCClampData *context = (BCClampData *)ctx;
  const CeedScalar   s       = context->time < 1 ? context->time : 1;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Coordinates
    const CeedScalar x  = coords[0][i];
    const CeedScalar y  = coords[1][i];
    const CeedScalar z  = coords[2][i];
    // Translation vector
    const CeedScalar lx = context->translation[0] * s, ly = context->translation[1] * s, lz = context->translation[2] * s;
    // Normalized rotation axis
    const CeedScalar kx = context->rotation_axis[0], ky = context->rotation_axis[1], kz = context->rotation_axis[2];
    // Rotation polynomial
    const CeedScalar c_0 = context->rotation_polynomial[0] * RATEL_PI_DOUBLE, c_1 = context->rotation_polynomial[1] * RATEL_PI_DOUBLE;
    const CeedScalar cx        = kx * x + ky * y + kz * z;
    // Rotation magnitude
    const CeedScalar theta     = (c_0 + c_1 * cx) * context->time;
    const CeedScalar cos_theta = cos(theta), sin_theta = sin(theta);

    // Compute new boundary displacement
    u[0][i] = lx + sin_theta * (-kz * y + ky * z) + (1 - cos_theta) * (-(ky * ky + kz * kz) * x + kx * ky * y + kx * kz * z);
    u[1][i] = ly + sin_theta * (kz * x + -kx * z) + (1 - cos_theta) * (kx * ky * x + -(kx * kx + kz * kz) * y + ky * kz * z);
    u[2][i] = lz + sin_theta * (-ky * x + kx * y) + (1 - cos_theta) * (kx * kz * x + ky * kz * y + -(kx * kx + ky * ky) * z);
    for (CeedInt j = 0; j < 3; j++) u[j][i] *= scale[i];
  }  // End of Quadrature Point Loop

  // Return
  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_clamp_boundary_qf_h
