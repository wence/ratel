/// @file
/// Ratel traction boundary condition QFunction source

#ifndef ratel_traction_boundary_qf_h
#define ratel_traction_boundary_qf_h

// traction boundary context
typedef struct {
  CeedScalar direction[3];
  CeedScalar time;
  CeedScalar final_time;
} BCTractionData;

// -----------------------------------------------------------------------------
// This QFunction computes the surface integral of the user traction vector on
//   the constrained faces.
//
// Computed:
//   t * (w detNb)
//
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ApplyTractionBCs)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // User context
  const BCTractionData *context = (BCTractionData *)ctx;
  const CeedScalar      s       = context->time < 1 ? context->time : 1;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetNb = q_data[0][i];

    // Traction surface integral
    for (CeedInt j = 0; j < 3; j++) {
      v[j][i] = -s * context->direction[j] * wdetNb;
    }

  }  // End of Quadrature Point Loop

  // Return
  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_traction_boundary_qf_h
