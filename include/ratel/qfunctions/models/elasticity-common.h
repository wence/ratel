/// @file
/// Common components for elasticity QFunctions

#ifndef ratel_common_elasticity_qf_h
#define ratel_common_elasticity_qf_h

#include <ceed.h>

#include "../../models/common-parameters.h"
#include "../utils.h"

typedef int (*RatelComputeFirstPiolaKirchhoff)(void *, const CeedInt, const CeedInt, const CeedInt, const CeedScalar *const *, CeedScalar *const *,
                                               CeedScalar[3][3], CeedScalar[3][3]);

// -----------------------------------------------------------------------------
// Common computation for F
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int ElasticityResidual(void *ctx, CeedInt Q, RatelComputeFirstPiolaKirchhoff compute_P, const CeedScalar *const *in,
                                             CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];

  // Outputs
  CeedScalar(*dvdX)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Coordinate transformation and First Piola-Kirchhoff
    CeedScalar    dXdx[3][3], P[3][3];
    const CeedInt stored_fields_input_offset = 2;
    compute_P(ctx, Q, i, stored_fields_input_offset, in, out, dXdx, P);

    // Apply dXdx^T and weight
    RatelMatMatTransposeMultAtQuadraturePoint(Q, i, wdetJ, dXdx, P, dvdX);
  }  // End of Quadrature Point Loop

  return 0;
}

// -----------------------------------------------------------------------------
// Common computation for dF
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int ElasticityJacobian(void *ctx, CeedInt Q, RatelComputeFirstPiolaKirchhoff compute_dP, const CeedScalar *const *in,
                                             CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];

  // Outputs
  CeedScalar(*delta_dvdX)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[0];

  // Context
  const CeedScalar *context                    = (CeedScalar *)ctx;
  const CeedScalar  rho                        = context[RATEL_COMMON_PARAMETER_RHO];
  const CeedScalar  shift_a                    = context[RATEL_COMMON_PARAMETER_SHIFT_A];
  const CeedInt     stored_fields_input_offset = 2 + ((shift_a != 0.0) ? 1.0 : 0.0);

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Coordinate transformation and linearized First Piola-Kirchhoff
    CeedScalar dXdx[3][3], dP[3][3];
    compute_dP(ctx, Q, i, stored_fields_input_offset, in, out, dXdx, dP);

    // Apply dXdx^T and weight
    RatelMatMatTransposeMultAtQuadraturePoint(Q, i, wdetJ, dXdx, dP, delta_dvdX);

    if (shift_a != 0.0) {
      // Inputs
      const CeedScalar *delta_u = in[2];

      // Outputs
      CeedScalar *delta_v = out[1];

      // Apply scaled mass matrix
      ScaledMassApplyAtQuadraturePoint(Q, i, 3, -shift_a * rho * q_data[0][i], delta_u, delta_v);
    }
  }  // End of Quadrature Point Loop

  return 0;
}

#endif  // ratel_common_elasticity_qf_h
