/// @file
/// Ratel Neo-Hookean hyperelasticity at finite strain in initial configuration with autodifferentiation QFunction source

#ifndef ratel_elasticity_neo_hookean_initial_ad_qf_h
#define ratel_elasticity_neo_hookean_initial_ad_qf_h

#include <math.h>

#include "../../models/neo-hookean.h"
#include "../utils.h"
#include "elasticity-common.h"
#include "elasticity-neo-hookean-common.h"

static const int RATEL_ELASTICITY_NEO_HOOKEAN_INITIAL_AD_TAPE_SIZE = 6;

#define UnpackArrayQ(x_packed, x) \
  for (int j = 0; j < sizeof(x_packed) / sizeof(x_packed[0]); j++) x[j][i] = x_packed[j];

#define PackArrayQ(x, x_packed) \
  for (int j = 0; j < sizeof(x_packed) / sizeof(x_packed[0]); j++) x_packed[j] = x[j][i];

// ----------------------------------------------------------------------------
// Strain Energy Function
// ----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER CeedScalar StrainEnergy(CeedScalar E_Voigt[6], CeedScalar lambda, CeedScalar mu) {
  // Calculate 2*E
  CeedScalar E2_Voigt[6];
  for (CeedInt i = 0; i < 6; i++) E2_Voigt[i] = E_Voigt[i] * 2;

  // log(J)
  CeedScalar detCm1 = RatelVoigtDetAM1(E2_Voigt);
  CeedScalar J      = sqrt(detCm1 + 1);
  CeedScalar logJ   = RatelLog1pSeries(detCm1) / 2.;

  // trace(E)
  CeedScalar traceE = RatelVoigtTrace(E_Voigt);

  return lambda * (J * J - 1) / 4 - lambda * logJ / 2 + mu * (-logJ + traceE);
};

// -----------------------------------------------------------------------------
//  Compute Second Piola Kirchhoff stress:
// -----------------------------------------------------------------------------
// -- Enzyme-AD
void       __enzyme_autodiff(void *, ...);
void       __enzyme_augmentfwd(void *, ...);
void       __enzyme_fwdsplit(void *, ...);
int        __enzyme_augmentsize(void *, ...);
extern int enzyme_tape, enzyme_const, enzyme_dup, enzyme_nofree, enzyme_allocated;

void *__enzyme_function_like[2] = {(void *)RatelLog1pSeries, "log1p"};

CEED_QFUNCTION_HELPER void SecondPiolaKirchhoffStress_NeoHookean_AD(const CeedScalar lambda, const CeedScalar mu, CeedScalar E_Voigt[6],
                                                                    CeedScalar S_Voigt[6]) {
  for (CeedInt i = 0; i < 6; i++) S_Voigt[i] = 0.;
  __enzyme_autodiff((void *)StrainEnergy, E_Voigt, S_Voigt, enzyme_const, lambda, enzyme_const, mu);
  for (CeedInt i = 3; i < 6; i++) S_Voigt[i] /= 2.;
}

CEED_QFUNCTION_HELPER void S_augmentfwd(double *S, double *E, const double lambda, const double mu, double *tape) {
  int tape_bytes = __enzyme_augmentsize((void *)SecondPiolaKirchhoffStress_NeoHookean_AD, enzyme_const, enzyme_const, enzyme_dup, enzyme_dup);
  __enzyme_augmentfwd((void *)SecondPiolaKirchhoffStress_NeoHookean_AD, enzyme_allocated, tape_bytes, enzyme_tape, tape, enzyme_nofree, enzyme_const,
                      lambda, enzyme_const, mu, E, (double *)NULL, S, (double *)NULL);
};

CEED_QFUNCTION_HELPER void dS_fwd(double *dS, double *dE, const double lambda, const double mu, const double *tape) {
  int tape_bytes = __enzyme_augmentsize((void *)SecondPiolaKirchhoffStress_NeoHookean_AD, enzyme_const, enzyme_const, enzyme_dup, enzyme_dup);
  for (CeedInt i = 0; i < 6; i++) dS[i] = 0.;
  __enzyme_fwdsplit((void *)SecondPiolaKirchhoffStress_NeoHookean_AD, enzyme_allocated, tape_bytes, enzyme_tape, tape, enzyme_const, lambda,
                    enzyme_const, mu, (double *)NULL, dE, (double *)NULL, dS);
};

// -----------------------------------------------------------------------------
// Compute First Piola-Kirchhoff
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int P_NeoHookeanInitial_AD(void *ctx, const CeedInt Q, const CeedInt i, const CeedInt stored_fields_input_offset,
                                                 const CeedScalar *const *in, CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar P[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*grad_u)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[1];
  CeedScalar(*S_Voigt)[CEED_Q_VLA]   = (CeedScalar(*)[CEED_Q_VLA])out[2];
  CeedScalar(*tape)[CEED_Q_VLA]      = (CeedScalar(*)[CEED_Q_VLA])out[3];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;
  const CeedScalar         TwoMu   = E / (1 + nu);
  const CeedScalar         mu      = TwoMu / 2;
  const CeedScalar         K_bulk  = E / (3 * (1 - 2 * nu));  // Bulk Modulus
  const CeedScalar         lambda  = (3 * K_bulk - TwoMu) / 3;

  // Formulation Terminology:
  //  I     : 3x3 Identity matrix
  //  C     : right Cauchy-Green tensor
  //  C_inv  : inverse of C
  //  F     : deformation gradient
  //  S     : 2nd Piola-Kirchhoff (in current config)
  //  P     : 1st Piola-Kirchhoff (in referential config)
  // Formulation:
  //  F =  I + grad_ue
  //  J = det(F)
  //  C = F(^T)*F
  //  S = mu*I + (lambda*log(J)-mu)*C_inv;
  //  P = F*S

  // Read spatial derivatives of u
  const CeedScalar du[3][3] = {
      {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
      {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
      {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
  };

  // -- Qdata
  dXdx[0][0] = q_data[1][i];
  dXdx[0][1] = q_data[2][i];
  dXdx[0][2] = q_data[3][i];
  dXdx[1][0] = q_data[4][i];
  dXdx[1][1] = q_data[5][i];
  dXdx[1][2] = q_data[6][i];
  dXdx[2][0] = q_data[7][i];
  dXdx[2][1] = q_data[8][i];
  dXdx[2][2] = q_data[9][i];

  // Compute grad_u
  //   dXdx = (dx/dX)^(-1)
  // Apply dXdx to du = grad_u
  RatelMatMatMultAtQuadraturePoint(Q, i, 1.0, du, dXdx, grad_u);

  // Compute the Deformation Gradient : F = I + grad_u
  const CeedScalar F[3][3] = {
      {grad_u[0][0][i] + 1, grad_u[0][1][i],     grad_u[0][2][i]    },
      {grad_u[1][0][i],     grad_u[1][1][i] + 1, grad_u[1][2][i]    },
      {grad_u[2][0][i],     grad_u[2][1][i],     grad_u[2][2][i] + 1}
  };

  const CeedScalar temp_grad_u[3][3] = {
      {grad_u[0][0][i], grad_u[0][1][i], grad_u[0][2][i]},
      {grad_u[1][0][i], grad_u[1][1][i], grad_u[1][2][i]},
      {grad_u[2][0][i], grad_u[2][1][i], grad_u[2][2][i]}
  };

  // Common components of finite strain calculations
  const CeedScalar Jm1 = RatelMatDetAM1(temp_grad_u);

  CeedScalar E_Voigt[6], C_inv_Voigt[6];
  GreenLagrangeStrain(temp_grad_u, E_Voigt);
  ComputeCinverse(E_Voigt, Jm1, C_inv_Voigt);

  // Compute Second Piola-Kirchhoff (S) with Enzyme-AD
  CeedScalar packed_tape[RATEL_ELASTICITY_NEO_HOOKEAN_INITIAL_AD_TAPE_SIZE];
  CeedScalar S_Voigt_packed[6], S[3][3];
  S_augmentfwd(S_Voigt_packed, E_Voigt, lambda, mu, packed_tape);
  RatelVoigtUnpack(S_Voigt_packed, S);

  // Compute the First Piola-Kirchhoff : P = F*S
  RatelMatMatMult(1.0, F, S, P);

  // Store tape and S_Voigt
  UnpackArrayQ(packed_tape, tape);
  UnpackArrayQ(S_Voigt_packed, S_Voigt);

  return 0;
}

// -----------------------------------------------------------------------------
// Compute linearization of First Piola-Kirchhoff
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int dP_NeoHookeanInitial_AD(void *ctx, const CeedInt Q, const CeedInt i, const CeedInt stored_fields_input_offset,
                                                  const CeedScalar *const *in, CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar dP[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*delta_ug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*grad_u)[3][CEED_Q_VLA]   = (const CeedScalar(*)[3][CEED_Q_VLA])in[stored_fields_input_offset + 0];
  const CeedScalar(*S_Voigt)[CEED_Q_VLA]     = (const CeedScalar(*)[CEED_Q_VLA])in[stored_fields_input_offset + 1];
  const CeedScalar(*tape)[CEED_Q_VLA]        = (const CeedScalar(*)[CEED_Q_VLA])in[stored_fields_input_offset + 2];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;

  // Constants
  const CeedScalar TwoMu  = E / (1 + nu);
  const CeedScalar mu     = TwoMu / 2;
  const CeedScalar K_bulk = E / (3 * (1 - 2 * nu));  // Bulk Modulus
  const CeedScalar lambda = (3 * K_bulk - TwoMu) / 3;

  // Read spatial derivatives of delta_u
  const CeedScalar delta_du[3][3] = {
      {delta_ug[0][0][i], delta_ug[1][0][i], delta_ug[2][0][i]},
      {delta_ug[0][1][i], delta_ug[1][1][i], delta_ug[2][1][i]},
      {delta_ug[0][2][i], delta_ug[1][2][i], delta_ug[2][2][i]}
  };

  // -- Qdata
  dXdx[0][0] = q_data[1][i];
  dXdx[0][1] = q_data[2][i];
  dXdx[0][2] = q_data[3][i];
  dXdx[1][0] = q_data[4][i];
  dXdx[1][1] = q_data[5][i];
  dXdx[1][2] = q_data[6][i];
  dXdx[2][0] = q_data[7][i];
  dXdx[2][1] = q_data[8][i];
  dXdx[2][2] = q_data[9][i];

  // Compute grad_delta_u
  //   dXdx = (dx/dX)^(-1)
  // Apply dXdx to delta_du = graddelta
  CeedScalar grad_delta_u[3][3];
  RatelMatMatMult(1.0, delta_du, dXdx, grad_delta_u);

  // Deformation Gradient : F = I + grad_u
  const CeedScalar F[3][3] = {
      {grad_u[0][0][i] + 1, grad_u[0][1][i],     grad_u[0][2][i]    },
      {grad_u[1][0][i],     grad_u[1][1][i] + 1, grad_u[1][2][i]    },
      {grad_u[2][0][i],     grad_u[2][1][i],     grad_u[2][2][i] + 1}
  };

  // deltaE - Green-Lagrange strain tensor
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};
  CeedScalar    delta_E_Voigt[6];
  for (CeedInt m = 0; m < 6; m++) {
    delta_E_Voigt[m] = 0;
    for (CeedInt n = 0; n < 3; n++) {
      delta_E_Voigt[m] += (grad_delta_u[n][ind_j[m]] * F[n][ind_k[m]] + F[n][ind_j[m]] * grad_delta_u[n][ind_k[m]]) / 2.;
    }
  }

  // Compute delta_S with Enzyme-AD
  CeedScalar packed_tape[RATEL_ELASTICITY_NEO_HOOKEAN_INITIAL_AD_TAPE_SIZE];
  CeedScalar delta_S_Voigt[6], deltaS[3][3];
  PackArrayQ(tape, packed_tape);
  dS_fwd(delta_S_Voigt, delta_E_Voigt, lambda, mu, packed_tape);
  RatelVoigtUnpack(delta_S_Voigt, deltaS);

  // Second Piola-Kirchhoff (S)
  CeedScalar S[3][3], S_Voigt_packed[6];
  PackArrayQ(S_Voigt, S_Voigt_packed);
  RatelVoigtUnpack(S_Voigt_packed, S);

  // delta_P = dPdF:deltaF = deltaF*S + F*deltaS
  RatelMatMatMultPlusMatMatMult(grad_delta_u, S, F, deltaS, dP);

  return 0;
}

// -----------------------------------------------------------------------------
// Residual evaluation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ElasticityResidual_NeoHookeanInitial_AD)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, P_NeoHookeanInitial_AD, in, out);
}

// -----------------------------------------------------------------------------
// Jacobian evaluation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ElasticityJacobian_NeoHookeanInitial_AD)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, dP_NeoHookeanInitial_AD, in, out);
}
// -----------------------------------------------------------------------------

#endif  // ratel_elasticity_neo_hookean_initial_ad_qf_h
