/// @file
/// Ratel Neo-Hookean hyperelasticity at finite strain common QFunction source

#ifndef ratel_elasticity_neo_hookean_common_qf_h
#define ratel_elasticity_neo_hookean_common_qf_h

#include <math.h>

#include "../../models/neo-hookean.h"
#include "../utils.h"

// -----------------------------------------------------------------------------
//  Compute e = (b - I)/2 = (Grad_u + Grad_u^T + Grad_u*Grad_u^T)/2
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER void GreenEulerStrain(const CeedScalar Grad_u[3][3], CeedScalar e[6]) {
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};
  for (CeedInt m = 0; m < 6; m++) {
    e[m] = (Grad_u[ind_j[m]][ind_k[m]] + Grad_u[ind_k[m]][ind_j[m]]) * 0.5;
    for (CeedInt n = 0; n < 3; n++) {
      e[m] += (Grad_u[ind_j[m]][n] * Grad_u[ind_k[m]][n]) * 0.5;
    }
  }
}

// -----------------------------------------------------------------------------
//  Compute E = (C - I)/2 = (grad_u + grad_u^T + grad_u^T*grad_u)/2
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER void GreenLagrangeStrain(const CeedScalar grad_u[3][3], CeedScalar E[6]) {
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};
  for (CeedInt m = 0; m < 6; m++) {
    E[m] = (grad_u[ind_j[m]][ind_k[m]] + grad_u[ind_k[m]][ind_j[m]]) * 0.5;
    for (CeedInt n = 0; n < 3; n++) {
      E[m] += (grad_u[n][ind_j[m]] * grad_u[n][ind_k[m]]) * 0.5;
    }
  }
}

// -----------------------------------------------------------------------------
//  Compute tau = 2*mu*e +  lambda*logJ*I
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER void KirchhoffTau_NeoHookean(CeedScalar lambda, CeedScalar mu, const CeedScalar e[6], CeedScalar logJ,
                                                   CeedScalar tau_Voigt[6]) {
  // https://ratel.micromorph.org/doc/modeling/materials/neo-hookean/#equation-eq-tau-neo-hookean
  tau_Voigt[0] = 2 * mu * e[0] + lambda * logJ;
  tau_Voigt[1] = 2 * mu * e[1] + lambda * logJ;
  tau_Voigt[2] = 2 * mu * e[2] + lambda * logJ;
  tau_Voigt[3] = 2 * mu * e[3];
  tau_Voigt[4] = 2 * mu * e[4];
  tau_Voigt[5] = 2 * mu * e[5];
}

// -----------------------------------------------------------------------------
//  Compute C^{-1}, inverse of right Cauchy-Green tensor
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER void ComputeCinverse(CeedScalar E_Voigt[6], const CeedScalar Jm1, CeedScalar C_inv_Voigt[6]) {
  CeedScalar E[3][3];
  RatelVoigtUnpack(E_Voigt, E);

  // C : right Cauchy-Green tensor
  // C = I + 2E
  const CeedScalar C[3][3] = {
      {2 * E[0][0] + 1, 2 * E[0][1],     2 * E[0][2]    },
      {2 * E[0][1],     2 * E[1][1] + 1, 2 * E[1][2]    },
      {2 * E[0][2],     2 * E[1][2],     2 * E[2][2] + 1}
  };

  // Compute C^(-1) : C-Inverse
  const CeedScalar detC = (Jm1 + 1.) * (Jm1 + 1.);
  RatelMatComputeInverseSymmetric(C, detC, C_inv_Voigt);
}

// -----------------------------------------------------------------------------
//  Compute Second Piola Kirchhoff stress:
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER void SecondKirchhoffStress_NeoHookean(const CeedScalar lambda, const CeedScalar mu, CeedScalar logJ, CeedScalar C_inv_Voigt[6],
                                                            CeedScalar E_Voigt[6], CeedScalar S_Voigt[6]) {
  CeedScalar E[3][3];
  RatelVoigtUnpack(E_Voigt, E);

  CeedScalar C_inv[3][3];
  RatelVoigtUnpack(C_inv_Voigt, C_inv);
  // Compute the Second Piola-Kirchhoff (S)
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};
  for (CeedInt m = 0; m < 6; m++) {
    S_Voigt[m] = (lambda * (logJ)) * C_inv_Voigt[m];
    for (CeedInt n = 0; n < 3; n++) {
      S_Voigt[m] += mu * C_inv[ind_j[m]][n] * 2 * E[n][ind_k[m]];
    }
  }
}

// -----------------------------------------------------------------------------
// Strain energy computation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(Energy_NeoHookean)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;
  const CeedScalar         TwoMu   = E / (1 + nu);
  const CeedScalar         mu      = TwoMu / 2;
  const CeedScalar         K_bulk  = E / (3 * (1 - 2 * nu));  // Bulk Modulus
  const CeedScalar         lambda  = (3 * K_bulk - TwoMu) / 3;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of u
    const CeedScalar du[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };
    // -- Qdata
    const CeedScalar wdetJ      = q_data[0][i];
    const CeedScalar dXdx[3][3] = {
        {q_data[1][i], q_data[2][i], q_data[3][i]},
        {q_data[4][i], q_data[5][i], q_data[6][i]},
        {q_data[7][i], q_data[8][i], q_data[9][i]}
    };

    // Compute Grad_u
    //   dXdx = (dx/dX)^(-1)
    // Apply dXdx to du = grad_u
    CeedScalar Grad_u[3][3];
    RatelMatMatMult(1.0, du, dXdx, Grad_u);

    // e - Euler strain tensor
    CeedScalar e[6];
    GreenEulerStrain(Grad_u, e);
    const CeedScalar Jm1  = RatelMatDetAM1(Grad_u);
    const CeedScalar logJ = RatelLog1pSeries(Jm1);

    // Strain energy psi(e) for compressible Neo-Hookean
    energy[i] = (lambda * logJ * logJ / 2. - mu * logJ + mu * (e[0] + e[1] + e[2])) * wdetJ;

  }  // End of Quadrature Point Loop

  return 0;
}

// -----------------------------------------------------------------------------
// Nodal diagnostic quantities
// -----------------------------------------------------------------------------
CEED_QFUNCTION(Diagnostic_NeoHookean)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;
  const CeedScalar         TwoMu   = E / (1 + nu);
  const CeedScalar         mu      = TwoMu / 2;
  const CeedScalar         K_bulk  = E / (3 * (1 - 2 * nu));  // Bulk Modulus
  const CeedScalar         lambda  = (3 * K_bulk - TwoMu) / 3;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of u
    const CeedScalar wdetJ    = q_data[0][i];
    const CeedScalar du[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };
    // -- Qdata
    const CeedScalar dXdx[3][3] = {
        {q_data[1][i], q_data[2][i], q_data[3][i]},
        {q_data[4][i], q_data[5][i], q_data[6][i]},
        {q_data[7][i], q_data[8][i], q_data[9][i]}
    };

    // Compute Grad_u
    //   dXdx = (dx/dX)^(-1)
    // Apply dXdx to du = grad_u
    CeedScalar Grad_u[3][3];
    RatelMatMatMult(1.0, du, dXdx, Grad_u);

    // Deformation gradient
    const CeedScalar F[3][3] = {
        {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
        {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
        {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
    };

    // Common components of finite strain calculations
    const CeedScalar Jm1  = RatelMatDetAM1(Grad_u);
    CeedScalar       logJ = RatelLog1pSeries(Jm1);

    CeedScalar E_Voigt[6], S_Voigt[6], C_inv_Voigt[6];
    GreenLagrangeStrain(Grad_u, E_Voigt);
    ComputeCinverse(E_Voigt, Jm1, C_inv_Voigt);
    SecondKirchhoffStress_NeoHookean(lambda, mu, logJ, C_inv_Voigt, E_Voigt, S_Voigt);

    CeedScalar S[3][3];
    RatelVoigtUnpack(S_Voigt, S);

    // Compute the First Piola-Kirchhoff : P = F*S
    CeedScalar P[3][3];
    RatelMatMatMult(1.0, F, S, P);

    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    CeedScalar sigma[3][3];
    RatelMatMatTransposeMult(1.0 / (Jm1 + 1.0), P, F, sigma);
    diagnostic[3][i] = sigma[0][0];
    diagnostic[4][i] = sigma[0][1];
    diagnostic[5][i] = sigma[0][1];
    diagnostic[6][i] = sigma[1][1];
    diagnostic[7][i] = sigma[1][2];
    diagnostic[8][i] = sigma[2][2];

    // Pressure
    diagnostic[9][i] = -lambda * logJ;

    // Strain tensor invariants, e = (b-I)/2
    CeedScalar e[6];
    GreenEulerStrain(Grad_u, e);

    diagnostic[10][i] = e[0] + e[1] + e[2];
    diagnostic[11][i] = 0.;
    for (CeedInt j = 0; j < 6; j++) {
      diagnostic[11][i] += e[j] * e[j] * (1 + (j >= 3));
    }

    diagnostic[12][i] = Jm1 + 1.;

    // Strain energy
    diagnostic[13][i] = (lambda * logJ * logJ / 2. - mu * logJ + mu * (e[0] + e[1] + e[2]));

    // ---------- Compute von-Mises stress----------
    CeedScalar tau[6], sigma_dev[6];
    KirchhoffTau_NeoHookean(lambda, mu, e, logJ, tau);
    const CeedScalar trace_tau = tau[0] + tau[1] + tau[2];
    for (CeedInt j = 0; j < 6; j++) {
      sigma_dev[j] = (tau[j] - (1. / 3) * trace_tau * (j < 3)) / (Jm1 + 1);
    }
    // -- sigma_dev:sigma_dev
    CeedScalar sigma_dev_contract = 0;
    for (CeedInt j = 0; j < 6; j++) {
      sigma_dev_contract += sigma_dev[j] * sigma_dev[j] * (1 + (j >= 3));
    }

    // Compute von-Mises stress: sigma_von = sqrt(3/2 sigma_dev:sigma_dev)
    diagnostic[14][i] = sqrt(3. * sigma_dev_contract / 2.);

    for (CeedInt j = 0; j < 15; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop

  return 0;
}

// -----------------------------------------------------------------------------
// Surface forces
// -----------------------------------------------------------------------------
CEED_QFUNCTION(SurfaceForce_NeoHookean)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;
  const CeedScalar         TwoMu   = E / (1 + nu);
  const CeedScalar         mu      = TwoMu / 2;
  const CeedScalar         K_bulk  = E / (3 * (1 - 2 * nu));  // Bulk Modulus
  const CeedScalar         lambda  = (3 * K_bulk - TwoMu) / 3;

  // Quadrature Point Loop
  for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of u
    const CeedScalar du[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };
    // -- Qdata
    // dXdx_initial = dX/dx_initial
    // X is natural coordinate sys OR Reference [-1, 1]^dim
    // x_initial is initial config coordinate system
    const CeedScalar dXdx[3][3] = {
        {q_data[1][i], q_data[2][i], q_data[3][i]},
        {q_data[4][i], q_data[5][i], q_data[6][i]},
        {q_data[7][i], q_data[8][i], q_data[9][i]}
    };
    // Normal
    const CeedScalar normal[3] = {q_data[10][i], q_data[11][i], q_data[12][i]};

    // Compute grad_u
    //   dXdx = (dx/dX)^(-1)
    // Apply dXdx to du = grad_u
    CeedScalar grad_u[3][3];
    RatelMatMatMult(1.0, du, dXdx, grad_u);

    // Compute the Deformation Gradient : F = I + grad_u
    const CeedScalar F[3][3] = {
        {grad_u[0][0] + 1, grad_u[0][1],     grad_u[0][2]    },
        {grad_u[1][0],     grad_u[1][1] + 1, grad_u[1][2]    },
        {grad_u[2][0],     grad_u[2][1],     grad_u[2][2] + 1}
    };

    // Common components of finite strain calculations
    const CeedScalar Jm1  = RatelMatDetAM1(grad_u);
    CeedScalar       logJ = RatelLog1pSeries(Jm1);

    CeedScalar E_Voigt[6], S_Voigt[6], C_inv_Voigt[6];
    GreenLagrangeStrain(grad_u, E_Voigt);
    ComputeCinverse(E_Voigt, Jm1, C_inv_Voigt);
    SecondKirchhoffStress_NeoHookean(lambda, mu, logJ, C_inv_Voigt, E_Voigt, S_Voigt);

    CeedScalar S[3][3];
    RatelVoigtUnpack(S_Voigt, S);

    // Compute the First Piola-Kirchhoff : P = F*S
    CeedScalar P[3][3];
    RatelMatMatMult(1.0, F, S, P);

    // Compute average displacement
    for (CeedInt j = 0; j < 3; j++) diagnostic[j][i] = q_data[0][i] * u[j][i];

    // Compute P N
    for (CeedInt j = 0; j < 3; j++) {
      diagnostic[3 + j][i] = 0.0;
      for (CeedInt k = 0; k < 3; k++) diagnostic[3 + j][i] -= q_data[0][i] * P[j][k] * normal[k];
    }

  }  // End of Quadrature Point Loop

  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_elasticity_neo_hookean_common_qf_h
