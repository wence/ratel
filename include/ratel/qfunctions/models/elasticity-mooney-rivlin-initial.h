/// @file
/// Ratel Mooney-Rivlin hyperelasticity at finite strain in initial configuration QFunction source

#ifndef ratel_elasticity_mooney_rivlin_initial_qf_h
#define ratel_elasticity_mooney_rivlin_initial_qf_h

#include <math.h>

#include "../../models/mooney-rivlin.h"
#include "../utils.h"
#include "elasticity-common.h"

// -----------------------------------------------------------------------------
//  Compute E = (C - I)/2 = (grad_u + grad_u^T + grad_u^T*grad_u)/2
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER void GreenLagrangeStrain(const CeedScalar grad_u[3][3], CeedScalar E[6]) {
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};
  for (CeedInt m = 0; m < 6; m++) {
    E[m] = (grad_u[ind_j[m]][ind_k[m]] + grad_u[ind_k[m]][ind_j[m]]) * 0.5;
    for (CeedInt n = 0; n < 3; n++) {
      E[m] += (grad_u[n][ind_j[m]] * grad_u[n][ind_k[m]]) * 0.5;
    }
  }
}

// -----------------------------------------------------------------------------
//  Compute tau = lambda*logJ*I + 2 (mu_1 + 2 mu_2) e + 2 mu_2 (trace(e) I - e) b
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER void KirchhoffTau_MooneyRivlin(CeedScalar lambda, CeedScalar mu_1, CeedScalar mu_2, const CeedScalar b[6],
                                                     const CeedScalar e[6], CeedScalar logJ, CeedScalar tau_Voigt[6]) {
  // https://ratel.micromorph.org/doc/modeling/materials/mooney-rivlin/#equation-mooney-rivlin-tau-stable
  const CeedScalar trace_e   = e[0] + e[1] + e[2];
  const CeedInt    ind[3][3] = {
         {0, 5, 4},
         {5, 1, 3},
         {4, 3, 2}
  };
  const CeedInt ind_k[6] = {0, 1, 2, 1, 0, 0}, ind_j[6] = {0, 1, 2, 2, 2, 1};
  for (CeedInt m = 0; m < 6; m++) {
    tau_Voigt[m] = 0.0;
    for (CeedInt n = 0; n < 3; n++) {
      const CeedInt ind_e = ind[ind_j[m]][n];
      const CeedInt ind_b = ind[ind_k[m]][n];
      tau_Voigt[m] += 2 * mu_2 * ((ind_e < 3 ? trace_e : 0.0) - e[ind_e]) * b[ind_b];
    }
  }

  const CeedScalar two_mu1_two_mu2 = 2 * (mu_1 + 2 * mu_2);
  tau_Voigt[0] += two_mu1_two_mu2 * e[0] + lambda * logJ;
  tau_Voigt[1] += two_mu1_two_mu2 * e[1] + lambda * logJ;
  tau_Voigt[2] += two_mu1_two_mu2 * e[2] + lambda * logJ;
  tau_Voigt[3] += two_mu1_two_mu2 * e[3];
  tau_Voigt[4] += two_mu1_two_mu2 * e[4];
  tau_Voigt[5] += two_mu1_two_mu2 * e[5];
}

// -----------------------------------------------------------------------------
// Common computations between P and dP
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int common_MooneyRivlinInitial(const CeedScalar mu_1, const CeedScalar mu_2, const CeedScalar lambda,
                                                     const CeedScalar grad_u[3][3], CeedScalar S_Voigt[6], CeedScalar C_Voigt[6],
                                                     CeedScalar C_inv_Voigt[6], CeedScalar *logJ) {
  // Green-Lagrange strain tensor
  // E = 1/2 (grad_u + grad_u^T + grad_u^T*grad_u)
  CeedScalar E_Voigt[6];
  GreenLagrangeStrain(grad_u, E_Voigt);

  CeedScalar E[3][3];
  RatelVoigtUnpack(E_Voigt, E);

  // Right Cauchy-Green tensor
  // C = I + 2E
  const CeedScalar C[3][3] = {
      {2 * E[0][0] + 1, 2 * E[0][1],     2 * E[0][2]    },
      {2 * E[0][1],     2 * E[1][1] + 1, 2 * E[1][2]    },
      {2 * E[0][2],     2 * E[1][2],     2 * E[2][2] + 1}
  };
  RatelVoigtPack(C, C_Voigt);

  // Compute invariants
  const CeedScalar tr_E = E[0][0] + E[1][1] + E[2][2];
  const CeedScalar Jm1  = RatelMatDetAM1(grad_u);

  // Compute C^(-1)
  const CeedScalar det_C = (Jm1 + 1.) * (Jm1 + 1.);
  RatelMatComputeInverseSymmetric(C, det_C, C_inv_Voigt);

  CeedScalar C_inv[3][3];
  RatelVoigtUnpack(C_inv_Voigt, C_inv);
  // Compute the Second Piola-Kirchhoff (S)
  // S = (lambda*logJ)*C_inv_Voigt + 2*(mu_1+2*mu_2)*C_inv*E + 2*mu_2*(tr(E)*I-E)
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};
  *logJ = RatelLog1pSeries(Jm1);
  for (CeedInt m = 0; m < 6; m++) {
    S_Voigt[m] = (lambda * (*logJ)) * C_inv_Voigt[m] + 2 * mu_2 * (tr_E * (m < 3) - E_Voigt[m]);
    for (CeedInt n = 0; n < 3; n++) {
      S_Voigt[m] += 2 * (mu_1 + 2 * mu_2) * C_inv[ind_j[m]][n] * E[n][ind_k[m]];
    }
  }
  return 0;
}

// -----------------------------------------------------------------------------
// Compute First Piola-Kirchhoff
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int P_MooneyRivlinInitial(void *ctx, const CeedInt Q, const CeedInt i, const CeedInt stored_fields_input_offset,
                                                const CeedScalar *const *in, CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar P[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*grad_u)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[1];

  // Context
  const MooneyRivlinPhysics *context = (MooneyRivlinPhysics *)ctx;
  const CeedScalar           mu_1    = context->mu_1;
  const CeedScalar           mu_2    = context->mu_2;
  const CeedScalar           lambda  = context->lambda;

  // Formulation Terminology:
  //  I     : 3x3 Identity matrix
  //  C     : right Cauchy-Green tensor
  //  C_inv : inverse of C
  //  F     : deformation gradient
  //  S     : 2nd Piola-Kirchhoff
  //  P     : 1st Piola-Kirchhoff

  // Read spatial derivatives of u
  const CeedScalar du[3][3] = {
      {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
      {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
      {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
  };
  dXdx[0][0] = q_data[1][i];
  dXdx[0][1] = q_data[2][i];
  dXdx[0][2] = q_data[3][i];
  dXdx[1][0] = q_data[4][i];
  dXdx[1][1] = q_data[5][i];
  dXdx[1][2] = q_data[6][i];
  dXdx[2][0] = q_data[7][i];
  dXdx[2][1] = q_data[8][i];
  dXdx[2][2] = q_data[9][i];

  // Compute grad_u
  //   dXdx = (dx/dX)^(-1)
  // Apply dXdx to du = grad_u
  RatelMatMatMultAtQuadraturePoint(Q, i, 1.0, du, dXdx, grad_u);

  // Compute The Deformation Gradient : F = I + grad_u
  const CeedScalar F[3][3] = {
      {grad_u[0][0][i] + 1, grad_u[0][1][i],     grad_u[0][2][i]    },
      {grad_u[1][0][i],     grad_u[1][1][i] + 1, grad_u[1][2][i]    },
      {grad_u[2][0][i],     grad_u[2][1][i],     grad_u[2][2][i] + 1}
  };

  const CeedScalar temp_grad_u[3][3] = {
      {grad_u[0][0][i], grad_u[0][1][i], grad_u[0][2][i]},
      {grad_u[1][0][i], grad_u[1][1][i], grad_u[1][2][i]},
      {grad_u[2][0][i], grad_u[2][1][i], grad_u[2][2][i]}
  };

  // Common components of finite strain calculations
  CeedScalar S_Voigt[6], C_Voigt[6], C_inv_Voigt[6], logJ;
  common_MooneyRivlinInitial(mu_1, mu_2, lambda, temp_grad_u, S_Voigt, C_Voigt, C_inv_Voigt, &logJ);

  // Second Piola-Kirchhoff (S)
  CeedScalar S[3][3];
  RatelVoigtUnpack(S_Voigt, S);

  // Compute the First Piola-Kirchhoff : P = F*S
  RatelMatMatMult(1.0, F, S, P);

  return 0;
}

// -----------------------------------------------------------------------------
// Compute linearization of First Piola-Kirchhoff
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int dP_MooneyRivlinInitial(void *ctx, const CeedInt Q, const CeedInt i, const CeedInt stored_fields_input_offset,
                                                 const CeedScalar *const *in, CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar dP[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*delta_ug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*grad_u)[3][CEED_Q_VLA]   = (const CeedScalar(*)[3][CEED_Q_VLA])in[stored_fields_input_offset + 0];

  // Context
  const MooneyRivlinPhysics *context = (MooneyRivlinPhysics *)ctx;
  const CeedScalar           mu_1    = context->mu_1;
  const CeedScalar           mu_2    = context->mu_2;
  const CeedScalar           lambda  = context->lambda;

  // Read spatial derivatives of delta_u
  const CeedScalar delta_du[3][3] = {
      {delta_ug[0][0][i], delta_ug[1][0][i], delta_ug[2][0][i]},
      {delta_ug[0][1][i], delta_ug[1][1][i], delta_ug[2][1][i]},
      {delta_ug[0][2][i], delta_ug[1][2][i], delta_ug[2][2][i]}
  };

  // -- Qdata
  dXdx[0][0] = q_data[1][i];
  dXdx[0][1] = q_data[2][i];
  dXdx[0][2] = q_data[3][i];
  dXdx[1][0] = q_data[4][i];
  dXdx[1][1] = q_data[5][i];
  dXdx[1][2] = q_data[6][i];
  dXdx[2][0] = q_data[7][i];
  dXdx[2][1] = q_data[8][i];
  dXdx[2][2] = q_data[9][i];

  // Compute graddeltau
  //   dXdx = (dx/dX)^(-1)
  // Apply dXdx to deltadu = graddelta
  // this is dF
  CeedScalar grad_delta_u[3][3];
  RatelMatMatMult(1.0, delta_du, dXdx, grad_delta_u);

  // Deformation Gradient : F = I + grad_u
  const CeedScalar F[3][3] = {
      {grad_u[0][0][i] + 1, grad_u[0][1][i],     grad_u[0][2][i]    },
      {grad_u[1][0][i],     grad_u[1][1][i] + 1, grad_u[1][2][i]    },
      {grad_u[2][0][i],     grad_u[2][1][i],     grad_u[2][2][i] + 1}
  };

  const CeedScalar temp_grad_u[3][3] = {
      {grad_u[0][0][i], grad_u[0][1][i], grad_u[0][2][i]},
      {grad_u[1][0][i], grad_u[1][1][i], grad_u[1][2][i]},
      {grad_u[2][0][i], grad_u[2][1][i], grad_u[2][2][i]}
  };

  // Common components of finite strain calculations
  CeedScalar S_Voigt[6], C_Voigt[6], C_inv_Voigt[6], logJ;
  common_MooneyRivlinInitial(mu_1, mu_2, lambda, temp_grad_u, S_Voigt, C_Voigt, C_inv_Voigt, &logJ);

  // dE - Green-Lagrange strain tensor
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};
  CeedScalar    dE_Voigt[6];
  for (CeedInt m = 0; m < 6; m++) {
    dE_Voigt[m] = 0;
    for (CeedInt n = 0; n < 3; n++) {
      dE_Voigt[m] += (grad_delta_u[n][ind_j[m]] * F[n][ind_k[m]] + F[n][ind_j[m]] * grad_delta_u[n][ind_k[m]]) / 2.;
    }
  }
  CeedScalar dE[3][3];
  RatelVoigtUnpack(dE_Voigt, dE);
  // C^(-1) : inverse of right Cauchy-Green tensor
  CeedScalar C_inv[3][3];
  RatelVoigtUnpack(C_inv_Voigt, C_inv);

  // -- C_inv:dE
  CeedScalar C_inv_contract_dE = RatelMatMatContract(1.0, C_inv, dE);
  // -- dE*C_inv
  CeedScalar dE_C_inv[3][3];
  RatelMatMatMult(1.0, dE, C_inv, dE_C_inv);
  // -- C_inv*dE*C_inv
  CeedScalar C_inv_dE_C_inv[3][3];
  // This product is symmetric and we only use the upper-triangular part
  // below, but naively compute the whole thing here
  RatelMatMatMult(1.0, C_inv, dE_C_inv, C_inv_dE_C_inv);
  // Compute dS = (mu_2)*((2*I:dE)*I - dE) + 2*d*C_inv_dE_C_inv + lambda*C_inv_contract_dE*C_inv_Voigt - 2*lambda*logJ*C_inv_dE_C_inv;
  // (2*I:dE)*I - dE = 2*trace(dE)*I - dE = 2trace(dE) - dE on the diagonal
  // (2*I:dE)*I - dE = -dE elsewhere
  // CeedScalar J = Jm1 + 1;
  CeedScalar trace_dE = dE[0][0] + dE[1][1] + dE[2][2];
  CeedScalar dS_Voigt[6];
  for (CeedInt i = 0; i < 6; i++) {
    dS_Voigt[i] = lambda * C_inv_contract_dE * C_inv_Voigt[i] + 2 * (mu_1 + 2 * mu_2 - lambda * logJ) * C_inv_dE_C_inv[ind_j[i]][ind_k[i]] +
                  2 * mu_2 * (trace_dE * (i < 3) - dE_Voigt[i]);
  }

  CeedScalar dS[3][3];
  RatelVoigtUnpack(dS_Voigt, dS);
  // Second Piola-Kirchhoff (S)
  CeedScalar S[3][3];
  RatelVoigtUnpack(S_Voigt, S);
  // dP = dPdF:dF = dF*S + F*dS
  RatelMatMatMultPlusMatMatMult(grad_delta_u, S, F, dS, dP);

  return 0;
}

// -----------------------------------------------------------------------------
// Residual evaluation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ElasticityResidual_MooneyRivlinInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, P_MooneyRivlinInitial, in, out);
}

// -----------------------------------------------------------------------------
// Jacobian evaluation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ElasticityJacobian_MooneyRivlinInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, dP_MooneyRivlinInitial, in, out);
}

// -----------------------------------------------------------------------------
// Strain energy computation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(Energy_MooneyRivlinInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context
  const MooneyRivlinPhysics *context = (MooneyRivlinPhysics *)ctx;
  const CeedScalar           mu_1    = context->mu_1;
  const CeedScalar           mu_2    = context->mu_2;
  const CeedScalar           lambda  = context->lambda;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of u
    const CeedScalar du[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };
    // -- Qdata
    const CeedScalar wdetJ      = q_data[0][i];
    const CeedScalar dXdx[3][3] = {
        {q_data[1][i], q_data[2][i], q_data[3][i]},
        {q_data[4][i], q_data[5][i], q_data[6][i]},
        {q_data[7][i], q_data[8][i], q_data[9][i]}
    };
    // Compute grad_u
    //   dXdx = (dx/dX)^(-1)
    // Apply dXdx to du = grad_u
    CeedScalar grad_u[3][3];
    RatelMatMatMult(1.0, du, dXdx, grad_u);

    // E - Green-Lagrange strain tensor
    // E = 1/2 (grad_u + grad_u^T + grad_u^T*grad_u)
    const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};
    CeedScalar    E2_Voigt[6];
    for (CeedInt m = 0; m < 6; m++) {
      E2_Voigt[m] = grad_u[ind_j[m]][ind_k[m]] + grad_u[ind_k[m]][ind_j[m]];
      for (CeedInt n = 0; n < 3; n++) {
        E2_Voigt[m] += grad_u[n][ind_j[m]] * grad_u[n][ind_k[m]];
      }
    }
    CeedScalar E2[3][3];
    RatelVoigtUnpack(E2_Voigt, E2);

    // C : right Cauchy-Green tensor
    // C = I + 2E
    const CeedScalar C[3][3] = {
        {E2[0][0] + 1, E2[0][1],     E2[0][2]    },
        {E2[0][1],     E2[1][1] + 1, E2[1][2]    },
        {E2[0][2],     E2[1][2],     E2[2][2] + 1}
    };
    // Compute CC = C*C = C^2
    CeedScalar CC[3][3];
    RatelMatMatMult(1.0, C, C, CC);

    const CeedScalar Jm1      = RatelMatDetAM1(grad_u);
    // CeedScalar J = Jm1 + 1;
    // Compute invariants
    // I_1 = trace(C)
    const CeedScalar I_1      = RatelTrace3(C);
    // Trace(C^2)
    const CeedScalar trace_CC = RatelTrace3(CC);
    // I_2 = 0.5(I_1^2 - trace(C^2))
    const CeedScalar I_2      = 0.5 * (I_1 * I_1 - trace_CC);

    const CeedScalar logJ = RatelLog1pSeries(Jm1);

    // Strain energy Phi(E) for Mooney-Rivlin
    energy[i] = (0.5 * lambda * (logJ) * (logJ) - (mu_1 + 2 * mu_2) * logJ + (mu_1 / 2.) * (I_1 - 3) + (mu_2 / 2.) * (I_2 - 3)) * wdetJ;

  }  // End of Quadrature Point Loop

  return 0;
}

// -----------------------------------------------------------------------------
// Nodal diagnostic quantities
// -----------------------------------------------------------------------------
CEED_QFUNCTION(Diagnostic_MooneyRivlinInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const MooneyRivlinPhysics *context = (MooneyRivlinPhysics *)ctx;
  const CeedScalar           mu_1    = context->mu_1;
  const CeedScalar           mu_2    = context->mu_2;
  const CeedScalar           lambda  = context->lambda;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of u
    const CeedScalar du[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };
    // -- Qdata
    const CeedScalar wdetJ      = q_data[0][i];
    const CeedScalar dXdx[3][3] = {
        {q_data[1][i], q_data[2][i], q_data[3][i]},
        {q_data[4][i], q_data[5][i], q_data[6][i]},
        {q_data[7][i], q_data[8][i], q_data[9][i]}
    };

    // Compute grad_u
    //   dXdx = (dx/dX)^(-1)
    // Apply dXdx to du = grad_u
    CeedScalar grad_u[3][3];
    RatelMatMatMult(1.0, du, dXdx, grad_u);

    // Deformation gradient
    const CeedScalar F[3][3] = {
        {grad_u[0][0] + 1, grad_u[0][1],     grad_u[0][2]    },
        {grad_u[1][0],     grad_u[1][1] + 1, grad_u[1][2]    },
        {grad_u[2][0],     grad_u[2][1],     grad_u[2][2] + 1}
    };

    // E - Green-Lagrange strain tensor
    //     E = 1/2 (grad_u + grad_u^T + grad_u^T*grad_u)
    const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};
    CeedScalar    E2_Voigt[6];
    for (CeedInt m = 0; m < 6; m++) {
      E2_Voigt[m] = grad_u[ind_j[m]][ind_k[m]] + grad_u[ind_k[m]][ind_j[m]];
      for (CeedInt n = 0; n < 3; n++) {
        E2_Voigt[m] += grad_u[n][ind_j[m]] * grad_u[n][ind_k[m]];
      }
    }
    CeedScalar E2[3][3];
    RatelVoigtUnpack(E2_Voigt, E2);

    // C : right Cauchy-Green tensor
    // C = I + 2E
    const CeedScalar C[3][3] = {
        {E2[0][0] + 1, E2[0][1],     E2[0][2]    },
        {E2[0][1],     E2[1][1] + 1, E2[1][2]    },
        {E2[0][2],     E2[1][2],     E2[2][2] + 1}
    };
    // Compute CC = C*C = C^2
    CeedScalar CC[3][3];
    RatelMatMatMult(1.0, C, C, CC);

    // Compute C^(-1) : C-Inverse
    const CeedScalar Jm1  = RatelMatDetAM1(grad_u);
    const CeedScalar logJ = RatelLog1pSeries(Jm1);
    CeedScalar       C_inv_Voigt[6], C_Voigt[6];
    const CeedScalar detC = (Jm1 + 1.) * (Jm1 + 1.);
    RatelMatComputeInverseSymmetric(C, detC, C_inv_Voigt);
    RatelVoigtPack(C, C_Voigt);

    // Compute the Second Piola-Kirchhoff (S)
    CeedScalar S_Voigt[6];
    for (CeedInt i = 0; i < 6; i++) {
      S_Voigt[i] = (lambda * logJ - mu_1 - 2 * mu_2) * C_inv_Voigt[i] + (mu_1 + mu_2 * RatelTrace3(C)) * (i < 3) /* identity I */ - mu_2 * C_Voigt[i];
    }
    CeedScalar S[3][3];
    RatelVoigtUnpack(S_Voigt, S);

    // Compute the First Piola-Kirchhoff : P = F*S
    CeedScalar P[3][3];
    RatelMatMatMult(1.0, F, S, P);

    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    CeedScalar sigma[3][3];
    RatelMatMatTransposeMult(1.0 / (Jm1 + 1.0), P, F, sigma);
    diagnostic[3][i] = sigma[0][0];
    diagnostic[4][i] = sigma[0][1];
    diagnostic[5][i] = sigma[0][1];
    diagnostic[6][i] = sigma[1][1];
    diagnostic[7][i] = sigma[1][2];
    diagnostic[8][i] = sigma[2][2];

    // Pressure
    diagnostic[9][i] = -lambda * logJ;

    // Stress tensor invariants
    diagnostic[10][i] = (E2[0][0] + E2[1][1] + E2[2][2]) / 2.;
    diagnostic[11][i] = 0.;
    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt m = 0; m < 3; m++) {
        diagnostic[11][i] += E2[j][m] * E2[m][j] / 4.;
      }
    }
    diagnostic[12][i] = Jm1 + 1;

    // Compute invariants
    // I_1 = trace(C)
    const CeedScalar I_1      = RatelTrace3(C);
    // Trace(C^2)
    const CeedScalar trace_CC = RatelTrace3(CC);
    // I_2 = 0.5(I_1^2 - trace(C^2))
    const CeedScalar I_2      = 0.5 * (pow(I_1, 2) - trace_CC);

    // Strain energy
    diagnostic[13][i] = (0.5 * lambda * logJ * logJ - (mu_1 + 2 * mu_2) * logJ + (mu_1 / 2.) * (I_1 - 3) + (mu_2 / 2.) * (I_2 - 3));

    // ---------- Compute von-Mises stress----------

    // Compute the trace of Cauchy stress: trace_sigma = trace(sigma)
    const CeedScalar trace_sigma = RatelTrace3(sigma);

    // Compute the the deviatoric part of Cauchy stress: sigma_dev = sigma - trace(sigma)/3
    const CeedScalar sigma_dev[3][3] = {
        {sigma[0][0] - trace_sigma / 3., sigma[0][1],                    sigma[0][2]                   },
        {sigma[1][0],                    sigma[1][1] - trace_sigma / 3., sigma[1][2]                   },
        {sigma[2][0],                    sigma[2][1],                    sigma[2][2] - trace_sigma / 3.}
    };

    // -- sigma_dev:sigma_dev
    CeedScalar sigma_dev_contract = RatelMatMatContract(1.0, sigma_dev, sigma_dev);

    // Compute von-Mises stress: sigma_von = sqrt(3/2 sigma_dev:sigma_dev)
    diagnostic[14][i] = sqrt(3. * sigma_dev_contract / 2.);

    for (CeedInt j = 0; j < 15; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop

  return 0;
}

// -----------------------------------------------------------------------------
// Surface forces
// -----------------------------------------------------------------------------
CEED_QFUNCTION(SurfaceForce_MooneyRivlinInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const MooneyRivlinPhysics *context = (MooneyRivlinPhysics *)ctx;
  const CeedScalar           mu_1    = context->mu_1;
  const CeedScalar           mu_2    = context->mu_2;
  const CeedScalar           lambda  = context->lambda;

  // Quadrature Point Loop
  for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of u
    const CeedScalar du[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };
    // -- Qdata
    // dXdx_initial = dX/dx_initial
    // X is natural coordinate sys OR Reference [-1, 1]^dim
    // x_initial is initial config coordinate system
    const CeedScalar dXdx[3][3] = {
        {q_data[1][i], q_data[2][i], q_data[3][i]},
        {q_data[4][i], q_data[5][i], q_data[6][i]},
        {q_data[7][i], q_data[8][i], q_data[9][i]}
    };
    // Normal
    const CeedScalar normal[3] = {q_data[10][i], q_data[11][i], q_data[12][i]};

    // Compute grad_u
    //   dXdx = (dx/dX)^(-1)
    // Apply dXdx to du = grad_u
    CeedScalar grad_u[3][3];
    RatelMatMatMult(1.0, du, dXdx, grad_u);

    // Compute The Deformation Gradient : F = I + grad_u
    const CeedScalar F[3][3] = {
        {grad_u[0][0] + 1, grad_u[0][1],     grad_u[0][2]    },
        {grad_u[1][0],     grad_u[1][1] + 1, grad_u[1][2]    },
        {grad_u[2][0],     grad_u[2][1],     grad_u[2][2] + 1}
    };

    // Common components of finite strain calculations
    CeedScalar S_Voigt[6], C_Voigt[6], C_inv_Voigt[6], logJ;
    common_MooneyRivlinInitial(mu_1, mu_2, lambda, grad_u, S_Voigt, C_Voigt, C_inv_Voigt, &logJ);

    // Second Piola-Kirchhoff (S)
    CeedScalar S[3][3];
    RatelVoigtUnpack(S_Voigt, S);

    // Compute the First Piola-Kirchhoff : P = F*S
    CeedScalar P[3][3];
    RatelMatMatMult(1.0, F, S, P);

    // Compute average displacement
    for (CeedInt j = 0; j < 3; j++) diagnostic[j][i] = q_data[0][i] * u[j][i];

    // Compute P N
    for (CeedInt j = 0; j < 3; j++) {
      diagnostic[3 + j][i] = 0.0;
      for (CeedInt k = 0; k < 3; k++) diagnostic[3 + j][i] -= q_data[0][i] * P[j][k] * normal[k];
    }

  }  // End of Quadrature Point Loop

  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_elasticity_mooney_rivlin_initial_qf_h
