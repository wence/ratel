/// @file
/// Ratel Neo-Hookean hyperelasticity at finite strain in initial configuration QFunction source

#ifndef ratel_elasticity_neo_hookean_initial_qf_h
#define ratel_elasticity_neo_hookean_initial_qf_h

#include <math.h>

#include "../../models/neo-hookean.h"
#include "../utils.h"
#include "elasticity-common.h"
#include "elasticity-neo-hookean-common.h"

// -----------------------------------------------------------------------------
// Compute First Piola-Kirchhoff
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int P_NeoHookeanInitial(void *ctx, const CeedInt Q, const CeedInt i, const CeedInt stored_fields_input_offset,
                                              const CeedScalar *const *in, CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar P[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*grad_u)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[1];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;
  const CeedScalar         TwoMu   = E / (1 + nu);
  const CeedScalar         mu      = TwoMu / 2;
  const CeedScalar         K_bulk  = E / (3 * (1 - 2 * nu));  // Bulk Modulus
  const CeedScalar         lambda  = (3 * K_bulk - TwoMu) / 3;

  // Formulation Terminology:
  //  I     : 3x3 Identity matrix
  //  C     : right Cauchy-Green tensor
  //  C_inv  : inverse of C
  //  F     : deformation gradient
  //  S     : 2nd Piola-Kirchhoff (in current config)
  //  P     : 1st Piola-Kirchhoff (in referential config)
  // Formulation:
  //  F =  I + grad_ue
  //  J = det(F)
  //  C = F(^T)*F
  //  S = mu*I + (lambda*log(J)-mu)*C_inv;
  //  P = F*S

  // Read spatial derivatives of u
  const CeedScalar du[3][3] = {
      {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
      {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
      {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
  };

  // -- Qdata
  dXdx[0][0] = q_data[1][i];
  dXdx[0][1] = q_data[2][i];
  dXdx[0][2] = q_data[3][i];
  dXdx[1][0] = q_data[4][i];
  dXdx[1][1] = q_data[5][i];
  dXdx[1][2] = q_data[6][i];
  dXdx[2][0] = q_data[7][i];
  dXdx[2][1] = q_data[8][i];
  dXdx[2][2] = q_data[9][i];

  // Compute grad_u
  //   dXdx = (dx/dX)^(-1)
  // Apply dXdx to du = grad_u
  RatelMatMatMultAtQuadraturePoint(Q, i, 1.0, du, dXdx, grad_u);

  // Compute the Deformation Gradient : F = I + grad_u
  const CeedScalar F[3][3] = {
      {grad_u[0][0][i] + 1, grad_u[0][1][i],     grad_u[0][2][i]    },
      {grad_u[1][0][i],     grad_u[1][1][i] + 1, grad_u[1][2][i]    },
      {grad_u[2][0][i],     grad_u[2][1][i],     grad_u[2][2][i] + 1}
  };

  const CeedScalar temp_grad_u[3][3] = {
      {grad_u[0][0][i], grad_u[0][1][i], grad_u[0][2][i]},
      {grad_u[1][0][i], grad_u[1][1][i], grad_u[1][2][i]},
      {grad_u[2][0][i], grad_u[2][1][i], grad_u[2][2][i]}
  };

  // Common components of finite strain calculations
  const CeedScalar Jm1  = RatelMatDetAM1(temp_grad_u);
  CeedScalar       logJ = RatelLog1pSeries(Jm1);

  CeedScalar E_Voigt[6], S_Voigt[6], C_inv_Voigt[6];
  GreenLagrangeStrain(temp_grad_u, E_Voigt);
  ComputeCinverse(E_Voigt, Jm1, C_inv_Voigt);
  SecondKirchhoffStress_NeoHookean(lambda, mu, logJ, C_inv_Voigt, E_Voigt, S_Voigt);

  CeedScalar S[3][3];
  RatelVoigtUnpack(S_Voigt, S);

  // Compute the First Piola-Kirchhoff : P = F*S
  RatelMatMatMult(1.0, F, S, P);

  return 0;
}

// -----------------------------------------------------------------------------
// Compute linearization of First Piola-Kirchhoff
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int dP_NeoHookeanInitial(void *ctx, const CeedInt Q, const CeedInt i, const CeedInt stored_fields_input_offset,
                                               const CeedScalar *const *in, CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar dP[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*delta_ug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*grad_u)[3][CEED_Q_VLA]   = (const CeedScalar(*)[3][CEED_Q_VLA])in[stored_fields_input_offset + 0];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;

  // Constants
  const CeedScalar TwoMu  = E / (1 + nu);
  const CeedScalar mu     = TwoMu / 2;
  const CeedScalar K_bulk = E / (3 * (1 - 2 * nu));  // Bulk Modulus
  const CeedScalar lambda = (3 * K_bulk - TwoMu) / 3;

  // Read spatial derivatives of delta_u
  const CeedScalar delta_du[3][3] = {
      {delta_ug[0][0][i], delta_ug[1][0][i], delta_ug[2][0][i]},
      {delta_ug[0][1][i], delta_ug[1][1][i], delta_ug[2][1][i]},
      {delta_ug[0][2][i], delta_ug[1][2][i], delta_ug[2][2][i]}
  };

  // -- Qdata
  dXdx[0][0] = q_data[1][i];
  dXdx[0][1] = q_data[2][i];
  dXdx[0][2] = q_data[3][i];
  dXdx[1][0] = q_data[4][i];
  dXdx[1][1] = q_data[5][i];
  dXdx[1][2] = q_data[6][i];
  dXdx[2][0] = q_data[7][i];
  dXdx[2][1] = q_data[8][i];
  dXdx[2][2] = q_data[9][i];

  // Compute grad_delta_u
  //   dXdx = (dx/dX)^(-1)
  // Apply dXdx to delta_du = graddelta
  CeedScalar grad_delta_u[3][3];
  RatelMatMatMult(1.0, delta_du, dXdx, grad_delta_u);

  // Deformation Gradient : F = I + grad_u
  const CeedScalar F[3][3] = {
      {grad_u[0][0][i] + 1, grad_u[0][1][i],     grad_u[0][2][i]    },
      {grad_u[1][0][i],     grad_u[1][1][i] + 1, grad_u[1][2][i]    },
      {grad_u[2][0][i],     grad_u[2][1][i],     grad_u[2][2][i] + 1}
  };

  const CeedScalar temp_grad_u[3][3] = {
      {grad_u[0][0][i], grad_u[0][1][i], grad_u[0][2][i]},
      {grad_u[1][0][i], grad_u[1][1][i], grad_u[1][2][i]},
      {grad_u[2][0][i], grad_u[2][1][i], grad_u[2][2][i]}
  };

  // deltaE - Green-Lagrange strain tensor
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};
  CeedScalar    delta_E_Voigt[6];
  for (CeedInt m = 0; m < 6; m++) {
    delta_E_Voigt[m] = 0;
    for (CeedInt n = 0; n < 3; n++) {
      delta_E_Voigt[m] += (grad_delta_u[n][ind_j[m]] * F[n][ind_k[m]] + F[n][ind_j[m]] * grad_delta_u[n][ind_k[m]]) / 2.;
    }
  }

  CeedScalar deltaE[3][3];
  RatelVoigtUnpack(delta_E_Voigt, deltaE);

  const CeedScalar Jm1  = RatelMatDetAM1(temp_grad_u);
  CeedScalar       logJ = RatelLog1pSeries(Jm1);

  CeedScalar E_Voigt[6], S_Voigt[6], C_inv_Voigt[6];
  GreenLagrangeStrain(temp_grad_u, E_Voigt);
  ComputeCinverse(E_Voigt, Jm1, C_inv_Voigt);
  SecondKirchhoffStress_NeoHookean(lambda, mu, logJ, C_inv_Voigt, E_Voigt, S_Voigt);

  CeedScalar C_inv[3][3];
  RatelVoigtUnpack(C_inv_Voigt, C_inv);

  CeedScalar S[3][3];
  RatelVoigtUnpack(S_Voigt, S);

  // deltaS = dSdE:deltaE
  //      = lambda(C_inv:deltaE)C_inv + 2(mu-lambda*log(J))C_inv*deltaE*C_inv
  // -- C_inv:deltaE
  CeedScalar Cinv_contract_E = RatelMatMatContract(1.0, C_inv, deltaE);
  // -- deltaE*C_inv
  CeedScalar deltaEC_inv[3][3];
  RatelMatMatMult(1.0, deltaE, C_inv, deltaEC_inv);
  // -- intermediate deltaS = C_inv*deltaE*C_inv
  CeedScalar deltaS[3][3];
  RatelMatMatMult(1.0, C_inv, deltaEC_inv, deltaS);
  // -- deltaS = lambda(C_inv:deltaE)C_inv - 2(lambda*log(J)-mu)*(intermediate)
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      deltaS[j][k] = lambda * Cinv_contract_E * C_inv[j][k] - 2. * (lambda * logJ - mu) * deltaS[j][k];
    }
  }
  // delta_P = dPdF:deltaF = deltaF*S + F*deltaS
  RatelMatMatMultPlusMatMatMult(grad_delta_u, S, F, deltaS, dP);

  return 0;
}

// -----------------------------------------------------------------------------
// Residual evaluation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ElasticityResidual_NeoHookeanInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, P_NeoHookeanInitial, in, out);
}

// -----------------------------------------------------------------------------
// Jacobian evaluation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ElasticityJacobian_NeoHookeanInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, dP_NeoHookeanInitial, in, out);
}
// -----------------------------------------------------------------------------

#endif  // ratel_elasticity_neo_hookean_initial_qf_h
