/// @file
/// Ratel Neo-Hookean hyperelasticity at finite strain in current configuration QFunction source

#ifndef ratel_elasticity_neo_hookean_current_qf_h
#define ratel_elasticity_neo_hookean_current_qf_h

#include <math.h>

#include "../../models/neo-hookean.h"
#include "../utils.h"
#include "elasticity-common.h"
#include "elasticity-neo-hookean-common.h"

// -----------------------------------------------------------------------------
// Compute First Piola-Kirchhoff
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int P_NeoHookeanCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedInt stored_fields_input_offset,
                                              const CeedScalar *const *in, CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar P[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*dXdx_stored)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[1];
  CeedScalar(*tau_save)[CEED_Q_VLA]       = (CeedScalar(*)[CEED_Q_VLA])out[2];
  CeedScalar(*lam_log_J)[CEED_Q_VLA]      = (CeedScalar(*)[CEED_Q_VLA])out[3];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;
  const CeedScalar         TwoMu   = E / (1 + nu);
  const CeedScalar         mu      = TwoMu / 2;
  const CeedScalar         K_bulk  = E / (3 * (1 - 2 * nu));  // Bulk Modulus
  const CeedScalar         lambda  = (3 * K_bulk - TwoMu) / 3;

  // Formulation Terminology:
  //  I     : 3x3 Identity matrix
  //  b     : left Cauchy-Green tensor
  //  F     : deformation gradient
  //  tau   : Kirchhoff stress (in current config)
  // Formulation:
  //  F =  I + Grad_ue
  //  J = det(F)
  //  b = F*F^{T}
  //  tau = mu*(b-I) +  lambda*logJ*I;

  // Read spatial derivatives of u
  const CeedScalar du[3][3] = {
      {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
      {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
      {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
  };
  // -- Qdata
  // dXdx_initial = dX/dx_initial
  // X is natural coordinate sys OR Reference [-1, 1]^dim
  // x_initial is initial config coordinate system
  const CeedScalar dXdx_initial[3][3] = {
      {q_data[1][i], q_data[2][i], q_data[3][i]},
      {q_data[4][i], q_data[5][i], q_data[6][i]},
      {q_data[7][i], q_data[8][i], q_data[9][i]}
  };

  // X is natural coordinate sys OR Reference system
  // x_initial is initial config coordinate system
  // Grad_u =du/dx_initial= du/dX * dX/dx_initial
  CeedScalar Grad_u[3][3];
  RatelMatMatMult(1.0, du, dXdx_initial, Grad_u);

  // Compute the Deformation Gradient : F = I + Grad_u
  const CeedScalar F[3][3] = {
      {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
      {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
      {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
  };
  CeedScalar e[6];
  GreenEulerStrain(Grad_u, e);

  const CeedScalar Jm1  = RatelMatDetAM1(Grad_u);
  const CeedScalar logJ = RatelLog1pSeries(Jm1);

  lam_log_J[0][i] = lambda * logJ;
  CeedScalar tau[6];
  KirchhoffTau_NeoHookean(lambda, mu, e, logJ, tau);
  for (CeedInt j = 0; j < 6; j++) tau_save[j][i] = tau[j];

  // Compute F^{-1}
  const CeedScalar detF = Jm1 + 1.;
  CeedScalar       F_inv_Voigt[9];
  RatelMatComputeInveseNonSymmetric(F, detF, F_inv_Voigt);
  CeedScalar F_inv[3][3];
  F_inv[0][0] = F_inv_Voigt[0];
  F_inv[0][1] = F_inv_Voigt[5];
  F_inv[0][2] = F_inv_Voigt[4];
  F_inv[1][0] = F_inv_Voigt[8];
  F_inv[1][1] = F_inv_Voigt[1];
  F_inv[1][2] = F_inv_Voigt[3];
  F_inv[2][0] = F_inv_Voigt[7];
  F_inv[2][1] = F_inv_Voigt[6];
  F_inv[2][2] = F_inv_Voigt[2];

  // x is current config coordinate system
  // dXdx = dX/dx = dX/dx_initial * F^{-1}
  // Note that F^{-1} = dx_initial/dx
  RatelMatMatMultAtQuadraturePoint(Q, i, 1.0, dXdx_initial, F_inv, dXdx_stored);
  dXdx[0][0] = dXdx_stored[0][0][i];
  dXdx[0][1] = dXdx_stored[0][1][i];
  dXdx[0][2] = dXdx_stored[0][2][i];
  dXdx[1][0] = dXdx_stored[1][0][i];
  dXdx[1][1] = dXdx_stored[1][1][i];
  dXdx[1][2] = dXdx_stored[1][2][i];
  dXdx[2][0] = dXdx_stored[2][0][i];
  dXdx[2][1] = dXdx_stored[2][1][i];
  dXdx[2][2] = dXdx_stored[2][2][i];

  RatelVoigtUnpack(tau, P);

  return 0;
}

// -----------------------------------------------------------------------------
// Compute linearization of First Piola-Kirchhoff
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int dP_NeoHookeanCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedInt stored_fields_input_offset,
                                               const CeedScalar *const *in, CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar dP[3][3]) {
  // Inputs
  const CeedScalar(*delta_ug)[3][CEED_Q_VLA]    = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*dXdx_stored)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[stored_fields_input_offset + 0];
  const CeedScalar(*tau)[CEED_Q_VLA]            = (const CeedScalar(*)[CEED_Q_VLA])in[stored_fields_input_offset + 1];
  const CeedScalar(*lam_log_J)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[stored_fields_input_offset + 2];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;

  // Constants
  const CeedScalar TwoMu  = E / (1 + nu);
  const CeedScalar mu     = TwoMu / 2;
  const CeedScalar K_bulk = E / (3 * (1 - 2 * nu));  // Bulk Modulus
  const CeedScalar lambda = (3 * K_bulk - TwoMu) / 3;

  // Read spatial derivatives of delta_u
  const CeedScalar delta_du[3][3] = {
      {delta_ug[0][0][i], delta_ug[1][0][i], delta_ug[2][0][i]},
      {delta_ug[0][1][i], delta_ug[1][1][i], delta_ug[2][1][i]},
      {delta_ug[0][2][i], delta_ug[1][2][i], delta_ug[2][2][i]}
  };

  // Retreive dXdx
  dXdx[0][0] = dXdx_stored[0][0][i];
  dXdx[0][1] = dXdx_stored[0][1][i];
  dXdx[0][2] = dXdx_stored[0][2][i];
  dXdx[1][0] = dXdx_stored[1][0][i];
  dXdx[1][1] = dXdx_stored[1][1][i];
  dXdx[1][2] = dXdx_stored[1][2][i];
  dXdx[2][0] = dXdx_stored[2][0][i];
  dXdx[2][1] = dXdx_stored[2][1][i];
  dXdx[2][2] = dXdx_stored[2][2][i];

  // Compute grad_du = \nabla_x (deltau) = deltau * dX/dx
  CeedScalar grad_du[3][3];
  RatelMatMatMult(1.0, delta_du, dXdx, grad_du);

  const CeedScalar temp_tau[3][3] = {
      {tau[0][i], tau[5][i], tau[4][i]},
      {tau[5][i], tau[1][i], tau[3][i]},
      {tau[4][i], tau[3][i], tau[2][i]}
  };

  // Compute grad_du_tau = grad_du*tau
  CeedScalar grad_du_tau[3][3];
  RatelMatMatMult(1.0, grad_du, temp_tau, grad_du_tau);

  // Compute depsilon = (grad_du + grad_du^T)/2
  const CeedScalar depsilon[3][3] = {
      {(grad_du[0][0] + grad_du[0][0]) / 2., (grad_du[0][1] + grad_du[1][0]) / 2., (grad_du[0][2] + grad_du[2][0]) / 2.},
      {(grad_du[1][0] + grad_du[0][1]) / 2., (grad_du[1][1] + grad_du[1][1]) / 2., (grad_du[1][2] + grad_du[2][1]) / 2.},
      {(grad_du[2][0] + grad_du[0][2]) / 2., (grad_du[2][1] + grad_du[1][2]) / 2., (grad_du[2][2] + grad_du[2][2]) / 2.}
  };

  // Compute trace(depsilon)
  CeedScalar trace_depsilon = RatelTrace3(depsilon);

  // Compute grad_du*tau + trace(depsilon)I
  grad_du_tau[0][0] += lambda * trace_depsilon;
  grad_du_tau[1][1] += lambda * trace_depsilon;
  grad_du_tau[2][2] += lambda * trace_depsilon;

  // Compute dp = grad_du*tau + trace(depsilon)I +2(mu-lambda*logJ)depsilon
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      dP[j][k] = grad_du_tau[j][k] + 2 * (mu - lam_log_J[0][i]) * depsilon[j][k];
    }
  }

  return 0;
}

// -----------------------------------------------------------------------------
// Residual evaluation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ElasticityResidual_NeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, P_NeoHookeanCurrent, in, out);
}

// -----------------------------------------------------------------------------
// Jacobian evaluation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ElasticityJacobian_NeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, dP_NeoHookeanCurrent, in, out);
}
// -----------------------------------------------------------------------------

#endif  // ratel_elasticity_neo_hookean_current_qf_h
