/// @file
/// Ratel linear elasticity QFunction source

#ifndef ratel_elasticity_linear_qf_h
#define ratel_elasticity_linear_qf_h

#include <math.h>

#include "../../models/neo-hookean.h"
#include "../utils.h"
#include "elasticity-common.h"

// -----------------------------------------------------------------------------
// Compute First Piola-Kirchhoff
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int Sigma_Linear(void *ctx, const CeedInt Q, const CeedInt i, const CeedInt stored_fields_input_offset,
                                       const CeedScalar *const *in, CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar sigma[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;

  // Read spatial derivatives of u
  const CeedScalar du[3][3] = {
      {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
      {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
      {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
  };

  // -- Qdata
  dXdx[0][0] = q_data[1][i];
  dXdx[0][1] = q_data[2][i];
  dXdx[0][2] = q_data[3][i];
  dXdx[1][0] = q_data[4][i];
  dXdx[1][1] = q_data[5][i];
  dXdx[1][2] = q_data[6][i];
  dXdx[2][0] = q_data[7][i];
  dXdx[2][1] = q_data[8][i];
  dXdx[2][2] = q_data[9][i];

  // Compute grad_u
  //   dXdx = (dx/dX)^(-1)
  // Apply dXdx to du = grad_u
  CeedScalar grad_u[3][3];
  RatelMatMatMult(1.0, du, dXdx, grad_u);

  // Compute Strain : e (epsilon)
  // e = 1/2 (grad u + (grad u)^T)
  const CeedScalar e[3][3] = {
      {(grad_u[0][0] + grad_u[0][0]) / 2., (grad_u[0][1] + grad_u[1][0]) / 2., (grad_u[0][2] + grad_u[2][0]) / 2.},
      {(grad_u[1][0] + grad_u[0][1]) / 2., (grad_u[1][1] + grad_u[1][1]) / 2., (grad_u[1][2] + grad_u[2][1]) / 2.},
      {(grad_u[2][0] + grad_u[0][2]) / 2., (grad_u[2][1] + grad_u[1][2]) / 2., (grad_u[2][2] + grad_u[2][2]) / 2.}
  };

  // Formulation uses Voigt notation:
  //  stress (sigma)      strain (epsilon)
  //
  //    [sigma00]             [e00]
  //    [sigma11]             [e11]
  //    [sigma22]   =  S   *  [e22]
  //    [sigma12]             [e12]
  //    [sigma02]             [e02]
  //    [sigma01]             [e01]
  //
  //        where
  //                         [1-nu   nu    nu                                    ]
  //                         [ nu   1-nu   nu                                    ]
  //                         [ nu    nu   1-nu                                   ]
  // S = E/((1+nu)*(1-2*nu)) [                  (1-2*nu)/2                       ]
  //                         [                             (1-2*nu)/2            ]
  //                         [                                        (1-2*nu)/2 ]
  // Above Voigt Notation is placed in a 3x3 matrix:
  const CeedScalar ss = E / ((1 + nu) * (1 - 2 * nu));

  const CeedScalar sigma_Voigt[6] = {ss * ((1 - nu) * e[0][0] + nu * e[1][1] + nu * e[2][2]),
                                     ss * (nu * e[0][0] + (1 - nu) * e[1][1] + nu * e[2][2]),
                                     ss * (nu * e[0][0] + nu * e[1][1] + (1 - nu) * e[2][2]),
                                     ss * (1 - 2 * nu) * e[1][2] * 0.5,
                                     ss * (1 - 2 * nu) * e[0][2] * 0.5,
                                     ss * (1 - 2 * nu) * e[0][1] * 0.5};
  RatelVoigtUnpack(sigma_Voigt, sigma);

  return 0;
}

// -----------------------------------------------------------------------------
// Compute linearization of First Piola-Kirchhoff
// -----------------------------------------------------------------------------
CEED_QFUNCTION_HELPER int dSigma_Linear(void *ctx, const CeedInt Q, const CeedInt i, const CeedInt stored_fields_input_offset,
                                        const CeedScalar *const *in, CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar dsigma[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]     = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*deltaug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;

  // Read spatial derivatives of u
  const CeedScalar delta_du[3][3] = {
      {deltaug[0][0][i], deltaug[1][0][i], deltaug[2][0][i]},
      {deltaug[0][1][i], deltaug[1][1][i], deltaug[2][1][i]},
      {deltaug[0][2][i], deltaug[1][2][i], deltaug[2][2][i]}
  };

  // -- Qdata
  dXdx[0][0] = q_data[1][i];
  dXdx[0][1] = q_data[2][i];
  dXdx[0][2] = q_data[3][i];
  dXdx[1][0] = q_data[4][i];
  dXdx[1][1] = q_data[5][i];
  dXdx[1][2] = q_data[6][i];
  dXdx[2][0] = q_data[7][i];
  dXdx[2][1] = q_data[8][i];
  dXdx[2][2] = q_data[9][i];

  // Compute grad_delta_u
  //   dXdx = (dx/dX)^(-1)
  // Apply dXdx to delta_du = grad_delta_u
  CeedScalar grad_delta_u[3][3];
  RatelMatMatMult(1.0, delta_du, dXdx, grad_delta_u);

  // Compute Strain : e (epsilon)
  // e = 1/2 (grad u + (grad u)^T)
  const CeedScalar de[3][3] = {
      {(grad_delta_u[0][0] + grad_delta_u[0][0]) / 2., (grad_delta_u[0][1] + grad_delta_u[1][0]) / 2.,
       (grad_delta_u[0][2] + grad_delta_u[2][0]) / 2.},
      {(grad_delta_u[1][0] + grad_delta_u[0][1]) / 2., (grad_delta_u[1][1] + grad_delta_u[1][1]) / 2.,
       (grad_delta_u[1][2] + grad_delta_u[2][1]) / 2.},
      {(grad_delta_u[2][0] + grad_delta_u[0][2]) / 2., (grad_delta_u[2][1] + grad_delta_u[1][2]) / 2.,
       (grad_delta_u[2][2] + grad_delta_u[2][2]) / 2.}
  };

  // Formulation uses Voigt notation:
  //  stress (sigma)      strain (epsilon)
  //
  //    [dsigma00]             [de00]
  //    [dsigma11]             [de11]
  //    [dsigma22]   =  S   *  [de22]
  //    [dsigma12]             [de12]
  //    [dsigma02]             [de02]
  //    [dsigma01]             [de01]
  //
  //        where
  //                         [1-nu   nu    nu                                    ]
  //                         [ nu   1-nu   nu                                    ]
  //                         [ nu    nu   1-nu                                   ]
  // S = E/((1+nu)*(1-2*nu)) [                  (1-2*nu)/2                       ]
  //                         [                             (1-2*nu)/2            ]
  //                         [                                        (1-2*nu)/2 ]
  // Above Voigt Notation is placed in a 3x3 matrix:
  const CeedScalar ss = E / ((1 + nu) * (1 - 2 * nu));

  const CeedScalar dsigma_Voigt[6] = {ss * ((1 - nu) * de[0][0] + nu * de[1][1] + nu * de[2][2]),
                                      ss * (nu * de[0][0] + (1 - nu) * de[1][1] + nu * de[2][2]),
                                      ss * (nu * de[0][0] + nu * de[1][1] + (1 - nu) * de[2][2]),
                                      ss * (1 - 2 * nu) * de[1][2] / 2,
                                      ss * (1 - 2 * nu) * de[0][2] / 2,
                                      ss * (1 - 2 * nu) * de[0][1] / 2};
  RatelVoigtUnpack(dsigma_Voigt, dsigma);

  return 0;
}

// -----------------------------------------------------------------------------
// Residual evaluation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ElasticityResidual_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, Sigma_Linear, in, out);
}

// -----------------------------------------------------------------------------
// Jacobian evaluation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ElasticityJacobian_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, dSigma_Linear, in, out);
}

// -----------------------------------------------------------------------------
// Strain energy computation
// -----------------------------------------------------------------------------
CEED_QFUNCTION(Energy_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;

  // Constants
  const CeedScalar TwoMu  = E / (1 + nu);
  const CeedScalar mu     = TwoMu / 2;
  const CeedScalar Kbulk  = E / (3 * (1 - 2 * nu));  // Bulk Modulus
  const CeedScalar lambda = (3 * Kbulk - TwoMu) / 3;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of u
    const CeedScalar du[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };
    // -- Qdata
    const CeedScalar wdetJ      = q_data[0][i];
    const CeedScalar dXdx[3][3] = {
        {q_data[1][i], q_data[2][i], q_data[3][i]},
        {q_data[4][i], q_data[5][i], q_data[6][i]},
        {q_data[7][i], q_data[8][i], q_data[9][i]}
    };

    // Compute grad_u
    //   dXdx = (dx/dX)^(-1)
    // Apply dXdx to du = grad_u
    CeedScalar grad_u[3][3];
    RatelMatMatMult(1.0, du, dXdx, grad_u);

    // Compute Strain : e (epsilon)
    // e = 1/2 (grad u + (grad u)^T)
    const CeedScalar e[3][3] = {
        {(grad_u[0][0] + grad_u[0][0]) / 2., (grad_u[0][1] + grad_u[1][0]) / 2., (grad_u[0][2] + grad_u[2][0]) / 2.},
        {(grad_u[1][0] + grad_u[0][1]) / 2., (grad_u[1][1] + grad_u[1][1]) / 2., (grad_u[1][2] + grad_u[2][1]) / 2.},
        {(grad_u[2][0] + grad_u[0][2]) / 2., (grad_u[2][1] + grad_u[1][2]) / 2., (grad_u[2][2] + grad_u[2][2]) / 2.}
    };

    // Strain energy
    const CeedScalar strain_vol = e[0][0] + e[1][1] + e[2][2];
    energy[i] =
        (lambda * strain_vol * strain_vol / 2. + strain_vol * mu + (e[0][1] * e[0][1] + e[0][2] * e[0][2] + e[1][2] * e[1][2]) * 2 * mu) * wdetJ;

  }  // End of Quadrature Point Loop

  return 0;
}

// -----------------------------------------------------------------------------
// Nodal diagnostic quantities
// -----------------------------------------------------------------------------
CEED_QFUNCTION(Diagnostic_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;

  // Constants
  const CeedScalar TwoMu  = E / (1 + nu);
  const CeedScalar mu     = TwoMu / 2;
  const CeedScalar Kbulk  = E / (3 * (1 - 2 * nu));  // Bulk Modulus
  const CeedScalar lambda = (3 * Kbulk - TwoMu) / 3;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of u
    const CeedScalar du[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };
    // -- Qdata
    const CeedScalar wdetJ      = q_data[0][i];
    const CeedScalar dXdx[3][3] = {
        {q_data[1][i], q_data[2][i], q_data[3][i]},
        {q_data[4][i], q_data[5][i], q_data[6][i]},
        {q_data[7][i], q_data[8][i], q_data[9][i]}
    };

    // Compute grad_u
    //   dXdx = (dx/dX)^(-1)
    // Apply dXdx to du = grad_u
    CeedScalar grad_u[3][3];
    RatelMatMatMult(1.0, du, dXdx, grad_u);

    // Compute Strain : e (epsilon)
    // e = 1/2 (grad u + (grad u)^T)
    const CeedScalar e[3][3] = {
        {(grad_u[0][0] + grad_u[0][0]) / 2., (grad_u[0][1] + grad_u[1][0]) / 2., (grad_u[0][2] + grad_u[2][0]) / 2.},
        {(grad_u[1][0] + grad_u[0][1]) / 2., (grad_u[1][1] + grad_u[1][1]) / 2., (grad_u[1][2] + grad_u[2][1]) / 2.},
        {(grad_u[2][0] + grad_u[0][2]) / 2., (grad_u[2][1] + grad_u[1][2]) / 2., (grad_u[2][2] + grad_u[2][2]) / 2.}
    };
    // Compute the Cauchy stress
    const CeedScalar ss = E / ((1 + nu) * (1 - 2 * nu));

    CeedScalar       sigma[3][3];
    const CeedScalar sigma_Voigt[6] = {ss * ((1 - nu) * e[0][0] + nu * e[1][1] + nu * e[2][2]),
                                       ss * (nu * e[0][0] + (1 - nu) * e[1][1] + nu * e[2][2]),
                                       ss * (nu * e[0][0] + nu * e[1][1] + (1 - nu) * e[2][2]),
                                       ss * (1 - 2 * nu) * e[1][2] * 0.5,
                                       ss * (1 - 2 * nu) * e[0][2] * 0.5,
                                       ss * (1 - 2 * nu) * e[0][1] * 0.5};
    RatelVoigtUnpack(sigma_Voigt, sigma);

    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    diagnostic[3][i] = sigma_Voigt[0];
    diagnostic[4][i] = sigma_Voigt[5];
    diagnostic[5][i] = sigma_Voigt[4];
    diagnostic[6][i] = sigma_Voigt[1];
    diagnostic[7][i] = sigma_Voigt[3];
    diagnostic[8][i] = sigma_Voigt[2];

    // Pressure
    const CeedScalar strain_vol = e[0][0] + e[1][1] + e[2][2];
    diagnostic[9][i]            = -lambda * strain_vol;

    // Stress tensor invariants
    diagnostic[10][i] = strain_vol;
    diagnostic[11][i] = 0.;
    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt m = 0; m < 3; m++) {
        diagnostic[11][i] += e[j][m] * e[m][j];
      }
    }
    diagnostic[12][i] = 1 + strain_vol;

    // Strain energy
    diagnostic[13][i] =
        (lambda * strain_vol * strain_vol / 2. + strain_vol * mu + (e[0][1] * e[0][1] + e[0][2] * e[0][2] + e[1][2] * e[1][2]) * 2 * mu);

    // ---------- Compute von-Mises stress----------

    // Compute the trace of Cauchy stress: trace_sigma = trace(sigma)
    const CeedScalar trace_sigma = RatelTrace3(sigma);

    // Compute the the deviatoric part of Cauchy stress: sigma_dev = sigma - trace(sigma)/3
    const CeedScalar sigma_dev[3][3] = {
        {sigma[0][0] - trace_sigma / 3., sigma[0][1],                    sigma[0][2]                   },
        {sigma[1][0],                    sigma[1][1] - trace_sigma / 3., sigma[1][2]                   },
        {sigma[2][0],                    sigma[2][1],                    sigma[2][2] - trace_sigma / 3.}
    };

    // -- sigma_dev:sigma_dev
    CeedScalar sigma_dev_contract = RatelMatMatContract(1.0, sigma_dev, sigma_dev);

    // Compute von-Mises stress: sigma_von = sqrt(3/2 sigma_dev:sigma_dev)
    diagnostic[14][i] = sqrt(3. * sigma_dev_contract / 2.);

    for (CeedInt j = 0; j < 15; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop

  return 0;
}

// -----------------------------------------------------------------------------
// Surface forces
// -----------------------------------------------------------------------------
CEED_QFUNCTION(SurfaceForce_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const NeoHookeanPhysics *context = (NeoHookeanPhysics *)ctx;
  const CeedScalar         E       = context->E;
  const CeedScalar         nu      = context->nu;

  // Quadrature Point Loop
  for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of u
    const CeedScalar du[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };

    // -- Qdata
    // dXdx_initial = dX/dx_initial
    // X is natural coordinate sys OR Reference [-1, 1]^dim
    // x_initial is initial config coordinate system
    const CeedScalar dXdx[3][3] = {
        {q_data[1][i], q_data[2][i], q_data[3][i]},
        {q_data[4][i], q_data[5][i], q_data[6][i]},
        {q_data[7][i], q_data[8][i], q_data[9][i]}
    };
    // Normal
    const CeedScalar normal[3] = {q_data[10][i], q_data[11][i], q_data[12][i]};

    // Compute grad_u
    //   dXdx = (dx/dX)^(-1)
    // Apply dXdx to du = grad_u
    CeedScalar grad_u[3][3];
    RatelMatMatMult(1.0, du, dXdx, grad_u);

    // Compute Strain : e (epsilon)
    // e = 1/2 (grad u + (grad u)^T)
    const CeedScalar e[3][3] = {
        {(grad_u[0][0] + grad_u[0][0]) / 2., (grad_u[0][1] + grad_u[1][0]) / 2., (grad_u[0][2] + grad_u[2][0]) / 2.},
        {(grad_u[1][0] + grad_u[0][1]) / 2., (grad_u[1][1] + grad_u[1][1]) / 2., (grad_u[1][2] + grad_u[2][1]) / 2.},
        {(grad_u[2][0] + grad_u[0][2]) / 2., (grad_u[2][1] + grad_u[1][2]) / 2., (grad_u[2][2] + grad_u[2][2]) / 2.}
    };

    // Formulation uses Voigt notation:
    //  stress (sigma)      strain (epsilon)
    //
    //    [sigma00]             [e00]
    //    [sigma11]             [e11]
    //    [sigma22]   =  S   *  [e22]
    //    [sigma12]             [e12]
    //    [sigma02]             [e02]
    //    [sigma01]             [e01]
    //
    //        where
    //                         [1-nu   nu    nu                                    ]
    //                         [ nu   1-nu   nu                                    ]
    //                         [ nu    nu   1-nu                                   ]
    // S = E/((1+nu)*(1-2*nu)) [                  (1-2*nu)/2                       ]
    //                         [                             (1-2*nu)/2            ]
    //                         [                                        (1-2*nu)/2 ]
    // Above Voigt Notation is placed in a 3x3 matrix:
    const CeedScalar ss = E / ((1 + nu) * (1 - 2 * nu));

    const CeedScalar sigma_Voigt[6] = {ss * ((1 - nu) * e[0][0] + nu * e[1][1] + nu * e[2][2]),
                                       ss * (nu * e[0][0] + (1 - nu) * e[1][1] + nu * e[2][2]),
                                       ss * (nu * e[0][0] + nu * e[1][1] + (1 - nu) * e[2][2]),
                                       ss * (1 - 2 * nu) * e[1][2] * 0.5,
                                       ss * (1 - 2 * nu) * e[0][2] * 0.5,
                                       ss * (1 - 2 * nu) * e[0][1] * 0.5};
    CeedScalar       sigma[3][3];
    RatelVoigtUnpack(sigma_Voigt, sigma);

    // Compute average displacement
    for (CeedInt j = 0; j < 3; j++) diagnostic[j][i] = q_data[0][i] * u[j][i];

    // Compute sigma N
    for (CeedInt j = 0; j < 3; j++) {
      diagnostic[3 + j][i] = 0.0;
      for (CeedInt k = 0; k < 3; k++) diagnostic[3 + j][i] -= q_data[0][i] * sigma[j][k] * normal[k];
    }

  }  // End of Quadrature Point Loop

  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_elasticity_linear_qf_h
