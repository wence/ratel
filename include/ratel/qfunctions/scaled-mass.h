/// @file
/// Ratel mass operator QFunction source

#ifndef ratel_scaled_mass_qf_h
#define ratel_scaled_mass_qf_h

#include <math.h>

#include "../models/scaled-mass.h"
#include "utils.h"

// -----------------------------------------------------------------------------
// Scaled mass operator with full volumetric qdata
// -----------------------------------------------------------------------------
CEED_QFUNCTION(ScaledMass)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  const ScaledMassContext *context = (ScaledMassContext *)ctx;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Scaled mass operator
    ScaledMassApplyAtQuadraturePoint(Q, i, context->num_comp, -context->rho * q_data[0][i], (const CeedScalar *)u, (CeedScalar *)v);
  }  // End of Quadrature Point Loop

  return 0;
}

// -----------------------------------------------------------------------------

#endif  // ratel_scaled_mass_qf_h
