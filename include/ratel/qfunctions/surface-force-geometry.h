/// @file
/// Ratel surface diagnostic geometric factors computation QFunction source

#ifndef ratel_surface_diagnostic_geometry_qf_h
#define ratel_surface_diagnostic_geometry_qf_h

#include "utils.h"

// -----------------------------------------------------------------------------
// This QFunction sets up the geometric factors required for integration and
//   coordinate transformations as well as gradient transformation
//
// -----------------------------------------------------------------------------
CEED_QFUNCTION(SetupSurfaceForceGeometry)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*J_cell)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[0];
  const CeedScalar(*J_face)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*w)                     = in[2];

  // Outputs
  CeedScalar(*q_data)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  const CeedInt dim = 3;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    // N_1, N_2, and N_3 are given by the cross product of the columns of dxdX
    CeedScalar normal[3];
    for (CeedInt j = 0; j < dim; j++) {
      // Equivalent code with no mod operations:
      // normal[j] = J_face[0][j+1]*J_face[1][j+2] - J_face[0][j+2]*J_face[1][j+1]
      normal[j] = J_face[0][(j + 1) % dim][i] * J_face[1][(j + 2) % dim][i] - J_face[0][(j + 2) % dim][i] * J_face[1][(j + 1) % dim][i];
    }
    const CeedScalar detJ_face = RatelNorm3(normal);
    // Gradient
    CeedScalar A[3][3];
    for (CeedInt j = 0; j < dim; j++) {
      for (CeedInt k = 0; k < dim; k++) {
        // Equivalent code with no mod operations:
        // A[k][j] = J_face[k+1][j+1]*J[k+2][j+2] - J_face[k+1][j+2]*J[k+2][j+1]
        A[k][j] = J_cell[(k + 1) % dim][(j + 1) % dim][i] * J_cell[(k + 2) % dim][(j + 2) % dim][i] -
                  J_cell[(k + 1) % dim][(j + 2) % dim][i] * J_cell[(k + 2) % dim][(j + 1) % dim][i];
      }
    }
    const CeedScalar detJ_cell = J_cell[0][0][i] * A[0][0] + J_cell[0][1][i] * A[0][1] + J_cell[0][2][i] * A[0][2];

    // Qdata
    // -- Interp-to-Interp q_data
    q_data[0][i] = w[i] * detJ_face;
    // -- Interp-to-Grad q_data
    // Inverse of change of coordinate matrix: X_i,j
    for (CeedInt j = 0; j < dim; j++) {
      for (CeedInt k = 0; k < dim; k++) {
        q_data[j * dim + k + 1][i] = A[j][k] / detJ_cell;
      }
    }
    // -- Normal vector
    for (CeedInt j = 0; j < 3; j++) q_data[j + 10][i] = normal[j] / detJ_face;

  }  // End of Quadrature Point Loop

  // Return
  return 0;
}
// -----------------------------------------------------------------------------

#endif  // ratel_surface_diagnostic_geometry_qf_h
