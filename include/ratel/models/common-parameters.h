/// @file
/// Ratel common material parameters

#ifndef ratel_common_material_parameters_h
#define ratel_common_material_parameters_h

#define RATEL_NUMBER_COMMON_PARAMETERS 3
#define RATEL_COMMON_PARAMETER_RHO 0
#define RATEL_COMMON_PARAMETER_SHIFT_V 1
#define RATEL_COMMON_PARAMETER_SHIFT_A 2

#endif  // ratel_common_material_parameters_h
