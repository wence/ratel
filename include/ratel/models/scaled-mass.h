/// @file
/// Ratel scaled mass matrix parameters

#ifndef ratel_scaled_mass_context_h
#define ratel_scaled_mass_context_h

#include <ceed.h>

#ifndef scaled_mass_context
#define scaled_mass_context
typedef struct {
  CeedInt    num_comp;
  CeedScalar rho;
} ScaledMassContext;
#endif

#endif  // ratel_scaled_mass_context_h
