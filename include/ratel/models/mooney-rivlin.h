/// @file
/// Ratel Mooney-Rivlin material parameters

#ifndef ratel_mooney_rivlin_h
#define ratel_mooney_rivlin_h

#include <ceed.h>

#include "common-parameters.h"

#ifndef mooney_rivlin_physics
#define mooney_rivlin_physics
typedef struct {
  // Common properties
  CeedScalar common_parameters[RATEL_NUMBER_COMMON_PARAMETERS];
  // Mooney Rivlin specific properties
  CeedScalar mu_1;
  CeedScalar mu_2;
  CeedScalar lambda;
} MooneyRivlinPhysics;
#endif

#endif  // ratel_mooney_rivlin_h
