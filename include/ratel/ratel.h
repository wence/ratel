#ifndef ratel_h
#define ratel_h

#include <petsc.h>
#include <petscdmplex.h>
#include <petscsnes.h>
#include <petscts.h>

#if PETSC_VERSION_LT(3, 18, 0)
#error "PETSc v3.18 or later is required"
#endif

/// @defgroup RatelCore      Core Ratel library
/// @defgroup RatelMethods   Numerical methods
/// @defgroup RatelBoundary  Boundary conditions
/// @defgroup RatelModels    Material model options
/// @defgroup RatelInternal  Internal functions

/*
  RATEL_EXTERN is used in this header to denote all publicly visible symbols.

  No other file should declare publicly visible symbols, thus it should never be
  used outside ratel.h.
 */
#if defined(__clang_analyzer__)
#define RATEL_EXTERN extern
#elif defined(__cplusplus)
#define RATEL_EXTERN extern "C"
#else
#define RATEL_EXTERN extern
#endif

/// Ratel library version numbering
/// @ingroup RatelCore
#define RATEL_VERSION_MAJOR 0
#define RATEL_VERSION_MINOR 1
#define RATEL_VERSION_PATCH 2
#define RATEL_VERSION_RELEASE false

/// Compile-time check that the the current library version is at least as
/// recent as the specified version. This macro is typically used in
/// @code
/// #if RATEL_VERSION_GE(0, 1, 0)
///   code path that needs at least 0.1.0
/// #else
///   fallback code for older versions
/// #endif
/// @endcode
///
/// A non-release version always compares as positive infinity.
///
/// @param major   Major version
/// @param minor   Minor version
/// @param patch   Patch (subminor) version
///
/// @ingroup RatelCore
/// @sa RatelGetVersion()
#define RATEL_VERSION_GE(major, minor, patch) \
  (!RATEL_VERSION_RELEASE ||                  \
   (RATEL_VERSION_MAJOR > major ||            \
    (RATEL_VERSION_MAJOR == major && (RATEL_VERSION_MINOR > minor || (RATEL_VERSION_MINOR == minor && RATEL_VERSION_PATCH >= patch)))))

RATEL_EXTERN PetscErrorCode RatelGetVersion(int *major, int *minor, int *patch, PetscBool *release);

/// Specify numerical method
/// @ingroup RatelMethods
typedef enum {
  /// Static elasticity, to be solved with a PETSc SNES
  RATEL_METHOD_STATIC_ELASTICITY      = 0,
  /// Quasistatic elastic, to be solved with a PETSc TS
  RATEL_METHOD_QUASISTATIC_ELASTICITY = 1,
  /// Fully dynamic elasticity, to be solved with a PETSc TS
  RATEL_METHOD_DYNAMIC_ELASTICITY     = 2,
} RatelMethodType;
RATEL_EXTERN const char *const RatelMethodTypes[];

/// Specify forcing term
/// @ingroup RatelCore
typedef enum {
  /// No forcing term
  RATEL_FORCING_NONE     = 0,
  /// Constant forcing given by forcing vector
  RATEL_FORCING_CONSTANT = 1,
  /// Forcing for linear elasticity manufactured solution
  RATEL_FORCING_MMS      = 2,
} RatelForcingType;
RATEL_EXTERN const char *const RatelForcingTypes[];

/// Specify boundary condition
/// @ingroup RatelBoundaries
typedef enum {
  /// Dirichlet clamped boundary
  RATEL_BOUNDARY_CLAMP    = 0,
  /// Neumann traction boundary,
  RATEL_BOUNDARY_TRACTION = 1,
} RatelBoundaryType;
RATEL_EXTERN const char *const RatelBoundaryTypes[];

/// Specify multigrid strategy
/// @ingroup RatelCore
typedef enum {
  /// P-multigrid, coarsen logarithmically, decreasing basis order to next lowest power of 2
  RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC = 0,
  /// P-multigrid, coarsen uniformly, decreasing basis order by 1
  RATEL_MULTIGRID_P_COARSENING_UNIFORM     = 1,
  /// P-multigrid, user defined coarsening strategy
  RATEL_MULTIGRID_P_COARSENING_USER        = 2,
  /// AMG only preconditioning
  RATEL_MULTIGRID_AMG_ONLY                 = 3,
  /// No multigrid preconditioning
  RATEL_MULTIGRID_NONE                     = 4,
} RatelMultigridType;
RATEL_EXTERN const char *const RatelMutigridTypes[];

typedef struct RatelUnits_private *RatelUnits;

/// Library context created by RatelInit()
/// @ingroup RatelCore
typedef struct Ratel_private *Ratel;

// Library context management
RATEL_EXTERN PetscErrorCode RatelInit(MPI_Comm comm, Ratel *ratel);
RATEL_EXTERN PetscErrorCode RatelView(Ratel ratel, PetscViewer viewer);
RATEL_EXTERN PetscErrorCode RatelDestroy(Ratel *ratel);

// User DM setup
RATEL_EXTERN PetscErrorCode RatelDMCreate(Ratel ratel, RatelMethodType method, DM *dm);

// Solver management
RATEL_EXTERN PetscErrorCode RatelTSSetDefaults(Ratel ratel, TS ts);
RATEL_EXTERN PetscErrorCode RatelTSSetFromOptions(Ratel ratel, TS ts);
RATEL_EXTERN PetscErrorCode RatelSNESSetDefaults(Ratel ratel, SNES snes);
RATEL_EXTERN PetscErrorCode RatelTSSetupPCMG(Ratel ratel, RatelMultigridType multigrid_type, TS ts);
RATEL_EXTERN PetscErrorCode RatelSNESSetupPCMG(Ratel ratel, RatelMultigridType multigrid_type, SNES snes);
RATEL_EXTERN PetscErrorCode RatelKSPSetupPCMG(Ratel ratel, RatelMultigridType multigrid_type, KSP ksp);

// Diagnostic quantities
RATEL_EXTERN PetscErrorCode RatelHasMMS(Ratel ratel, PetscBool *has_mms);
RATEL_EXTERN PetscErrorCode RatelComputeMMSL2Error(Ratel ratel, Vec U, PetscScalar *l2_error);
RATEL_EXTERN PetscErrorCode RatelGetExpectedStrainEnergy(Ratel ratel, PetscScalar *expected_strain_energy);
RATEL_EXTERN PetscErrorCode RatelComputeStrainEnergy(Ratel ratel, Vec U, PetscScalar time, PetscScalar *strain_energy);
RATEL_EXTERN PetscErrorCode RatelGetSurfaceForceFaces(Ratel ratel, PetscInt *num_faces, const PetscInt **face_numbers);
RATEL_EXTERN PetscErrorCode RatelComputeSurfaceForces(Ratel ratel, Vec U, PetscScalar time, PetscScalar **surface_forces);
RATEL_EXTERN PetscErrorCode RatelComputeDiagnosticQuantities(Ratel ratel, Vec U, PetscScalar time, Vec *D);

// Monitor routines
typedef PetscErrorCode              RatelTSMonitorFunction(TS, PetscInt, PetscReal, Vec, void *);
RATEL_EXTERN RatelTSMonitorFunction RatelTSMonitorStrainEnergy;
RATEL_EXTERN RatelTSMonitorFunction RatelTSMonitorSurfaceForces;
RATEL_EXTERN RatelTSMonitorFunction RatelTSMonitorDiagnosticQuantities;
RATEL_EXTERN PetscErrorCode         RatelTSMonitorSet(Ratel ratel, TS ts, RatelTSMonitorFunction monitor);

#endif  // ratel_h
