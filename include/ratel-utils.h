#ifndef ratel_utils_h
#define ratel_utils_h

#include <ceed.h>
#include <petsc.h>
#include <ratel-impl.h>
#include <ratel.h>

RATEL_INTERN PetscErrorCode RatelIntArrayC2P(PetscInt num_entries, CeedInt **array_ceed, PetscInt **array_petsc);
RATEL_INTERN PetscErrorCode RatelIntArrayP2C(PetscInt num_entries, PetscInt **array_petsc, CeedInt **array_ceed);

/**
  @brief Translate PetscMemType to CeedMemType

  @param[in]  mem_type  PetscMemType

  @return Equivalent CeedMemType
**/
/// @ingroup RatelInternal
static inline CeedMemType RatelMemTypeP2C(PetscMemType mem_type) { return PetscMemTypeDevice(mem_type) ? CEED_MEM_DEVICE : CEED_MEM_HOST; }

/**
  @brief Involute negative indices, essential BC DoFs are encoded in closure indices as -(i+1)

  @param[in]  i  DoF index

  @return involuted index value
**/
static inline PetscInt RatelInvolute(PetscInt i) { return i >= 0 ? i : -(i + 1); }

/**
  @brief Convert from DMPolytopeType to CeedElemTopology

  @param[in]  cell_type  DMPolytopeType for the cell

  @return CeedElemTopology, or 0 if no equivelent CeedElemTopology was found
**/
static inline CeedElemTopology RatelElemTopologyP2C(DMPolytopeType cell_type) {
  switch (cell_type) {
    case DM_POLYTOPE_TRIANGLE:
      return CEED_TOPOLOGY_TRIANGLE;
    case DM_POLYTOPE_QUADRILATERAL:
      return CEED_TOPOLOGY_QUAD;
    case DM_POLYTOPE_TETRAHEDRON:
      return CEED_TOPOLOGY_TET;
    case DM_POLYTOPE_HEXAHEDRON:
      return CEED_TOPOLOGY_HEX;
    default:
      return 0;
  }
}

#endif  // ratel_utils_h
