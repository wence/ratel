#ifndef ratel_types_h
#define ratel_types_h

#include <ratel-impl.h>
#include <ratel.h>

RATEL_EXTERN const char *const RatelForcingTypesCL[];
RATEL_EXTERN const char *const RatelMultigridTypesCL[];

#endif  // ratel_types_h
