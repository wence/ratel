#ifndef ratel_diagnostic_h
#define ratel_diagnostic_h

#include <petsc.h>
#include <ratel-impl.h>
#include <ratel.h>

RATEL_INTERN PetscErrorCode RatelComputeDiagnosticQuantities_Internal(Ratel ratel, Vec U, PetscScalar time, Vec D);
RATEL_INTERN PetscErrorCode RatelComputeSurfaceForces_Internal(Ratel ratel, Vec U, PetscScalar time, PetscScalar *surface_forces);

#endif  // ratel_diagnostic_h
