#ifndef ratel_cl_options_h
#define ratel_cl_options_h

#include <petsc.h>
#include <ratel-impl.h>
#include <ratel.h>

RATEL_INTERN PetscErrorCode RatelProcessCommandLineOptions(Ratel ratel);

#endif  // ratel_cl_options_h
