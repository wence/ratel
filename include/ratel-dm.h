#ifndef ratel_dm_h
#define ratel_dm_h

#include <ceed.h>
#include <petsc.h>
#include <petscdmplex.h>
#include <petscfe.h>
#include <ratel-impl.h>
#include <ratel.h>

RATEL_INTERN PetscErrorCode RatelDMCreateFromOptions(Ratel ratel, VecType vec_type, DM *dm);
RATEL_INTERN PetscErrorCode RatelDMCreateFaceLabel(Ratel ratel, DM dm, PetscInt material_index, PetscInt dm_face, char **face_label_name);

RATEL_INTERN PetscErrorCode RatelBasisCreateFromPlex(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height,
                                                     PetscInt dm_field, CeedBasis *basis);
RATEL_INTERN PetscErrorCode RatelOrientedBasisCreateCellToFaceFromPlex(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt face,
                                                                       PetscInt dm_field, CeedBasis *basis);
RATEL_INTERN PetscErrorCode RatelRestrictionCreateFromPlex(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height,
                                                           PetscInt dm_field, CeedElemRestriction *elem_restr);
RATEL_INTERN PetscErrorCode RatelGetRestrictionForDomain(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height,
                                                         PetscInt dm_field, CeedInt Q, CeedInt q_data_size, CeedElemRestriction *elem_restr_q,
                                                         CeedElemRestriction *elem_restr_x, CeedElemRestriction *elem_restr_qd_i);

#endif  // ratel_dm_h
