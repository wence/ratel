#ifndef ratel_boundary_h
#define ratel_boundary_h

#include <petsc.h>
#include <ratel-impl.h>
#include <ratel.h>

RATEL_INTERN PetscErrorCode RatelCreateBCLabel(DM dm, const char name[]);
RATEL_INTERN PetscErrorCode RatelDMAddBoundariesDirichlet(Ratel ratel, DM dm);
RATEL_INTERN PetscErrorCode RatelDMAddBoundariesSlip(Ratel ratel, DM dm);
RATEL_INTERN PetscErrorCode RatelCeedAddBoundariesDirichletClamp(Ratel ratel, DM dm);
RATEL_INTERN PetscErrorCode RatelCeedAddBoundariesNeumann(Ratel ratel, DM dm);
RATEL_INTERN PetscErrorCode RatelSetupSurfaceForceCentroids(Ratel ratel, DM dm);
RATEL_INTERN PetscErrorCode RatelBoundaryMMS(PetscInt dim, PetscReal t, const PetscReal coords[], PetscInt num_comp_u, PetscScalar *u, void *ctx);

#endif  // ratel_boundary_h
