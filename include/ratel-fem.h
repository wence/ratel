#ifndef ratel_fem_h
#define ratel_fem_h

#include <ceed.h>
#include <petsc.h>
#include <petscdmplex.h>
#include <petscsnes.h>
#include <petscts.h>
#include <ratel-impl.h>
#include <ratel.h>

// User DM setup
RATEL_INTERN PetscErrorCode RatelSetupDMByOrder_FEM(Ratel ratel, PetscBool setup_boundary, PetscInt order, PetscInt q_extra, PetscInt num_comp_u,
                                                    PetscInt coord_order, DM dm);
RATEL_INTERN PetscErrorCode RatelSetupResidualUCtx_FEM(Ratel ratel, RatelOperatorApplyContext ctx_residual_u);
RATEL_INTERN PetscErrorCode RatelSetupResidualUtCtx_FEM(Ratel ratel, RatelOperatorApplyContext ctx_residual_ut);
RATEL_INTERN PetscErrorCode RatelSetupResidualUttCtx_FEM(Ratel ratel, RatelOperatorApplyContext ctx_residual_utt);
RATEL_INTERN PetscErrorCode RatelSetupFormJacobianCtx_FEM(Ratel ratel, RatelFormJacobianContext ctx_form_jacobian);
RATEL_INTERN PetscErrorCode SetupProlongRestrictCtx_FEM(Ratel ratel, PetscInt level, RatelProlongRestrictContext ctx_prolong_restrict);
RATEL_INTERN PetscErrorCode RatelSetupProlongRestrictCtx_FEM(Ratel, PetscInt, RatelProlongRestrictContext);
RATEL_INTERN PetscErrorCode RatelSetupResidualJacobian_FEM(Ratel ratel, RatelModelData model_data, DMLabel domain_label, PetscInt material_index);
RATEL_EXTERN PetscErrorCode RatelDMCreate_FEM(Ratel ratel, DM *dm);

// Multigrid
RATEL_INTERN PetscErrorCode RatelSetupMultigridLevel_FEM(Ratel ratel, RatelModelData model_data, PetscInt level, PetscInt material_index);
RATEL_INTERN PetscErrorCode RatelKSPSetupPCMG_FEM(Ratel ratel, RatelMultigridType multigrid_type, KSP ksp);

#endif  // ratel_fem_h
