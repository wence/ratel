#ifndef ratel_impl_h
#define ratel_impl_h

#ifdef __clang_analyzer__
#define RATEL_INTERN
#else
#define RATEL_INTERN RATEL_EXTERN __attribute__((visibility("hidden")))
#endif

#include <ceed.h>
#include <petsc.h>
#include <ratel.h>

RATEL_INTERN const char *RatelJitSourceRootDefault;

#define RatelQFunctionRelativePath(absolute_path) strstr(absolute_path, "ratel/qfunctions")

#if !(CEED_VERSION_GE(0, 10, 1))
#error "libCEED v0.10.1 or later is required"
#endif

#define RatelCeedChk(ratel, ierr)                                   \
  do {                                                              \
    if (ierr != CEED_ERROR_SUCCESS) {                               \
      const char *error_message;                                    \
      CeedGetErrorMessage((ratel)->ceed, &error_message);           \
      SETERRQ(PETSC_COMM_SELF, PETSC_ERR_LIB, "%s", error_message); \
    }                                                               \
  } while (0);

#define RatelCeedCall(ratel, ...)         \
  do {                                    \
    PetscErrorCode ierr_q_ = __VA_ARGS__; \
    RatelCeedChk(ratel, ierr_q_);         \
  } while (0);

#define RATEL_MAX_MATERIALS 16
#define RATEL_MAX_STORED_FIELDS 16
#define RATEL_MAX_BOUNDARY_FACES 16
#define RATEL_MAX_MULTIGRID_LEVELS 16

/// RATEL_DEBUG_COLOR default value, forward RatelDebug* declarations & macros
#define RATEL_DEBUG_NO_COLOR 255
#define RATEL_DEBUG_RED 196
#define RATEL_DEBUG_ORANGE 208
RATEL_INTERN void RatelDebugImpl(const Ratel, const char *, ...);
RATEL_INTERN void RatelDebugImpl256(const Ratel, const unsigned char, const char *, ...);
#define RatelDebug256(ratel, color, ...) RatelDebugImpl256(ratel, color, __VA_ARGS__)
#define RatelDebug(ratel, ...) RatelDebug256(ratel, (unsigned char)RATEL_DEBUG_NO_COLOR, __VA_ARGS__)

// Default value option
#define RATEL_DECIDE -42

// Units
struct RatelUnits_private {
  // Fundamental units
  PetscScalar meter;
  PetscScalar kilogram;
  PetscScalar second;
  // Derived unit
  PetscScalar Pascal;
};

// PETSc operator contexts
typedef struct RatelOperatorApplyContext_private *RatelOperatorApplyContext;
struct RatelOperatorApplyContext_private {
  DM                    dm_x, dm_y;
  Vec                   X_loc, Y_loc;
  Ratel                 ratel;
  CeedVector            x_loc_ceed, y_loc_ceed;
  CeedQFunctionContext *ctx_phys, *ctx_phys_smoother;
  CeedOperator          op;
  PetscReal             t;
  PetscLogDouble        flops;
};

typedef struct RatelProlongRestrictContext_private *RatelProlongRestrictContext;
struct RatelProlongRestrictContext_private {
  DM             dm_c, dm_f;
  Vec            C_X_loc, C_Y_loc, F_X_loc, F_Y_loc;
  Ratel          ratel;
  CeedVector     c_x_loc_ceed, c_y_loc_ceed, f_x_loc_ceed, f_y_loc_ceed;
  CeedOperator   op_prolong, op_restrict;
  PetscLogDouble flops_prolong, flops_restrict;
  PetscLogEvent  event_prolong, event_restrict;
};

// Jacobian setup context
typedef struct RatelFormJacobianContext_private *RatelFormJacobianContext;
struct RatelFormJacobianContext_private {
  PetscInt                  num_levels;
  RatelOperatorApplyContext ctx_jacobian_fine;
  VecType                   vec_type;
  Mat                      *mat_jacobian, mat_jacobian_coarse;
  Ratel                     ratel;
  CeedVector                coo_values_ceed;
  CeedOperator              op_coarse;
};

// Material models
typedef struct RatelModelFunctions_private *RatelModelFunctions;
struct RatelModelFunctions_private {
  PetscFunctionList setupPhysics, setupSmootherPhysics, getModelData;
};

typedef struct RatelModelData_private *RatelModelData;
struct RatelModelData_private {
  const char       *name_for_display;
  CeedQFunctionUser setup_geo, setup_surface_force_geo, residual_u, residual_ut, residual_utt, jacobian, energy, diagnostic, surface_force, error;
  const char       *setup_geo_loc, *setup_surface_force_geo_loc, *residual_u_loc, *residual_ut_loc, *residual_utt_loc, *jacobian_loc, *energy_loc,
      *diagnostic_loc, *surface_force_loc, *error_loc;
  CeedInt            num_comp_u, q_data_size, surface_force_q_data_size, number_fields_stored;
  CeedInt           *field_sizes;
  const char *const *field_names;
  CeedQuadMode       quadrature_mode;
  PetscBool          has_ut_term;
  PetscLogDouble     flops_qf_jacobian_u, flops_qf_jacobian_ut, flops_qf_jacobian_utt;
};

// Ratel context
struct Ratel_private {
  MPI_Comm        comm;
  RatelMethodType method;
  PetscBool       is_debug;
  PetscBool       is_ceed_backend_gpu;
  // PETSc
  DM                        dm_orig, *dm_hierarchy, dm_energy, dm_diagnostic, dm_surface_displacement, dm_surface_force;
  KSP                       ksp_diagnostic_projection;
  Vec                      *X, X_loc_residual_u, *X_loc, *Y_loc, D, D_loc, E_loc, F_loc, M_loc;
  Mat                      *mat_jacobian, mat_jacobian_coarse, *mat_prolong_restrict;
  RatelOperatorApplyContext ctx_residual_u, ctx_residual_ut, ctx_residual_utt, *ctx_jacobian, ctx_energy, ctx_diagnostic, ctx_diagnostic_projection,
      ctx_surface_force, ctx_error;
  RatelFormJacobianContext     ctx_form_jacobian;
  RatelProlongRestrictContext *ctx_prolong_restrict;
  PetscClassId                 libceed_class_id;
  PetscLogEvent                event_jacobian, *event_prolong, *event_restrict;
  PetscViewer                  monitor_viewer_strain_energy, monitor_viewer_surface_forces, monitor_viewer_diagnostic_quantities;
  PetscBool                    is_monitor_vtk;
  char                        *monitor_vtk_file_name, *monitor_vtk_file_extension;
  // Ceed
  char        *ceed_resource;
  Ceed         ceed;
  CeedOperator op_dirichlet, op_residual_u, op_residual_ut, op_residual_utt, *op_jacobian, *op_prolong, *op_restrict, op_energy, op_diagnostic,
      op_mass_diagnostic, *op_surface_force, op_error;
  CeedContextFieldLabel op_dirichlet_time_label, op_residual_u_time_label, op_jacobian_time_label, op_jacobian_shift_v_label,
      op_jacobian_shift_a_label;
  CeedVector *x_loc_ceed, *y_loc_ceed, d_loc_ceed, e_loc_ceed, f_loc_ceed, m_loc_ceed, coo_values_ceed;
  // Materials
  PetscInt             num_materials;
  char                *material_names[RATEL_MAX_MATERIALS];
  char                *material_models[RATEL_MAX_MATERIALS];
  char                *material_label_name;
  PetscInt            *material_label_values[RATEL_MAX_MATERIALS];
  PetscInt             material_sub_operator_index[RATEL_MAX_MATERIALS];
  CeedQFunctionContext material_ctx_phys[RATEL_MAX_MATERIALS];
  CeedQFunctionContext material_ctx_phys_smoother[RATEL_MAX_MATERIALS];
  RatelModelFunctions  model_functions;
  RatelUnits           model_units;
  PetscBool            has_ut_term;
  PetscReal            shift_v, shift_a;
  // Forcing
  RatelForcingType forcing_choice;
  // Boundaries
  PetscInt    bc_clamp_count;
  PetscInt    bc_clamp_nonzero_count;
  PetscInt    bc_clamp_faces[RATEL_MAX_BOUNDARY_FACES];
  PetscInt    bc_slip_count;
  PetscInt    bc_slip_faces[RATEL_MAX_BOUNDARY_FACES];
  PetscInt    bc_slip_components_count[RATEL_MAX_BOUNDARY_FACES];
  PetscInt    bc_slip_components[RATEL_MAX_BOUNDARY_FACES][3];
  PetscInt    bc_traction_count;
  PetscInt    bc_traction_faces[RATEL_MAX_BOUNDARY_FACES];
  PetscInt    surface_force_face_count;
  PetscInt    surface_force_dm_faces[RATEL_MAX_BOUNDARY_FACES];
  PetscScalar surface_force_face_area[RATEL_MAX_BOUNDARY_FACES];
  PetscScalar surface_force_face_centroid[RATEL_MAX_BOUNDARY_FACES][3];
  char       *surface_force_face_label_names[RATEL_MAX_MATERIALS][RATEL_MAX_BOUNDARY_FACES];
  // Multigrid
  PetscBool          is_cl_multigrid_type;
  RatelMultigridType cl_multigrid_type;
  RatelMultigridType multigrid_type;
  PetscInt           num_multigrid_levels, num_multigrid_levels_setup;
  PetscInt           multigrid_level_orders[RATEL_MAX_MULTIGRID_LEVELS];
  PetscInt           multigrid_fine_order, multigrid_coarse_order;
  PetscLogDouble    *flops_jacobian, *flops_prolong, *flops_restrict;
  // Testing
  PetscBool   has_mms;
  PetscScalar expected_strain_energy;
  // Other options
  PetscInt diagnostic_order, diagnostic_geometry_order;
  PetscInt q_extra, q_extra_surface_force;
  // Current time set in contexts
  PetscReal time;
};

#endif  // ratel_impl_h
