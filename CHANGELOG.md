# Changelog/Release Notes

On this page we provide a summary of the main API changes, new features and examples for each release of Ratel.

(main)=

## Current `main` branch

### New features

- Add `RatelComputeSurfaceForces` for computing final forces on surfaces, identified by DMPlex face id number.
- Add support for computing Jacobians from strain energy density functional from residual evaluation via Automatic Differentiation with Enzyme-AD.

### Breaking changes

- Update names and command line arguments for material models for consistency.
- Use PETSc option `options_file` over redundant `yml` option in tests and examples; removed `options_yml` argument from `RatelInit`.
- Docker images built and tested as part of CI process for latest `main`.
- Update Boolean arguments `-view_diagnostic_quantities` and `-ts_monitor_diagnostic_quantities` to optionally allow viewer type.
- Remove command line option `-output_dir`; this functionality is provided with setting the output filepath for the viewer via `-view_diagnostic_quantities` and `-ts_monitor_diagnostic_quantities`.
- Remove `RatelVectorView` in favor of base PETSc functionality.

(v0-1-2)=

## v0.1.2 (15 April 2022)

Bugfix for install paths in makefile.

(v0-1-1)=

## v0.1.1 (14 April 2022)

Bugfix for Mass QFunction source file relative path.

(v0-1)=

## v0.1 (11 April 2022)

Initial Ratel API.
The Ratel solid mechanics library is based upon libCEED and PETSc.
libCEED v0.10.1 or later and PETSc v3.17 or later is required.
