# Ratel: Extensible, performance-portable solid mechanics

```{include} README.md
:start-after: "<!-- abstract -->"
:end-before: "# Getting Started"
```

:::{figure} doc/img/logo.png
Ratel beam twisting simulation
:::

```{toctree}
:caption: Contents
:maxdepth: 2

doc/intro
examples/index
doc/using
doc/api/index
doc/modeling/index
doc/ratel-dev
Contributing <CONTRIBUTING>
Code of Conduct <CODE_OF_CONDUCT>
Release Notes <CHANGELOG>
```

# Indices and tables

- {ref}`genindex`
- {ref}`search`

```{bibliography}
```
