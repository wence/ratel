/// @file
/// Test Ratel DM Jacobian creation

//TESTARGS(name="linear-box") -ceed {ceed_resource} -options_file tests/elasticity-linear.yml
//TESTARGS(name="linear-simplex") -ceed {ceed_resource} -options_file tests/elasticity-linear-simplex.yml
//TESTARGS(name="linear-quad-coords") -ceed {ceed_resource} -options_file tests/elasticity-linear-quad-coords.yml

const char help[] = "Ratel - test case 102\n";

#include <petsc.h>
#include <ratel.h>
#include <string.h>

int main(int argc, char **argv) {
  MPI_Comm    comm;
  Ratel       ratel_aij, ratel_shell;
  SNES        snes_aij, snes_shell;
  Mat         jacobian_aij, jacobian_shell;
  Vec         X, Y_aij, Y_shell;
  DM          dm_aij, dm_shell;
  PetscScalar l1_error = 0.0;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context and DM for MATSHELL
  PetscCall(RatelInit(comm, &ratel_shell));
  PetscCall(RatelDMCreate(ratel_shell, RATEL_METHOD_STATIC_ELASTICITY, &dm_shell));

  // Work vector
  PetscCall(DMCreateGlobalVector(dm_shell, &X));

  // Initalize Ratel context and DM for MATAIJ
  const char *fine_mat_type                      = "aij";
  char        aij_mat_option[PETSC_MAX_PATH_LEN] = "";
  VecType     vec_type                           = VECSTANDARD;
  PetscCall(VecGetType(X, &vec_type));
  if (!strcmp(vec_type, VECCUDA) || !strcmp(vec_type, VECSEQCUDA) || !strcmp(vec_type, VECMPICUDA)) fine_mat_type = "aijcusparse";
  else if (!strcmp(vec_type, VECKOKKOS) || !strcmp(vec_type, VECSEQKOKKOS) || !strcmp(vec_type, VECMPIKOKKOS)) fine_mat_type = "aijkokkos";
  PetscCall(PetscSNPrintf(aij_mat_option, PETSC_MAX_PATH_LEN, "-fine_dm_mat_type %s", fine_mat_type));

  // Initalize Ratel context and DM for MATAIJ
  PetscCall(PetscOptionsInsertString(NULL, aij_mat_option));
  PetscCall(RatelInit(comm, &ratel_aij));
  PetscCall(RatelDMCreate(ratel_aij, RATEL_METHOD_STATIC_ELASTICITY, &dm_aij));

  // Work vectors
  PetscCall(VecDuplicate(X, &Y_aij));
  PetscCall(VecDuplicate(X, &Y_shell));

  // Setup SNESs
  PetscCall(SNESCreate(comm, &snes_aij));
  PetscCall(SNESSetDM(snes_aij, dm_aij));
  PetscCall(SNESCreate(comm, &snes_shell));
  PetscCall(SNESSetDM(snes_shell, dm_shell));

  // Setup Jacobians
  PetscCall(VecSet(X, 0.1));
  PetscCall(DMCreateMatrix(dm_aij, &jacobian_aij));
  PetscCall(DMCreateMatrix(dm_shell, &jacobian_shell));
  PetscCall(SNESComputeFunction(snes_aij, X, Y_aij));
  PetscCall(SNESComputeFunction(snes_shell, X, Y_shell));
  PetscCall(VecAXPY(Y_aij, -1.0, Y_shell));
  PetscCall(VecNorm(Y_aij, NORM_1, &l1_error));
  if (l1_error > 700 * PETSC_MACHINE_EPSILON) printf("Error in residual: Y_aij - Y_shell = %0.5e\n", l1_error);
  PetscCall(SNESComputeJacobian(snes_aij, X, jacobian_aij, jacobian_aij));
  PetscCall(SNESComputeJacobian(snes_shell, X, jacobian_shell, jacobian_shell));

  // Apply Jacobians and check
  PetscCall(MatMult(jacobian_aij, X, Y_aij));
  PetscCall(MatMult(jacobian_shell, X, Y_shell));
  PetscCall(VecAXPY(Y_aij, -1.0, Y_shell));
  PetscCall(VecNorm(Y_aij, NORM_1, &l1_error));
  if (l1_error > 700 * PETSC_MACHINE_EPSILON) printf("Error in Jacobian: Y_aij - Y_shell = %0.5e\n", l1_error);

  // Cleanup
  PetscCall(SNESDestroy(&snes_aij));
  PetscCall(SNESDestroy(&snes_shell));
  PetscCall(DMDestroy(&dm_aij));
  PetscCall(DMDestroy(&dm_shell));
  PetscCall(MatDestroy(&jacobian_aij));
  PetscCall(MatDestroy(&jacobian_shell));
  PetscCall(VecDestroy(&X));
  PetscCall(VecDestroy(&Y_aij));
  PetscCall(VecDestroy(&Y_shell));
  PetscCall(RatelDestroy(&ratel_aij));
  PetscCall(RatelDestroy(&ratel_shell));

  return PetscFinalize();
}
