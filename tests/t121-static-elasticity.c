/// @file
/// Test Ratel PCMG setup for SNES

//TESTARGS(name="neo-hookean") -ceed {ceed_resource} -options_file tests/elasticity-neo-hookean-initial.yml
//TESTARGS(name="neo-hookean-low-order") -ceed {ceed_resource} -options_file tests/elasticity-neo-hookean-initial-low-order.yml
//TESTARGS(name="neo-hookean-ad") -ceed {ceed_resource} -options_file tests/elasticity-neo-hookean-initial-ad.yml
//TESTARGS(name="neo-hookean-amg-only") -ceed {ceed_resource} -options_file tests/elasticity-neo-hookean-current-amg-only.yml
//TESTARGS(name="neo-hookean-dirichlet") -ceed {ceed_resource} -options_file tests/elasticity-neo-hookean-current-dirichlet.yml
//TESTARGS(name="mooney-rivlin") -ceed {ceed_resource} -options_file tests/elasticity-mooney-rivlin-initial.yml
//TESTARGS(name="mooney-rivlin-quad-coarse") -ceed {ceed_resource} -options_file tests/elasticity-mooney-rivlin-initial-quad-coarse.yml
//TESTARGS(name="mooney-rivlin-user-coarsening") -ceed {ceed_resource} -options_file tests/elasticity-mooney-rivlin-initial-user-coarsening.yml

const char help[] = "Ratel - test case 121\n";

#include <petsc.h>
#include <ratel.h>
#include <string.h>

int main(int argc, char **argv) {
  MPI_Comm    comm;
  Ratel       ratel;
  SNES        snes;
  KSP         ksp;
  DM          dm;
  Vec         U;
  PetscScalar strain_energy = 0.0, expected_strain_energy = 0.0;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_METHOD_STATIC_ELASTICITY, &dm));

  // Create SNES
  PetscCall(SNESCreate(comm, &snes));
  PetscCall(SNESSetDM(snes, dm));
  PetscCall(RatelSNESSetDefaults(ratel, snes));

  // P-multigrid based preconditioning
  PetscCall(SNESGetKSP(snes, &ksp));
  PetscCall(RatelKSPSetupPCMG(ratel, RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC, ksp));

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm, &U));

  // Solve
  PetscCall(VecSet(U, 0.0));
  PetscCall(SNESSolve(snes, NULL, U));

  // Verify
  PetscCall(RatelGetExpectedStrainEnergy(ratel, &expected_strain_energy));
  PetscCall(RatelComputeStrainEnergy(ratel, U, 1.0, &strain_energy));
  if (fabs(strain_energy - expected_strain_energy) > 6e-3) {
    // LCOV_EXCL_START
    printf("Error: strain energy error = %0.5e\n", fabs(strain_energy - expected_strain_energy));
    // LCOV_EXCL_STOP
  }

  // Cleanup
  PetscCall(SNESDestroy(&snes));
  PetscCall(DMDestroy(&dm));
  PetscCall(VecDestroy(&U));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
