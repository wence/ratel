/// @file
/// Test Ratel view

//TESTARGS(name="view") -options_file tests/t001-view.yml

const char help[] = "Ratel - test case 001\n";

#include <petsc.h>
#include <ratel.h>
#include <string.h>

int main(int argc, char **argv) {
  MPI_Comm comm;
  Ratel    ratel;
  SNES     snes;
  DM       dm;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_METHOD_STATIC_ELASTICITY, &dm));

  // Create SNES
  PetscCall(SNESCreate(comm, &snes));
  PetscCall(SNESSetDM(snes, dm));

  // P-multigrid based preconditioning
  PetscCall(RatelSNESSetupPCMG(ratel, RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC, snes));

  // View
  PetscCall(RatelView(ratel, PETSC_VIEWER_STDOUT_WORLD));

  // Cleanup
  PetscCall(SNESDestroy(&snes));
  PetscCall(DMDestroy(&dm));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
