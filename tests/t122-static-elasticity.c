/// @file
/// Test Ratel diagnostic face forces

//TESTARGS(name="linear-face-forces") -ceed {ceed_resource} -options_file tests/elasticity-linear-face-forces.yml

const char help[] = "Ratel - test case 122\n";

#include <petsc.h>
#include <ratel.h>
#include <string.h>

int main(int argc, char **argv) {
  MPI_Comm        comm;
  Ratel           ratel;
  SNES            snes;
  KSP             ksp;
  DM              dm;
  Vec             U;
  PetscScalar    *face_forces;
  PetscInt        num_faces;
  const PetscInt *faces;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_METHOD_STATIC_ELASTICITY, &dm));

  // Create SNES
  PetscCall(SNESCreate(comm, &snes));
  PetscCall(SNESSetDM(snes, dm));
  PetscCall(RatelSNESSetDefaults(ratel, snes));

  // P-multigrid based preconditioning
  PetscCall(SNESGetKSP(snes, &ksp));
  PetscCall(RatelKSPSetupPCMG(ratel, RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC, ksp));

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm, &U));

  // Solve
  PetscCall(VecSet(U, 0.0));
  PetscCall(SNESSolve(snes, NULL, U));

  // Verify
  PetscCall(RatelGetSurfaceForceFaces(ratel, &num_faces, &faces));
  PetscCall(RatelComputeSurfaceForces(ratel, U, 1.0, &face_forces));
  for (PetscInt i = 0; i < num_faces; i++) {
    char        option_name[25];
    PetscInt    max_n = 3;
    PetscScalar applied_traction[max_n];

    PetscOptionsBegin(comm, NULL, "", NULL);
    PetscCall(PetscSNPrintf(option_name, sizeof option_name, "-bc_traction_%" PetscInt_FMT, faces[i]));
    PetscCall(PetscOptionsScalarArray(option_name, "Traction vector for constrained face", NULL, applied_traction, &max_n, NULL));
    PetscOptionsEnd();

    for (PetscInt j = 0; j < max_n; j++) {
      if (fabs(applied_traction[j] + face_forces[(2 * i + 1) * max_n + j]) > 5e-4) {
        // LCOV_EXCL_START
        printf("Error: face %" PetscInt_FMT ", force component %" PetscInt_FMT " = %0.5e != %0.5e\n", faces[i], j,
               face_forces[(2 * i + 1) * max_n + j], applied_traction[j]);
        // LCOV_EXCL_STOP
      }
    }
  }

  // Cleanup
  PetscCall(SNESDestroy(&snes));
  PetscCall(DMDestroy(&dm));
  PetscCall(VecDestroy(&U));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
