/// @file
/// Test Ratel view

//TESTARGS(name="view") -options_file tests/t001-view.yml

const char help[] = "Ratel - test case 002\n";

#include <petsc.h>
#include <ratel.h>
#include <string.h>

int main(int argc, char **argv) {
  MPI_Comm comm;
  Ratel    ratel;
  TS       ts;
  DM       dm;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_METHOD_STATIC_ELASTICITY, &dm));

  // Create TS
  PetscCall(TSCreate(comm, &ts));
  PetscCall(TSSetDM(ts, dm));

  // P-multigrid based preconditioning
  PetscCall(RatelTSSetupPCMG(ratel, RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC, ts));
  PetscCall(RatelTSSetFromOptions(ratel, ts));

  // View
  PetscCall(RatelView(ratel, PETSC_VIEWER_STDOUT_WORLD));

  // Cleanup
  PetscCall(TSDestroy(&ts));
  PetscCall(DMDestroy(&dm));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
