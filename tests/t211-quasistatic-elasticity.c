/// @file
/// Test TS create and solve from Ratel DM

//TESTARGS(name="neo-hookean") -ceed {ceed_resource} -options_file tests/elasticity-neo-hookean-initial.yml
//TESTARGS(name="neo-hookean-low-order") -ceed {ceed_resource} -options_file tests/elasticity-neo-hookean-initial-low-order.yml
//TESTARGS(name="neo-hookean-ad") -ceed {ceed_resource} -options_file tests/elasticity-neo-hookean-initial-ad.yml
//TESTARGS(name="neo-hookean-amg-only") -ceed {ceed_resource} -options_file tests/elasticity-neo-hookean-current-amg-only.yml
//TESTARGS(name="neo-hookean-dirichlet") -ceed {ceed_resource} -options_file tests/elasticity-neo-hookean-current-dirichlet.yml
//TESTARGS(name="mooney-rivlin") -ceed {ceed_resource} -options_file tests/elasticity-mooney-rivlin-initial.yml

const char help[] = "Ratel - test case 211\n";

#include <petsc.h>
#include <ratel.h>
#include <string.h>

int main(int argc, char **argv) {
  MPI_Comm    comm;
  Ratel       ratel;
  TS          ts;
  SNES        snes;
  KSP         ksp;
  PC          pc;
  DM          dm;
  Vec         U;
  PetscScalar strain_energy = 0.0, expected_strain_energy = 0.0;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_METHOD_QUASISTATIC_ELASTICITY, &dm));

  // Create TS
  PetscCall(TSCreate(comm, &ts));
  PetscCall(TSSetDM(ts, dm));
  PetscCall(RatelTSSetDefaults(ratel, ts));
  PetscCall(TSSetTimeStep(ts, 0.5));
  PetscCall(TSSetMaxTime(ts, 1.0));
  PetscCall(TSSetFromOptions(ts));

  // Diagonal based preconditioning
  PetscCall(TSGetSNES(ts, &snes));
  PetscCall(SNESGetKSP(snes, &ksp));
  PetscCall(KSPGetPC(ksp, &pc));
  PetscCall(PCSetType(pc, PCJACOBI));
  PetscCall(PCJacobiSetType(pc, PC_JACOBI_DIAGONAL));

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm, &U));

  // Solve
  PetscCall(VecSet(U, 0.0));
  PetscCall(TSSolve(ts, U));

  // Verify
  PetscCall(RatelGetExpectedStrainEnergy(ratel, &expected_strain_energy));
  PetscCall(RatelComputeStrainEnergy(ratel, U, 1.0, &strain_energy));
  if (fabs(strain_energy - expected_strain_energy) > 6e-3) {
    // LCOV_EXCL_START
    printf("Error: strain energy error = %0.5e\n", fabs(strain_energy - expected_strain_energy));
    // LCOV_EXCL_STOP
  }

  // Cleanup
  PetscCall(TSDestroy(&ts));
  PetscCall(DMDestroy(&dm));
  PetscCall(VecDestroy(&U));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
