/// @file
/// Test more complex Ratel DM initialization and destruction

//TESTARGS(name="neo-hookean") -ceed {ceed_resource} -options_file tests/elasticity-neo-hookean-initial.yml
//TESTARGS(name="neo-hookean-low-order") -ceed {ceed_resource} -options_file tests/elasticity-neo-hookean-initial-low-order.yml
//TESTARGS(name="neo-hookean-dirichlet") -ceed {ceed_resource} -options_file tests/elasticity-neo-hookean-current-dirichlet.yml
//TESTARGS(name="mooney-rivlin") -ceed {ceed_resource} -options_file tests/elasticity-mooney-rivlin-initial.yml

const char help[] = "Ratel - test case 101\n";

#include <petsc.h>
#include <ratel.h>
#include <string.h>

int main(int argc, char **argv) {
  MPI_Comm comm;
  Ratel    ratel;
  DM       dm;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_METHOD_STATIC_ELASTICITY, &dm));

  // Cleanup
  PetscCall(DMDestroy(&dm));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
