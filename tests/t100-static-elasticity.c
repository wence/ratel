/// @file
/// Test Ratel DM initialization and destruction

//TESTARGS(name="linear-box") -ceed {ceed_resource} -options_file tests/elasticity-linear.yml
//TESTARGS(name="linear-simplex") -ceed {ceed_resource} -options_file tests/elasticity-linear-simplex.yml
//TESTARGS(name="linear-quad-coords") -ceed {ceed_resource} -options_file tests/elasticity-linear-quad-coords.yml

const char help[] = "Ratel - test case 100\n";

#include <petsc.h>
#include <ratel.h>
#include <string.h>

int main(int argc, char **argv) {
  MPI_Comm comm;
  Ratel    ratel;
  DM       dm;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_METHOD_STATIC_ELASTICITY, &dm));

  // Cleanup
  PetscCall(DMDestroy(&dm));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
