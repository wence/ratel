ARG BASE_IMAGE=registry.gitlab.com/micromorph/ratel:petsc
FROM ${BASE_IMAGE}
ARG CEED_GIT_COMMIT=e66f277557ebebe6123fb2e0baf38257c8ba6a4a

ENV CEED_DIR=/usr/local/libCEED
RUN git clone --depth=1 https://github.com/CEED/libCEED.git && \
  cd libCEED && \
  git fetch origin $CEED_GIT_COMMIT && \
  git checkout $CEED_GIT_COMMIT && \
  OPT='-O2 -march=haswell -ffp-contract=fast'; \
  make -j
RUN make -C libCEED -j install prefix=$CEED_DIR

FROM ${BASE_IMAGE}
ENV CEED_DIR=/usr/local/libCEED
COPY --from=0 $CEED_DIR $CEED_DIR
RUN echo "CEED_DIR=\"$CEED_DIR\"" >> /etc/environment

LABEL maintainer='Jeremy L Thompson <jeremy@jeremylt.org>'
LABEL description='libCEED and PETSc with MPICH and packages for Ratel development'
