The Ratel Docker images have the following chain of dependencies:

1. `petsc`
2. `libceed`
3. `dev-env`
4. `latest`

The default is to build with `PetscInt == int32` in the PETSc image, but this can be changed by use of the build arguments `PETSC_DIR` and `ADDITIONAL_PETSC_OPTS`,

To build a single container with optional arguments in the `$RATEL_DIR/containers/docker` directory:
```console
$ docker build -t registry.gitlab.com/micromorph/ratel:petsc-int64 petsc --build-arg PETSC_DIR="/usr/local/petsc/mpich-int64-real-opt" --build-arg ADDITIONAL_PETSC_OPTS="--with-64-bit-indices"
```

To build only the `PetscInt == int32` development environment containers locally in the `$RATEL_DIR/containers/docker` directory:
```console
$ SKIP_INT64_BUILD=1 ./build-containers.sh
```

To build and deploy `PetscInt == int32` and `PetscInt == int64` development environment containers from the `$RATEL_DIR/containers/docker` directory:
```console
$ docker login registry.gitlab.com
$ DEPLOY=1 ./build-containers.sh
```
