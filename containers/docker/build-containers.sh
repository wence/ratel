#!/bin/sh -ex
# Note: This script assumes the runner is logged in via a command such as
# $ docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
export DOCKER_REGISTRY_BASE=registry.gitlab.com/micromorph

docker_push () {
    if [[ x$CI_COMMIT_BRANCH = xmain || ! -z ${DEPLOY+x} ]]; then
        docker push "$@"
    fi
}

# int32 build
echo "Building int32 containers"
docker build -t $DOCKER_REGISTRY_BASE/ratel:petsc petsc
docker_push $DOCKER_REGISTRY_BASE/ratel:petsc
docker build -t $DOCKER_REGISTRY_BASE/ratel:libceed libceed
docker_push $DOCKER_REGISTRY_BASE/ratel:libceed
docker build -t $DOCKER_REGISTRY_BASE/ratel:dev-env dev-env
docker_push $DOCKER_REGISTRY_BASE/ratel:dev-env

# int64 build
if [ -z ${SKIP_INT64_BUILD+x} ]; then
    echo "Building int64 containers"
    docker build -t $DOCKER_REGISTRY_BASE/ratel:petsc-int64 petsc --build-arg PETSC_DIR="/usr/local/petsc/mpich-int64-real-opt" --build-arg ADDITIONAL_PETSC_OPTS="--with-64-bit-indices"
    docker_push $DOCKER_REGISTRY_BASE/ratel:petsc-int64
    docker build -t $DOCKER_REGISTRY_BASE/ratel:libceed-int64 libceed --build-arg BASE_IMAGE=$DOCKER_REGISTRY_BASE/ratel:petsc-int64
    docker_push $DOCKER_REGISTRY_BASE/ratel:libceed-int64
    docker build -t $DOCKER_REGISTRY_BASE/ratel:dev-env-int64 dev-env --build-arg BASE_IMAGE=$DOCKER_REGISTRY_BASE/ratel:libceed-int64
    docker_push $DOCKER_REGISTRY_BASE/ratel:dev-env-int64
else
    echo "Skipping int64 containers"
fi
