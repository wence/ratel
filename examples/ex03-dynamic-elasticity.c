/// @file
/// Ratel dynamic elasticity example

//TESTARGS(name="neo-hookean-initial") -ceed {ceed_resource} -quiet -options_file examples/ex03-dynamic-elasticity-neo-hookean-initial.yml
//TESTARGS(name="neo-hookean-initial-ad") -ceed {ceed_resource} -quiet -options_file examples/ex03-dynamic-elasticity-neo-hookean-initial-ad.yml
//TESTARGS(name="mooney-rivlin-initial") -ceed {ceed_resource} -quiet -options_file examples/ex03-dynamic-elasticity-mooney-rivlin-initial.yml
//TESTARGS(name="schwarz-pendulum") -ceed {ceed_resource} -quiet -options_file examples/ex03-dynamic-elasticity-schwarz-pendulum.yml -ts_max_time 0.25 -ts_dt 0.05 -dm_plex_tps_extent 2,1,1  -dm_plex_tps_refine 1 -dm_plex_tps_layers 1 -expected_strain_energy 3.462869242229e-08

const char help[] = "Ratel - dynamic elasticity example\n";

#include <petsc.h>
#include <ratel.h>
#include <string.h>

int main(int argc, char **argv) {
  MPI_Comm    comm;
  Ratel       ratel;
  PetscViewer diagnostic_viewer;
  TS          ts;
  DM          dm;
  Vec         U, V, D;
  PetscScalar final_time = 1.0, l2_error = 1.0, strain_energy = 0.0, expected_strain_energy = 0.0;
  PetscReal   u_norms[3], *face_forces;
  PetscBool   quiet = PETSC_FALSE, view_diagnostics = PETSC_FALSE;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));

  // Read command line options
  comm = PETSC_COMM_WORLD;
  PetscOptionsBegin(comm, NULL, "Ratel Dynamic example", NULL);
  PetscCall(PetscOptionsBool("-quiet", "Suppress summary outputs", NULL, quiet, &quiet, NULL));
  PetscCall(PetscOptionsViewer("-view_diagnostic_quantities", "View diagnostic quantities", NULL, &diagnostic_viewer, NULL, &view_diagnostics));
  PetscOptionsEnd();

  // Initialize Ratel context and create DM
  PetscCall(RatelInit(comm, &ratel));
  PetscCall(RatelDMCreate(ratel, RATEL_METHOD_DYNAMIC_ELASTICITY, &dm));

  // Create TS
  PetscCall(TSCreate(comm, &ts));
  PetscCall(TSSetDM(ts, dm));
  PetscCall(RatelTSSetDefaults(ratel, ts));
  PetscCall(TSSetMaxTime(ts, final_time));
  // Avoid stepping past the final loading condition (because the solution might not be valid there)
  PetscCall(TSSetExactFinalTime(ts, TS_EXACTFINALTIME_MATCHSTEP));

  // P-multigrid based preconditioning
  PetscCall(RatelTSSetupPCMG(ratel, RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC, ts));

  // Set additional command line options
  PetscCall(RatelTSSetFromOptions(ratel, ts));
  PetscCall(TSSetFromOptions(ts));

  // View Ratel setup
  if (!quiet) {
    // LCOV_EXCL_START
    PetscCall(PetscPrintf(comm, "----- Ratel Dynamic Elasticity -----\n\n"));
    PetscCall(RatelView(ratel, PETSC_VIEWER_STDOUT_WORLD));
    // LCOV_EXCL_STOP
  }

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm, &U));
  // Name vector so it isn't automatically named (via address) in output files
  PetscCall(PetscObjectSetName((PetscObject)U, "U"));
  PetscCall(VecDuplicate(U, &V));

  // Solve
  PetscPreLoadBegin(PETSC_FALSE, "Ratel Solve");
  PetscCall(VecSet(U, 0.0));
  PetscCall(TSSetTime(ts, 0.));
  PetscCall(TSSetStepNumber(ts, 0));
  PetscCall(TS2SetSolution(ts, U, V));
  if (PetscPreLoadingOn) {
    // LCOV_EXCL_START
    SNES      snes;
    PetscReal rtol;
    PetscCall(TSGetSNES(ts, &snes));
    PetscCall(SNESGetTolerances(snes, NULL, &rtol, NULL, NULL, NULL));
    PetscCall(SNESSetTolerances(snes, PETSC_DEFAULT, .99, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT));
    PetscCall(TSStep(ts));
    PetscCall(SNESSetTolerances(snes, PETSC_DEFAULT, rtol, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT));
    // LCOV_EXCL_STOP
  } else {
    PetscCall(TSSolve(ts, NULL));
  }
  PetscPreLoadEnd();
  PetscCall(TSGetSolveTime(ts, &final_time));

  // Verify MMS
  PetscBool has_mms = PETSC_FALSE;
  PetscCall(RatelHasMMS(ratel, &has_mms));
  if (has_mms) {
    PetscCall(RatelComputeMMSL2Error(ratel, U, &l2_error));
    if (!quiet || l2_error > 5e-8) PetscCall(PetscPrintf(comm, "L2 error: %0.12e\n", l2_error));
  }

  // Verify strain energy
  PetscCall(RatelComputeStrainEnergy(ratel, U, final_time, &strain_energy));
  PetscCall(RatelGetExpectedStrainEnergy(ratel, &expected_strain_energy));
  PetscCall(RatelComputeSurfaceForces(ratel, U, final_time, &face_forces));
  PetscCall(VecStrideNormAll(U, NORM_MAX, u_norms));
  if (!quiet) {
    // LCOV_EXCL_START
    const PetscInt *faces;

    PetscCall(PetscPrintf(comm, "Final time: %f\n", final_time));
    PetscInt num_faces, num_comp_u = 3;

    PetscCall(RatelGetSurfaceForceFaces(ratel, &num_faces, &faces));
    for (PetscInt i = 0; i < num_faces; i++) {
      PetscCall(PetscPrintf(comm, "Surface %" PetscInt_FMT ":\n", faces[i]));
      PetscInt offset = (2 * i) * num_comp_u;
      PetscCall(
          PetscPrintf(comm, "  Centroid: [%0.12e, %0.12e, %0.12e]\n", face_forces[offset + 0], face_forces[offset + 1], face_forces[offset + 2]));
      offset = (2 * i + 1) * num_comp_u;
      PetscCall(
          PetscPrintf(comm, "  Force:    [%0.12e, %0.12e, %0.12e]\n", face_forces[offset + 0], face_forces[offset + 1], face_forces[offset + 2]));
    }
    PetscCall(PetscPrintf(comm, "Computed strain energy: %0.12e\n", strain_energy));
    PetscCall(PetscPrintf(comm, "Max displacements: [%0.12e, %0.12e, %0.12e]\n", u_norms[0], u_norms[1], u_norms[2]));
    // LCOV_EXCL_STOP
  }
  if ((fabs(expected_strain_energy) > 1e-14) && (!quiet || fabs(strain_energy - expected_strain_energy) > 0.05)) {
    // LCOV_EXCL_START
    PetscCall(PetscPrintf(comm, "Strain energy error: %0.12e\n", fabs(strain_energy - expected_strain_energy)));
    // LCOV_EXCL_STOP
  }

  // Compute diagnostic quantities
  PetscCall(RatelComputeDiagnosticQuantities(ratel, U, final_time, &D));
  if (view_diagnostics) PetscCall(VecView(D, diagnostic_viewer));

  // Cleanup
  PetscCall(PetscViewerDestroy(&diagnostic_viewer));
  PetscCall(TSDestroy(&ts));
  PetscCall(DMDestroy(&dm));
  PetscCall(VecDestroy(&U));
  PetscCall(VecDestroy(&V));
  PetscCall(VecDestroy(&D));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
