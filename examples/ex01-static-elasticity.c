/// @file
/// Ratel static elasticity example

//TESTARGS(name="linear-mms") -ceed {ceed_resource} -quiet -options_file examples/ex01-static-elasticity-linear-mms.yml
//TESTARGS(name="neo-hookean-current") -ceed {ceed_resource} -quiet -options_file examples/ex01-static-elasticity-neo-hookean-current.yml
//TESTARGS(name="neo-hookean-initial") -ceed {ceed_resource} -quiet -options_file examples/ex01-static-elasticity-neo-hookean-initial.yml
//TESTARGS(name="neo-hookean-initial-ad") -ceed {ceed_resource} -quiet -options_file examples/ex01-static-elasticity-neo-hookean-initial-ad.yml
//TESTARGS(name="mooney-rivlin-initial") -ceed {ceed_resource} -quiet -options_file examples/ex01-static-elasticity-mooney-rivlin-initial.yml

const char help[] = "Ratel - static elasticity example\n";

#include <petsc.h>
#include <ratel.h>
#include <string.h>

int main(int argc, char **argv) {
  MPI_Comm      comm;
  PetscLogStage stage_snes_solve;
  Ratel         ratel;
  PetscViewer   diagnostic_viewer;
  SNES          snes;
  DM            dm;
  Vec           U, D;
  PetscScalar   l2_error = 1.0, strain_energy = 0.0, expected_strain_energy = 0.0;
  PetscBool     quiet = PETSC_FALSE, view_diagnostics = PETSC_FALSE;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));

  // Read command line options
  comm = PETSC_COMM_WORLD;
  PetscOptionsBegin(comm, NULL, "Ratel Elasticity example", NULL);
  PetscCall(PetscOptionsBool("-quiet", "Suppress summary outputs", NULL, quiet, &quiet, NULL));
  PetscCall(PetscOptionsViewer("-view_diagnostic_quantities", "View diagnostic quantities", NULL, &diagnostic_viewer, NULL, &view_diagnostics));
  PetscOptionsEnd();

  // Initialize Ratel context and create DM
  PetscCall(RatelInit(comm, &ratel));
  PetscCall(RatelDMCreate(ratel, RATEL_METHOD_STATIC_ELASTICITY, &dm));

  // Create SNES
  PetscCall(SNESCreate(comm, &snes));
  PetscCall(SNESSetDM(snes, dm));
  PetscCall(RatelSNESSetDefaults(ratel, snes));

  // P-multigrid based preconditioning
  PetscCall(RatelSNESSetupPCMG(ratel, RATEL_MULTIGRID_P_COARSENING_LOGARITHMIC, snes));

  // Set additional command line options
  PetscCall(SNESSetFromOptions(snes));

  // View Ratel setup
  if (!quiet) {
    // LCOV_EXCL_START
    PetscCall(PetscPrintf(comm, "----- Ratel Static Elasticity -----\n\n"));
    PetscCall(RatelView(ratel, PETSC_VIEWER_STDOUT_WORLD));
    // LCOV_EXCL_STOP
  }

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm, &U));

  // Solve
  PetscCall(VecSet(U, 0.0));
  {
    PetscCall(PetscLogStageRegister("Ratel Solve", &stage_snes_solve));
    PetscCall(PetscLogStagePush(stage_snes_solve));
    PetscCall(SNESSolve(snes, NULL, U));
    PetscCall(PetscLogStagePop());
  }

  // Verify MMS
  PetscBool has_mms = PETSC_FALSE;
  PetscCall(RatelHasMMS(ratel, &has_mms));
  if (has_mms) {
    PetscCall(RatelComputeMMSL2Error(ratel, U, &l2_error));
    if (!quiet || l2_error > 5e-8) PetscCall(PetscPrintf(comm, "L2 Error: %0.12e\n", l2_error));
  }

  // Verify strain energy
  PetscCall(RatelComputeStrainEnergy(ratel, U, 1.0, &strain_energy));
  PetscCall(RatelGetExpectedStrainEnergy(ratel, &expected_strain_energy));
  if (!quiet) PetscCall(PetscPrintf(comm, "Computed strain energy: %0.12e\n", strain_energy));
  if ((fabs(expected_strain_energy) > 1e-14) && (!quiet || fabs(strain_energy - expected_strain_energy) > 6e-3)) {
    // LCOV_EXCL_START
    PetscCall(PetscPrintf(comm, "Strain energy error: %0.12e\n", fabs(strain_energy - expected_strain_energy)));
    // LCOV_EXCL_STOP
  }

  // Compute diagnostic quantities
  PetscCall(RatelComputeDiagnosticQuantities(ratel, U, 1.0, &D));
  if (view_diagnostics) PetscCall(VecView(D, diagnostic_viewer));

  // Cleanup
  PetscCall(PetscViewerDestroy(&diagnostic_viewer));
  PetscCall(SNESDestroy(&snes));
  PetscCall(DMDestroy(&dm));
  PetscCall(VecDestroy(&U));
  PetscCall(VecDestroy(&D));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
