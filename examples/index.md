(examples)=
# Example Applications

Ratel provides several examples for direct use as simulation drivers and as starters for more complete applications and workflows.
See {ref}`using` for common options to specify the mesh, materials, algebraic solvers, etc.

(example-static)=
## Static Elasticity

This code solves the steady-state static momentum balance equations using unstructured high-order finite/spectral element spatial discretizations.
We consider three formulations used in solid mechanics applications:

* Linear elasticity
* Neo-Hookean hyperelasticity at finite strain
* Mooney-Rivlin hyperelasticity at finite strain

All four of these formulations are for compressible materials.

In the top level directory, build by using:

```console
$ make examples
```

and run with:

```console
$ ./build/ex01-static-elasticity -options_file [.yml file] -view_diagnostic_quantities
```

(example-quasistatic)=
## Quasistatic Elasticity

This code solves the same steady-state formulation as the static elasticity example, but with an additional time stepping parameter for boundary conditions and forcing terms.

In the top level directory, build by using:

```console
$ make examples
```

and run with:

```console
$ ./build/ex02-quasistatic-elasticity -options_file [.yml file] -view_diagnostic_quantities
```

The quasistatic formulation is built off of the [PETSc Time Stepper (TS) object](https://petsc.org/release/docs/manualpages/TS/index.html).

(example-dynamic)=
## Elastodynamics

The elastodynamics example solves the momentum balance equations with the same constitutive models as the static and quasistatic examples.

In the top level directory, build by using:

```console
$ make examples
```

and run with:

```console
$ ./build/ex03-dynamic-elasticity -options_file [.yml file] -view_diagnostic_quantities
```

The dynamic formulation uses the TSAlpha2 [PETSc Time Stepper (TS) object](https://petsc.org/release/docs/manualpages/TS/index.html), which impliments the implicit Generalized-Alpha method for second-order systems.
